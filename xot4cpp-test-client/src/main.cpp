
//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/exceptions/Xot4cppException.hpp>
#include <xot4cpp/example/SimpleCaller.hpp>
#include <xot4cpp/example/SimpleReceiver.hpp>
#include <xot4cpp/logger/Logger.hpp>

#include <sstream>
#include <string>

#include <getopt.h>

extern void processArguments(int p_argc, char** p_argv);
static bool caller = false;
static bool receiver = false;

int main(int p_argc, char** p_argv) {

    int rv = 0;
    try {
        processArguments(p_argc, p_argv);
        if (caller && receiver) {
            XOT4CPP_TRACE("Only one specify argument please");

        } else if (caller) {
            XOT4CPP_TRACE("caller");
            xot4cpp::example::SimpleCaller::main(p_argc, p_argv);

        } else if (receiver) {
            XOT4CPP_TRACE("receiver");
            xot4cpp::example::SimpleReceiver::main(p_argc, p_argv);

        } else {
            XOT4CPP_TRACE("Please specify an argument: --caller (-c) or --receiver (-r)");
        }

    } catch (std::exception& e) {
        XOT4CPP_DEBUG("Exception: " << e.what());
        rv = -1;
    }

    XOT4CPP_TRACE("done");

    return rv;
}

extern void processArguments(int p_argc, char** p_argv) {

    static struct option long_options[] = {
        {"caller", no_argument, 0, 'c'},
        {"receiver", no_argument, 0, 'r'},
        {0, 0, 0, 0}
    };

    int option_index = 0;

    for (int c = getopt_long(p_argc, p_argv, "cr", long_options, &option_index);
            (-1 != c);
            c = getopt_long(p_argc, p_argv, "cr", long_options, &option_index)) {

        switch (c) {
            case 0:
            {
                std::stringstream stringstream;
                stringstream << "option " << long_options[option_index].name;
                if (optarg) {
                    stringstream << " with arg %s" << optarg;
                }
                XOT4CPP_TRACE(stringstream.str());
                break;
            }

            case 'c':
            {
                // happy days
                caller = true;
                break;
            }

            case 'r':
            {
                // happy days
                receiver = true;
                break;
            }

            case '?':
            {
                /* getopt_long already printed an error message. */
                break;
            }

            default:
            {
                THROW_XOT4CPP_EXCEPTION("Unhandled option " << c);
            }
        }
    }

    /* Print any remaining command line arguments (not options). */
    if (optind < p_argc) {
        std::stringstream stringstream;
        stringstream << "non-option ARGV-elements: ";
        while (optind < p_argc) {
            stringstream << p_argv[optind++];
        }
        XOT4CPP_TRACE(stringstream.str());
    }
}
