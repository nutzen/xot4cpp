#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/exceptions/Xot4cppException.hpp>
#include <xot4cpp/exceptions/IOException.hpp>
#include <xot4cpp/exceptions/SocketTimeoutException.hpp>
#include <xot4cpp/exceptions/X25StateTransitionFailure.hpp>

using namespace xot4cpp::exceptions;

BOOST_AUTO_TEST_SUITE(xot4cpp_exceptions_Xot4cppException_test_suite)

BOOST_AUTO_TEST_CASE(test_Simple) {

    const char* expected = "[src/xot4cpp/exceptions/Xot4cppExceptionTest.cpp:21] hello world";
    try {
        THROW_XOT4CPP_EXCEPTION("hello world");
        BOOST_FAIL("this should not happen");

    } catch (std::exception& e) {
        BOOST_CHECK_EQUAL(expected, e.what());
    }
}

BOOST_AUTO_TEST_CASE(test_Xot4cppException) {

    const char* expected = "[src/xot4cpp/exceptions/Xot4cppExceptionTest.cpp:33] hello world";
    try {
        THROW_XOT4CPP_EXCEPTION("hello world");
        BOOST_FAIL("this should not happen");

    } catch (SocketTimeoutException& e) {
        BOOST_FAIL("this should not happen");

    } catch (IOException& e) {
        BOOST_FAIL("this should not happen");

    } catch (X25StateTransitionFailure& e) {
        BOOST_FAIL("this should not happen");

    } catch (Xot4cppException& e) {
        BOOST_CHECK_EQUAL(expected, e.what());

    } catch (std::exception& e) {
        BOOST_FAIL("this should not happen");
    }
}

BOOST_AUTO_TEST_CASE(test_X25StateTransitionFailure) {

    const char* expected = "[src/xot4cpp/exceptions/Xot4cppExceptionTest.cpp:57] hello world";
    try {
        THROW_XOT4CPP_X25STATETRANSITIONFAILURE("hello world");
        BOOST_FAIL("this should not happen");

    } catch (SocketTimeoutException& e) {
        BOOST_FAIL("this should not happen");

    } catch (IOException& e) {
        BOOST_FAIL("this should not happen");

    } catch (X25StateTransitionFailure& e) {
        BOOST_CHECK_EQUAL(expected, e.what());

    } catch (Xot4cppException& e) {
        BOOST_FAIL("this should not happen");

    } catch (std::exception& e) {
        BOOST_FAIL("this should not happen");
    }
}

BOOST_AUTO_TEST_CASE(test_IOException) {

    const char* expected = "[src/xot4cpp/exceptions/Xot4cppExceptionTest.cpp:81] hello world";
    try {
        THROW_XOT4CPP_IOEXCEPTION("hello world");
        BOOST_FAIL("this should not happen");

    } catch (SocketTimeoutException& e) {
        BOOST_FAIL("this should not happen");

    } catch (IOException& e) {
        BOOST_CHECK_EQUAL(expected, e.what());

    } catch (X25StateTransitionFailure& e) {
        BOOST_FAIL("this should not happen");

    } catch (Xot4cppException& e) {
        BOOST_FAIL("this should not happen");

    } catch (std::exception& e) {
        BOOST_FAIL("this should not happen");
    }
}

BOOST_AUTO_TEST_CASE(test_SocketTimeoutException) {

    const char* expected = "[src/xot4cpp/exceptions/Xot4cppExceptionTest.cpp:105] hello world";
    try {
        THROW_XOT4CPP_SOCKETTIMEOUTEXCEPTION("hello world");
        BOOST_FAIL("this should not happen");

    } catch (SocketTimeoutException& e) {
        BOOST_CHECK_EQUAL(expected, e.what());

    } catch (IOException& e) {
        BOOST_FAIL("this should not happen");

    } catch (X25StateTransitionFailure& e) {
        BOOST_FAIL("this should not happen");

    } catch (Xot4cppException& e) {
        BOOST_FAIL("this should not happen");

    } catch (std::exception& e) {
        BOOST_FAIL("this should not happen");
    }
}

BOOST_AUTO_TEST_SUITE_END()
