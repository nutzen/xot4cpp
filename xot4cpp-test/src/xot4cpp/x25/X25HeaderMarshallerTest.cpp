#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/X25HeaderMarshaller.hpp>
#include <xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.hpp>
#include <xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.hpp>

#include <xot4cpp/stream/OctetInputStream.hpp>
#include <xot4cpp/stream/OctetOutputStream.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x25;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_X25HeaderMarshaller_test_suite)

BOOST_AUTO_TEST_CASE(test_simple) {

    const unsigned int expectedSize = 3;
    const Octet expected[3] = {
        0x11,
        0x05,
        0x0f
    };
    const Octet gfi = 0x01;
    const Octet lcgn = 0x01;
    const Octet lcn = 0x05;
    const Octet pti = 0x0f;

    const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifierMarshaller::unmarshal(gfi);
    {
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::DataForUser, generalFormatIdentifier->qualifiedData());
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::ForLocalAcknowledgment, generalFormatIdentifier->deliveryConfirmation());
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::Modulo8Sequencing, generalFormatIdentifier->protocolIdentification());
    }

    const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier = lci::LogicalChannelIdentifier::create(lcgn, lcn);
    {
        BOOST_CHECK_EQUAL(lcgn, logicalChannelIdentifier->logicalChannelGroupNumber());
        BOOST_CHECK_EQUAL(lcn, logicalChannelIdentifier->logicalChannelNumber());
    }

    const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier = pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::fromInt(pti & 0x00ff));

    const X25Header::SharedPointer source = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    {
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::DataForUser, source->generalFormatIdentifier()->qualifiedData());
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::ForLocalAcknowledgment, source->generalFormatIdentifier()->deliveryConfirmation());
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::Modulo8Sequencing, source->generalFormatIdentifier()->protocolIdentification());

        BOOST_CHECK_EQUAL(lcgn, source->logicalChannelIdentifier()->logicalChannelGroupNumber());
        BOOST_CHECK_EQUAL(lcn, source->logicalChannelIdentifier()->logicalChannelNumber());

        BOOST_CHECK_EQUAL(pti, pti::PacketTypeIdentifierMarshaller::marshalModule8(source->packetTypeIdentifier()));
    }

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25HeaderMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();

        BOOST_CHECK_EQUAL(expectedSize, intermediate.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], intermediate[index]);
        }
    }

    const X25Header::SharedPointer target = X25HeaderMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
    {
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::DataForUser, target->generalFormatIdentifier()->qualifiedData());
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::ForLocalAcknowledgment, target->generalFormatIdentifier()->deliveryConfirmation());
        BOOST_CHECK_EQUAL(gfi::GeneralFormatIdentifier::Modulo8Sequencing, target->generalFormatIdentifier()->protocolIdentification());

        BOOST_CHECK_EQUAL(lcgn, target->logicalChannelIdentifier()->logicalChannelGroupNumber());
        BOOST_CHECK_EQUAL(lcn, target->logicalChannelIdentifier()->logicalChannelNumber());

        BOOST_CHECK_EQUAL(pti, pti::PacketTypeIdentifierMarshaller::marshalModule8(target->packetTypeIdentifier()));
    }
}

BOOST_AUTO_TEST_SUITE_END()
