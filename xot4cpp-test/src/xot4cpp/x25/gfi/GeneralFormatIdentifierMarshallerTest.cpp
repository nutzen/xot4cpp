#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x25::gfi;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_gfi_GeneralFormatIdentifierMarshaller_test_suite)

BOOST_AUTO_TEST_CASE(test_simple) {

    const Octet gfi = 0x01;

    const GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = GeneralFormatIdentifierMarshaller::unmarshal(gfi);
    BOOST_CHECK_EQUAL(GeneralFormatIdentifier::DataForUser, generalFormatIdentifier->qualifiedData());
    BOOST_CHECK_EQUAL(GeneralFormatIdentifier::ForLocalAcknowledgment, generalFormatIdentifier->deliveryConfirmation());
    BOOST_CHECK_EQUAL(GeneralFormatIdentifier::Modulo8Sequencing, generalFormatIdentifier->protocolIdentification());

    const Octet target = GeneralFormatIdentifierMarshaller::marshal(generalFormatIdentifier);
    BOOST_CHECK_EQUAL(gfi, target);
}

BOOST_AUTO_TEST_SUITE_END()
