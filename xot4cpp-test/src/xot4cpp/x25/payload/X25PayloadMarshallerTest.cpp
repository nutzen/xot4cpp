#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/payload/X25PayloadMarshaller.hpp>

#include <xot4cpp/x25/facilities/PacketSizeSelection.hpp>
#include <xot4cpp/x25/facilities/WindowSizeSelection.hpp>

#include <xot4cpp/x25/payload/CallPacketPayload.hpp>
#include <xot4cpp/x25/payload/ClearConfirmationPayload.hpp>
#include <xot4cpp/x25/payload/ClearPacketPayload.hpp>
#include <xot4cpp/x25/payload/DataPacketPayload.hpp>
#include <xot4cpp/x25/payload/ResetPacketPayload.hpp>
#include <xot4cpp/x25/payload/RestartPacketPayload.hpp>

#include <xot4cpp/stream/OctetInputStream.hpp>
#include <xot4cpp/stream/OctetOutputStream.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x25::payload;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_payload_X25PayloadMarshaller_test_suite)


BOOST_AUTO_TEST_CASE(test_CallPacketPayload) {

    const unsigned int expectedIntermediateSize = 17;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, 0xc4};

    const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("1634567");
    const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("11009000");
    x25::facilities::Facility::List facilities;
    facilities.push_back(x25::facilities::WindowSizeSelection::create(1, 1));
    facilities.push_back(x25::facilities::PacketSizeSelection::create(
            x25::facilities::PacketSizeSelectionEnumerations::Size128octets,
            x25::facilities::PacketSizeSelectionEnumerations::Size128octets));
    const Octets userData(1, (Octet) 0xc4);

    const CallPacketPayload::SharedPointer source = CallPacketPayload::downCast(
            CallPacketPayload::create(calledAddress, callingAddress, facilities, userData));

    BOOST_CHECK_EQUAL(calledAddress->address(), source->calledAddress()->address());
    BOOST_CHECK_EQUAL(callingAddress->address(), source->callingAddress()->address());
    BOOST_CHECK_EQUAL(facilities.size(), source->facilities().size());

    BOOST_CHECK_EQUAL(userData.size(), source->userData().size());
    for (unsigned int index = 0; userData.size() > index; ++index) {
        BOOST_CHECK_EQUAL(userData[index], source->userData()[index]);
    }

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PayloadMarshaller::marshalCallPacketPayload(outputStream,
                CallPacketPayload::upCast(source), false);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const CallPacketPayload::SharedPointer result = CallPacketPayload::downCast(
            X25PayloadMarshaller::unmarshalCallPacketPayload(xot4cpp::stream::OctetInputStream::create(intermediate), false));

    BOOST_CHECK_EQUAL(source->calledAddress()->address(), result->calledAddress()->address());
    BOOST_CHECK_EQUAL(source->callingAddress()->address(), result->callingAddress()->address());
    BOOST_CHECK_EQUAL(source->facilities().size(), result->facilities().size());

    BOOST_CHECK_EQUAL(source->userData().size(), result->userData().size());
    for (unsigned int index = 0; source->userData().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->userData()[index], result->userData()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_ClearPacketPayloadWithOptionalFields) {

    const unsigned int expectedIntermediateSize = 19;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x39, 0x30, 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, 0xc4};

    const x25::codes::CauseCodeEnumerations::ClearRequestEnumeration cause = x25::codes::CauseCodeEnumerations::ClearRequestShipAbsent;
    const x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostic = x25::codes::DiagnosticCodeEnumerations::TimerExpired;
    const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("1634567");
    const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("11009000");
    x25::facilities::Facility::List facilities;
    facilities.push_back(x25::facilities::WindowSizeSelection::create(1, 1));
    facilities.push_back(x25::facilities::PacketSizeSelection::create(
            x25::facilities::PacketSizeSelectionEnumerations::Size128octets,
            x25::facilities::PacketSizeSelectionEnumerations::Size128octets));
    const Octets userData(1, (Octet) 0xc4);

    const ClearPacketPayload::SharedPointer source = ClearPacketPayload::downCast(
            ClearPacketPayload::create(cause, diagnostic, calledAddress, callingAddress, facilities, userData));

    BOOST_CHECK_EQUAL(cause, source->cause());
    BOOST_CHECK_EQUAL(diagnostic, source->diagnostic());
    BOOST_CHECK_EQUAL(calledAddress->address(), source->calledAddress()->address());
    BOOST_CHECK_EQUAL(callingAddress->address(), source->callingAddress()->address());
    BOOST_CHECK_EQUAL(facilities.size(), source->facilities().size());

    BOOST_CHECK_EQUAL(userData.size(), source->userData().size());
    for (unsigned int index = 0; userData.size() > index; ++index) {
        BOOST_CHECK_EQUAL(userData[index], source->userData()[index]);
    }

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PayloadMarshaller::marshalClearPacketPayload(outputStream,
                ClearPacketPayload::upCast(source), false);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const ClearPacketPayload::SharedPointer result = ClearPacketPayload::downCast(
            X25PayloadMarshaller::unmarshalClearPacketPayload(xot4cpp::stream::OctetInputStream::create(intermediate), false));

    BOOST_CHECK_EQUAL(source->cause(), result->cause());
    BOOST_CHECK_EQUAL(source->diagnostic(), result->diagnostic());
    BOOST_CHECK_EQUAL(source->calledAddress()->address(), result->calledAddress()->address());
    BOOST_CHECK_EQUAL(source->callingAddress()->address(), result->callingAddress()->address());
    BOOST_CHECK_EQUAL(source->facilities().size(), result->facilities().size());

    BOOST_CHECK_EQUAL(source->userData().size(), result->userData().size());
    for (unsigned int index = 0; source->userData().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->userData()[index], result->userData()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_ClearPacketPayload) {

    const unsigned int expectedIntermediateSize = 2;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x00, 0x00};

    const x25::codes::CauseCodeEnumerations::ClearRequestEnumeration cause = x25::codes::CauseCodeEnumerations::ClearRequestDteOriginated;
    const x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostic = x25::codes::DiagnosticCodeEnumerations::NoAdditionalInformation;

    const ClearPacketPayload::SharedPointer source = ClearPacketPayload::downCast(
            ClearPacketPayload::create(cause, diagnostic));

    BOOST_CHECK_EQUAL(cause, source->cause());
    BOOST_CHECK_EQUAL(diagnostic, source->diagnostic());
    BOOST_CHECK(0 == source->calledAddress().get());
    BOOST_CHECK(0 == source->callingAddress().get());
    BOOST_CHECK_EQUAL(0, source->facilities().size());
    BOOST_CHECK_EQUAL(0, source->userData().size());

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PayloadMarshaller::marshalClearPacketPayload(outputStream,
                ClearPacketPayload::upCast(source), false);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const ClearPacketPayload::SharedPointer result = ClearPacketPayload::downCast(
            X25PayloadMarshaller::unmarshalClearPacketPayload(xot4cpp::stream::OctetInputStream::create(intermediate), false));

    BOOST_CHECK_EQUAL(source->cause(), result->cause());
    BOOST_CHECK_EQUAL(source->diagnostic(), result->diagnostic());
    BOOST_CHECK(0 == result->calledAddress().get());
    BOOST_CHECK(0 == result->callingAddress().get());
    BOOST_CHECK_EQUAL(0, result->facilities().size());
    BOOST_CHECK_EQUAL(0, result->userData().size());
}

BOOST_AUTO_TEST_CASE(test_ClearConfirmationPayloadWithOptionalFields) {

    const unsigned int expectedIntermediateSize = 16;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07};

    const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("1634567");
    const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("11009000");
    x25::facilities::Facility::List facilities;
    facilities.push_back(x25::facilities::WindowSizeSelection::create(1, 1));
    facilities.push_back(x25::facilities::PacketSizeSelection::create(
            x25::facilities::PacketSizeSelectionEnumerations::Size128octets,
            x25::facilities::PacketSizeSelectionEnumerations::Size128octets));

    const ClearConfirmationPayload::SharedPointer source = ClearConfirmationPayload::downCast(
            ClearConfirmationPayload::create(calledAddress, callingAddress, facilities));

    BOOST_CHECK_EQUAL(calledAddress->address(), source->calledAddress()->address());
    BOOST_CHECK_EQUAL(callingAddress->address(), source->callingAddress()->address());
    BOOST_CHECK_EQUAL(facilities.size(), source->facilities().size());

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PayloadMarshaller::marshalClearConfirmationPayload(outputStream,
                ClearConfirmationPayload::upCast(source), false);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const ClearConfirmationPayload::SharedPointer result = ClearConfirmationPayload::downCast(
            X25PayloadMarshaller::unmarshalClearConfirmationPayload(xot4cpp::stream::OctetInputStream::create(intermediate), false));

    BOOST_CHECK_EQUAL(source->calledAddress()->address(), result->calledAddress()->address());
    BOOST_CHECK_EQUAL(source->callingAddress()->address(), result->callingAddress()->address());
    BOOST_CHECK_EQUAL(source->facilities().size(), result->facilities().size());
}

BOOST_AUTO_TEST_CASE(test_ClearConfirmationPayload) {

    const unsigned int expectedIntermediateSize = 0;
    const Octet expectedIntermediate[expectedIntermediateSize] = {};

    const ClearConfirmationPayload::SharedPointer source = ClearConfirmationPayload::downCast(
            ClearConfirmationPayload::create());

    BOOST_CHECK(0 == source->calledAddress().get());
    BOOST_CHECK(0 == source->callingAddress().get());
    BOOST_CHECK_EQUAL(0, source->facilities().size());

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PayloadMarshaller::marshalClearConfirmationPayload(outputStream,
                ClearConfirmationPayload::upCast(source), false);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const ClearConfirmationPayload::SharedPointer result = ClearConfirmationPayload::downCast(
            X25PayloadMarshaller::unmarshalClearConfirmationPayload(xot4cpp::stream::OctetInputStream::create(intermediate), false));

    BOOST_CHECK(0 == result->calledAddress().get());
    BOOST_CHECK(0 == result->callingAddress().get());
    BOOST_CHECK_EQUAL(0, result->facilities().size());
}

BOOST_AUTO_TEST_CASE(test_ResetPacketPayload) {

    const unsigned int expectedIntermediateSize = 2;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x03, 0x00};

    const x25::codes::CauseCodeEnumerations::ResetRequestEnumeration cause = x25::codes::CauseCodeEnumerations::ResetRequestRemoteProcedureError;
    const x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostic = x25::codes::DiagnosticCodeEnumerations::NoAdditionalInformation;

    const ResetPacketPayload::SharedPointer source = ResetPacketPayload::downCast(
            ResetPacketPayload::create(cause, diagnostic));

    BOOST_CHECK_EQUAL(cause, source->cause());
    BOOST_CHECK_EQUAL(diagnostic, source->diagnostic());

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PayloadMarshaller::marshalResetPacketPayload(outputStream,
                ResetPacketPayload::upCast(source));
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const ResetPacketPayload::SharedPointer result = ResetPacketPayload::downCast(
            X25PayloadMarshaller::unmarshalResetPacketPayload(xot4cpp::stream::OctetInputStream::create(intermediate)));

    BOOST_CHECK_EQUAL(source->cause(), result->cause());
    BOOST_CHECK_EQUAL(source->diagnostic(), result->diagnostic());
}

BOOST_AUTO_TEST_CASE(test_RestartPacketPayload) {

    const unsigned int expectedIntermediateSize = 2;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x00, 0x50};

    const x25::codes::CauseCodeEnumerations::RestartRequestEnumeration cause = x25::codes::CauseCodeEnumerations::RestartRequestDteRestarting;
    const x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostic = x25::codes::DiagnosticCodeEnumerations::Miscellaneous;

    const RestartPacketPayload::SharedPointer source = RestartPacketPayload::downCast(
            RestartPacketPayload::create(cause, diagnostic));

    BOOST_CHECK_EQUAL(cause, source->cause());
    BOOST_CHECK_EQUAL(diagnostic, source->diagnostic());

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PayloadMarshaller::marshalRestartPacketPayload(outputStream,
                RestartPacketPayload::upCast(source));
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const RestartPacketPayload::SharedPointer result = RestartPacketPayload::downCast(
            X25PayloadMarshaller::unmarshalRestartPacketPayload(xot4cpp::stream::OctetInputStream::create(intermediate)));

    BOOST_CHECK_EQUAL(source->cause(), result->cause());
    BOOST_CHECK_EQUAL(source->diagnostic(), result->diagnostic());
}

BOOST_AUTO_TEST_CASE(test_DataPacketPayload) {

    const unsigned int expectedIntermediateSize = 11;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64};
    const Octets data(expectedIntermediate, expectedIntermediate + expectedIntermediateSize);

    const DataPacketPayload::SharedPointer source = DataPacketPayload::downCast(
            DataPacketPayload::create(data));

    BOOST_CHECK_EQUAL(data.size(), source->userData().size());
    for (unsigned int index = 0; data.size() > index; ++index) {
        BOOST_CHECK_EQUAL(data[index], source->userData()[index]);
    }

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PayloadMarshaller::marshalDataPacketPayload(outputStream,
                DataPacketPayload::upCast(source));
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const DataPacketPayload::SharedPointer result = DataPacketPayload::downCast(
            X25PayloadMarshaller::unmarshalDataPacketPayload(xot4cpp::stream::OctetInputStream::create(intermediate)));

    BOOST_CHECK_EQUAL(source->userData().size(), result->userData().size());
    for (unsigned int index = 0; source->userData().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->userData()[index], result->userData()[index]);
    }
}


BOOST_AUTO_TEST_SUITE_END()
