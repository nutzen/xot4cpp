#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/facilities/PacketSizeSelectionEnumerations.hpp>

using namespace xot4cpp::x25::facilities;

// PacketSizeSelectionEnumerationsTest

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_facilities_PacketSizeSelectionEnumerations_test_suite)

void testSimple(const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_source) {

    {
        const std::string intermediate = PacketSizeSelectionEnumerations::toDescription(p_source);
        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration enumeration = PacketSizeSelectionEnumerations::fromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }

    {
        const int intermediate = PacketSizeSelectionEnumerations::toInt(p_source);
        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration enumeration = PacketSizeSelectionEnumerations::fromInt(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }

    {
        const unsigned int intermediate = PacketSizeSelectionEnumerations::toPacketSize(p_source);
        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration enumeration = PacketSizeSelectionEnumerations::fromPacketSize(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    testSimple(PacketSizeSelectionEnumerations::Size16octets);
    testSimple(PacketSizeSelectionEnumerations::Size32octets);
    testSimple(PacketSizeSelectionEnumerations::Size64octets);
    testSimple(PacketSizeSelectionEnumerations::Size128octets);
    testSimple(PacketSizeSelectionEnumerations::Size256octets);
    testSimple(PacketSizeSelectionEnumerations::Size512octets);
    testSimple(PacketSizeSelectionEnumerations::Size1024octets);
    testSimple(PacketSizeSelectionEnumerations::Size2048octets);
    testSimple(PacketSizeSelectionEnumerations::Size4096octets);
}

BOOST_AUTO_TEST_SUITE_END()
