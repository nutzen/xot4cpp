#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/facilities/FacilityMarshaller.hpp>

// one parameter
#include <xot4cpp/x25/facilities/ReverseChargingAndFastSelect.hpp>
#include <xot4cpp/x25/facilities/ThroughputClass.hpp>
#include <xot4cpp/x25/facilities/ClosedUserGroupSelection.hpp>
#include <xot4cpp/x25/facilities/ChargingInformationRequest.hpp>
#include <xot4cpp/x25/facilities/CalledLineAddressModifiedNotification.hpp>
#include <xot4cpp/x25/facilities/ClosedUserGroupWithOutgoingAccess.hpp>
#include <xot4cpp/x25/facilities/QualityOfServiceNegotiationMinimumThroughputClass.hpp>
#include <xot4cpp/x25/facilities/ExpeditedDataNegotiation.hpp>
// two parameters
#include <xot4cpp/x25/facilities/BilateralClosedUserGroupSelection.hpp>
#include <xot4cpp/x25/facilities/PacketSizeSelection.hpp>
#include <xot4cpp/x25/facilities/WindowSizeSelection.hpp>
#include <xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionBasicFormat.hpp>
#include <xot4cpp/x25/facilities/TransitDelaySelectionAndIndication.hpp>
// variable parameters
#include <xot4cpp/x25/facilities/ChargingCallDuration.hpp>
#include <xot4cpp/x25/facilities/ChargingSegmentCount.hpp>
#include <xot4cpp/x25/facilities/CallRedirectionNotification.hpp>
#include <xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionExtendedFormat.hpp>
#include <xot4cpp/x25/facilities/ChargingMonetaryUnit.hpp>
#include <xot4cpp/x25/facilities/NetworkUserIdentification.hpp>
#include <xot4cpp/x25/facilities/CalledAddressExtensionOsi.hpp>
#include <xot4cpp/x25/facilities/QualityOfServiceNegotiationEndToEndTransitDelay.hpp>
#include <xot4cpp/x25/facilities/CallingAddressExtensionOsi.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x25::facilities;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_facilities_FacilityMarshaller_test_suite)


Facility::SharedPointer remarshal(const Facility::SharedPointer& source) {

    Facility::SharedPointer target;
    Octets intermediate;
    {
        Facility::List facilities;
        facilities.push_back(source);

        intermediate = FacilityMarshaller::marshal(facilities);
    }
    {
        Facility::List facilities = FacilityMarshaller::unmarshal(intermediate);

        BOOST_CHECK_EQUAL(1, facilities.size());
        target = facilities[0];
    }
    return target;
}

// one parameter

BOOST_AUTO_TEST_CASE(test_ReverseChargingAndFastSelect) {

    typedef ReverseChargingAndFastSelect MyFacility;
    const Octet parameter = (Octet) 1;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter, source->parameter());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter(), result->parameter());
}

BOOST_AUTO_TEST_CASE(test_ThroughputClass) {

    typedef ThroughputClass MyFacility;
    const Octet parameter = (Octet) 2;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter, source->parameter());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter(), result->parameter());
}

BOOST_AUTO_TEST_CASE(test_ClosedUserGroupSelection) {

    typedef ClosedUserGroupSelection MyFacility;
    const Octet parameter = (Octet) 4;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter, source->parameter());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter(), result->parameter());
}

BOOST_AUTO_TEST_CASE(test_ChargingInformationRequest) {

    typedef ChargingInformationRequest MyFacility;
    const Octet parameter = (Octet) 8;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter, source->parameter());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter(), result->parameter());
}

BOOST_AUTO_TEST_CASE(test_CalledLineAddressModifiedNotification) {

    typedef CalledLineAddressModifiedNotification MyFacility;
    const Octet parameter = (Octet) 0x10;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter, source->parameter());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter(), result->parameter());
}

BOOST_AUTO_TEST_CASE(test_ClosedUserGroupWithOutgoingAccess) {

    typedef ClosedUserGroupWithOutgoingAccess MyFacility;
    const Octet parameter = (Octet) 0x20;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter, source->parameter());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter(), result->parameter());
}

BOOST_AUTO_TEST_CASE(test_QualityOfServiceNegotiationMinimumThroughputClass) {

    typedef QualityOfServiceNegotiationMinimumThroughputClass MyFacility;
    const Octet parameter = (Octet) 0x40;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter, source->parameter());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter(), result->parameter());
}

BOOST_AUTO_TEST_CASE(test_ExpeditedDataNegotiation) {

    typedef ExpeditedDataNegotiation MyFacility;
    const Octet parameter = (Octet) 0x80;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter, source->parameter());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter(), result->parameter());
}

// two parameters

BOOST_AUTO_TEST_CASE(test_BilateralClosedUserGroupSelection) {

    typedef BilateralClosedUserGroupSelection MyFacility;
    const Octet parameter0 = (Octet) 0x01;
    const Octet parameter1 = (Octet) 0x02;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter0, parameter1));
    BOOST_CHECK_EQUAL(parameter0, source->parameter0());
    BOOST_CHECK_EQUAL(parameter1, source->parameter1());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter0(), result->parameter0());
    BOOST_CHECK_EQUAL(source->parameter1(), result->parameter1());
}

BOOST_AUTO_TEST_CASE(test_PacketSizeSelection) {

    typedef PacketSizeSelection MyFacility;
    const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration transmitPacketSize = PacketSizeSelectionEnumerations::Size4096octets;
    const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration receivePacketSize = PacketSizeSelectionEnumerations::Size1024octets;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(transmitPacketSize, receivePacketSize));
    BOOST_CHECK_EQUAL(transmitPacketSize, source->transmitPacketSize());
    BOOST_CHECK_EQUAL(receivePacketSize, source->receivePacketSize());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->transmitPacketSize(), result->transmitPacketSize());
    BOOST_CHECK_EQUAL(source->receivePacketSize(), result->receivePacketSize());
}

BOOST_AUTO_TEST_CASE(test_WindowSizeSelection) {

    typedef WindowSizeSelection MyFacility;
    const unsigned int transmissionWindowSize = 0x05;
    const unsigned int receiveWindowSize = 0x02;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(transmissionWindowSize, receiveWindowSize));
    BOOST_CHECK_EQUAL(transmissionWindowSize, source->transmissionWindowSize());
    BOOST_CHECK_EQUAL(receiveWindowSize, source->receiveWindowSize());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->transmissionWindowSize(), result->transmissionWindowSize());
    BOOST_CHECK_EQUAL(source->receiveWindowSize(), result->receiveWindowSize());
}

BOOST_AUTO_TEST_CASE(test_RecognizedPrivateOperatingAgencySelectionBasicFormat) {

    typedef RecognizedPrivateOperatingAgencySelectionBasicFormat MyFacility;
    const Octet parameter0 = (Octet) 0x40;
    const Octet parameter1 = (Octet) 0x80;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter0, parameter1));
    BOOST_CHECK_EQUAL(parameter0, source->parameter0());
    BOOST_CHECK_EQUAL(parameter1, source->parameter1());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter0(), result->parameter0());
    BOOST_CHECK_EQUAL(source->parameter1(), result->parameter1());
}

BOOST_AUTO_TEST_CASE(test_TransitDelaySelectionAndIndication) {

    typedef TransitDelaySelectionAndIndication MyFacility;
    const Octet parameter0 = (Octet) 0x11;
    const Octet parameter1 = (Octet) 0x12;

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter0, parameter1));
    BOOST_CHECK_EQUAL(parameter0, source->parameter0());
    BOOST_CHECK_EQUAL(parameter1, source->parameter1());

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter0(), result->parameter0());
    BOOST_CHECK_EQUAL(source->parameter1(), result->parameter1());
}

// variable parameters

BOOST_AUTO_TEST_CASE(test_ChargingCallDuration) {

    typedef ChargingCallDuration MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x01);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_ChargingSegmentCount) {

    typedef ChargingSegmentCount MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x02);
    parameter.push_back((Octet) 0x03);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_CallRedirectionNotification) {

    typedef CallRedirectionNotification MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x04);
    parameter.push_back((Octet) 0x05);
    parameter.push_back((Octet) 0x06);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_RecognizedPrivateOperatingAgencySelectionExtendedFormat) {

    typedef RecognizedPrivateOperatingAgencySelectionExtendedFormat MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x07);
    parameter.push_back((Octet) 0x08);
    parameter.push_back((Octet) 0x09);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_ChargingMonetaryUnit) {

    typedef ChargingMonetaryUnit MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x10);
    parameter.push_back((Octet) 0x11);
    parameter.push_back((Octet) 0x12);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_NetworkUserIdentification) {

    typedef NetworkUserIdentification MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x13);
    parameter.push_back((Octet) 0x14);
    parameter.push_back((Octet) 0x15);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_CalledAddressExtensionOsi) {

    typedef CalledAddressExtensionOsi MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x17);
    parameter.push_back((Octet) 0x18);
    parameter.push_back((Octet) 0x19);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_QualityOfServiceNegotiationEndToEndTransitDelay) {

    typedef QualityOfServiceNegotiationEndToEndTransitDelay MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x20);
    parameter.push_back((Octet) 0x21);
    parameter.push_back((Octet) 0x22);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_CallingAddressExtensionOsi) {

    typedef CallingAddressExtensionOsi MyFacility;
    Octets parameter;
    parameter.push_back((Octet) 0x30);
    parameter.push_back((Octet) 0x31);
    parameter.push_back((Octet) 0x32);
    parameter.push_back((Octet) 0x33);

    const MyFacility::SharedPointer source = MyFacility::downCast(MyFacility::create(parameter));
    BOOST_CHECK_EQUAL(parameter.size(), source->parameter().size());
    for (unsigned int index = 0; parameter.size() > index; ++index) {
        BOOST_CHECK_EQUAL(parameter[index], source->parameter()[index]);
    }

    const Facility::SharedPointer intermediate = remarshal(MyFacility::upCast(source));
    BOOST_CHECK_EQUAL(source->enumeration(), intermediate->enumeration());

    const MyFacility::SharedPointer result = MyFacility::downCast(intermediate);
    BOOST_CHECK_EQUAL(source->parameter().size(), result->parameter().size());
    for (unsigned int index = 0; source->parameter().size() > index; ++index) {
        BOOST_CHECK_EQUAL(source->parameter()[index], result->parameter()[index]);
    }
}

BOOST_AUTO_TEST_SUITE_END()
