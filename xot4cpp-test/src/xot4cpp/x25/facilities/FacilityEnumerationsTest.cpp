#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/facilities/FacilityEnumerations.hpp>

using namespace xot4cpp::x25::facilities;

// FacilityEnumerationsTest

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_facilities_FacilityEnumerations_test_suite)

void testSimple(const FacilityEnumerations::FacilityEnumeration& p_source) {

    {
        const std::string intermediate = FacilityEnumerations::toDescription(p_source);
        const FacilityEnumerations::FacilityEnumeration enumeration = FacilityEnumerations::fromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }

    {
        const int intermediate = FacilityEnumerations::toInt(p_source);
        const FacilityEnumerations::FacilityEnumeration enumeration = FacilityEnumerations::fromInt(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    testSimple(FacilityEnumerations::ReverseChargingAndFastSelect);
    testSimple(FacilityEnumerations::ThroughputClass);
    testSimple(FacilityEnumerations::ClosedUserGroupSelection);
    testSimple(FacilityEnumerations::ChargingInformationRequest);
    testSimple(FacilityEnumerations::CalledLineAddressModifiedNotification);
    testSimple(FacilityEnumerations::ClosedUserGroupWithOutgoingAccess);
    testSimple(FacilityEnumerations::QualityOfServiceNegotiationMinimumThroughputClass);
    testSimple(FacilityEnumerations::ExpeditedDataNegotiation);
    testSimple(FacilityEnumerations::BilateralClosedUserGroupSelection);
    testSimple(FacilityEnumerations::PacketSizeSelection);
    testSimple(FacilityEnumerations::WindowSizeSelection);
    testSimple(FacilityEnumerations::RecognizedPrivateOperatingAgencySelectionBasicFormat);
    testSimple(FacilityEnumerations::TransitDelaySelectionAndIndication);
    testSimple(FacilityEnumerations::ChargingCallDuration);
    testSimple(FacilityEnumerations::ChargingSegmentCount);
    testSimple(FacilityEnumerations::CallRedirectionNotification);
    testSimple(FacilityEnumerations::RecognizedPrivateOperatingAgencySelectionExtendedFormat);
    testSimple(FacilityEnumerations::ChargingMonetaryUnit);
    testSimple(FacilityEnumerations::NetworkUserIdentification);
    testSimple(FacilityEnumerations::CalledAddressExtensionOsi);
    testSimple(FacilityEnumerations::QualityOfServiceNegotiationEndToEndTransitDelay);
    testSimple(FacilityEnumerations::CallingAddressExtensionOsi);
}

BOOST_AUTO_TEST_SUITE_END()
