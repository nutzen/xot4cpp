#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/transitions/TransitionEnumerations.hpp>

using namespace xot4cpp::x25::transitions;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_states_TransitionEnumerations_test_suite)

void testSimple(const TransitionEnumerations::TransitionEnumeration& p_source) {

    {
        const std::string intermediate = TransitionEnumerations::toDescription(p_source);
        const TransitionEnumerations::TransitionEnumeration pti = TransitionEnumerations::fromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, pti);
    }
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    // Call set-up and clearing DESCRIPTION
    testSimple(TransitionEnumerations::CallRequest);
    testSimple(TransitionEnumerations::CallIncoming);
    testSimple(TransitionEnumerations::CallAccepted);
    testSimple(TransitionEnumerations::CallConnected);
    testSimple(TransitionEnumerations::ClearRequest);
    testSimple(TransitionEnumerations::ClearIndication);
    testSimple(TransitionEnumerations::ClearConfirmation);
    // Data and interrupt DESCRIPTION
    testSimple(TransitionEnumerations::Data);
    testSimple(TransitionEnumerations::Interrupt);
    testSimple(TransitionEnumerations::InterruptConfirmation);
    // Flow control and reset DESCRIPTION
    testSimple(TransitionEnumerations::ReceiverReady);
    testSimple(TransitionEnumerations::ReceiverNotReady);
    testSimple(TransitionEnumerations::Reject);
    testSimple(TransitionEnumerations::ResetRequest);
    testSimple(TransitionEnumerations::ResetIndication);
    testSimple(TransitionEnumerations::ResetConfirmation);
    // Restart DESCRIPTION
    testSimple(TransitionEnumerations::RestartRequest);
    testSimple(TransitionEnumerations::RestartIndication);
    testSimple(TransitionEnumerations::RestartConfirmation);
    // Diagnostic DESCRIPTION
    testSimple(TransitionEnumerations::Diagnostic);
}

BOOST_AUTO_TEST_SUITE_END()
