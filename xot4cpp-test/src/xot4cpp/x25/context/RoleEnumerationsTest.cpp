#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/context/RoleEnumerations.hpp>

using namespace xot4cpp::x25::context;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_context_RoleEnumerations_test_suite)

void testSimple(const RoleEnumerations::RoleEnumeration& p_source) {

    {
        const std::string intermediate = RoleEnumerations::toDescription(p_source);
        const RoleEnumerations::RoleEnumeration pti = RoleEnumerations::fromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, pti);
    }
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    testSimple(RoleEnumerations::DTE);
    testSimple(RoleEnumerations::DCE);

    try {
        RoleEnumerations::fromDescription("no such role");
        BOOST_FAIL("This should not happen");
        
    } catch (std::exception& e) {
        const char* expected = "[src/xot4cpp/x25/context/RoleEnumerations.cpp:46] Unhandled value: no such role";
        BOOST_CHECK_EQUAL(expected, e.what());
    }
}

BOOST_AUTO_TEST_SUITE_END()
