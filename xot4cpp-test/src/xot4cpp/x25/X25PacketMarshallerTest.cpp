#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/X25PacketMarshaller.hpp>

#include <xot4cpp/x25/facilities/PacketSizeSelection.hpp>
#include <xot4cpp/x25/facilities/WindowSizeSelection.hpp>

#include <xot4cpp/x25/payload/CallPacketPayload.hpp>
#include <xot4cpp/x25/payload/ClearConfirmationPayload.hpp>
#include <xot4cpp/x25/payload/ClearPacketPayload.hpp>
#include <xot4cpp/x25/payload/DataPacketPayload.hpp>
#include <xot4cpp/x25/payload/ResetPacketPayload.hpp>
#include <xot4cpp/x25/payload/RestartPacketPayload.hpp>

#include <xot4cpp/stream/OctetInputStream.hpp>
#include <xot4cpp/stream/OctetOutputStream.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x25;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_X25PacketMarshaller_test_suite)


BOOST_AUTO_TEST_CASE(test_CallPacketPayload) {

    const unsigned int expectedIntermediateSize = 20;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x10, 0x01, 0x0b, 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, 0xc4};

    X25Header::SharedPointer header;
    {
        const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifier::create(
                gfi::GeneralFormatIdentifier::DataForUser,
                gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
                gfi::GeneralFormatIdentifier::Modulo8Sequencing);
        const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier =
                lci::LogicalChannelIdentifier::create(0, 1);
        const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
                pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::CallRequest_IncomingCall);
        header = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    payload::CallPacketPayload::SharedPointer payload;
    {
        const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("1634567");
        const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("11009000");
        facilities::Facility::List facilities;
        facilities.push_back(facilities::WindowSizeSelection::create(1, 1));
        facilities.push_back(facilities::PacketSizeSelection::create(
                facilities::PacketSizeSelectionEnumerations::Size128octets,
                facilities::PacketSizeSelectionEnumerations::Size128octets));
        const Octets userData(1, (Octet) 0xc4);
        payload = payload::CallPacketPayload::downCast(
                payload::CallPacketPayload::create(calledAddress, callingAddress, facilities, userData));
    }

    const X25Packet::SharedPointer source = X25Packet::create(header,
            payload::CallPacketPayload::upCast(payload));

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PacketMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const X25Packet::SharedPointer result = X25PacketMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
}

BOOST_AUTO_TEST_CASE(testCallPacketPayloadForCallConnect) {

    const unsigned int expectedIntermediateSize = 3;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x18, 0x00, 0x0f};


    X25Header::SharedPointer header;
    {
        const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifier::create(
                gfi::GeneralFormatIdentifier::DataForUser,
                gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
                gfi::GeneralFormatIdentifier::Modulo8Sequencing);
        const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier =
                lci::LogicalChannelIdentifier::create(8, 0);
        const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
                pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::CallAccepted_CallConnected);
        header = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    payload::CallPacketPayload::SharedPointer payload = payload::CallPacketPayload::downCast(
            payload::CallPacketPayload::create());

    const X25Packet::SharedPointer source = X25Packet::create(header,
            payload::CallPacketPayload::upCast(payload));

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PacketMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const X25Packet::SharedPointer result = X25PacketMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
}

BOOST_AUTO_TEST_CASE(test_ClearPacketPayload) {

    const unsigned int expectedIntermediateSize = 5;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x10, 0x01, 0x13, 0x39, 0x30};

    X25Header::SharedPointer header;
    {
        const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifier::create(
                gfi::GeneralFormatIdentifier::DataForUser,
                gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
                gfi::GeneralFormatIdentifier::Modulo8Sequencing);
        const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier =
                lci::LogicalChannelIdentifier::create(0, 1);
        const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
                pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication);
        header = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    payload::ClearPacketPayload::SharedPointer payload;
    {
        const x25::codes::CauseCodeEnumerations::ClearRequestEnumeration cause = x25::codes::CauseCodeEnumerations::ClearRequestShipAbsent;
        const x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostic = x25::codes::DiagnosticCodeEnumerations::TimerExpired;
        payload = payload::ClearPacketPayload::downCast(
                payload::ClearPacketPayload::create(cause, diagnostic));
    }

    const X25Packet::SharedPointer source = X25Packet::create(header,
            payload::ClearPacketPayload::upCast(payload));

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PacketMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const X25Packet::SharedPointer result = X25PacketMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
}

BOOST_AUTO_TEST_CASE(test_ClearConfirmationPayload) {

    const unsigned int expectedIntermediateSize = 3;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x10, 0x01, 0x17};

    X25Header::SharedPointer header;
    {
        const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifier::create(
                gfi::GeneralFormatIdentifier::DataForUser,
                gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
                gfi::GeneralFormatIdentifier::Modulo8Sequencing);
        const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier =
                lci::LogicalChannelIdentifier::create(0, 1);
        const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
                pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::ClearConfirmation);
        header = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    payload::ClearConfirmationPayload::SharedPointer payload;
    {
        payload = payload::ClearConfirmationPayload::downCast(
                payload::ClearConfirmationPayload::create());
    }

    const X25Packet::SharedPointer source = X25Packet::create(header,
            payload::ClearConfirmationPayload::upCast(payload));

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PacketMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const X25Packet::SharedPointer result = X25PacketMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
}

BOOST_AUTO_TEST_CASE(test_ResetPacketPayload) {

    const unsigned int expectedIntermediateSize = 5;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x10, 0x01, 0x1b, 0x07, 0x30};

    X25Header::SharedPointer header;
    {
        const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifier::create(
                gfi::GeneralFormatIdentifier::DataForUser,
                gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
                gfi::GeneralFormatIdentifier::Modulo8Sequencing);
        const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier =
                lci::LogicalChannelIdentifier::create(0, 1);
        const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
                pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication);
        header = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    payload::ResetPacketPayload::SharedPointer payload;
    {
        const x25::codes::CauseCodeEnumerations::ResetRequestEnumeration cause = x25::codes::CauseCodeEnumerations::ResetRequestNetworkCongestion;
        const x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostic = x25::codes::DiagnosticCodeEnumerations::TimerExpired;
        payload = payload::ResetPacketPayload::downCast(
                payload::ResetPacketPayload::create(cause, diagnostic));
    }

    const X25Packet::SharedPointer source = X25Packet::create(header,
            payload::ResetPacketPayload::upCast(payload));

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PacketMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const X25Packet::SharedPointer result = X25PacketMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
}

BOOST_AUTO_TEST_CASE(test_RestartPacketPayload) {

    const unsigned int expectedIntermediateSize = 5;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x10, 0x01, 0xfb, 0x03, 0x00};

    X25Header::SharedPointer header;
    {
        const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifier::create(
                gfi::GeneralFormatIdentifier::DataForUser,
                gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
                gfi::GeneralFormatIdentifier::Modulo8Sequencing);
        const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier =
                lci::LogicalChannelIdentifier::create(0, 1);
        const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
                pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication);
        header = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    payload::RestartPacketPayload::SharedPointer payload;
    {
        const x25::codes::CauseCodeEnumerations::RestartRequestEnumeration cause = x25::codes::CauseCodeEnumerations::RestartRequestNetworkCongestion;
        const x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostic = x25::codes::DiagnosticCodeEnumerations::NoAdditionalInformation;
        payload = payload::RestartPacketPayload::downCast(
                payload::RestartPacketPayload::create(cause, diagnostic));
    }

    const X25Packet::SharedPointer source = X25Packet::create(header,
            payload::RestartPacketPayload::upCast(payload));

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PacketMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const X25Packet::SharedPointer result = X25PacketMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
}

BOOST_AUTO_TEST_CASE(test_DataPacketPayload) {

    const unsigned int expectedIntermediateSize = 14;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x10, 0x01, 0x22, 0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64};

    X25Header::SharedPointer header;
    {
        const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifier::create(
                gfi::GeneralFormatIdentifier::DataForUser,
                gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
                gfi::GeneralFormatIdentifier::Modulo8Sequencing);
        const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier =
                lci::LogicalChannelIdentifier::create(0, 1);
        const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
                pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::Data, false, 1, 1);
        header = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    payload::DataPacketPayload::SharedPointer payload;
    {
        const unsigned int dataSize = 11;
        const Octet data[dataSize] = {0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64};
        payload = payload::DataPacketPayload::downCast(
                payload::DataPacketPayload::create(Octets(data, data + dataSize)));
    }

    const X25Packet::SharedPointer source = X25Packet::create(header,
            payload::DataPacketPayload::upCast(payload));

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        X25PacketMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const X25Packet::SharedPointer result = X25PacketMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
}


BOOST_AUTO_TEST_SUITE_END()
