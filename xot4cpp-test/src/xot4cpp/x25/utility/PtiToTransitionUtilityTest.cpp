#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/utility/PtiToTransitionUtility.hpp>

using namespace xot4cpp::x25::utility;

using namespace xot4cpp::x25::context;
using namespace xot4cpp::x25::pti;
using namespace xot4cpp::x25::transitions;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_utility_PtiToTransitionUtility_test_suite)

BOOST_AUTO_TEST_CASE(test_Simple) {

    // Call set-up and clearing DESCRIPTION
    BOOST_CHECK_EQUAL(TransitionEnumerations::CallRequest,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::CallRequest_IncomingCall));
    BOOST_CHECK_EQUAL(TransitionEnumerations::CallIncoming,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::CallRequest_IncomingCall));
    BOOST_CHECK_EQUAL(TransitionEnumerations::CallAccepted,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::CallAccepted_CallConnected));
    BOOST_CHECK_EQUAL(TransitionEnumerations::CallConnected,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::CallAccepted_CallConnected));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ClearRequest,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ClearIndication,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ClearConfirmation,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::ClearConfirmation));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ClearConfirmation,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::ClearConfirmation));
    // Data and interrupt DESCRIPTION
    BOOST_CHECK_EQUAL(TransitionEnumerations::Data,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::Data));
    BOOST_CHECK_EQUAL(TransitionEnumerations::Data,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::Data));
    BOOST_CHECK_EQUAL(TransitionEnumerations::Interrupt,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::Interrupt));
    BOOST_CHECK_EQUAL(TransitionEnumerations::Interrupt,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::Interrupt));
    BOOST_CHECK_EQUAL(TransitionEnumerations::InterruptConfirmation,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::InterruptConfirmation));
    BOOST_CHECK_EQUAL(TransitionEnumerations::InterruptConfirmation,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::InterruptConfirmation));
    // Flow control and reset DESCRIPTION
    BOOST_CHECK_EQUAL(TransitionEnumerations::ReceiverReady,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::ReceiverReady));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ReceiverReady,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::ReceiverReady));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ReceiverNotReady,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::ReceiverNotReady));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ReceiverNotReady,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::ReceiverNotReady));
    BOOST_CHECK_EQUAL(TransitionEnumerations::Reject,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::Reject));
    BOOST_CHECK_EQUAL(TransitionEnumerations::Reject,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::Reject));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ResetRequest,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ResetIndication,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ResetConfirmation,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::ResetConfirmation));
    BOOST_CHECK_EQUAL(TransitionEnumerations::ResetConfirmation,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::ResetConfirmation));
    // Restart DESCRIPTION
    BOOST_CHECK_EQUAL(TransitionEnumerations::RestartRequest,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication));
    BOOST_CHECK_EQUAL(TransitionEnumerations::RestartIndication,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication));
    BOOST_CHECK_EQUAL(TransitionEnumerations::RestartConfirmation,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::RestartConfirmation));
    BOOST_CHECK_EQUAL(TransitionEnumerations::RestartConfirmation,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::RestartConfirmation));
    // Diagnostic DESCRIPTION
    BOOST_CHECK_EQUAL(TransitionEnumerations::Diagnostic,
            PtiToTransitionUtility::transition(RoleEnumerations::DTE, PacketTypeIdentifierEnumerations::Diagnostic));
    BOOST_CHECK_EQUAL(TransitionEnumerations::Diagnostic,
            PtiToTransitionUtility::transition(RoleEnumerations::DCE, PacketTypeIdentifierEnumerations::Diagnostic));
}

BOOST_AUTO_TEST_SUITE_END()
