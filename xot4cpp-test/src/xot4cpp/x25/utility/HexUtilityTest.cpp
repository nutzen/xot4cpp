#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/utility/HexUtility.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x25::utility;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_utility_HexUtility_test_suite)

BOOST_AUTO_TEST_CASE(test_Simple) {

    const unsigned int size = 20;
    const Octet source[size] = {0x10, 0x01, 0x0b, 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, 0xc4};
    const std::string expected = "10 01 0b 87 16 34 56 71 10 09 00 00 06 43 01 01 42 07 07 c4";

    const std::string target = HexUtility::toHex(Octets(source, source + size));
    BOOST_CHECK_EQUAL(expected, target);
}

BOOST_AUTO_TEST_SUITE_END()
