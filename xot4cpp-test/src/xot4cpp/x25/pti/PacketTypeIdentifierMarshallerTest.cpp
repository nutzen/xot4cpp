#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x25::pti;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_pti_PacketTypeIdentifierMarshaller_test_suite)


void testSimpleModulo8(const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_source) {

    const PacketTypeIdentifier::SharedPointer identifier = PacketTypeIdentifier::create(p_source);

    const Octet intermediate = PacketTypeIdentifierMarshaller::marshalModule8(identifier);
    BOOST_CHECK_EQUAL(PacketTypeIdentifierEnumerations::toInt(p_source), (intermediate & 0x00ff));

    const PacketTypeIdentifier::SharedPointer target = PacketTypeIdentifierMarshaller::unmarshalModule8(intermediate);
    BOOST_CHECK_EQUAL(p_source, target->enumeration());
    BOOST_CHECK_EQUAL(false, target->isDataPacketTypeIdentifier());
    BOOST_CHECK_EQUAL(false, target->isReceiverPacketTypeIdentifier());
}

void testSimpleModulo8(const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_source,
        const unsigned int& p_packetReceiveSequenceNumber) {

    const PacketTypeIdentifier::SharedPointer identifier = PacketTypeIdentifier::create(p_source, p_packetReceiveSequenceNumber);

    const Octet intermediate = PacketTypeIdentifierMarshaller::marshalModule8(identifier);
    const PacketTypeIdentifier::SharedPointer target = PacketTypeIdentifierMarshaller::unmarshalModule8(intermediate);
    BOOST_CHECK_EQUAL(p_source, target->enumeration());
    BOOST_CHECK_EQUAL(false, target->isDataPacketTypeIdentifier());
    BOOST_CHECK_EQUAL(true, target->isReceiverPacketTypeIdentifier());

    const ReceiverPacketTypeIdentifier::SharedPointer pti = ReceiverPacketTypeIdentifier::downCast(target);
    BOOST_CHECK_EQUAL(p_packetReceiveSequenceNumber, pti->packetReceiveSequenceNumber());
}

void testSimpleModulo8(const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_source,
        const bool& p_moreData,
        const unsigned int& p_packetReceiveSequenceNumber,
        const unsigned int& p_packetSendSequenceNumber) {

    const PacketTypeIdentifier::SharedPointer identifier = PacketTypeIdentifier::create(p_source, p_moreData, p_packetReceiveSequenceNumber, p_packetSendSequenceNumber);

    const Octet intermediate = PacketTypeIdentifierMarshaller::marshalModule8(identifier);
    const PacketTypeIdentifier::SharedPointer target = PacketTypeIdentifierMarshaller::unmarshalModule8(intermediate);
    BOOST_CHECK_EQUAL(p_source, target->enumeration());
    BOOST_CHECK_EQUAL(true, target->isDataPacketTypeIdentifier());
    BOOST_CHECK_EQUAL(false, target->isReceiverPacketTypeIdentifier());

    const DataPacketTypeIdentifier::SharedPointer pti = DataPacketTypeIdentifier::downCast(target);
    BOOST_CHECK_EQUAL(p_moreData, pti->moreData());
    BOOST_CHECK_EQUAL(p_packetReceiveSequenceNumber, pti->packetReceiveSequenceNumber());
    BOOST_CHECK_EQUAL(p_packetSendSequenceNumber, pti->packetSendSequenceNumber());
}

BOOST_AUTO_TEST_CASE(test_SimpleModulo8) {

    testSimpleModulo8(PacketTypeIdentifierEnumerations::CallRequest_IncomingCall);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::CallAccepted_CallConnected);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::ClearConfirmation);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::Data, true, 2, 1);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::Interrupt);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::InterruptConfirmation);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::ReceiverReady, 7);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::ReceiverNotReady, 6);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::Reject, 5);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::ResetConfirmation);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::RestartConfirmation);
    testSimpleModulo8(PacketTypeIdentifierEnumerations::Diagnostic);
}

BOOST_AUTO_TEST_SUITE_END()
