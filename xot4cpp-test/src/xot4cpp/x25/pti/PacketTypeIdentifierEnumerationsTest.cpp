#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/pti/PacketTypeIdentifierEnumerations.hpp>

using namespace xot4cpp::x25::pti;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_pti_PacketTypeIdentifierEnumerations_test_suite)

void testSimple(const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_source) {

    {
        const std::string intermediate = PacketTypeIdentifierEnumerations::toDescription(p_source);
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration pti = PacketTypeIdentifierEnumerations::fromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, pti);
    }

    {
        const int intermediate = PacketTypeIdentifierEnumerations::toInt(p_source);
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration pti = PacketTypeIdentifierEnumerations::fromInt(intermediate);

        BOOST_CHECK_EQUAL(p_source, pti);
    }
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    testSimple(PacketTypeIdentifierEnumerations::CallRequest_IncomingCall);
    testSimple(PacketTypeIdentifierEnumerations::CallAccepted_CallConnected);
    testSimple(PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication);
    testSimple(PacketTypeIdentifierEnumerations::ClearConfirmation);
    testSimple(PacketTypeIdentifierEnumerations::Data);
    testSimple(PacketTypeIdentifierEnumerations::Interrupt);
    testSimple(PacketTypeIdentifierEnumerations::InterruptConfirmation);
    testSimple(PacketTypeIdentifierEnumerations::ReceiverReady);
    testSimple(PacketTypeIdentifierEnumerations::ReceiverNotReady);
    testSimple(PacketTypeIdentifierEnumerations::Reject);
    testSimple(PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication);
    testSimple(PacketTypeIdentifierEnumerations::ResetConfirmation);
    testSimple(PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication);
    testSimple(PacketTypeIdentifierEnumerations::RestartConfirmation);
    testSimple(PacketTypeIdentifierEnumerations::Diagnostic);
}

BOOST_AUTO_TEST_SUITE_END()
