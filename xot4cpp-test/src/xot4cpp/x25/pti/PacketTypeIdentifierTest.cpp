#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/pti/PacketTypeIdentifier.hpp>

using namespace xot4cpp::x25::pti;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_pti_PacketTypeIdentifier_test_suite)

void testSimple(const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_source) {

    const PacketTypeIdentifier::SharedPointer packetTypeIdentifier = PacketTypeIdentifier::create(p_source);
    BOOST_CHECK_EQUAL(p_source, packetTypeIdentifier->enumeration());
    BOOST_CHECK_EQUAL(false, packetTypeIdentifier->isDataPacketTypeIdentifier());
    BOOST_CHECK_EQUAL(false, packetTypeIdentifier->isReceiverPacketTypeIdentifier());
}

void testSimple(
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_source,
        const unsigned int& p_packetReceiveSequenceNumber) {

    const PacketTypeIdentifier::SharedPointer packetTypeIdentifier = PacketTypeIdentifier::create(p_source, p_packetReceiveSequenceNumber);
    BOOST_CHECK_EQUAL(p_source, packetTypeIdentifier->enumeration());
    BOOST_CHECK_EQUAL(false, packetTypeIdentifier->isDataPacketTypeIdentifier());
    BOOST_CHECK_EQUAL(true, packetTypeIdentifier->isReceiverPacketTypeIdentifier());

    const ReceiverPacketTypeIdentifier::SharedPointer identifier = ReceiverPacketTypeIdentifier::downCast(packetTypeIdentifier);
    BOOST_CHECK_EQUAL(p_packetReceiveSequenceNumber, identifier->packetReceiveSequenceNumber());
}

void testSimple(
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_source,
        const bool& p_moreData,
        const unsigned int& p_packetReceiveSequenceNumber,
        const unsigned int& p_packetSendSequenceNumber) {

    const PacketTypeIdentifier::SharedPointer packetTypeIdentifier = PacketTypeIdentifier::create(p_source, p_moreData, p_packetReceiveSequenceNumber, p_packetSendSequenceNumber);
    BOOST_CHECK_EQUAL(p_source, packetTypeIdentifier->enumeration());
    BOOST_CHECK_EQUAL(true, packetTypeIdentifier->isDataPacketTypeIdentifier());
    BOOST_CHECK_EQUAL(false, packetTypeIdentifier->isReceiverPacketTypeIdentifier());

    const DataPacketTypeIdentifier::SharedPointer identifier = DataPacketTypeIdentifier::downCast(packetTypeIdentifier);
    BOOST_CHECK_EQUAL(p_moreData, identifier->moreData());
    BOOST_CHECK_EQUAL(p_packetReceiveSequenceNumber, identifier->packetReceiveSequenceNumber());
    BOOST_CHECK_EQUAL(p_packetSendSequenceNumber, identifier->packetSendSequenceNumber());
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    testSimple(PacketTypeIdentifierEnumerations::CallRequest_IncomingCall);
    testSimple(PacketTypeIdentifierEnumerations::CallAccepted_CallConnected);
    testSimple(PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication);
    testSimple(PacketTypeIdentifierEnumerations::ClearConfirmation);
    testSimple(PacketTypeIdentifierEnumerations::Data, false, 1, 2);
    testSimple(PacketTypeIdentifierEnumerations::Interrupt);
    testSimple(PacketTypeIdentifierEnumerations::InterruptConfirmation);
    testSimple(PacketTypeIdentifierEnumerations::ReceiverReady, 2);
    testSimple(PacketTypeIdentifierEnumerations::ReceiverNotReady, 3);
    testSimple(PacketTypeIdentifierEnumerations::Reject, 4);
    testSimple(PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication);
    testSimple(PacketTypeIdentifierEnumerations::ResetConfirmation);
    testSimple(PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication);
    testSimple(PacketTypeIdentifierEnumerations::RestartConfirmation);
    testSimple(PacketTypeIdentifierEnumerations::Diagnostic);
}

BOOST_AUTO_TEST_SUITE_END()
