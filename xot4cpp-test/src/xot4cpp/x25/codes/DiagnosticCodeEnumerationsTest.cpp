#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/codes/DiagnosticCodeEnumerations.hpp>

using namespace xot4cpp::x25::codes;

// DiagnosticCodeEnumerationsTest

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_codes_DiagnosticCodeEnumerations_test_suite)

void testSimple(const DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_source) {

    {
        const std::string intermediate = DiagnosticCodeEnumerations::toDescription(p_source);
        const DiagnosticCodeEnumerations::DiagnosticCodeEnumeration enumeration = DiagnosticCodeEnumerations::fromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }

    {
        const int intermediate = DiagnosticCodeEnumerations::toInt(p_source);
        const DiagnosticCodeEnumerations::DiagnosticCodeEnumeration enumeration = DiagnosticCodeEnumerations::fromInt(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    testSimple(DiagnosticCodeEnumerations::NoAdditionalInformation);
    testSimple(DiagnosticCodeEnumerations::InvalidPacketSendSequenceNumber);
    testSimple(DiagnosticCodeEnumerations::InvalidPacketReceiveSequenceNumber);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalid);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateR1PacketLevelReady);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateR2DteRestartRequest);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateR3DceRestartIndication);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateP1Ready);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateP2DteWaiting);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateP3DceWaiting);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateP4DataTransfer);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateP5CallCollision);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateP6DteClearRequest);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateP7DceClearIndication);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateD1FlowControlReady);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateD2DteResetReady);
    testSimple(DiagnosticCodeEnumerations::PacketTypeInvalidForStateD3DceResetIndication);
    testSimple(DiagnosticCodeEnumerations::PacketNotAllowed);
    testSimple(DiagnosticCodeEnumerations::UnidentifiablePacket);
    testSimple(DiagnosticCodeEnumerations::CallOnOneWayLogicalChannel);
    testSimple(DiagnosticCodeEnumerations::InvalidPacketTypeOnAPermanentVirtualCircuit);
    testSimple(DiagnosticCodeEnumerations::PacketOnUnassignedLogicalChannelNumber);
    testSimple(DiagnosticCodeEnumerations::RejectNotSubscribedTo);
    testSimple(DiagnosticCodeEnumerations::PacketTooShort);
    testSimple(DiagnosticCodeEnumerations::PacketTooLong);
    testSimple(DiagnosticCodeEnumerations::InvalidGeneralFormatIdentifier);
    testSimple(DiagnosticCodeEnumerations::RestartOrRegistrationPacketWithNonzeroLci);
    testSimple(DiagnosticCodeEnumerations::PacketTypeNotCompatibleWithFacility);
    testSimple(DiagnosticCodeEnumerations::UnauthorizedInterruptConfirmation);
    testSimple(DiagnosticCodeEnumerations::UnauthorizedInterrupt);
    testSimple(DiagnosticCodeEnumerations::UnauthorizedReject);
    testSimple(DiagnosticCodeEnumerations::TimerExpired);
    testSimple(DiagnosticCodeEnumerations::TimerExpiredForIncomingCall);
    testSimple(DiagnosticCodeEnumerations::TimerExpiredForClearIndication);
    testSimple(DiagnosticCodeEnumerations::TimerExpiredForResetIndication);
    testSimple(DiagnosticCodeEnumerations::TimerExpiredForRestartIndication);
    testSimple(DiagnosticCodeEnumerations::TimerExpiredForCallDeflection);
    testSimple(DiagnosticCodeEnumerations::CallSetupClearingOrRegistrationProblem);
    testSimple(DiagnosticCodeEnumerations::FacilityCodeNotAllowed);
    testSimple(DiagnosticCodeEnumerations::FacilityParameterNotAllowed);
    testSimple(DiagnosticCodeEnumerations::InvalidCalledAddress);
    testSimple(DiagnosticCodeEnumerations::InvalidCallingAddress);
    testSimple(DiagnosticCodeEnumerations::InvalidFacilityLength);
    testSimple(DiagnosticCodeEnumerations::IncomingCallBarred);
    testSimple(DiagnosticCodeEnumerations::NoLogicalChannelAvailable);
    testSimple(DiagnosticCodeEnumerations::CallCollision);
    testSimple(DiagnosticCodeEnumerations::DuplicateFacilityRequested);
    testSimple(DiagnosticCodeEnumerations::NonzeroAddressLength);
    testSimple(DiagnosticCodeEnumerations::NonzeroFacilityLength);
    testSimple(DiagnosticCodeEnumerations::FacilityNotProvidedWhenExpected);
    testSimple(DiagnosticCodeEnumerations::InvalidItuTSpecifiedDteFacility);
    testSimple(DiagnosticCodeEnumerations::MaximumNumberOfCallRedirectionsOrDeflectionsExceeded);
    testSimple(DiagnosticCodeEnumerations::Miscellaneous);
    testSimple(DiagnosticCodeEnumerations::ImproperCauseCodeForDte);
    testSimple(DiagnosticCodeEnumerations::OctetNotAaligned);
    testSimple(DiagnosticCodeEnumerations::InconsistentQBitSetting);
    testSimple(DiagnosticCodeEnumerations::NetworkUserIdentificationProblem);
    testSimple(DiagnosticCodeEnumerations::InternationalProblem);
    testSimple(DiagnosticCodeEnumerations::RemoteNetworkProblem);
    testSimple(DiagnosticCodeEnumerations::InternationalProtocolProblem);
    testSimple(DiagnosticCodeEnumerations::InternationalLinkOutOfOrder);
    testSimple(DiagnosticCodeEnumerations::InternationalLinkBusy);
    testSimple(DiagnosticCodeEnumerations::TransitNetworkFacilityProblem);
    testSimple(DiagnosticCodeEnumerations::RemoteNetworkFacilityProblem);
    testSimple(DiagnosticCodeEnumerations::InternationalRoutingProblem);
    testSimple(DiagnosticCodeEnumerations::TemporaryRoutingProblem);
    testSimple(DiagnosticCodeEnumerations::UnknownCalledDataNetworkIdentificationCode);
    testSimple(DiagnosticCodeEnumerations::MaintenanceActionClearX25VcCommandIssued);
    // 0xf..
    testSimple(DiagnosticCodeEnumerations::NormalTermination);
    testSimple(DiagnosticCodeEnumerations::OutOfResources);
    testSimple(DiagnosticCodeEnumerations::AuthenticationFailure);
    testSimple(DiagnosticCodeEnumerations::InboundUserDataTooLarge);
    testSimple(DiagnosticCodeEnumerations::IdleTimerExpired);
}

BOOST_AUTO_TEST_SUITE_END()
