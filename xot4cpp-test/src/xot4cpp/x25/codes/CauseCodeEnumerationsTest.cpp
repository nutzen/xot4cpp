#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/codes/CauseCodeEnumerations.hpp>

using namespace xot4cpp::x25::codes;

// CauseCodeEnumerationsTest

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_codes_CauseCodeEnumerations_test_suite)

void testSimple(const CauseCodeEnumerations::ClearRequestEnumeration& p_source) {

    {
        const std::string intermediate = CauseCodeEnumerations::toDescription(p_source);
        const CauseCodeEnumerations::ClearRequestEnumeration enumeration = CauseCodeEnumerations::clearRequestFromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }

    {
        const int intermediate = CauseCodeEnumerations::toInt(p_source);
        const CauseCodeEnumerations::ClearRequestEnumeration enumeration = CauseCodeEnumerations::clearRequestFromInt(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }
}

void testSimple(const CauseCodeEnumerations::ResetRequestEnumeration& p_source) {

    {
        const std::string intermediate = CauseCodeEnumerations::toDescription(p_source);
        const CauseCodeEnumerations::ResetRequestEnumeration enumeration = CauseCodeEnumerations::resetRequestFromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }

    {
        const int intermediate = CauseCodeEnumerations::toInt(p_source);
        const CauseCodeEnumerations::ResetRequestEnumeration enumeration = CauseCodeEnumerations::resetRequestFromInt(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }
}

void testSimple(const CauseCodeEnumerations::RestartRequestEnumeration& p_source) {

    {
        const std::string intermediate = CauseCodeEnumerations::toDescription(p_source);
        const CauseCodeEnumerations::RestartRequestEnumeration enumeration = CauseCodeEnumerations::restartRequestFromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }

    {
        const int intermediate = CauseCodeEnumerations::toInt(p_source);
        const CauseCodeEnumerations::RestartRequestEnumeration enumeration = CauseCodeEnumerations::restartRequestFromInt(intermediate);

        BOOST_CHECK_EQUAL(p_source, enumeration);
    }
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    testSimple(CauseCodeEnumerations::ClearRequestDteOriginated);
    testSimple(CauseCodeEnumerations::ClearRequestNumberBusy);
    testSimple(CauseCodeEnumerations::ClearRequestInvalidFacilityRequest);
    testSimple(CauseCodeEnumerations::ClearRequestNetworkCongestion);
    testSimple(CauseCodeEnumerations::ClearRequestOutOfOrder);
    testSimple(CauseCodeEnumerations::ClearRequestAccessBarred);
    testSimple(CauseCodeEnumerations::ClearRequestNotObtainable);
    testSimple(CauseCodeEnumerations::ClearRequestRemoteProcedureError);
    testSimple(CauseCodeEnumerations::ClearRequestLocalProcedureError);
    testSimple(CauseCodeEnumerations::ClearRequestRpoaOutOfOrder);
    testSimple(CauseCodeEnumerations::ClearRequestReverseChargingNotAccepted);
    testSimple(CauseCodeEnumerations::ClearRequestIncompatibleDestination);
    testSimple(CauseCodeEnumerations::ClearRequestFastSelectNotAccepted);
    testSimple(CauseCodeEnumerations::ClearRequestShipAbsent);

    // ResetRequestEnumeration
    testSimple(CauseCodeEnumerations::ResetRequestDteOriginated);
    testSimple(CauseCodeEnumerations::ResetRequestOutOfOrder);
    testSimple(CauseCodeEnumerations::ResetRequestRemoteProcedureError);
    testSimple(CauseCodeEnumerations::ResetRequestLocalProcedureError);
    testSimple(CauseCodeEnumerations::ResetRequestNetworkCongestion);
    testSimple(CauseCodeEnumerations::ResetRequestRemoteDteOperational);
    testSimple(CauseCodeEnumerations::ResetRequestNetworkOperational);
    testSimple(CauseCodeEnumerations::ResetRequestIncompatibleDestination);
    testSimple(CauseCodeEnumerations::ResetRequestNetworkOutOfOrder);

    // RestartRequestEnumeration
    testSimple(CauseCodeEnumerations::RestartRequestDteRestarting);
    testSimple(CauseCodeEnumerations::RestartRequestLocalProcedureError);
    testSimple(CauseCodeEnumerations::RestartRequestNetworkCongestion);
    testSimple(CauseCodeEnumerations::RestartRequestNetworkOperational);
    testSimple(CauseCodeEnumerations::RestartRequestRegistrationCancellationConfirmed);
}

BOOST_AUTO_TEST_SUITE_END()
