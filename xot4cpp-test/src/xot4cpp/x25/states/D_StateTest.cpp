#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/states/D1_FlowControlReadyState.hpp>
#include <xot4cpp/x25/states/D2_DTEResetingState.hpp>
#include <xot4cpp/x25/states/D3_DCEResetingState.hpp>

using namespace xot4cpp::x25::states;
using namespace xot4cpp::x25::transitions;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_codes_D_State_test_suite)

BOOST_AUTO_TEST_CASE(test_D1_FlowControlReadyState) {

    // Data
    {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Data;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // Interrupt
    {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Interrupt;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // InterruptConfirmation
    {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::InterruptConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ReceiverReady
    {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ReceiverReady;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ReceiverNotReady
    {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ReceiverNotReady;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // Reject
    {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Reject;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // Diagnostic
    try {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Diagnostic;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }

    // ResetRequest
    {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D2_DTEReseting;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ResetIndication
    {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D3_DCEReseting;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ResetConfirmation
    try {
        const State::SharedPointer state = D1_FlowControlReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
}

BOOST_AUTO_TEST_CASE(test_D2_DTEResetingState) {

    // ResetRequest
    {
        const State::SharedPointer state = D2_DTEResetingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D2_DTEReseting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetRequest;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ResetIndication
    {
        const State::SharedPointer state = D2_DTEResetingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D2_DTEReseting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D1_FlowControlReady;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ResetConfirmation
    {
        const State::SharedPointer state = D2_DTEResetingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D2_DTEReseting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetConfirmation;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D1_FlowControlReady;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
}

BOOST_AUTO_TEST_CASE(test_D3_DCEResetingState) {

    // ResetRequest
    {
        const State::SharedPointer state = D3_DCEResetingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D3_DCEReseting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D1_FlowControlReady;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ResetIndication
    {
        const State::SharedPointer state = D3_DCEResetingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D3_DCEReseting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetIndication;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ResetConfirmation
    {
        const State::SharedPointer state = D3_DCEResetingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D3_DCEReseting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetConfirmation;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D1_FlowControlReady;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
}

BOOST_AUTO_TEST_SUITE_END()
