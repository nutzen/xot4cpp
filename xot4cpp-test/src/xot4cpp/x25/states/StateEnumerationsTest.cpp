#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/states/StateEnumerations.hpp>

using namespace xot4cpp::x25::states;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_states_StateEnumerations_test_suite)

void testSimple(const StateEnumerations::StateEnumeration& p_source) {

    {
        const std::string intermediate = StateEnumerations::toDescription(p_source);
        const StateEnumerations::StateEnumeration pti = StateEnumerations::fromDescription(intermediate);

        BOOST_CHECK_EQUAL(p_source, pti);
    }
}

BOOST_AUTO_TEST_CASE(test_Simple) {

        // P-States
        testSimple(StateEnumerations::P1_Ready);
        testSimple(StateEnumerations::P2_DTEWaiting);
        testSimple(StateEnumerations::P3_DCEWaiting);
        testSimple(StateEnumerations::P4_DataTransfer);
        testSimple(StateEnumerations::P5_CallCollision);
        testSimple(StateEnumerations::P6_DTEClearing);
        testSimple(StateEnumerations::P7_DCEClearing);
        // D-States
        testSimple(StateEnumerations::D1_FlowControlReady);
        testSimple(StateEnumerations::D2_DTEReseting);
        testSimple(StateEnumerations::D3_DCEReseting);
        // R-States
        testSimple(StateEnumerations::R1_PacketLayerReady);
        testSimple(StateEnumerations::R2_DTERestarting);
        testSimple(StateEnumerations::R3_DCERestarting);
}

BOOST_AUTO_TEST_SUITE_END()
