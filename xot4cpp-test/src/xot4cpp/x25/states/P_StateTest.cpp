#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/states/P1_ReadyState.hpp>
#include <xot4cpp/x25/states/P2_DTEWaitingState.hpp>
#include <xot4cpp/x25/states/P3_DCEWaitingState.hpp>
#include <xot4cpp/x25/states/P4_DataTransferState.hpp>
#include <xot4cpp/x25/states/P5_CallCollisionState.hpp>
#include <xot4cpp/x25/states/P6_DTEClearingState.hpp>
#include <xot4cpp/x25/states/P7_DCEClearingState.hpp>

using namespace xot4cpp::x25::states;
using namespace xot4cpp::x25::transitions;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_codes_P_State_test_suite)

BOOST_AUTO_TEST_CASE(test_P1_ReadyState) {

    // Data
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Data;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // Interrupt
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Interrupt;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // InterruptConfirmation
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::InterruptConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ReceiverReady
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ReceiverReady;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ReceiverNotReady
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ReceiverNotReady;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // Reject
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Reject;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // Diagnostic
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Diagnostic;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }

    // ResetRequest
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ResetIndication
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetIndication;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ResetConfirmation
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }

    // CallRequest
    {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P2_DTEWaiting;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // CallIncoming
    {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallIncoming;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P3_DCEWaiting;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // CallAccepted
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallAccepted;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallConnected
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallConnected;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ClearRequest
    {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P6_DTEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearIndication
    {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P7_DCEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearConfirmation
    try {
        const State::SharedPointer state = P1_ReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
}

BOOST_AUTO_TEST_CASE(test_P2_DTEWaitingState) {

    // CallRequest
    try {
        const State::SharedPointer state = P2_DTEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P2_DTEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallIncoming
    {
        const State::SharedPointer state = P2_DTEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P2_DTEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallIncoming;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P5_CallCollision;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // CallAccepted
    try {
        const State::SharedPointer state = P2_DTEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P2_DTEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallAccepted;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallConnected
    {
        const State::SharedPointer state = P2_DTEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P2_DTEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallConnected;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D1_FlowControlReady;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearRequest
    {
        const State::SharedPointer state = P2_DTEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P2_DTEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P6_DTEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearIndication
    {
        const State::SharedPointer state = P2_DTEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P2_DTEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P7_DCEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearConfirmation
    try {
        const State::SharedPointer state = P2_DTEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P2_DTEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
}

BOOST_AUTO_TEST_CASE(test_P3_DCEWaitingState) {

    // CallRequest
    {
        const State::SharedPointer state = P3_DCEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P3_DCEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P5_CallCollision;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // CallIncoming
    try {
        const State::SharedPointer state = P3_DCEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P3_DCEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallIncoming;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallAccepted
    {
        const State::SharedPointer state = P3_DCEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P3_DCEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallAccepted;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D1_FlowControlReady;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // CallConnected
    try {
        const State::SharedPointer state = P3_DCEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P3_DCEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallConnected;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ClearRequest
    {
        const State::SharedPointer state = P3_DCEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P3_DCEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P6_DTEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearIndication
    {
        const State::SharedPointer state = P3_DCEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P3_DCEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P7_DCEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearConfirmation
    try {
        const State::SharedPointer state = P3_DCEWaitingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P3_DCEWaiting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
}

BOOST_AUTO_TEST_CASE(test_P4_DataTransferState) {

    // ResetRequest
    {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D2_DTEReseting;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, state->enumeration());
    }
    // ResetIndication
    {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D3_DCEReseting;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, state->enumeration());
    }
    // ResetConfirmation
    try {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }

    // CallRequest
    try {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallIncoming
    try {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallIncoming;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallAccepted
    try {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallAccepted;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallConnected
    try {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallConnected;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ClearRequest
    {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P6_DTEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearIndication
    {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P7_DCEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearConfirmation
    try {
        const State::SharedPointer state = P4_DataTransferState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::D1_FlowControlReady, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
}

BOOST_AUTO_TEST_CASE(test_P5_CallCollisionState) {

    // CallRequest
    try {
        const State::SharedPointer state = P5_CallCollisionState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P5_CallCollision, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallIncoming
    try {
        const State::SharedPointer state = P5_CallCollisionState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P5_CallCollision, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallIncoming;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallAccepted
    try {
        const State::SharedPointer state = P5_CallCollisionState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P5_CallCollision, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallAccepted;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallConnected
    {
        const State::SharedPointer state = P5_CallCollisionState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P5_CallCollision, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallConnected;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::D1_FlowControlReady;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearRequest
    {
        const State::SharedPointer state = P5_CallCollisionState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P5_CallCollision, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P6_DTEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearIndication
    {
        const State::SharedPointer state = P5_CallCollisionState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P5_CallCollision, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P7_DCEClearing;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearConfirmation
    try {
        const State::SharedPointer state = P5_CallCollisionState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P5_CallCollision, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
}

BOOST_AUTO_TEST_CASE(test_P6_DTEClearingState) {

    // CallRequest
    try {
        const State::SharedPointer state = P6_DTEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P6_DTEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallIncoming
    {
        const State::SharedPointer state = P6_DTEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P6_DTEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallIncoming;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // CallAccepted
    try {
        const State::SharedPointer state = P6_DTEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P6_DTEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallAccepted;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallConnected
    {
        const State::SharedPointer state = P6_DTEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P6_DTEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallConnected;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ClearRequest
    {
        const State::SharedPointer state = P6_DTEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P6_DTEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ClearIndication
    {
        const State::SharedPointer state = P6_DTEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P6_DTEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P1_Ready;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearConfirmation
    {
        const State::SharedPointer state = P6_DTEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P6_DTEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearConfirmation;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P1_Ready;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
}

BOOST_AUTO_TEST_CASE(test_P7_DCEClearingState) {

    // CallRequest
    {
        const State::SharedPointer state = P7_DCEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P7_DCEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // CallIncoming
    try {
        const State::SharedPointer state = P7_DCEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P7_DCEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallIncoming;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallAccepted
    {
        const State::SharedPointer state = P7_DCEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P7_DCEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallAccepted;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // CallConnected
    try {
        const State::SharedPointer state = P7_DCEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P7_DCEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallConnected;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ClearRequest
    {
        const State::SharedPointer state = P7_DCEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P7_DCEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P1_Ready;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // ClearIndication
    {
        const State::SharedPointer state = P7_DCEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P7_DCEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearIndication;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ClearConfirmation
    {
        const State::SharedPointer state = P7_DCEClearingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P7_DCEClearing, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearConfirmation;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P1_Ready;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
}

BOOST_AUTO_TEST_SUITE_END()
