#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/states/R1_PacketLayerReadyState.hpp>
#include <xot4cpp/x25/states/R2_DTERestartingState.hpp>
#include <xot4cpp/x25/states/R3_DCERestartingState.hpp>

using namespace xot4cpp::x25::states;
using namespace xot4cpp::x25::transitions;

BOOST_AUTO_TEST_SUITE(xot4cpp_x25_codes_R_State_test_suite)

BOOST_AUTO_TEST_CASE(test_R1_PacketLayerReadyState) {

    // Data
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Data;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // Interrupt
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Interrupt;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // InterruptConfirmation
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::InterruptConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ReceiverReady
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ReceiverReady;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ReceiverNotReady
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ReceiverNotReady;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // Reject
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Reject;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // Diagnostic
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::Diagnostic;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }

    // ResetRequest
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ResetIndication
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetIndication;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ResetConfirmation
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ResetConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallRequest
    {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // CallIncoming
    {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallIncoming;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // CallAccepted
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallAccepted;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // CallConnected
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::CallConnected;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
    // ClearRequest
    {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ClearIndication
    {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearIndication;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // ClearConfirmation
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::ClearConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }

    // RestartRequest
    {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::R2_DTERestarting;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // RestartIndication
    {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::R3_DCERestarting;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // RestartConfirmation
    try {
        const State::SharedPointer state = R1_PacketLayerReadyState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::P1_Ready, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartConfirmation;
        state->handle(*stateContext.get(), transition);
        BOOST_FAIL("This should not happen");

    } catch (std::exception& e) {
        // ignore
    }
}

BOOST_AUTO_TEST_CASE(test_R2_DTERestarting) {

    // RestartRequest
    {
        const State::SharedPointer state = R2_DTERestartingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::R2_DTERestarting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartRequest;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // RestartIndication
    {
        const State::SharedPointer state = R2_DTERestartingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::R2_DTERestarting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartIndication;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P1_Ready;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // RestartConfirmation
    {
        const State::SharedPointer state = R2_DTERestartingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::R2_DTERestarting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartConfirmation;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P1_Ready;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
}

BOOST_AUTO_TEST_CASE(test_R3_DCERestartingState) {

    // RestartRequest
    {
        const State::SharedPointer state = R3_DCERestartingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::R3_DCERestarting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartRequest;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P1_Ready;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
    // RestartIndication
    {
        const State::SharedPointer state = R3_DCERestartingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::R3_DCERestarting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartIndication;
        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());
    }
    // RestartConfirmation
    {
        const State::SharedPointer state = R3_DCERestartingState::create();
        BOOST_CHECK_EQUAL(StateEnumerations::R3_DCERestarting, state->enumeration());

        const StateContext::SharedPointer stateContext = StateContext::create(State::SharedPointer());
        BOOST_CHECK_EQUAL(State::SharedPointer(), stateContext->state());

        const TransitionEnumerations::TransitionEnumeration transition = TransitionEnumerations::RestartConfirmation;
        const StateEnumerations::StateEnumeration stateEnumeration = StateEnumerations::P1_Ready;

        state->handle(*stateContext.get(), transition);
        BOOST_CHECK_NE(State::SharedPointer(), stateContext->state());
        BOOST_CHECK_EQUAL(stateEnumeration, stateContext->state()->enumeration());
    }
}

BOOST_AUTO_TEST_SUITE_END()
