#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/stream/Pipe.hpp>

#include <string>

using namespace xot4cpp;
using namespace xot4cpp::stream;

BOOST_AUTO_TEST_SUITE(xot4cpp_stream_Pipe_test_suite)

Octets octets(const std::string& p_string) {

    Octets octets;

    for (unsigned int index = 0; p_string.size() > index; ++index) {

        Octet octet = static_cast<Octet> (p_string[index]);
        octets.push_back(octet);
    }

    return octets;
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    const Pipe::SharedPointer pipe = Pipe::create();
    const InputStream::SharedPointer inputStream = Pipe::upCastAsInputStream(pipe);
    const OutputStream::SharedPointer outputStream = Pipe::upCastAsOutputStream(pipe);

    BOOST_CHECK_EQUAL(0, inputStream->available());
    BOOST_CHECK_EQUAL(-1, inputStream->read());

    {
        const Octets expected = octets("hello world!");

        outputStream->write(expected);
        BOOST_CHECK_EQUAL(12, inputStream->available());

        const Octets actual = inputStream->read(12);
        BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), actual.begin(), actual.end());

        BOOST_CHECK_EQUAL(0, inputStream->available());
        BOOST_CHECK_EQUAL(-1, inputStream->read());
    }

    {
        Octets expected;
        Octets actual;
        outputStream->write(octets("hello world!"));
        BOOST_CHECK_EQUAL(12, inputStream->available());

        expected = octets("hell");
        actual = inputStream->read(4);
        BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), actual.begin(), actual.end());

        BOOST_CHECK_EQUAL(8, inputStream->available());

        expected = octets("o wo");
        actual = inputStream->read(4);
        BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), actual.begin(), actual.end());

        BOOST_CHECK_EQUAL(4, inputStream->available());

        expected = octets("rld!");
        actual = inputStream->read(4);
        BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), actual.begin(), actual.end());

        BOOST_CHECK_EQUAL(0, inputStream->available());
        BOOST_CHECK_EQUAL(-1, inputStream->read());
    }

    {
        const Octets expected = octets("hello world!");
        Octets actual;

        outputStream->write(octets("he"));
        BOOST_CHECK_EQUAL(2, inputStream->available());

        outputStream->write(octets("llo "));
        BOOST_CHECK_EQUAL(6, inputStream->available());

        outputStream->write(octets("world!"));
        BOOST_CHECK_EQUAL(12, inputStream->available());

        actual = inputStream->read(12);
        BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), actual.begin(), actual.end());
    }
}

BOOST_AUTO_TEST_SUITE_END()
