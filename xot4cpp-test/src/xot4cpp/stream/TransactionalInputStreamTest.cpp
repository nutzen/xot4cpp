#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/stream/OctetInputStream.hpp>
#include <xot4cpp/stream/TransactionalInputStream.hpp>

using namespace xot4cpp;
using namespace xot4cpp::stream;

BOOST_AUTO_TEST_SUITE(xot4cpp_stream_TransactionalInputStream_test_suite)

Octets octets(const std::string& p_string) {

    Octets octets;

    for (unsigned int index = 0; p_string.size() > index; ++index) {
        octets.push_back(static_cast<Octet> (p_string[index]));
    }

    return octets;
}

BOOST_AUTO_TEST_CASE(test_Simple) {

    const InputStream::SharedPointer source = OctetInputStream::create(octets("hello world!"));
    const TransactionalInputStream::SharedPointer inputStream = TransactionalInputStream::create(source);
    BOOST_CHECK_EQUAL(12, inputStream->available());

    {
        const Octets expected = octets("hello ");
        const Octets actual = inputStream->read(6);
        BOOST_CHECK_EQUAL(6, actual.size());
        BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), actual.begin(), actual.end());
        BOOST_CHECK_EQUAL(6, inputStream->available());
    }

    inputStream->rollback();
    BOOST_CHECK_EQUAL(12, inputStream->available());
    BOOST_CHECK_EQUAL(0x68, inputStream->read());
    BOOST_CHECK_EQUAL(11, inputStream->available());

    inputStream->commit();
    {
        const Octets expected = octets("ello w");
        const Octets actual = inputStream->read(6);
        BOOST_CHECK_EQUAL(6, actual.size());
        BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), actual.begin(), actual.end());
        BOOST_CHECK_EQUAL(5, inputStream->available());
    }
}

BOOST_AUTO_TEST_SUITE_END()
