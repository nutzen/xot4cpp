#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/stream/OctetInputStream.hpp>
#include <xot4cpp/stream/OctetOutputStream.hpp>

using namespace xot4cpp;
using namespace xot4cpp::stream;

BOOST_AUTO_TEST_SUITE(xot4cpp_stream_OctetStream_test_suite)

BOOST_AUTO_TEST_CASE(test_Simple) {

    const Octet source0 = 0;

    Octets source1;
    {
        source1.push_back(0);
        source1.push_back(1);
        source1.push_back(2);
        source1.push_back(3);
    }

    Octets source2;
    {
        source2.push_back(0);
        source2.push_back(1);
        source2.push_back(2);
        source2.push_back(3);
        source2.push_back(4);
    }

    const OutputStream::SharedPointer outputStream = OctetOutputStream::create();
    outputStream->write(source0);
    outputStream->write(source1);
    outputStream->write(source2);

    const OctetOutputStream::SharedPointer octetOutputStream = OctetOutputStream::downCast(outputStream);
    const Octets intermediate = octetOutputStream->octets();

    const InputStream::SharedPointer inputStream = OctetInputStream::create(intermediate);
    const Octet result0 = inputStream->read();
    const Octets result1 = inputStream->read(source1.size());
    const Octets result2 = inputStream->read(source2.size());

    BOOST_CHECK_EQUAL(source0, result0);

    BOOST_CHECK_EQUAL(source1.size(), result1.size());
    for (unsigned int index = 0; source1.size() > index; ++index) {
        BOOST_CHECK_EQUAL(source1[index], result1[index]);
    }

    BOOST_CHECK_EQUAL(source2.size(), result2.size());
    for (unsigned int index = 0; source2.size() > index; ++index) {
        BOOST_CHECK_EQUAL(source2[index], result2[index]);
    }
}

BOOST_AUTO_TEST_SUITE_END()
