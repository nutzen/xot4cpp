#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x121/X121Address.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x121;

BOOST_AUTO_TEST_SUITE(xot4cpp_x121_X121Address_test_suite)

BOOST_AUTO_TEST_CASE(test_with6Digits) {

    const std::string source = "666666";
    const unsigned int size = 3;
    const Octet expected[size] = {0x66, 0x66, 0x66};
    X121Address::SharedPointer address = X121Address::create(source);

    BOOST_CHECK_EQUAL(source, address->address());
    BOOST_CHECK_EQUAL(size, address->addressAsOctets().size());

    for (unsigned int index = 0; size > index; ++index) {
        BOOST_CHECK_EQUAL(expected[index], address->addressAsOctets()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_with7Digits) {

    const std::string source = "1634567";
    const unsigned int size = 4;
    const Octet expected[size] = {0x16, 0x34, 0x56, 0x70};
    X121Address::SharedPointer address = X121Address::create(source);

    BOOST_CHECK_EQUAL(source, address->address());
    BOOST_CHECK_EQUAL(size, address->addressAsOctets().size());

    for (unsigned int index = 0; size > index; ++index) {
        BOOST_CHECK_EQUAL(expected[index], address->addressAsOctets()[index]);
    }
}

BOOST_AUTO_TEST_CASE(test_with8Digits) {

    const std::string source = "01634567";
    const unsigned int size = 4;
    const Octet expected[size] = {0x01, 0x63, 0x45, 0x67};
    X121Address::SharedPointer address = X121Address::create(source);

    BOOST_CHECK_EQUAL(source, address->address());
    BOOST_CHECK_EQUAL(size, address->addressAsOctets().size());

    for (unsigned int index = 0; size > index; ++index) {
        BOOST_CHECK_EQUAL(expected[index], address->addressAsOctets()[index]);
    }
}

BOOST_AUTO_TEST_SUITE_END()
