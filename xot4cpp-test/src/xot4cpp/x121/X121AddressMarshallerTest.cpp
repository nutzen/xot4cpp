#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x121/X121AddressMarshaller.hpp>

#include <xot4cpp/stream/OctetInputStream.hpp>
#include <xot4cpp/stream/OctetOutputStream.hpp>

using namespace xot4cpp;

BOOST_AUTO_TEST_SUITE(xot4cpp_x121_X121AddressMarshaller_test_suite)

Octets marshalAddressBlock(
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress,
        const bool p_aBit) {

    const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
    x121::X121AddressMarshaller::marshal(outputStream, p_calledAddress, p_callingAddress, p_aBit);

    return xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
}

BOOST_AUTO_TEST_CASE(test_marshalAddressBlock_0) {

    const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("123456");
    const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("123456");

    {
        const bool aBit = false;
        const unsigned int expectedSize = 7;
        const Octet expected[expectedSize] = {0x66, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56};

        const Octets actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

        BOOST_CHECK_EQUAL(expectedSize, actual.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], actual[index]);
        }
    }

    {
        const bool aBit = true;
        const unsigned int expectedSize = 7;
        const Octet expected[expectedSize] = {0x66, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56};
        const Octets actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

        BOOST_CHECK_EQUAL(expectedSize, actual.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], actual[index]);
        }
    }
}

BOOST_AUTO_TEST_CASE(test_marshalAddressBlock_1) {

    const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("123456");
    const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("1234567");

    {
        const bool aBit = false;
        const unsigned int expectedSize = 8;
        const Octet expected[expectedSize] = {0x76, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56, 0x70};

        const Octets actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

        BOOST_CHECK_EQUAL(expectedSize, actual.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], actual[index]);
        }
    }

    {
        const bool aBit = true;
        const unsigned int expectedSize = 8;
        const Octet expected[expectedSize] = {0x67, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56, 0x70};
        const Octets actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

        BOOST_CHECK_EQUAL(expectedSize, actual.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], actual[index]);
        }
    }
}

BOOST_AUTO_TEST_CASE(test_marshalAddressBlock_2) {

    const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("1234567");
    const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("123456");

    {
        const bool aBit = false;
        const unsigned int expectedSize = 8;
        const Octet expected[expectedSize] = {0x67, 0x12, 0x34, 0x56, 0x71, 0x23, 0x45, 0x60};

        const Octets actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

        BOOST_CHECK_EQUAL(expectedSize, actual.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], actual[index]);
        }
    }

    {
        const bool aBit = true;
        const unsigned int expectedSize = 8;
        const Octet expected[expectedSize] = {0x76, 0x12, 0x34, 0x56, 0x71, 0x23, 0x45, 0x60};
        const Octets actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

        BOOST_CHECK_EQUAL(expectedSize, actual.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], actual[index]);
        }
    }
}

BOOST_AUTO_TEST_CASE(test_marshalAddressBlock_3) {

    const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("1234567");
    const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("12345");

    {
        const bool aBit = false;
        const unsigned int expectedSize = 7;
        const Octet expected[expectedSize] = {0x57, 0x12, 0x34, 0x56, 0x71, 0x23, 0x45};

        const Octets actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

        BOOST_CHECK_EQUAL(expectedSize, actual.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], actual[index]);
        }
    }

    {
        const bool aBit = true;
        const unsigned int expectedSize = 7;
        const Octet expected[expectedSize] = {0x75, 0x12, 0x34, 0x56, 0x71, 0x23, 0x45};
        const Octets actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

        BOOST_CHECK_EQUAL(expectedSize, actual.size());
        for (unsigned int index = 0; expectedSize > index; ++index) {
            BOOST_CHECK_EQUAL(expected[index], actual[index]);
        }
    }
}

BOOST_AUTO_TEST_CASE(test_extractCallerId) {

    const Octet rawAddressBlock[9] = {0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00};
    Octets addressBlock(rawAddressBlock, rawAddressBlock + 9);

    // a-bit set
    {
        const bool aBit = true;
        const xot4cpp::stream::InputStream::SharedPointer inputStream = xot4cpp::stream::OctetInputStream::create(addressBlock);
        const x121::X121AddressPair::SharedPointer addressPair = x121::X121AddressMarshaller::unmarshal(inputStream, aBit);
        BOOST_CHECK_EQUAL(std::string("16345671"), addressPair->calledAddress()->address());
        BOOST_CHECK_EQUAL(std::string("1009000"), addressPair->callingAddress()->address());
    }
    // a-bit unset
    {
        const bool aBit = false;
        const xot4cpp::stream::InputStream::SharedPointer inputStream = xot4cpp::stream::OctetInputStream::create(addressBlock);
        const x121::X121AddressPair::SharedPointer addressPair = x121::X121AddressMarshaller::unmarshal(inputStream, aBit);
        BOOST_CHECK_EQUAL(std::string("1634567"), addressPair->calledAddress()->address());
        BOOST_CHECK_EQUAL(std::string("11009000"), addressPair->callingAddress()->address());
    }
}

BOOST_AUTO_TEST_SUITE_END()

