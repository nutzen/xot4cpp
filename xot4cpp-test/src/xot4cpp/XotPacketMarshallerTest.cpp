#include <boost/test/unit_test.hpp>

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/XotPacketMarshaller.hpp>

#include <xot4cpp/x25/facilities/PacketSizeSelection.hpp>
#include <xot4cpp/x25/facilities/WindowSizeSelection.hpp>

#include <xot4cpp/x25/payload/CallPacketPayload.hpp>
#include <xot4cpp/x25/payload/ClearConfirmationPayload.hpp>
#include <xot4cpp/x25/payload/ClearPacketPayload.hpp>

#include <xot4cpp/stream/OctetInputStream.hpp>
#include <xot4cpp/stream/OctetOutputStream.hpp>

using namespace xot4cpp;
using namespace xot4cpp::x25;

BOOST_AUTO_TEST_SUITE(xot4cpp_XotPacketMarshaller_test_suite)


BOOST_AUTO_TEST_CASE(test_CallPacket) {

    const unsigned int expectedIntermediateSize = 24;
    const Octet expectedIntermediate[expectedIntermediateSize] = {0x00, 0x00, 0x00, 0x14, 0x10, 0x01, 0x0b, 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, 0xc4};

    X25Header::SharedPointer header;
    {
        const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifier::create(
                gfi::GeneralFormatIdentifier::DataForUser,
                gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
                gfi::GeneralFormatIdentifier::Modulo8Sequencing);
        const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier =
                lci::LogicalChannelIdentifier::create(0, 1);
        const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
                pti::PacketTypeIdentifier::create(pti::PacketTypeIdentifierEnumerations::CallRequest_IncomingCall);
        header = X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    payload::CallPacketPayload::SharedPointer payload;
    {
        const x121::X121Address::SharedPointer calledAddress = x121::X121Address::create("1634567");
        const x121::X121Address::SharedPointer callingAddress = x121::X121Address::create("11009000");
        facilities::Facility::List facilities;
        facilities.push_back(facilities::WindowSizeSelection::create(1, 1));
        facilities.push_back(facilities::PacketSizeSelection::create(
                facilities::PacketSizeSelectionEnumerations::Size128octets,
                facilities::PacketSizeSelectionEnumerations::Size128octets));
        const Octets userData(1, (Octet) 0xc4);
        payload = payload::CallPacketPayload::downCast(
                payload::CallPacketPayload::create(calledAddress, callingAddress, facilities, userData));
    }

    const X25Packet::SharedPointer packet = X25Packet::create(header,
            payload::CallPacketPayload::upCast(payload));
    const XotPacket::SharedPointer source = XotPacket::create(packet);

    Octets intermediate;
    {
        const xot4cpp::stream::OutputStream::SharedPointer outputStream = xot4cpp::stream::OctetOutputStream::create();
        XotPacketMarshaller::marshal(outputStream, source);
        intermediate = xot4cpp::stream::OctetOutputStream::downCast(outputStream)->octets();
    }
    BOOST_CHECK_EQUAL(expectedIntermediateSize, intermediate.size());
    for (unsigned int index = 0; expectedIntermediateSize > index; ++index) {
        BOOST_CHECK_EQUAL(expectedIntermediate[index], intermediate[index]);
    }

    const XotPacket::SharedPointer result = XotPacketMarshaller::unmarshal(xot4cpp::stream::OctetInputStream::create(intermediate));
}


BOOST_AUTO_TEST_SUITE_END()
