#!/bin/bash

echo "-----------------------------------------------------------------------" &&
 echo "running tests - raw" &&
 dist/Debug/GNU-Linux-x86/xot4cpp-test &&
 echo "-----------------------------------------------------------------------" &&
 echo "running tests - with valgrind" &&
 valgrind -v --tool=memcheck --leak-check=yes dist/Debug/GNU-Linux-x86/xot4cpp-test &&
 echo "-----------------------------------------------------------------------" &&
 echo "running tests - with time" &&
 time dist/Debug/GNU-Linux-x86/xot4cpp-test
