#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_CONF=Debug
CND_DISTDIR=dist

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/xot4cpp/x25/X25PacketMarshallerTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/P_StateTest.o \
	${OBJECTDIR}/src/main.o \
	${OBJECTDIR}/src/xot4cpp/XotPacketMarshallerTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/utility/PtiToTransitionUtilityTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/codes/DiagnosticCodeEnumerationsTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/D_StateTest.o \
	${OBJECTDIR}/src/xot4cpp/stream/TransactionalInputStreamTest.o \
	${OBJECTDIR}/src/xot4cpp/stream/OctetStreamTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshallerTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/X25HeaderMarshallerTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/utility/HexUtilityTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/codes/CauseCodeEnumerationsTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerationsTest.o \
	${OBJECTDIR}/src/xot4cpp/exceptions/Xot4cppExceptionTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/StateEnumerationsTest.o \
	${OBJECTDIR}/src/xot4cpp/stream/PipeTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityMarshallerTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/X25PayloadMarshallerTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/R_StateTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierMarshallerTest.o \
	${OBJECTDIR}/src/xot4cpp/x121/X121AddressMarshallerTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionEnumerationsTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityEnumerationsTest.o \
	${OBJECTDIR}/src/xot4cpp/x121/X121AddressTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/context/RoleEnumerationsTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierTest.o \
	${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerationsTest.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lboost_unit_test_framework -Wl,-rpath ../dist/Debug/GNU-Linux-x86 -L../dist/Debug/GNU-Linux-x86 -lxot4cpp -lboost_system -lboost_thread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-Debug.mk dist/Debug/GNU-Linux-x86/xot4cpp-test

dist/Debug/GNU-Linux-x86/xot4cpp-test: ../dist/Debug/GNU-Linux-x86/libxot4cpp.so

dist/Debug/GNU-Linux-x86/xot4cpp-test: ${OBJECTFILES}
	${MKDIR} -p dist/Debug/GNU-Linux-x86
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/xot4cpp-test ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/src/xot4cpp/x25/X25PacketMarshallerTest.o: src/xot4cpp/x25/X25PacketMarshallerTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/X25PacketMarshallerTest.o src/xot4cpp/x25/X25PacketMarshallerTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/P_StateTest.o: src/xot4cpp/x25/states/P_StateTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/P_StateTest.o src/xot4cpp/x25/states/P_StateTest.cpp

${OBJECTDIR}/src/main.o: src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/main.o src/main.cpp

${OBJECTDIR}/src/xot4cpp/XotPacketMarshallerTest.o: src/xot4cpp/XotPacketMarshallerTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/XotPacketMarshallerTest.o src/xot4cpp/XotPacketMarshallerTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/utility/PtiToTransitionUtilityTest.o: src/xot4cpp/x25/utility/PtiToTransitionUtilityTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/utility
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/utility/PtiToTransitionUtilityTest.o src/xot4cpp/x25/utility/PtiToTransitionUtilityTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/codes/DiagnosticCodeEnumerationsTest.o: src/xot4cpp/x25/codes/DiagnosticCodeEnumerationsTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/codes
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/codes/DiagnosticCodeEnumerationsTest.o src/xot4cpp/x25/codes/DiagnosticCodeEnumerationsTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/D_StateTest.o: src/xot4cpp/x25/states/D_StateTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/D_StateTest.o src/xot4cpp/x25/states/D_StateTest.cpp

${OBJECTDIR}/src/xot4cpp/stream/TransactionalInputStreamTest.o: src/xot4cpp/stream/TransactionalInputStreamTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/TransactionalInputStreamTest.o src/xot4cpp/stream/TransactionalInputStreamTest.cpp

${OBJECTDIR}/src/xot4cpp/stream/OctetStreamTest.o: src/xot4cpp/stream/OctetStreamTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/OctetStreamTest.o src/xot4cpp/stream/OctetStreamTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshallerTest.o: src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshallerTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/gfi
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshallerTest.o src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshallerTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/X25HeaderMarshallerTest.o: src/xot4cpp/x25/X25HeaderMarshallerTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/X25HeaderMarshallerTest.o src/xot4cpp/x25/X25HeaderMarshallerTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/utility/HexUtilityTest.o: src/xot4cpp/x25/utility/HexUtilityTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/utility
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/utility/HexUtilityTest.o src/xot4cpp/x25/utility/HexUtilityTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/codes/CauseCodeEnumerationsTest.o: src/xot4cpp/x25/codes/CauseCodeEnumerationsTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/codes
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/codes/CauseCodeEnumerationsTest.o src/xot4cpp/x25/codes/CauseCodeEnumerationsTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerationsTest.o: src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerationsTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerationsTest.o src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerationsTest.cpp

${OBJECTDIR}/src/xot4cpp/exceptions/Xot4cppExceptionTest.o: src/xot4cpp/exceptions/Xot4cppExceptionTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/exceptions
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/exceptions/Xot4cppExceptionTest.o src/xot4cpp/exceptions/Xot4cppExceptionTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/StateEnumerationsTest.o: src/xot4cpp/x25/states/StateEnumerationsTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/StateEnumerationsTest.o src/xot4cpp/x25/states/StateEnumerationsTest.cpp

${OBJECTDIR}/src/xot4cpp/stream/PipeTest.o: src/xot4cpp/stream/PipeTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/PipeTest.o src/xot4cpp/stream/PipeTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityMarshallerTest.o: src/xot4cpp/x25/facilities/FacilityMarshallerTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityMarshallerTest.o src/xot4cpp/x25/facilities/FacilityMarshallerTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/X25PayloadMarshallerTest.o: src/xot4cpp/x25/payload/X25PayloadMarshallerTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/X25PayloadMarshallerTest.o src/xot4cpp/x25/payload/X25PayloadMarshallerTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/R_StateTest.o: src/xot4cpp/x25/states/R_StateTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/R_StateTest.o src/xot4cpp/x25/states/R_StateTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierMarshallerTest.o: src/xot4cpp/x25/pti/PacketTypeIdentifierMarshallerTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/pti
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierMarshallerTest.o src/xot4cpp/x25/pti/PacketTypeIdentifierMarshallerTest.cpp

${OBJECTDIR}/src/xot4cpp/x121/X121AddressMarshallerTest.o: src/xot4cpp/x121/X121AddressMarshallerTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x121
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x121/X121AddressMarshallerTest.o src/xot4cpp/x121/X121AddressMarshallerTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionEnumerationsTest.o: src/xot4cpp/x25/transitions/TransitionEnumerationsTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionEnumerationsTest.o src/xot4cpp/x25/transitions/TransitionEnumerationsTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityEnumerationsTest.o: src/xot4cpp/x25/facilities/FacilityEnumerationsTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityEnumerationsTest.o src/xot4cpp/x25/facilities/FacilityEnumerationsTest.cpp

${OBJECTDIR}/src/xot4cpp/x121/X121AddressTest.o: src/xot4cpp/x121/X121AddressTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x121
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x121/X121AddressTest.o src/xot4cpp/x121/X121AddressTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/context/RoleEnumerationsTest.o: src/xot4cpp/x25/context/RoleEnumerationsTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/context
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/context/RoleEnumerationsTest.o src/xot4cpp/x25/context/RoleEnumerationsTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierTest.o: src/xot4cpp/x25/pti/PacketTypeIdentifierTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/pti
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierTest.o src/xot4cpp/x25/pti/PacketTypeIdentifierTest.cpp

${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerationsTest.o: src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerationsTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/pti
	${RM} $@.d
	$(COMPILE.cc) -g -I../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerationsTest.o src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerationsTest.cpp

# Subprojects
.build-subprojects:
	cd .. && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Debug
	${RM} dist/Debug/GNU-Linux-x86/xot4cpp-test

# Subprojects
.clean-subprojects:
	cd .. && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
