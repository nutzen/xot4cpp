#ifndef _XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIER_HPP_
#define	_XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "PacketTypeIdentifierEnumerations.hpp"
#include "../../TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace pti {

            /**
             *                .=================================.
             *                ||                    ||         ||
             *    LCN <------ ||  M  M  M  M  M  M  || MX  X0  ||---> PACKET
             *                ||                    ||         ||     CONTENT
             *                `================================='
             *                |<- - - - - - - PTI - - - - - - ->|
             *
             *     M = MODIFIER BIT   MX = MODIFIER/OTHER BIT   X0 = 0 OR 1 BIT
             *
             *      (MX,X0) SETTING                   PACKET TYPES
             *       ---------------    ---------------------------------------
             *         (1,1)            All CALLING/CLEARING, INTERRUPT, RESET,
             *                             RESTART, and REGISTRATION packets
             *         (0,1)            RECEIVER READY, RECEIVER NOT READY,
             *                             REJECT, DIAGNOSTIC packets
             *         (X,0)            DATA packets
             */
            class PacketTypeIdentifier {
            public:
                typedef boost::shared_ptr<PacketTypeIdentifier> SharedPointer;

            protected:
                PacketTypeIdentifier(const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration);

            public:
                virtual ~PacketTypeIdentifier(void);

                static SharedPointer create(
                        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration);
                static SharedPointer create(
                        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
                        const unsigned int& p_packetReceiveSequenceNumber);
                static SharedPointer create(
                        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
                        const bool& p_moreData,
                        const unsigned int& p_packetReceiveSequenceNumber,
                        const unsigned int& p_packetSendSequenceNumber);

                const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& enumeration(void) const;
                virtual bool isDataPacketTypeIdentifier(void) const;
                virtual bool isReceiverPacketTypeIdentifier(void) const;

            private:
                PacketTypeIdentifier(void);
                PacketTypeIdentifier(const PacketTypeIdentifier&);
                PacketTypeIdentifier & operator =(const PacketTypeIdentifier&);

                const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration m_enumeration;

            };
        };
    };
};

#include "DataPacketTypeIdentifier.hpp"
#include "ReceiverPacketTypeIdentifier.hpp"

#endif	/* _XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIER_HPP_ */
