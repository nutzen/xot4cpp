#ifndef _XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIERENUMERATIONUTILITY_HPP_
#define	_XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIERENUMERATIONUTILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../../TypeDefinitions.hpp"

#include <string>

namespace xot4cpp {

    namespace x25 {

        namespace pti {

            namespace PacketTypeIdentifierEnumerations {

                /**
                 *
                 * .========================================================================.
                 * || Packet type                          || Binary code      || Hex code ||
                 * || DTE to DCE    ( / DCE to DTE )       || 8 7 6 5  4 3 2 1 ||  0xXX    ||
                 * |========================================================================|
                 * || Call set-up and clearing - - - - - - || - - - -  - - - - ||  - --    ||
                 * || Call Request    / Incoming Call      || 0 0 0 0  1 0 1 1 ||  0x0b    ||
                 * || Call Accepted   / Call Connected     || 0 0 0 0  1 1 1 1 ||  0x0f    ||
                 * || Clear Request   / Clear Indication   || 0 0 0 1  0 0 1 1 ||  0x13    ||
                 * || Clear Confirmation                   || 0 0 0 1  0 1 1 1 ||  0x17    ||
                 * || Data and interrupt - - - - - - - - - || - - - -  - - - - ||  - --    ||
                 * || Data                                 || X X X X  X X X 0 ||  0xX0    ||
                 * || Interrupt                            || 0 0 1 0  0 0 1 1 ||  0x23    ||
                 * || Interrupt Confirmation               || 0 0 1 0  0 1 1 1 ||  0x27    ||
                 * || Flow control and reset - - - - - - - || - - - -  - - - - ||  - --    ||
                 * || Receiver Ready                       || X X X 0  0 0 0 1 ||  0xX1    ||
                 * || Receiver Not Ready                   || X X X 0  0 1 0 1 ||  0xX5    ||
                 * || Reject                               || X X X 0  1 0 0 1 ||  0xX9    ||
                 * || Reset Request   / Reset Indication   || 0 0 0 1  1 0 1 1 ||  0x1b    ||
                 * || Reset Confirmation                   || 0 0 0 1  1 1 1 1 ||  0x1f    ||
                 * || Restart  - - - - - - - - - - - - - - || - - - -  - - - - ||  - --    ||
                 * || Restart Request / Restart Indication || 1 1 1 1  1 0 1 1 ||  0xfb    ||
                 * || Restart Confirmation                 || 1 1 1 1  1 1 1 1 ||  0xff    ||
                 * || Diagnostic - - - - - - - - - - - - - || - - - -  - - - - ||  - --    ||
                 * || Diagnostic                           || 1 1 1 1  0 0 0 1 ||  0xf1    ||
                 * `========================================================================'
                 *
                 */
                enum PacketTypeIdentifierEnumeration {
                    CallRequest_IncomingCall,
                    CallAccepted_CallConnected,
                    ClearRequest_ClearIndication,
                    ClearConfirmation,
                    Data,
                    Interrupt,
                    InterruptConfirmation,
                    ReceiverReady,
                    ReceiverNotReady,
                    Reject,
                    ResetRequest_ResetIndication,
                    ResetConfirmation,
                    RestartRequest_RestartIndication,
                    RestartConfirmation,
                    Diagnostic
                };

                extern std::string toDescription(const PacketTypeIdentifierEnumeration& p_source);
                extern PacketTypeIdentifierEnumeration fromDescription(const std::string& p_source);

                extern int toInt(const PacketTypeIdentifierEnumeration& p_source);
                extern PacketTypeIdentifierEnumeration fromInt(const int& p_source);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIERENUMERATIONUTILITY_HPP_ */
