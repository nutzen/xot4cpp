#ifndef _XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIERMARSHALLER_HPP_
#define	_XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIERMARSHALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "PacketTypeIdentifier.hpp"
#include "../../TypeDefinitions.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace pti {

            /**
             * @todo add module 128 support
             */
            namespace PacketTypeIdentifierMarshaller {

                extern Octet marshalModule8(const PacketTypeIdentifier::SharedPointer& p_source);

                extern PacketTypeIdentifier::SharedPointer unmarshalModule8(const Octet& p_source);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PTI_PACKETTYPEIDENTIFIERMARSHALLER_HPP_ */
