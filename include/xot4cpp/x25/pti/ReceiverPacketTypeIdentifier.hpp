#ifndef _XOT4CPP_X25_PTI_RECEIVERPACKETTYPEIDENTIFIER_HPP_
#define	_XOT4CPP_X25_PTI_RECEIVERPACKETTYPEIDENTIFIER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "PacketTypeIdentifier.hpp"
#include "../../TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace pti {

            /**
             *
             * MODULO 8 :
             *
             *        .====================================================.
             *  O  1  ||  Q    D  ||   X   X   ||        L C G N          || R
             *  C     |====================================================| E
             *  T  1  ||                    L  C  N                       || C
             *  E     |====================================================| E
             *  T  1  ||   Pr3   Pr2   Pr1   0     Pt4   Pt3   Pt2   Pt1  || I
             *  S     `====================================================' V
             *
             * MODULO 128 :
             *
             *        .====================================================.
             *  O  1  ||  Q    D  ||   X   X   ||        L C G N          || R
             *  C     |====================================================| E
             *  T  1  ||                    L  C  N                       || C
             *  E     |====================================================| E
             *  T  1  ||   0     0     0     0     Pt4   Pt3   Pt2   Pt1  || I
             *  S     |====================================================| V
             *     1  ||   Pr7   Pr6   Pr5   Pr4   Pr3   Pr2   Pr1   M    || E
             *        `===================================================='
             *
             * P(R)     Packet receive sequence number which appears in data
             *          and flow control packets or the called DTE address which
             *          may appear in call setup, clearing and registration
             *          packets.
             *
             * P(T)     Packet type.
             *
             * M        More data bit which appears only in data packets. The field
             *          is set to 1 to indicate that the packet is part of a
             *          sequence of packets that should be treated as a logical
             *          whole.
             */
            class ReceiverPacketTypeIdentifier : public PacketTypeIdentifier {
            public:
                typedef boost::shared_ptr<ReceiverPacketTypeIdentifier> SharedPointer;

                static SharedPointer downCast(const PacketTypeIdentifier::SharedPointer& p_pointer);
                static PacketTypeIdentifier::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                ReceiverPacketTypeIdentifier(
                        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
                        const unsigned int& p_packetReceiveSequenceNumber);

            public:
                virtual ~ReceiverPacketTypeIdentifier(void);

                static SharedPointer create(
                        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
                        const unsigned int& p_packetReceiveSequenceNumber);

                virtual bool isReceiverPacketTypeIdentifier(void) const;
                const unsigned int& packetReceiveSequenceNumber(void) const;

            private:
                ReceiverPacketTypeIdentifier(void);
                ReceiverPacketTypeIdentifier(const ReceiverPacketTypeIdentifier&);
                ReceiverPacketTypeIdentifier & operator =(const ReceiverPacketTypeIdentifier&);

                const unsigned int m_packetReceiveSequenceNumber;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PTI_RECEIVERPACKETTYPEIDENTIFIER_HPP_ */
