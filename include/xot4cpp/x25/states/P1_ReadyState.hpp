#ifndef _XOT4CPP_X25_STATES_P1_READYSTATE_HPP_
#define	_XOT4CPP_X25_STATES_P1_READYSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class P1_ReadyState : public State {
            private:
                P1_ReadyState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~P1_ReadyState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                P1_ReadyState(void);
                P1_ReadyState(const P1_ReadyState&);
                P1_ReadyState & operator =(const P1_ReadyState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_P1_READYSTATE_HPP_ */
