#ifndef _XOT4CPP_X25_STATES_D1_FLOWCONTROLREADYSTATE_HPP_
#define	_XOT4CPP_X25_STATES_D1_FLOWCONTROLREADYSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class D1_FlowControlReadyState : public State {
            private:
                D1_FlowControlReadyState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~D1_FlowControlReadyState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                D1_FlowControlReadyState(void);
                D1_FlowControlReadyState(const D1_FlowControlReadyState&);
                D1_FlowControlReadyState & operator =(const D1_FlowControlReadyState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_D1_FLOWCONTROLREADYSTATE_HPP_ */
