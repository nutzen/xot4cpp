#ifndef _XOT4CPP_X25_STATES_R3_DCERESTARTINGSTATE_HPP_
#define	_XOT4CPP_X25_STATES_R3_DCERESTARTINGSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class R3_DCERestartingState : public State {
            private:
                R3_DCERestartingState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~R3_DCERestartingState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                R3_DCERestartingState(void);
                R3_DCERestartingState(const R3_DCERestartingState&);
                R3_DCERestartingState & operator =(const R3_DCERestartingState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_R3_DCERESTARTINGSTATE_HPP_ */
