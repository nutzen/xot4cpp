#ifndef _XOT4CPP_X25_STATES_D2_DTERESETINGSTATE_HPP_
#define	_XOT4CPP_X25_STATES_D2_DTERESETINGSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class D2_DTEResetingState : public State {
            private:
                D2_DTEResetingState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~D2_DTEResetingState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                D2_DTEResetingState(void);
                D2_DTEResetingState(const D2_DTEResetingState&);
                D2_DTEResetingState & operator =(const D2_DTEResetingState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_D2_DTERESETINGSTATE_HPP_ */
