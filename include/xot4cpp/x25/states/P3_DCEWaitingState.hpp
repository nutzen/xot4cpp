#ifndef _XOT4CPP_X25_STATES_P3_DCEWAITINGSTATE_HPP_
#define	_XOT4CPP_X25_STATES_P3_DCEWAITINGSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class P3_DCEWaitingState : public State {
            private:
                P3_DCEWaitingState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~P3_DCEWaitingState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                P3_DCEWaitingState(void);
                P3_DCEWaitingState(const P3_DCEWaitingState&);
                P3_DCEWaitingState & operator =(const P3_DCEWaitingState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_P3_DCEWAITINGSTATE_HPP_ */
