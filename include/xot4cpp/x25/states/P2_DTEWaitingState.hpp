#ifndef _XOT4CPP_X25_STATES_P2_DTEWAITINGSTATE_HPP_
#define	_XOT4CPP_X25_STATES_P2_DTEWAITINGSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class P2_DTEWaitingState : public State {
            private:
                P2_DTEWaitingState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~P2_DTEWaitingState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                P2_DTEWaitingState(void);
                P2_DTEWaitingState(const P2_DTEWaitingState&);
                P2_DTEWaitingState & operator =(const P2_DTEWaitingState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_P2_DTEWAITINGSTATE_HPP_ */
