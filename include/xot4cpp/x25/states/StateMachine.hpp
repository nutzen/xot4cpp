#ifndef _XOT4CPP_X25_STATES_STATEMACHINE_HPP_
#define	_XOT4CPP_X25_STATES_STATEMACHINE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class StateMachine {
            public:
                typedef boost::shared_ptr<StateMachine> SharedPointer;
                typedef boost::recursive_mutex Mutex;
                typedef boost::lock_guard<Mutex> MutexGuard;

            private:
                StateMachine(const StateContext::SharedPointer& p_stateContext);

            public:
                virtual ~StateMachine(void);

                static SharedPointer create(void);

                virtual const StateEnumerations::StateEnumeration& stateEnumeration(void) const;
                virtual void handle(const transitions::TransitionEnumerations::TransitionEnumeration& p_transitionEnumeration);

            private:
                StateMachine(void);
                StateMachine(const StateMachine&);
                StateMachine & operator =(const StateMachine&);

                Mutex m_mutex;
                const StateContext::SharedPointer m_stateContext;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_STATEMACHINE_HPP_ */
