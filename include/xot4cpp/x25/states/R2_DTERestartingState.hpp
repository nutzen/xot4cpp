#ifndef _XOT4CPP_X25_STATES_R2_DTERESTARTINGSTATE_HPP_
#define	_XOT4CPP_X25_STATES_R2_DTERESTARTINGSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class R2_DTERestartingState : public State {
            private:
                R2_DTERestartingState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~R2_DTERestartingState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                R2_DTERestartingState(void);
                R2_DTERestartingState(const R2_DTERestartingState&);
                R2_DTERestartingState & operator =(const R2_DTERestartingState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_R2_DTERESTARTINGSTATE_HPP_ */
