#ifndef _XOT4CPP_X25_STATES_STATEENUMERATIONS_HPP_
#define	_XOT4CPP_X25_STATES_STATEENUMERATIONS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <string>

namespace xot4cpp {

    namespace x25 {

        namespace states {

            namespace StateEnumerations {

                enum StateEnumeration {
                    // P-states
                    P1_Ready,
                    P2_DTEWaiting,
                    P3_DCEWaiting,
                    P4_DataTransfer,
                    P5_CallCollision,
                    P6_DTEClearing,
                    P7_DCEClearing,
                    // D-states
                    D1_FlowControlReady,
                    D2_DTEReseting,
                    D3_DCEReseting,
                    // R-states
                    R1_PacketLayerReady,
                    R2_DTERestarting,
                    R3_DCERestarting
                };

                extern std::string toDescription(const StateEnumeration& p_source);
                extern StateEnumeration fromDescription(const std::string& p_source);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_STATEENUMERATIONS_HPP_ */
