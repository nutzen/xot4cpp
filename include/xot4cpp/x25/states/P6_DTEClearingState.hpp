#ifndef _XOT4CPP_X25_STATES_P6_DTECLEARINGSTATE_HPP_
#define	_XOT4CPP_X25_STATES_P6_DTECLEARINGSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class P6_DTEClearingState : public State {
            private:
                P6_DTEClearingState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~P6_DTEClearingState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                P6_DTEClearingState(void);
                P6_DTEClearingState(const P6_DTEClearingState&);
                P6_DTEClearingState & operator =(const P6_DTEClearingState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_P6_DTECLEARINGSTATE_HPP_ */
