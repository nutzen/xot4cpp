#ifndef _XOT4CPP_X25_STATES_P7_DCECLEARINGSTATE_HPP_
#define	_XOT4CPP_X25_STATES_P7_DCECLEARINGSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class P7_DCEClearingState : public State {
            private:
                P7_DCEClearingState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~P7_DCEClearingState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                P7_DCEClearingState(void);
                P7_DCEClearingState(const P7_DCEClearingState&);
                P7_DCEClearingState & operator =(const P7_DCEClearingState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_P7_DCECLEARINGSTATE_HPP_ */
