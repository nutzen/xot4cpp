#ifndef _XOT4CPP_X25_STATES_P5_CALLCOLLISIONSTATE_HPP_
#define	_XOT4CPP_X25_STATES_P5_CALLCOLLISIONSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class P5_CallCollisionState : public State {
            private:
                P5_CallCollisionState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~P5_CallCollisionState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                P5_CallCollisionState(void);
                P5_CallCollisionState(const P5_CallCollisionState&);
                P5_CallCollisionState & operator =(const P5_CallCollisionState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_P5_CALLCOLLISIONSTATE_HPP_ */
