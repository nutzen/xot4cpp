#ifndef _XOT4CPP_X25_STATES_P4_DATATRANSFERSTATE_HPP_
#define	_XOT4CPP_X25_STATES_P4_DATATRANSFERSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"
#include "StateContext.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class P4_DataTransferState : public State, StateContext {
            private:
                P4_DataTransferState(const StateEnumerations::StateEnumeration& p_enumeration,
                        const State::SharedPointer& p_state);

            public:
                virtual ~P4_DataTransferState(void);

                static State::SharedPointer create(void);

                virtual const StateEnumerations::StateEnumeration& enumeration(void) const;
                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                P4_DataTransferState(void);
                P4_DataTransferState(const P4_DataTransferState&);
                P4_DataTransferState & operator =(const P4_DataTransferState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_P4_DATATRANSFERSTATE_HPP_ */
