#ifndef _XOT4CPP_X25_STATES_R1_PACKETLAYERREADYSTATE_HPP_
#define	_XOT4CPP_X25_STATES_R1_PACKETLAYERREADYSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"
#include "StateContext.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class R1_PacketLayerReadyState : public State, StateContext {
            private:
                R1_PacketLayerReadyState(const StateEnumerations::StateEnumeration& p_enumeration,
                        const State::SharedPointer& p_state);

            public:
                virtual ~R1_PacketLayerReadyState(void);

                static State::SharedPointer create(void);

                virtual const StateEnumerations::StateEnumeration& enumeration(void) const;
                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                R1_PacketLayerReadyState(void);
                R1_PacketLayerReadyState(const R1_PacketLayerReadyState&);
                R1_PacketLayerReadyState & operator =(const R1_PacketLayerReadyState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_R1_PACKETLAYERREADYSTATE_HPP_ */
