#ifndef _XOT4CPP_X25_STATES_STATECONTEXT_HPP_
#define	_XOT4CPP_X25_STATES_STATECONTEXT_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class StateContext {
            public:
                typedef boost::shared_ptr<StateContext> SharedPointer;

            protected:
                StateContext(const State::SharedPointer& p_state);

            public:
                virtual ~StateContext(void);

                static SharedPointer create(const State::SharedPointer& p_state);

                const State::SharedPointer& state(void) const;
                virtual void setState(const State::SharedPointer& p_state);

            private:
                StateContext(void);
                StateContext(const StateContext&);
                StateContext & operator =(const StateContext&);

                State::SharedPointer m_state;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_STATECONTEXT_HPP_ */
