#ifndef _XOT4CPP_X25_STATES_D3_DCERESETINGSTATE_HPP_
#define	_XOT4CPP_X25_STATES_D3_DCERESETINGSTATE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "State.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace states {

            class D3_DCEResetingState : public State {
            private:
                D3_DCEResetingState(const StateEnumerations::StateEnumeration& p_enumeration);

            public:
                virtual ~D3_DCEResetingState(void);

                static State::SharedPointer create(void);

                virtual void handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition);

            private:
                D3_DCEResetingState(void);
                D3_DCEResetingState(const D3_DCEResetingState&);
                D3_DCEResetingState & operator =(const D3_DCEResetingState&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_D3_DCERESETINGSTATE_HPP_ */
