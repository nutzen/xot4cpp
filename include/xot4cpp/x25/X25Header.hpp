#ifndef _XOT4CPP_X25_X25HEADER_HPP_
#define	_XOT4CPP_X25_X25HEADER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "gfi/GeneralFormatIdentifier.hpp"
#include "lci/LogicalChannelIdentifier.hpp"
#include "pti/PacketTypeIdentifier.hpp"
#include "../TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        /**
         *                              4   4    8    8     NUMBER OF BITS
         *                          .===================.
         *                          || G | L || L || P ||
         *                          || F | C || C || T ||
         *                          || I | G || N || I ||
         *                          ||   | N ||   ||   ||
         *                          `==================='
         *                             ^   ^    ^    ^
         *                             |   |    |    |
         * GENERAL FORMAT IDENTIFIER---'   |    |    `-- PACKET TYPE
         *                                 |    |        IDENTIFIER
         * LOGICAL CHANNEL GROUP NUMBER----'    |
         *                                      |
         * LOGICAL CHANNEL NUMBER---------------'
         */
        class X25Header {
        public:
            typedef boost::shared_ptr<X25Header> SharedPointer;

        private:
            X25Header(
                    const gfi::GeneralFormatIdentifier::SharedPointer& p_generalFormatIdentifier,
                    const lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
                    const pti::PacketTypeIdentifier::SharedPointer& p_packetTypeIdentifier);

        public:
            static SharedPointer create(
                    const gfi::GeneralFormatIdentifier::SharedPointer& p_generalFormatIdentifier,
                    const lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
                    const pti::PacketTypeIdentifier::SharedPointer& p_packetTypeIdentifier);

            virtual ~X25Header(void);

            const gfi::GeneralFormatIdentifier::SharedPointer& generalFormatIdentifier(void) const;
            const lci::LogicalChannelIdentifier::SharedPointer& logicalChannelIdentifier(void) const;
            const pti::PacketTypeIdentifier::SharedPointer& packetTypeIdentifier(void) const;

        private:
            X25Header(void);
            X25Header(const X25Header&);
            X25Header & operator =(const X25Header&);

            const gfi::GeneralFormatIdentifier::SharedPointer m_generalFormatIdentifier;
            const lci::LogicalChannelIdentifier::SharedPointer m_logicalChannelIdentifier;
            const pti::PacketTypeIdentifier::SharedPointer m_packetTypeIdentifier;
        };
    };
};

#endif	/* _XOT4CPP_X25_X25HEADER_HPP_ */
