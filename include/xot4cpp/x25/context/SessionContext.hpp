#ifndef _XOT4CPP_X25_CONTEXT_SESSIONCONTEXT_HPP_
#define	_XOT4CPP_X25_CONTEXT_SESSIONCONTEXT_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../../x121/X121Address.hpp"
#include "../codes/CauseCodeEnumerations.hpp"
#include "../codes/DiagnosticCodeEnumerations.hpp"
#include "../facilities/Facility.hpp"
#include "../lci/LogicalChannelIdentifier.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace context {

            class SessionContext {
            public:
                typedef boost::shared_ptr<SessionContext> SharedPointer;

            private:
                SessionContext(
                        const x121::X121Address::SharedPointer& p_localAddress,
                        const facilities::Facility::List& p_facilities,
                        const x121::X121Address::SharedPointer& p_remoteAddress,
                        const lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier);

            public:
                virtual ~SessionContext(void);

                static SharedPointer create(
                        const x121::X121Address::SharedPointer& p_localAddress,
                        const facilities::Facility::List& p_facilities,
                        const x121::X121Address::SharedPointer& p_remoteAddress = x121::X121Address::SharedPointer(),
                        const lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier = lci::LogicalChannelIdentifier::create(0x0f, 0xff));

                const x121::X121Address::SharedPointer& localAddress(void) const;
                const facilities::Facility::List& facilities(void) const;

                x121::X121Address::SharedPointer& remoteAddress(void);
                lci::LogicalChannelIdentifier::SharedPointer& logicalChannelIdentifier(void);
                unsigned int& packetReceiveSequenceNumber(void);
                unsigned int& packetSendSequenceNumber(void);
                codes::CauseCodeEnumerations::ClearRequestEnumeration& cause(void);
                codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& diagnostic(void);

            private:
                SessionContext(void);
                SessionContext(const SessionContext&);
                SessionContext & operator =(const SessionContext&);

                const x121::X121Address::SharedPointer m_localAddress;
                const facilities::Facility::List m_facilities;

                x121::X121Address::SharedPointer m_remoteAddress;
                lci::LogicalChannelIdentifier::SharedPointer m_logicalChannelIdentifier;
                unsigned int m_packetReceiveSequenceNumber;
                unsigned int m_packetSendSequenceNumber;
                codes::CauseCodeEnumerations::ClearRequestEnumeration m_cause;
                codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration m_diagnostic;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_CONTEXT_SESSIONCONTEXT_HPP_ */
