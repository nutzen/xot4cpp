#ifndef _XOT4CPP_X25_CONTEXT_SYSTEMCONTEXT_HPP_
#define	_XOT4CPP_X25_CONTEXT_SYSTEMCONTEXT_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../../x121/X121Address.hpp"
#include "../facilities/Facility.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace context {

            class SystemContext {
            public:
                typedef boost::shared_ptr<SystemContext> SharedPointer;

            private:
                SystemContext(
                        const x121::X121Address::SharedPointer& p_localAddress,
                        const facilities::Facility::List& p_facilities,
                        const unsigned short& p_lciSeed);

            public:
                virtual ~SystemContext(void);

                static SharedPointer create(
                        const x121::X121Address::SharedPointer& p_localAddress,
                        const facilities::Facility::List& p_facilities = defaultFacilities());

                const x121::X121Address::SharedPointer& localAddress(void) const;
                const facilities::Facility::List& facilities(void) const;
                const unsigned short nextLci(void);

            protected:
                static facilities::Facility::List defaultFacilities(void);

            private:
                SystemContext(void);
                SystemContext(const SystemContext&);
                SystemContext & operator =(const SystemContext&);

                const x121::X121Address::SharedPointer m_localAddress;
                const facilities::Facility::List m_facilities;
                volatile unsigned short m_lciSeed;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_CONTEXT_SYSTEMCONTEXT_HPP_ */
