#ifndef _XOT4CPP_X25_CONTEXT_ROLEENUMERATIONS_HPP_
#define	_XOT4CPP_X25_CONTEXT_ROLEENUMERATIONS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace context {

            namespace RoleEnumerations {

                enum RoleEnumeration {
                    DTE,
                    DCE
                };

                extern std::string toDescription(const RoleEnumeration& p_source);
                extern RoleEnumeration fromDescription(const std::string& p_source);
            }
        }
    };
};

#endif	/* _XOT4CPP_X25_CONTEXT_ROLEENUMERATIONS_HPP_ */
