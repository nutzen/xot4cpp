#ifndef _XOT4CPP_X25_CODES_CAUSECODEENUMERATIONS_HPP_
#define	_XOT4CPP_X25_CODES_CAUSECODEENUMERATIONS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <string>

namespace xot4cpp {

    namespace x25 {

        namespace codes {

            namespace CauseCodeEnumerations {

                enum ClearRequestEnumeration {
                    ClearRequestDteOriginated,
                    ClearRequestNumberBusy,
                    ClearRequestInvalidFacilityRequest,
                    ClearRequestNetworkCongestion,
                    ClearRequestOutOfOrder,
                    ClearRequestAccessBarred,
                    ClearRequestNotObtainable,
                    ClearRequestRemoteProcedureError,
                    ClearRequestLocalProcedureError,
                    ClearRequestRpoaOutOfOrder,
                    ClearRequestReverseChargingNotAccepted,
                    ClearRequestIncompatibleDestination,
                    ClearRequestFastSelectNotAccepted,
                    ClearRequestShipAbsent
                };

                extern std::string toDescription(const ClearRequestEnumeration& p_source);
                extern ClearRequestEnumeration clearRequestFromDescription(const std::string& p_source);

                extern int toInt(const ClearRequestEnumeration& p_source);
                extern ClearRequestEnumeration clearRequestFromInt(const int& p_source);

                enum ResetRequestEnumeration {
                    ResetRequestDteOriginated,
                    ResetRequestOutOfOrder,
                    ResetRequestRemoteProcedureError,
                    ResetRequestLocalProcedureError,
                    ResetRequestNetworkCongestion,
                    ResetRequestRemoteDteOperational,
                    ResetRequestNetworkOperational,
                    ResetRequestIncompatibleDestination,
                    ResetRequestNetworkOutOfOrder
                };

                extern std::string toDescription(const ResetRequestEnumeration& p_source);
                extern ResetRequestEnumeration resetRequestFromDescription(const std::string& p_source);

                extern int toInt(const ResetRequestEnumeration& p_source);
                extern ResetRequestEnumeration resetRequestFromInt(const int& p_source);

                enum RestartRequestEnumeration {
                    RestartRequestDteRestarting,
                    RestartRequestLocalProcedureError,
                    RestartRequestNetworkCongestion,
                    RestartRequestNetworkOperational,
                    RestartRequestRegistrationCancellationConfirmed
                };

                extern std::string toDescription(const RestartRequestEnumeration& p_source);
                extern RestartRequestEnumeration restartRequestFromDescription(const std::string& p_source);

                extern int toInt(const RestartRequestEnumeration& p_source);
                extern RestartRequestEnumeration restartRequestFromInt(const int& p_source);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_CODES_CAUSECODEENUMERATIONS_HPP_ */
