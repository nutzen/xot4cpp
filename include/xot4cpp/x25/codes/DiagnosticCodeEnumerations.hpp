#ifndef _XOT4CPP_X25_CODES_DIAGNOSTICCODEENUMERATIONS_HPP_
#define	_XOT4CPP_X25_CODES_DIAGNOSTICCODEENUMERATIONS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <string>

namespace xot4cpp {

    namespace x25 {

        namespace codes {

            namespace DiagnosticCodeEnumerations {

                enum DiagnosticCodeEnumeration {
                    NoAdditionalInformation,
                    InvalidPacketSendSequenceNumber,
                    InvalidPacketReceiveSequenceNumber,
                    PacketTypeInvalid,
                    PacketTypeInvalidForStateR1PacketLevelReady,
                    PacketTypeInvalidForStateR2DteRestartRequest,
                    PacketTypeInvalidForStateR3DceRestartIndication,
                    PacketTypeInvalidForStateP1Ready,
                    PacketTypeInvalidForStateP2DteWaiting,
                    PacketTypeInvalidForStateP3DceWaiting,
                    PacketTypeInvalidForStateP4DataTransfer,
                    PacketTypeInvalidForStateP5CallCollision,
                    PacketTypeInvalidForStateP6DteClearRequest,
                    PacketTypeInvalidForStateP7DceClearIndication,
                    PacketTypeInvalidForStateD1FlowControlReady,
                    PacketTypeInvalidForStateD2DteResetReady,
                    PacketTypeInvalidForStateD3DceResetIndication,
                    PacketNotAllowed,
                    UnidentifiablePacket,
                    CallOnOneWayLogicalChannel,
                    InvalidPacketTypeOnAPermanentVirtualCircuit,
                    PacketOnUnassignedLogicalChannelNumber,
                    RejectNotSubscribedTo,
                    PacketTooShort,
                    PacketTooLong,
                    InvalidGeneralFormatIdentifier,
                    RestartOrRegistrationPacketWithNonzeroLci,
                    PacketTypeNotCompatibleWithFacility,
                    UnauthorizedInterruptConfirmation,
                    UnauthorizedInterrupt,
                    UnauthorizedReject,
                    TimerExpired,
                    TimerExpiredForIncomingCall,
                    TimerExpiredForClearIndication,
                    TimerExpiredForResetIndication,
                    TimerExpiredForRestartIndication,
                    TimerExpiredForCallDeflection,
                    CallSetupClearingOrRegistrationProblem,
                    FacilityCodeNotAllowed,
                    FacilityParameterNotAllowed,
                    InvalidCalledAddress,
                    InvalidCallingAddress,
                    InvalidFacilityLength,
                    IncomingCallBarred,
                    NoLogicalChannelAvailable,
                    CallCollision,
                    DuplicateFacilityRequested,
                    NonzeroAddressLength,
                    NonzeroFacilityLength,
                    FacilityNotProvidedWhenExpected,
                    InvalidItuTSpecifiedDteFacility,
                    MaximumNumberOfCallRedirectionsOrDeflectionsExceeded,
                    Miscellaneous,
                    ImproperCauseCodeForDte,
                    OctetNotAaligned,
                    InconsistentQBitSetting,
                    NetworkUserIdentificationProblem,
                    InternationalProblem,
                    RemoteNetworkProblem,
                    InternationalProtocolProblem,
                    InternationalLinkOutOfOrder,
                    InternationalLinkBusy,
                    TransitNetworkFacilityProblem,
                    RemoteNetworkFacilityProblem,
                    InternationalRoutingProblem,
                    TemporaryRoutingProblem,
                    UnknownCalledDataNetworkIdentificationCode,
                    MaintenanceActionClearX25VcCommandIssued,
                    // 0xf..
                    NormalTermination,
                    OutOfResources,
                    AuthenticationFailure,
                    InboundUserDataTooLarge,
                    IdleTimerExpired
                };

                extern std::string toDescription(const DiagnosticCodeEnumeration& p_source);
                extern DiagnosticCodeEnumeration fromDescription(const std::string& p_source);

                extern int toInt(const DiagnosticCodeEnumeration& p_source);
                extern DiagnosticCodeEnumeration fromInt(const int& p_source);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_CODES_DIAGNOSTICCODEENUMERATIONS_HPP_ */
