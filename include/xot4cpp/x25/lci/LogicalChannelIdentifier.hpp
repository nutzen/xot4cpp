#ifndef _XOT4CPP_X25_LCI_LOGICALCHANNELIDENTIFIER_HPP_
#define	_XOT4CPP_X25_LCI_LOGICALCHANNELIDENTIFIER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../../TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        // Logical channel identifier (LCI) = LCGN + LCN
        namespace lci {

            /**
             *                      4               8         NUMBER OF BITS
             *                .=================================.
             *                ||    L     |         L          ||
             *    GFI <------ ||    C     |         C          ||---> PTI
             *                ||    G     |         N          ||
             *                ||    N     |                    ||
             *                `================================='`
             *                |<- - - - - - - LCI - - - - - - ->|
             */
            class LogicalChannelIdentifier {
            public:
                typedef boost::shared_ptr<LogicalChannelIdentifier> SharedPointer;

            private:
                LogicalChannelIdentifier(
                        const Octet& p_logicalChannelGroupNumber,
                        const Octet& p_logicalChannelNumber);

            public:
                static SharedPointer create(
                        const unsigned int& p_logicalChannelIdentifier);

                static SharedPointer create(
                        const Octet& p_logicalChannelGroupNumber,
                        const Octet& p_logicalChannelNumber);

                virtual ~LogicalChannelIdentifier(void);

                const Octet& logicalChannelGroupNumber(void) const;
                const Octet& logicalChannelNumber(void) const;

            private:
                LogicalChannelIdentifier(void);
                LogicalChannelIdentifier(const LogicalChannelIdentifier&);
                LogicalChannelIdentifier & operator =(const LogicalChannelIdentifier&);

                const Octet m_logicalChannelGroupNumber /* LCGN 4-bit value*/;
                const Octet m_logicalChannelNumber /* LCN */;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_LCI_LOGICALCHANNELIDENTIFIER_HPP_ */
