#ifndef _XOT4CPP_X25_FACILITIES_CHARGINGCALLDURATION_HPP_
#define	_XOT4CPP_X25_FACILITIES_CHARGINGCALLDURATION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "BasicVariableLengthParameterFacility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class ChargingCallDuration : public BasicVariableLengthParameterFacility {
            public:
                typedef boost::shared_ptr<ChargingCallDuration> SharedPointer;

                static SharedPointer downCast(const Facility::SharedPointer& p_pointer);
                static Facility::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                ChargingCallDuration(const Octets& p_parameter);

            public:
                virtual ~ChargingCallDuration(void);

                static Facility::SharedPointer create(const Octets& p_parameter);

            private:
                ChargingCallDuration(void);
                ChargingCallDuration(const ChargingCallDuration&);
                ChargingCallDuration & operator =(const ChargingCallDuration&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_CHARGINGCALLDURATION_HPP_ */
