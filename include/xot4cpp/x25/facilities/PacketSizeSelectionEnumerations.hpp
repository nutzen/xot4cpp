#ifndef _XOT4CPP_X25_FACILITIES_PACKETSIZESELECTIONENUMERATIONS_HPP_
#define	_XOT4CPP_X25_FACILITIES_PACKETSIZESELECTIONENUMERATIONS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <string>

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            namespace PacketSizeSelectionEnumerations {

                enum PacketSizeSelectionEnumeration {
                    Size16octets,
                    Size32octets,
                    Size64octets,
                    Size128octets,
                    Size256octets,
                    Size512octets,
                    Size1024octets,
                    Size2048octets,
                    Size4096octets
                };

                extern std::string toDescription(const PacketSizeSelectionEnumeration& p_source);
                extern PacketSizeSelectionEnumeration fromDescription(const std::string& p_source);

                extern int toInt(const PacketSizeSelectionEnumeration& p_source);
                extern PacketSizeSelectionEnumeration fromInt(const int& p_source);

                extern unsigned int toPacketSize(const PacketSizeSelectionEnumeration& p_source);
                extern PacketSizeSelectionEnumeration fromPacketSize(const unsigned int& p_source);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_PACKETSIZESELECTIONENUMERATIONS_HPP_ */
