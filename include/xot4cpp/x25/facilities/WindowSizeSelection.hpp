#ifndef _XOT4CPP_X25_FACILITIES_WINDOWSIZESELECTION_HPP_
#define	_XOT4CPP_X25_FACILITIES_WINDOWSIZESELECTION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Facility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class WindowSizeSelection : public Facility {
            public:
                typedef boost::shared_ptr<WindowSizeSelection> SharedPointer;

                static SharedPointer downCast(const Facility::SharedPointer& p_pointer);
                static Facility::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                WindowSizeSelection(
                        const unsigned int& p_transmissionWindowSize,
                        const unsigned int& p_receiveWindowSize);

            public:
                virtual ~WindowSizeSelection(void);

                static Facility::SharedPointer create(
                        const unsigned int& p_transmissionWindowSize,
                        const unsigned int& p_receiveWindowSize);

                const unsigned int& transmissionWindowSize(void) const;
                const unsigned int& receiveWindowSize(void) const;

            private:
                WindowSizeSelection(void);
                WindowSizeSelection(const WindowSizeSelection&);
                WindowSizeSelection & operator =(const WindowSizeSelection&);

                const unsigned int m_transmissionWindowSize;
                const unsigned int m_receiveWindowSize;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_WINDOWSIZESELECTION_HPP_ */
