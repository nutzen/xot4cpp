#ifndef _XOT4CPP_X25_FACILITIES_BILATERALCLOSEDUSERGROUPSELECTION_HPP_
#define	_XOT4CPP_X25_FACILITIES_BILATERALCLOSEDUSERGROUPSELECTION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "BasicTwoByteParameterFacility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class BilateralClosedUserGroupSelection : public BasicTwoByteParameterFacility {
            public:
                typedef boost::shared_ptr<BilateralClosedUserGroupSelection> SharedPointer;

                static SharedPointer downCast(const Facility::SharedPointer& p_pointer);
                static Facility::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                BilateralClosedUserGroupSelection(const Octet& p_parameter0, const Octet& p_parameter1);

            public:
                virtual ~BilateralClosedUserGroupSelection(void);

                static Facility::SharedPointer create(const Octet& p_parameter0, const Octet& p_parameter1);

            private:
                BilateralClosedUserGroupSelection(void);
                BilateralClosedUserGroupSelection(const BilateralClosedUserGroupSelection&);
                BilateralClosedUserGroupSelection & operator =(const BilateralClosedUserGroupSelection&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_BILATERALCLOSEDUSERGROUPSELECTION_HPP_ */
