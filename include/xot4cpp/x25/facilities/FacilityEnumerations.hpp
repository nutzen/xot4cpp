#ifndef _XOT4CPP_X25_FACILITIES_FACILITYENUMERATIONS_HPP_
#define	_XOT4CPP_X25_FACILITIES_FACILITYENUMERATIONS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <string>

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            namespace FacilityEnumerations {

                enum FacilityEnumeration {
                    ReverseChargingAndFastSelect,
                    ThroughputClass,
                    ClosedUserGroupSelection,
                    ChargingInformationRequest,
                    CalledLineAddressModifiedNotification,
                    ClosedUserGroupWithOutgoingAccess,
                    QualityOfServiceNegotiationMinimumThroughputClass,
                    ExpeditedDataNegotiation,
                    BilateralClosedUserGroupSelection,
                    PacketSizeSelection,
                    WindowSizeSelection,
                    RecognizedPrivateOperatingAgencySelectionBasicFormat,
                    TransitDelaySelectionAndIndication,
                    ChargingCallDuration,
                    ChargingSegmentCount,
                    CallRedirectionNotification,
                    RecognizedPrivateOperatingAgencySelectionExtendedFormat,
                    ChargingMonetaryUnit,
                    NetworkUserIdentification,
                    CalledAddressExtensionOsi,
                    QualityOfServiceNegotiationEndToEndTransitDelay,
                    CallingAddressExtensionOsi
                };

                extern std::string toDescription(const FacilityEnumeration& p_source);
                extern FacilityEnumeration fromDescription(const std::string& p_source);

                extern int toInt(const FacilityEnumeration& p_source);
                extern FacilityEnumeration fromInt(const int& p_source);
            };
        };
    };
};

#endif	/* F_XOT4CPP_X25_FACILITIES_FACILITYENUMERATIONS_HPP_ */
