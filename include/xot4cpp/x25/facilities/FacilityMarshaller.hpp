#ifndef _XOT4CPP_X25_FACILITIES_FACILITYMARSHALLER_HPP_
#define	_XOT4CPP_X25_FACILITIES_FACILITYMARSHALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Facility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            namespace FacilityMarshaller {

                extern Octets marshal(const Facility::List& p_source);
                extern Facility::List unmarshal(const Octets& p_source);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_FACILITYMARSHALLER_HPP_ */
