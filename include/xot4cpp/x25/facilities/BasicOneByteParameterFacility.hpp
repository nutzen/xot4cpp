#ifndef _XOT4CPP_X25_FACILITIES_BASICONEBYTEPARAMETERFACILITY_HPP_
#define	_XOT4CPP_X25_FACILITIES_BASICONEBYTEPARAMETERFACILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Facility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            /**
             *
             * @todo improve parameter abstraction in all subclasses.
             *
             * This class is only a gap filler to speed up initial development.
             * It should be removed one day.
             */
            class BasicOneByteParameterFacility : public Facility {
            protected:
                BasicOneByteParameterFacility(
                        const FacilityEnumerations::FacilityEnumeration& p_enumeration,
                        const Octet& p_parameter);
            public:
                virtual ~BasicOneByteParameterFacility(void);

                const Octet& parameter(void) const;

            private:
                const Octet m_parameter;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_BASICONEBYTEPARAMETERFACILITY_HPP_ */
