#ifndef _XOT4CPP_X25_FACILITIES_CALLREDIRECTIONNOTIFICATION_HPP_
#define	_XOT4CPP_X25_FACILITIES_CALLREDIRECTIONNOTIFICATION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "BasicVariableLengthParameterFacility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class CallRedirectionNotification : public BasicVariableLengthParameterFacility {
            public:
                typedef boost::shared_ptr<CallRedirectionNotification> SharedPointer;

                static SharedPointer downCast(const Facility::SharedPointer& p_pointer);
                static Facility::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                CallRedirectionNotification(const Octets& p_parameter);

            public:
                virtual ~CallRedirectionNotification(void);

                static Facility::SharedPointer create(const Octets& p_parameter);

            private:
                CallRedirectionNotification(void);
                CallRedirectionNotification(const CallRedirectionNotification&);
                CallRedirectionNotification & operator =(const CallRedirectionNotification&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_CALLREDIRECTIONNOTIFICATION_HPP_ */
