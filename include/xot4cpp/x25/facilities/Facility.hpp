#ifndef _XOT4CPP_X25_FACILITIES_FACILITY_HPP_
#define	_XOT4CPP_X25_FACILITIES_FACILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "FacilityEnumerations.hpp"
#include "../../TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>
#include <vector>

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class Facility {
            public:
                typedef boost::shared_ptr<Facility> SharedPointer;
                typedef std::vector<SharedPointer> List;

            protected:
                Facility(const FacilityEnumerations::FacilityEnumeration& p_enumeration);

            public:
                virtual ~Facility(void);

                const FacilityEnumerations::FacilityEnumeration& enumeration(void) const;

            private:
                Facility(void);
                Facility(const Facility&);
                Facility & operator =(const Facility&);

                const FacilityEnumerations::FacilityEnumeration m_enumeration;

            };
        };
    };
};

#endif	/* FACILITY_HPP */
