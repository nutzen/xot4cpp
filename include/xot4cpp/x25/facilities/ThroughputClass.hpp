#ifndef _XOT4CPP_X25_FACILITIES_THROUGHPUTCLASS_HPP_
#define	_XOT4CPP_X25_FACILITIES_THROUGHPUTCLASS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "BasicOneByteParameterFacility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class ThroughputClass : public BasicOneByteParameterFacility {
            public:
                typedef boost::shared_ptr<ThroughputClass> SharedPointer;

                static SharedPointer downCast(const Facility::SharedPointer& p_pointer);
                static Facility::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                ThroughputClass(const Octet& p_parameter);

            public:
                virtual ~ThroughputClass(void);

                static Facility::SharedPointer create(const Octet& p_parameter);

            private:
                ThroughputClass(void);
                ThroughputClass(const ThroughputClass&);
                ThroughputClass & operator =(const ThroughputClass&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_THROUGHPUTCLASS_HPP_ */
