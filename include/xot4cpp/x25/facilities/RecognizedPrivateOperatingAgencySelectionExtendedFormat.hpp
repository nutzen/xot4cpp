#ifndef _XOT4CPP_X25_FACILITIES_RECOGNIZEDPRIVATEOPERATINGAGENCYSELECTIONEXTENDEDFORMAT_HPP_
#define	_XOT4CPP_X25_FACILITIES_RECOGNIZEDPRIVATEOPERATINGAGENCYSELECTIONEXTENDEDFORMAT_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "BasicVariableLengthParameterFacility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class RecognizedPrivateOperatingAgencySelectionExtendedFormat : public BasicVariableLengthParameterFacility {
            public:
                typedef boost::shared_ptr<RecognizedPrivateOperatingAgencySelectionExtendedFormat> SharedPointer;

                static SharedPointer downCast(const Facility::SharedPointer& p_pointer);
                static Facility::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                RecognizedPrivateOperatingAgencySelectionExtendedFormat(const Octets& p_parameter);

            public:
                virtual ~RecognizedPrivateOperatingAgencySelectionExtendedFormat(void);

                static Facility::SharedPointer create(const Octets& p_parameter);

            private:
                RecognizedPrivateOperatingAgencySelectionExtendedFormat(void);
                RecognizedPrivateOperatingAgencySelectionExtendedFormat(const RecognizedPrivateOperatingAgencySelectionExtendedFormat&);
                RecognizedPrivateOperatingAgencySelectionExtendedFormat & operator =(const RecognizedPrivateOperatingAgencySelectionExtendedFormat&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_RECOGNIZEDPRIVATEOPERATINGAGENCYSELECTIONEXTENDEDFORMAT_HPP_ */
