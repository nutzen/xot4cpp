#ifndef _XOT4CPP_X25_FACILITIES_BASICTWOBYTEPARAMETERFACILITY_HPP_
#define	_XOT4CPP_X25_FACILITIES_BASICTWOBYTEPARAMETERFACILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Facility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            /**
             *
             * @todo improve parameter abstraction in all subclasses.
             *
             * This class is only a gap filler to speed up initial development.
             * It should be removed Two day.
             */
            class BasicTwoByteParameterFacility : public Facility {
            protected:
                BasicTwoByteParameterFacility(
                        const FacilityEnumerations::FacilityEnumeration& p_enumeration,
                        const Octet& p_parameter0,
                        const Octet& p_parameter1);
            public:
                virtual ~BasicTwoByteParameterFacility(void);

                const Octet& parameter0(void) const;
                const Octet& parameter1(void) const;

            private:
                const Octet m_parameter0;
                const Octet m_parameter1;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_BASICTWOBYTEPARAMETERFACILITY_HPP_ */
