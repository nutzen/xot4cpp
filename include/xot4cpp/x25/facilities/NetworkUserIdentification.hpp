#ifndef _XOT4CPP_X25_FACILITIES_NETWORKUSERIDENTIFICATION_HPP_
#define	_XOT4CPP_X25_FACILITIES_NETWORKUSERIDENTIFICATION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "BasicVariableLengthParameterFacility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class NetworkUserIdentification : public BasicVariableLengthParameterFacility {
            public:
                typedef boost::shared_ptr<NetworkUserIdentification> SharedPointer;

                static SharedPointer downCast(const Facility::SharedPointer& p_pointer);
                static Facility::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                NetworkUserIdentification(const Octets& p_parameter);

            public:
                virtual ~NetworkUserIdentification(void);

                static Facility::SharedPointer create(const Octets& p_parameter);

            private:
                NetworkUserIdentification(void);
                NetworkUserIdentification(const NetworkUserIdentification&);
                NetworkUserIdentification & operator =(const NetworkUserIdentification&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_NETWORKUSERIDENTIFICATION_HPP_ */
