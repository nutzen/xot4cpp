#ifndef _XOT4CPP_X25_FACILITIES_BASICVARIABLELENGTHPARAMETERFACILITY_HPP_
#define	_XOT4CPP_X25_FACILITIES_BASICVARIABLELENGTHPARAMETERFACILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Facility.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            /**
             *
             * @todo improve parameter abstraction in all subclasses.
             *
             * This class is only a gap filler to speed up initial development.
             * It should be removed one day.
             */
            class BasicVariableLengthParameterFacility : public Facility {
            protected:
                BasicVariableLengthParameterFacility(
                        const FacilityEnumerations::FacilityEnumeration& p_enumeration,
                        const Octets& p_parameter);
            public:
                virtual ~BasicVariableLengthParameterFacility(void);

                const Octets& parameter(void) const;

            private:
                const Octets m_parameter;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_BASICVARIABLELENGTHPARAMETERFACILITY_HPP_ */
