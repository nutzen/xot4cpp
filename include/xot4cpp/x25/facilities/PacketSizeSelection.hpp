#ifndef _XOT4CPP_X25_FACILITIES_PACKETSIZESELECTION_HPP_
#define	_XOT4CPP_X25_FACILITIES_PACKETSIZESELECTION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Facility.hpp"
#include "PacketSizeSelectionEnumerations.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            class PacketSizeSelection : public Facility {
            public:
                typedef boost::shared_ptr<PacketSizeSelection> SharedPointer;

                static SharedPointer downCast(const Facility::SharedPointer& p_pointer);
                static Facility::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                PacketSizeSelection(
                        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_transmitPacketSize,
                        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_receivePacketSize);

            public:
                virtual ~PacketSizeSelection(void);

                static Facility::SharedPointer create(
                        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_transmitPacketSize,
                        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_receivePacketSize);

                const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& transmitPacketSize(void) const;
                const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& receivePacketSize(void) const;

            private:
                PacketSizeSelection(void);
                PacketSizeSelection(const PacketSizeSelection&);
                PacketSizeSelection & operator =(const PacketSizeSelection&);

                const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration m_transmitPacketSize;
                const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration m_receivePacketSize;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_FACILITIES_PACKETSIZESELECTION_HPP_ */
