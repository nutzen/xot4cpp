#ifndef _XOT4CPP_X25_X25PACKET_HPP_
#define	_XOT4CPP_X25_X25PACKET_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Header.hpp"
#include "payload/X25Payload.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        class X25Packet {
        public:
            typedef boost::shared_ptr<X25Packet> SharedPointer;

        private:
            X25Packet(const X25Header::SharedPointer& p_header,
                    const payload::X25Payload::SharedPointer& p_payload);

        public:
            virtual ~X25Packet(void);

            static SharedPointer create(const X25Header::SharedPointer& p_header,
                    const payload::X25Payload::SharedPointer& p_payload);

            const X25Header::SharedPointer& header(void) const;
            const payload::X25Payload::SharedPointer& payload(void) const;

        private:
            X25Packet(void);
            X25Packet(const X25Packet&);
            X25Packet & operator =(const X25Packet&);

            const X25Header::SharedPointer m_header;
            const payload::X25Payload::SharedPointer m_payload;

        };
    };
};

#endif	/* _XOT4CPP_X25_X25PACKET_HPP_ */

