#ifndef _XOT4CPP_X25_X25HEADERMARSHALLER_HPP_
#define	_XOT4CPP_X25_X25HEADERMARSHALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Header.hpp"
#include "../stream/InputStream.hpp"
#include "../stream/OutputStream.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace X25HeaderMarshaller {

            extern void marshal(const stream::OutputStream::SharedPointer& p_outputStream, const X25Header::SharedPointer& p_header);

            extern X25Header::SharedPointer unmarshal(const stream::InputStream::SharedPointer& p_inputStream);
        };
    };
};

#endif	/* _XOT4CPP_X25_X25HEADERMARSHALLER_HPP_ */
