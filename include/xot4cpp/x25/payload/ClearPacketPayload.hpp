#ifndef _XOT4CPP_X25_PAYLOAD_CLEARPACKETPAYLOAD_HPP_
#define	_XOT4CPP_X25_PAYLOAD_CLEARPACKETPAYLOAD_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Payload.hpp"

#include "../../x121/X121Address.hpp"
#include "../codes/CauseCodeEnumerations.hpp"
#include "../codes/DiagnosticCodeEnumerations.hpp"
#include "../facilities/Facility.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace payload {

            /**
             *
             *        .====================================================.
             * O   1  ||  0    0  ||   X   X   ||        L C G N          || C
             * C      |====================================================| L
             * T   1  ||                    L  C  N                       || E
             * E      |====================================================| A
             * T   1  ||                    P  T  I                       || R
             * S      |====================================================|
             *     1  ||          C L E A R I N G   C A U S E             || I
             *        |====================================================| N
             *     1  ||       C L E A R I N G   D I A G N O S T I C      || D
             *        |====================================================| I
             *     17 ||   ADDRESSING IN CALL REQUEST FORMAT IF USED      || C
             *        |====================================================| A
             *     N  ||   FACILITIES IN CALL REQUEST FORMAT IF USED      || T
             *        |====================================================| I
             *     16 ||       C L E A R   U S E R    D A T A             || O
             *        `====================================================' N
             *
             */
            class ClearPacketPayload : public X25Payload {
            public:
                typedef boost::shared_ptr<ClearPacketPayload> SharedPointer;

                static SharedPointer downCast(const X25Payload::SharedPointer& p_pointer);
                static X25Payload::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                ClearPacketPayload(
                        const codes::CauseCodeEnumerations::ClearRequestEnumeration& p_cause,
                        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic,
                        const x121::X121Address::SharedPointer& p_calledAddress,
                        const x121::X121Address::SharedPointer& p_callingAddress,
                        const facilities::Facility::List& p_facilities,
                        const Octets& p_userData);

            public:
                virtual ~ClearPacketPayload(void);

                static X25Payload::SharedPointer create(
                        const codes::CauseCodeEnumerations::ClearRequestEnumeration& p_cause,
                        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic,
                        const x121::X121Address::SharedPointer& p_calledAddress = x121::X121Address::SharedPointer(),
                        const x121::X121Address::SharedPointer& p_callingAddress = x121::X121Address::SharedPointer(),
                        const facilities::Facility::List& p_facilities = facilities::Facility::List(),
                        const Octets& p_userData = Octets());

                const codes::CauseCodeEnumerations::ClearRequestEnumeration& cause(void) const;
                const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& diagnostic(void) const;
                const x121::X121Address::SharedPointer& calledAddress(void) const;
                const x121::X121Address::SharedPointer& callingAddress(void) const;
                const facilities::Facility::List& facilities(void) const;
                const Octets& userData(void) const;

            private:
                ClearPacketPayload(void);
                ClearPacketPayload(const ClearPacketPayload&);
                ClearPacketPayload & operator =(const ClearPacketPayload&);

                const codes::CauseCodeEnumerations::ClearRequestEnumeration m_cause;
                const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration m_diagnostic;
                const x121::X121Address::SharedPointer m_calledAddress /* optional */;
                const x121::X121Address::SharedPointer m_callingAddress /* optional */;
                const facilities::Facility::List m_facilities /* optional */;
                const Octets m_userData /* optional */;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PAYLOAD_CLEARPACKETPAYLOAD_HPP_ */
