/* 
 * File:   EmptyPacketPayload.hpp
 * Author: joe.els
 *
 * Created on November 10, 2011, 2:29 PM
 */

#ifndef _XOT4CPP_X25_PAYLOAD_EMPTYPACKETPAYLOAD_HPP_
#define	_XOT4CPP_X25_PAYLOAD_EMPTYPACKETPAYLOAD_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Payload.hpp"
#include "../../TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace payload {

/**
 *
 *       .====================================================.
 * O  1  ||  0    D  ||   X   X   ||        L C G N          ||
 * C     |====================================================|
 * T  1  ||                    L  C  N                       ||
 * E     |====================================================|
 * T  1  ||                    P  T  I                       ||
 *       `===================================================='
 *
 */
            class EmptyPacketPayload : public X25Payload {
            public:
                typedef boost::shared_ptr<EmptyPacketPayload> SharedPointer;

                static SharedPointer downCast(const X25Payload::SharedPointer& p_pointer);
                static X25Payload::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                EmptyPacketPayload(void);

            public:
                virtual ~EmptyPacketPayload(void);

                static X25Payload::SharedPointer create(void);

            private:
                EmptyPacketPayload(const EmptyPacketPayload&);
                EmptyPacketPayload & operator =(const EmptyPacketPayload&);

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PAYLOAD_EMPTYPACKETPAYLOAD_HPP_ */
