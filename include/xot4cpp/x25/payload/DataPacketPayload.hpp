#ifndef _XOT4CPP_X25_PAYLOAD_DATAPACKETPAYLOAD_HPP_
#define	_XOT4CPP_X25_PAYLOAD_DATAPACKETPAYLOAD_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Payload.hpp"
#include "../../TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace payload {

            /**
             *
             *       .====================================================.
             * O  1  ||  0    D  ||   X   X   ||        L C G N          || D
             * C     |====================================================| A
             * T  1  ||                    L  C  N                       || T
             * E     |====================================================| A
             * T  1  ||                    P  T  I                       ||
             * S     |====================================================|
             *    N  ||               U S E R    D A T A                 ||
             *       `===================================================='
             *
             */
            class DataPacketPayload : public X25Payload {
            public:
                typedef boost::shared_ptr<DataPacketPayload> SharedPointer;

                static SharedPointer downCast(const X25Payload::SharedPointer& p_pointer);
                static X25Payload::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                DataPacketPayload(const Octets& p_userData);

            public:
                virtual ~DataPacketPayload(void);

                static X25Payload::SharedPointer create(const Octets& p_userData);

                const Octets& userData(void) const;

            private:
                DataPacketPayload(void);
                DataPacketPayload(const DataPacketPayload&);
                DataPacketPayload & operator =(const DataPacketPayload&);

                const Octets m_userData;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PAYLOAD_DATAPACKETPAYLOAD_HPP_ */
