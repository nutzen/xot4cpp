#ifndef _XOT4CPP_X25_PAYLOAD_RESETPACKETPAYLOAD_HPP_
#define	_XOT4CPP_X25_PAYLOAD_RESETPACKETPAYLOAD_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Payload.hpp"
#include "../codes/CauseCodeEnumerations.hpp"
#include "../codes/DiagnosticCodeEnumerations.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace payload {

            /**
             *
             *        .====================================================.
             * O   1  ||  0    0  ||   X   X   ||        L C G N          || R
             * C      |====================================================| E
             * T   1  ||                    L  C  N                       || S
             * E      |====================================================| E
             * T   1  ||                    P  T  I                       || T
             * S      |====================================================|
             *     1  ||             R E S E T   C A U S E                ||
             *        |====================================================|
             *     1  ||       C L E A R I N G   D I A G N O S T I C      ||
             *        `===================================================='
             *
             */
            class ResetPacketPayload : public X25Payload {
            public:
                typedef boost::shared_ptr<ResetPacketPayload> SharedPointer;

                static SharedPointer downCast(const X25Payload::SharedPointer& p_pointer);
                static X25Payload::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                ResetPacketPayload(
                        const codes::CauseCodeEnumerations::ResetRequestEnumeration& p_cause,
                        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic);

            public:
                virtual ~ResetPacketPayload(void);

                static X25Payload::SharedPointer create(
                        const codes::CauseCodeEnumerations::ResetRequestEnumeration& p_cause,
                        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic);

                const codes::CauseCodeEnumerations::ResetRequestEnumeration& cause(void) const;
                const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& diagnostic(void) const;

            private:
                ResetPacketPayload(void);
                ResetPacketPayload(const ResetPacketPayload&);
                ResetPacketPayload & operator =(const ResetPacketPayload&);

                const codes::CauseCodeEnumerations::ResetRequestEnumeration m_cause;
                const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration m_diagnostic;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PAYLOAD_RESETPACKETPAYLOAD_HPP_ */
