#ifndef _XOT4CPP_X25_PAYLOAD_X25PAYLOADMARSHALLER_HPP_
#define	_XOT4CPP_X25_PAYLOAD_X25PAYLOADMARSHALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Payload.hpp"
#include "../../stream/InputStream.hpp"
#include "../../stream/OutputStream.hpp"
#include "../../TypeDefinitions.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace payload {

            namespace X25PayloadMarshaller {

                extern void marshalCallPacketPayload(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const X25Payload::SharedPointer& p_source,
                        const bool& p_aBit);

                extern X25Payload::SharedPointer unmarshalCallPacketPayload(
                        const stream::InputStream::SharedPointer& p_inputStream,
                        const bool& p_aBit);

                extern void marshalClearPacketPayload(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const X25Payload::SharedPointer& p_source,
                        const bool& p_aBit);

                extern X25Payload::SharedPointer unmarshalClearPacketPayload(
                        const stream::InputStream::SharedPointer& p_inputStream,
                        const bool& p_aBit);

                extern void marshalClearConfirmationPayload(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const X25Payload::SharedPointer& p_source,
                        const bool& p_aBit);

                extern X25Payload::SharedPointer unmarshalClearConfirmationPayload(
                        const stream::InputStream::SharedPointer& p_inputStream,
                        const bool& p_aBit);

                extern void marshalResetPacketPayload(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const X25Payload::SharedPointer& p_source);

                extern X25Payload::SharedPointer unmarshalResetPacketPayload(
                        const stream::InputStream::SharedPointer& p_inputStream);

                extern void marshalRestartPacketPayload(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const X25Payload::SharedPointer& p_source);

                extern X25Payload::SharedPointer unmarshalRestartPacketPayload(
                        const stream::InputStream::SharedPointer& p_inputStream);

                extern void marshalDataPacketPayload(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const X25Payload::SharedPointer& p_source);

                extern X25Payload::SharedPointer unmarshalDataPacketPayload(
                        const stream::InputStream::SharedPointer& p_inputStream);

                extern void marshalEmptyPacketPayload(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const X25Payload::SharedPointer& p_source);

                extern X25Payload::SharedPointer unmarshalEmptyPacketPayload(
                        const stream::InputStream::SharedPointer& p_inputStream);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PAYLOAD_X25PAYLOADMARSHALLER_HPP_ */
