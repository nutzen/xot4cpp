#ifndef _XOT4CPP_X25_PAYLOAD_X25PAYLOAD_HPP_
#define	_XOT4CPP_X25_PAYLOAD_X25PAYLOAD_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace payload {

            class X25Payload {
            public:
                typedef boost::shared_ptr<X25Payload> SharedPointer;

            protected:
                X25Payload(void);

            public:
                virtual ~X25Payload(void);

            private:
                X25Payload(const X25Payload&);
                X25Payload & operator =(const X25Payload&);

            };
        };
    };
};

#endif	/* X25PAYLOAD_HPP */
