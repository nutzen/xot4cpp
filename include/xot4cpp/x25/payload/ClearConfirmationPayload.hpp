#ifndef _XOT4CPP_X25_PAYLOAD_CLEARCONFIRMATIONPAYLOAD_HPP_
#define	_XOT4CPP_X25_PAYLOAD_CLEARCONFIRMATIONPAYLOAD_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Payload.hpp"
#include "../../x121/X121Address.hpp"
#include "../facilities/Facility.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace payload {

            /**
             *
             *        .====================================================.
             * O   1  ||  0    0  ||   X   X   ||        L C G N          || C
             * C      |====================================================| L
             * T   1  ||                    L  C  N                       || E
             * E      |====================================================| A
             * T   1  ||                    P  T  I                       || R
             * S      |====================================================|
             *     17 ||   ADDRESSING IN CALL REQUEST FORMAT IF USED      || C
             *        |====================================================| O
             *     N  ||   FACILITIES IN CALL REQUEST FORMAT IF USED      || N
             *        `===================================================='
             *
             */
            class ClearConfirmationPayload : public X25Payload {
            public:
                typedef boost::shared_ptr<ClearConfirmationPayload> SharedPointer;

                static SharedPointer downCast(const X25Payload::SharedPointer& p_pointer);
                static X25Payload::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                ClearConfirmationPayload(
                        const x121::X121Address::SharedPointer& p_calledAddress,
                        const x121::X121Address::SharedPointer& p_callingAddress,
                        const facilities::Facility::List& p_facilities);

            public:
                virtual ~ClearConfirmationPayload(void);

                static X25Payload::SharedPointer create(
                        const x121::X121Address::SharedPointer& p_calledAddress = x121::X121Address::SharedPointer(),
                        const x121::X121Address::SharedPointer& p_callingAddress = x121::X121Address::SharedPointer(),
                        const facilities::Facility::List& p_facilities = facilities::Facility::List());

                const x121::X121Address::SharedPointer& calledAddress(void) const;
                const x121::X121Address::SharedPointer& callingAddress(void) const;
                const facilities::Facility::List& facilities(void) const;

            private:
                ClearConfirmationPayload(void);
                ClearConfirmationPayload(const ClearConfirmationPayload&);
                ClearConfirmationPayload & operator =(const ClearConfirmationPayload&);

                const x121::X121Address::SharedPointer m_calledAddress /* optional */;
                const x121::X121Address::SharedPointer m_callingAddress /* optional */;
                const facilities::Facility::List m_facilities /* optional */;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PAYLOAD_CLEARCONFIRMATIONPAYLOAD_HPP_ */
