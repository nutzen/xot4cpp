#ifndef _XOT4CPP_X25_PAYLOAD_CALLPACKETPAYLOAD_HPP_
#define	_XOT4CPP_X25_PAYLOAD_CALLPACKETPAYLOAD_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Payload.hpp"
#include "../../x121/X121Address.hpp"
#include "../facilities/Facility.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace payload {

            /**
             *
             *       .====================================================.
             * O  1  ||  0    D  ||   X   X   ||        L C G N          || C
             * C     |====================================================| A
             * T  1  ||                    L  C  N                       || L
             * E     |====================================================| L
             * T  1  ||                    P  T  I                       ||
             * S     |====================================================| R
             *    1  ||  CALLED ADDR LENGTH   ||   CALLING ADDR LENGTH   || E
             *       |====================================================| Q
             *    8  ||           C A L L E D    A D D R E S S           || U
             *       |====================================================| E
             *    8  ||          C A L L I N G    A D D R E S S          || S
             *       |====================================================| T
             *    1  ||       F A C I L I T I E S    L E N G T H         ||
             *       |====================================================| P
             *    N  ||          F  A  C  I  L  I  T  I  E  S            || K
             *       |====================================================| T
             *    16 ||        C A L L    U S E R    D A T A             ||
             *       `===================================================='
             *
             */
            class CallPacketPayload : public X25Payload {
            public:
                typedef boost::shared_ptr<CallPacketPayload> SharedPointer;

                static SharedPointer downCast(const X25Payload::SharedPointer& p_pointer);
                static X25Payload::SharedPointer upCast(const SharedPointer& p_pointer);

            private:
                CallPacketPayload(
                        const x121::X121Address::SharedPointer& p_calledAddress,
                        const x121::X121Address::SharedPointer& p_callingAddress,
                        const facilities::Facility::List& p_facilities,
                        const Octets& p_userData);

            public:
                virtual ~CallPacketPayload(void);

                static X25Payload::SharedPointer create(
                        const x121::X121Address::SharedPointer& p_calledAddress = x121::X121Address::SharedPointer(),
                        const x121::X121Address::SharedPointer& p_callingAddress = x121::X121Address::SharedPointer(),
                        const facilities::Facility::List& p_facilities = facilities::Facility::List(),
                        const Octets& p_userData = Octets());

                const x121::X121Address::SharedPointer& calledAddress(void) const;
                const x121::X121Address::SharedPointer& callingAddress(void) const;
                const facilities::Facility::List& facilities(void) const;
                const Octets& userData(void) const;

            private:
                CallPacketPayload(void);
                CallPacketPayload(const CallPacketPayload&);
                CallPacketPayload & operator =(const CallPacketPayload&);

                const x121::X121Address::SharedPointer m_calledAddress /* optional for Call_Connected / Call_Accept */;
                const x121::X121Address::SharedPointer m_callingAddress /* optional for Call_Connected / Call_Accept */;
                const facilities::Facility::List m_facilities /* optional for Call_Connected / Call_Accept */;
                const Octets m_userData /* optional */;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_PAYLOAD_CALLPACKETPAYLOAD_HPP_ */
