#ifndef _XOT4CPP_X25_GFI_GENERALFORMATIDENTIFIER_HPP_
#define	_XOT4CPP_X25_GFI_GENERALFORMATIDENTIFIER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../../TypeDefinitions.hpp"

#include "boost/smart_ptr.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace gfi {

            // GFI 4bit value, default 00 01

            /**
             *                  .=============================.
             *  FRAME LAYER     ||     ||     ||             ||    LOGICAL
             *  CONTROL BYTE <--||  Q  ||  D  ||   X    X    ||--> CHANNEL
             *                  ||     ||     ||             ||  IDENTIFIER
             *                  `============================='
             *
             *     Q: QUALIFIED DATA BIT        - 0 = Data for user
             *                                    1 = Data for PAD
             *
             *     D: DELIVERY CONFIRMATION BIT - 0 = For local acknowledgment
             *                                    1 = For remote acknowledgment
             *
             *    XX: PROTOCOL IDENTIFICATION  - 00 = Reserved for future use
             *                                   01 = Modulo 8 sequencing
             *                                   10 = Modulo 128 sequencing
             *                                   11 = Extended format
             */
            class GeneralFormatIdentifier {
            public:
                typedef boost::shared_ptr<GeneralFormatIdentifier> SharedPointer;

                /**
                 * Q: QUALIFIED DATA BIT
                 * - 0 = Data for user
                 * - 1 = Data for PAD
                 */
                enum QualifiedData {
                    DataForUser = 0x00,
                    DataForPad = 0x01
                };

                /**
                 * D: DELIVERY CONFIRMATION BIT
                 * - 0 = For local acknowledgment
                 * - 1 = For remote acknowledgment
                 */
                enum DeliveryConfirmation {
                    ForLocalAcknowledgment = 0x00,
                    ForRemoteAcknowledgment = 0x01
                };

                /**
                 * XX: PROTOCOL IDENTIFICATION
                 * - 00 = Reserved for future use
                 * - 01 = Modulo 8 sequencing
                 * - 10 = Modulo 128 sequencing
                 * - 11 = Extended format
                 */
                enum ProtocolIdentification {
                    ReservedForFutureUse = 0x00,
                    Modulo8Sequencing = 0x01,
                    Modulo128Sequencing = 0x02,
                    ExtendedFormat = 0x03
                };

            private:
                GeneralFormatIdentifier(const QualifiedData& p_qualifiedData,
                        const DeliveryConfirmation& p_deliveryConfirmation,
                        const ProtocolIdentification& p_protocolIdentification);

            public:
                static SharedPointer create(const QualifiedData& p_qualifiedData,
                        const DeliveryConfirmation& p_deliveryConfirmation,
                        const ProtocolIdentification& p_protocolIdentification);

                virtual ~GeneralFormatIdentifier(void);

                const QualifiedData& qualifiedData(void) const;
                const DeliveryConfirmation& deliveryConfirmation(void) const;
                const ProtocolIdentification& protocolIdentification(void) const;

            private:
                GeneralFormatIdentifier(void);
                GeneralFormatIdentifier(const GeneralFormatIdentifier&);
                GeneralFormatIdentifier & operator =(const GeneralFormatIdentifier&);

                const QualifiedData m_qualifiedData;
                const DeliveryConfirmation m_deliveryConfirmation;
                const ProtocolIdentification m_protocolIdentification;
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_GFI_GENERALFORMATIDENTIFIER_HPP_ */
