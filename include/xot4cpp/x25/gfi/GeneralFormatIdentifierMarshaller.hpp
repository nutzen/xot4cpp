#ifndef _XOT4CPP_X25_GFI_GENERALFORMATIDENTIFIERMARSHALLER_HPP_
#define	_XOT4CPP_X25_GFI_GENERALFORMATIDENTIFIERMARSHALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "GeneralFormatIdentifier.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace gfi {

            namespace GeneralFormatIdentifierMarshaller {

                extern Octet marshal(const GeneralFormatIdentifier::SharedPointer& p_source);

                extern GeneralFormatIdentifier::SharedPointer unmarshal(const Octet& p_source);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_GFI_GENERALFORMATIDENTIFIERMARSHALLER_HPP_ */
