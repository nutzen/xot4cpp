#ifndef _XOT4CPP_X25_X25PACKETMARSHALLER_HPP_
#define	_XOT4CPP_X25_X25PACKETMARSHALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X25Packet.hpp"
#include "../stream/InputStream.hpp"
#include "../stream/OutputStream.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace X25PacketMarshaller {

            extern void marshal(
                    const stream::OutputStream::SharedPointer& p_outputStream,
                    const X25Packet::SharedPointer& p_source);

            extern X25Packet::SharedPointer unmarshal(
                    const stream::InputStream::SharedPointer& p_inputStream);
        };
    };
};

#endif	/* _XOT4CPP_X25_X25PACKETMARSHALLER_HPP_ */
