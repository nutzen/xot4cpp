#ifndef _XOT4CPP_X25_STATES_X25CONTROLLER_HPP_
#define	_XOT4CPP_X25_STATES_X25CONTROLLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../stream/Pipe.hpp"
#include "../transport/TransportWrapper.hpp"
#include "context/SessionContext.hpp"
#include "transitions/TransitionResult.hpp"
#include "X25Packet.hpp"
#include "states/StateMachine.hpp"

#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>

namespace xot4cpp {

    namespace x25 {

        class X25Controller {
        public:
            typedef boost::shared_ptr<X25Controller> SharedPointer;
            typedef boost::thread Thread;
            typedef boost::shared_ptr<Thread> Thread_SharedPointer;

        private:
            X25Controller(
                    const Thread_SharedPointer& p_thread,
                    const stream::Pipe::SharedPointer& p_localToRemote,
                    const stream::Pipe::SharedPointer& p_remoteToLocal,
                    const states::StateMachine::SharedPointer& p_stateMachine,
                    const context::SessionContext::SharedPointer& p_sessionContext,
                    const transport::TransportWrapper::SharedPointer& p_wrapper,
                    const bool& p_running);

        public:
            virtual ~X25Controller(void);

            static SharedPointer create(
                    const context::SessionContext::SharedPointer& p_sessionContext,
                    const transport::TransportWrapper::SharedPointer& p_wrapper);

            const stream::Pipe::SharedPointer& localToRemote(void) const;
            const stream::Pipe::SharedPointer& remoteToLocal(void) const;

            const bool isConnected(void) const;

            virtual void operator()(void);

            void accept(void);
            void connect(const Octets& p_userData);
            void disconnect(void);

        private:
            void poll(void);
            void submit(const X25Packet::SharedPointer& p_packet);
            void handle(const transitions::TransitionResult::SharedPointer& p_result);

            static std::vector<Octets> splitData(const context::SessionContext::SharedPointer& p_sessionContext, const Octets& p_userData);

            void callSetupCheck(void) const;
            bool isInFlowControlReadyState(void) const;
            bool isInFlowControlState(void) const;

            X25Controller(void);
            X25Controller(const X25Controller&);
            X25Controller & operator =(const X25Controller&);

            Thread_SharedPointer m_thread;
            const stream::Pipe::SharedPointer m_localToRemote;
            const stream::Pipe::SharedPointer m_remoteToLocal;
            const states::StateMachine::SharedPointer m_stateMachine;
            const context::SessionContext::SharedPointer m_sessionContext;
            const transport::TransportWrapper::SharedPointer m_wrapper;
            volatile bool m_running;

        };
    };
};

#endif	/* _XOT4CPP_X25_STATES_X25CONTROLLER_HPP_ */
