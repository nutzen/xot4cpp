#ifndef _XOT4CPP_X25_TRANSITIONS_TRANSITIONRESULT_HPP_
#define	_XOT4CPP_X25_TRANSITIONS_TRANSITIONRESULT_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../X25Packet.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace transitions {

            enum PostTransitionActionEnumeration {
                None,
                Poll,
                Submit
            };

            class TransitionResult {
            public:
                typedef boost::shared_ptr<TransitionResult> SharedPointer;

            private:
                TransitionResult(
                        const PostTransitionActionEnumeration& p_action,
                        const X25Packet::SharedPointer& p_packet);

            public:
                virtual ~TransitionResult(void);

                static SharedPointer create(
                        const PostTransitionActionEnumeration& p_action,
                        const X25Packet::SharedPointer& p_packet = X25Packet::SharedPointer());

                const PostTransitionActionEnumeration& action(void) const;
                const X25Packet::SharedPointer& packet(void) const;

            private:
                TransitionResult(void);
                TransitionResult(const TransitionResult&);
                TransitionResult & operator =(const TransitionResult&);

                const PostTransitionActionEnumeration m_action;
                const X25Packet::SharedPointer m_packet /* optional */;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_TRANSITIONS_TRANSITIONRESULT_HPP_ */
