#ifndef _XOT4CPP_X25_TRANSITIONS_CLEARCONFIRMATIONTRANSITION_HPP_
#define	_XOT4CPP_X25_TRANSITIONS_CLEARCONFIRMATIONTRANSITION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Transition.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace transitions {

            class ClearConfirmationTransition : public Transition {
            private:
                ClearConfirmationTransition(
                        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
                        const TransitionParameters::SharedPointer& p_parameters);

            public:
                virtual ~ClearConfirmationTransition(void);

                static SharedPointer create(
                        const TransitionParameters::SharedPointer& p_parameters);

            protected:
                virtual TransitionResult::SharedPointer operator ()(
                        const TransitionParameters::SharedPointer& p_parameters) const;

            private:
                ClearConfirmationTransition(void);
                ClearConfirmationTransition(const ClearConfirmationTransition&);
                ClearConfirmationTransition & operator =(const ClearConfirmationTransition&);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_TRANSITIONS_CLEARCONFIRMATIONTRANSITION_HPP_ */
