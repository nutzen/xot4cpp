#ifndef _XOT4CPP_X25_TRANSITIONS_DATATRANSITION_HPP_
#define	_XOT4CPP_X25_TRANSITIONS_DATATRANSITION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Transition.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace transitions {

            class DataTransition : public Transition {
            private:
                DataTransition(
                        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
                        const TransitionParameters::SharedPointer& p_parameters);

            public:
                virtual ~DataTransition(void);

                static SharedPointer create(
                        const TransitionParameters::SharedPointer& p_parameters);

            protected:
                virtual TransitionResult::SharedPointer operator ()(
                        const TransitionParameters::SharedPointer& p_parameters) const;

            private:
                DataTransition(void);
                DataTransition(const DataTransition&);
                DataTransition & operator =(const DataTransition&);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_TRANSITIONS_DATATRANSITION_HPP_ */
