#ifndef _XOT4CPP_X25_TRANSITIONS_TRANSITIONPARAMETERS_HPP_
#define	_XOT4CPP_X25_TRANSITIONS_TRANSITIONPARAMETERS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../../stream/OutputStream.hpp"
#include "../context/RoleEnumerations.hpp"
#include "../context/SessionContext.hpp"
#include "../states/StateMachine.hpp"
#include "../X25Packet.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace transitions {

            class TransitionParameters {
            public:
                typedef boost::shared_ptr<TransitionParameters> SharedPointer;

            private:
                TransitionParameters(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const states::StateMachine::SharedPointer& p_stateMachine,
                        const context::SessionContext::SharedPointer& p_sessionContext,
                        const context::RoleEnumerations::RoleEnumeration& p_origin,
                        const X25Packet::SharedPointer& p_packet);

            public:
                virtual ~TransitionParameters(void);

                static SharedPointer create(
                        const stream::OutputStream::SharedPointer& p_outputStream,
                        const states::StateMachine::SharedPointer& p_stateMachine,
                        const context::SessionContext::SharedPointer& p_sessionContext,
                        const context::RoleEnumerations::RoleEnumeration& p_origin,
                        const X25Packet::SharedPointer& p_packet);

                const stream::OutputStream::SharedPointer& outputStream(void) const;
                const states::StateMachine::SharedPointer& stateMachine(void) const;
                const context::SessionContext::SharedPointer& sessionContext(void) const;
                const context::RoleEnumerations::RoleEnumeration& origin(void) const;
                const X25Packet::SharedPointer& packet(void) const;

            private:
                TransitionParameters(void);
                TransitionParameters(const TransitionParameters&);
                TransitionParameters & operator =(const TransitionParameters&);

                const stream::OutputStream::SharedPointer m_outputStream;
                const states::StateMachine::SharedPointer m_stateMachine;
                const context::SessionContext::SharedPointer m_sessionContext;
                const context::RoleEnumerations::RoleEnumeration m_origin;
                const X25Packet::SharedPointer m_packet;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_TRANSITIONS_TRANSITIONPARAMETERS_HPP_ */
