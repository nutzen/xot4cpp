#ifndef _XOT4CPP_X25_TRANSITIONS_TRANSITIONENUMERATIONS_HPP_
#define	_XOT4CPP_X25_TRANSITIONS_TRANSITIONENUMERATIONS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <string>

namespace xot4cpp {

    namespace x25 {

        namespace transitions {

            namespace TransitionEnumerations {

                enum TransitionEnumeration {
                    // Call set-up and clearing DESCRIPTION
                    CallRequest,
                    CallIncoming,
                    CallAccepted,
                    CallConnected,
                    ClearRequest,
                    ClearIndication,
                    ClearConfirmation,
                    // Data and interrupt DESCRIPTION
                    Data,
                    Interrupt,
                    InterruptConfirmation,
                    // Flow control and reset DESCRIPTION
                    ReceiverReady,
                    ReceiverNotReady,
                    Reject,
                    ResetRequest,
                    ResetIndication,
                    ResetConfirmation,
                    // Restart DESCRIPTION
                    RestartRequest,
                    RestartIndication,
                    RestartConfirmation,
                    // Diagnostic DESCRIPTION
                    Diagnostic
                };

                extern std::string toDescription(const TransitionEnumeration& p_source);
                extern TransitionEnumeration fromDescription(const std::string& p_source);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_TRANSITIONS_TRANSITIONENUMERATIONS_HPP_ */
