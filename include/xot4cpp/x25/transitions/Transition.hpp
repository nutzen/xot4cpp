#ifndef _XOT4CPP_X25_TRANSITIONS_TRANSITION_HPP_
#define	_XOT4CPP_X25_TRANSITIONS_TRANSITION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "TransitionEnumerations.hpp"
#include "TransitionParameters.hpp"
#include "TransitionResult.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace transitions {

            class Transition {
            public:
                typedef boost::shared_ptr<Transition> SharedPointer;

            protected:
                Transition(
                        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
                        const TransitionParameters::SharedPointer& p_parameters);

            public:
                virtual ~Transition(void);

                static SharedPointer create(
                        const TransitionParameters::SharedPointer& p_parameters);

                virtual const TransitionEnumerations::TransitionEnumeration& enumeration(void) const;
                TransitionResult::SharedPointer operator ()(void) const;

            protected:
                virtual TransitionResult::SharedPointer operator ()(
                        const TransitionParameters::SharedPointer& p_parameters) const = 0;

            private:
                Transition(void);
                Transition(const Transition&);
                Transition & operator =(const Transition&);

                const TransitionEnumerations::TransitionEnumeration m_enumeration;
                const TransitionParameters::SharedPointer m_parameters;

            };
        };
    };
};

#endif	/* _XOT4CPP_X25_TRANSITIONS_TRANSITION_HPP_ */
