#ifndef _XOT4CPP_X25_UTILITY_HEXUTILITY_HPP_
#define	_XOT4CPP_X25_UTILITY_HEXUTILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../../TypeDefinitions.hpp"

#include <string>

namespace xot4cpp {

    namespace x25 {

        namespace utility {

            namespace HexUtility {

                extern std::string toHex(const Octet& p_octet);
                extern std::string toHex(const Octets& p_octets);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_UTILITY_HEXUTILITY_HPP_ */
