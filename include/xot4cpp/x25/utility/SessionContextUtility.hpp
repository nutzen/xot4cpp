#ifndef _XOT4CPP_X25_UTILITY_SESSIONCONTEXTUTILITY_HPP_
#define	_XOT4CPP_X25_UTILITY_SESSIONCONTEXTUTILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../X25Packet.hpp"
#include "../context/SessionContext.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace utility {

            namespace SessionContextUtility {

                extern void updatePacketReceiveSequenceNumber(
                        const context::SessionContext::SharedPointer& p_sessionContext);

                extern void updatePacketSendSequenceNumber(
                        const context::SessionContext::SharedPointer& p_sessionContext);

                extern unsigned int receivePacketSize(
                        const context::SessionContext::SharedPointer& p_sessionContext);

                extern unsigned int transmitPacketSize(
                        const context::SessionContext::SharedPointer& p_sessionContext);

                extern unsigned int receiveWindowSize(
                        const context::SessionContext::SharedPointer& p_sessionContext);

                extern unsigned int transmissionWindowSize(
                        const context::SessionContext::SharedPointer& p_sessionContext);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_UTILITY_SESSIONCONTEXTUTILITY_HPP_ */
