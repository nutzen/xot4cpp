#ifndef _XOT4CPP_X25_UTILITY_X25PACKETUTILITY_HPP_
#define	_XOT4CPP_X25_UTILITY_X25PACKETUTILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../X25Packet.hpp"
#include "../context/SessionContext.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace utility {

            namespace X25PacketUtility {

                extern X25Packet::SharedPointer callRequest(
                        const context::SessionContext::SharedPointer& p_sessionContext,
                        const Octets& p_userData);

                extern X25Packet::SharedPointer callAccept(
                        const context::SessionContext::SharedPointer& p_sessionContext,
                        const Octets& p_userData = Octets());

                extern X25Packet::SharedPointer clearConfirm(
                        const context::SessionContext::SharedPointer& p_sessionContext);

                extern X25Packet::SharedPointer clearRequest(
                        const context::SessionContext::SharedPointer& p_sessionContext);

                extern X25Packet::SharedPointer data(
                        const context::SessionContext::SharedPointer& p_sessionContext,
                        const Octets& p_userData,
                        const bool& p_moreData);

                extern X25Packet::SharedPointer receiverReady(
                        const context::SessionContext::SharedPointer& p_sessionContext);

                extern X25Packet::SharedPointer resetConfirm(
                        const context::SessionContext::SharedPointer& p_sessionContext);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_UTILITY_X25PACKETUTILITY_HPP_ */
