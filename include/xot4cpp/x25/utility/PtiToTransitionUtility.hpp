#ifndef _XOT4CPP_X25_UTILITY_PTITOTRANSITIONUTILITY_HPP_
#define	_XOT4CPP_X25_UTILITY_PTITOTRANSITIONUTILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../context/RoleEnumerations.hpp"
#include "../pti/PacketTypeIdentifierEnumerations.hpp"
#include "../transitions/TransitionEnumerations.hpp"
#include "../transitions/TransitionParameters.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace utility {

            namespace PtiToTransitionUtility {

                extern transitions::TransitionEnumerations::TransitionEnumeration transition(
                        const transitions::TransitionParameters::SharedPointer& p_parameters);

                extern transitions::TransitionEnumerations::TransitionEnumeration transition(
                        const context::RoleEnumerations::RoleEnumeration& p_role,
                        const pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_packetTypeIdentifier);
            };
        };
    };
};

#endif	/* _XOT4CPP_X25_UTILITY_PTITOTRANSITIONUTILITY_HPP_ */
