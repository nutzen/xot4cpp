#ifndef _XOT4CPP_TRANSPORT_TRANSPORTWRAPPER_HPP_
#define	_XOT4CPP_TRANSPORT_TRANSPORTWRAPPER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../x25/X25Packet.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace transport {

        class TransportWrapper {
        public:
            typedef boost::shared_ptr<TransportWrapper> SharedPointer;

        protected:
            TransportWrapper(void);

        public:
            virtual ~TransportWrapper(void);

            virtual x25::X25Packet::SharedPointer poll(void) = 0;
            virtual void submit(const x25::X25Packet::SharedPointer& p_packet) = 0;


        private:
            TransportWrapper(const TransportWrapper&);
            TransportWrapper & operator =(const TransportWrapper&);

        };
    };
};

#endif	/* _XOT4CPP_TRANSPORT_TRANSPORTWRAPPER_HPP_ */
