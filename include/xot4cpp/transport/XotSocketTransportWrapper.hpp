#ifndef _XOT4CPP_TRANSPORT_XOTSOCKETTRANSPORTWRAPPER_HPP_
#define	_XOT4CPP_TRANSPORT_XOTSOCKETTRANSPORTWRAPPER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "TransportWrapper.hpp"
#include "../stream/TransactionalInputStream.hpp"
#include "../socket/Socket.hpp"

namespace xot4cpp {

    namespace transport {

        class XotSocketTransportWrapper : public TransportWrapper {
        private:
            XotSocketTransportWrapper(
                    const TransportWrapper::SharedPointer& p_wrapper,
                    const stream::TransactionalInputStream::SharedPointer& p_inputStream);

        public:
            virtual ~XotSocketTransportWrapper(void);

            static TransportWrapper::SharedPointer create(const socket::Socket::SharedPointer& p_socket);

            virtual x25::X25Packet::SharedPointer poll(void);
            virtual void submit(const x25::X25Packet::SharedPointer& p_packet);

        private:
            XotSocketTransportWrapper(void);
            XotSocketTransportWrapper(const XotSocketTransportWrapper&);
            XotSocketTransportWrapper & operator =(const XotSocketTransportWrapper&);

            const TransportWrapper::SharedPointer m_wrapper;
            const stream::TransactionalInputStream::SharedPointer m_inputStream;

        };
    };
};

#endif	/* _XOT4CPP_TRANSPORT_XOTSOCKETTRANSPORTWRAPPER_HPP_ */
