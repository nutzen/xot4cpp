#ifndef _XOT4CPP_TRANSPORT_XOTSTREAMTRANSPORTWRAPPER_HPP_
#define	_XOT4CPP_TRANSPORT_XOTSTREAMTRANSPORTWRAPPER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "TransportWrapper.hpp"
#include "../stream/InputStream.hpp"
#include "../stream/OutputStream.hpp"

namespace xot4cpp {

    namespace transport {

        class XotStreamTransportWrapper : public TransportWrapper {
        private:
            XotStreamTransportWrapper(
                    const stream::InputStream::SharedPointer& p_inputStream,
                    const stream::OutputStream::SharedPointer& p_outputStream);
        public:
            virtual ~XotStreamTransportWrapper(void);

            static TransportWrapper::SharedPointer create(
                    const stream::InputStream::SharedPointer& p_inputStream,
                    const stream::OutputStream::SharedPointer& p_outputStream);

            virtual x25::X25Packet::SharedPointer poll(void);
            virtual void submit(const x25::X25Packet::SharedPointer& p_packet);

        private:
            XotStreamTransportWrapper(void);
            XotStreamTransportWrapper(const XotStreamTransportWrapper&);
            XotStreamTransportWrapper & operator =(const XotStreamTransportWrapper&);

            const stream::InputStream::SharedPointer m_inputStream;
            const stream::OutputStream::SharedPointer m_outputStream;

        };
    };
};

#endif	/* _XOT4CPP_TRANSPORT_XOTSTREAMTRANSPORTWRAPPER_HPP_ */
