#ifndef _XOT4CPP_SOCKET_SERVERSOCKET_HPP_
#define	_XOT4CPP_SOCKET_SERVERSOCKET_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Socket.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace socket {

        class ServerSocket {
        public:
            typedef boost::shared_ptr<ServerSocket> SharedPointer;

        private:
            ServerSocket(const int& p_socketFileDescriptor);

        public:
            virtual ~ServerSocket(void);

            static SharedPointer bind(
                    const char* p_port,
                    const char* p_host = 0);

            static SharedPointer bind(
                    const int& p_port,
                    const char* p_host = 0);

            static SharedPointer create(
                    const int& p_socketFileDescriptor);

            Socket::SharedPointer accept(void);

        private:
            ServerSocket(void);
            ServerSocket(const ServerSocket&);
            ServerSocket & operator =(const ServerSocket&);

            const int m_socketFileDescriptor;

        };
    };
};

#endif	/* _XOT4CPP_SOCKET_SERVERSOCKET_HPP_ */
