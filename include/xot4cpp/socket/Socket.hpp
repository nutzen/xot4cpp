#ifndef _XOT4CPP_SOCKET_SOCKET_HPP_
#define	_XOT4CPP_SOCKET_SOCKET_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../TypeDefinitions.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace socket {

        class Socket {
        public:
            typedef boost::shared_ptr<Socket> SharedPointer;

        private:
            Socket(const int& p_socketFileDescriptor);

        public:
            virtual ~Socket(void);

            static SharedPointer connect(
                    const char* p_host,
                    const char* p_port);

            static SharedPointer connect(
                    const char* p_host,
                    const int& p_port);

            static SharedPointer create(
                    const int& p_socketFileDescriptor);

            size_t read(const size_t& p_size, Octet* p_buffer);
            size_t write(const size_t& p_size, const Octet* p_buffer);

        private:
            Socket(void);
            Socket(const Socket&);
            Socket & operator =(const Socket&);

            const int m_socketFileDescriptor;

        };
    };
};

#endif	/* _XOT4CPP_SOCKET_SOCKET_HPP_ */
