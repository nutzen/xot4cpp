#ifndef _XOT4CPP_TYPEDEFINITIONS_HPP_
#define	_XOT4CPP_TYPEDEFINITIONS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <vector>
#include <stack>

namespace xot4cpp {

    typedef unsigned char Octet;
    typedef std::vector<Octet> Octets;

    typedef std::stack<Octet> OctetStack;
    typedef std::deque<Octet> OctetStackSequence;

};

#endif	/* _XOT4CPP_TYPEDEFINITIONS_HPP_ */
