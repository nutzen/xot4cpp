#ifndef _XOT4CPP_STREAM_SOCKETINPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_SOCKETINPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "InputStream.hpp"
#include "../socket/Socket.hpp"

namespace xot4cpp {

    namespace stream {

        class SocketInputStream : public InputStream {
        public:

        private:
            SocketInputStream(const socket::Socket::SharedPointer& p_socket);

        public:
            virtual ~SocketInputStream(void);

            static InputStream::SharedPointer create(const socket::Socket::SharedPointer& p_socket);

            virtual int read(void);
            virtual Octets read(const unsigned int& p_size);

        private:
            SocketInputStream(void);
            SocketInputStream(const SocketInputStream&);
            SocketInputStream & operator =(const SocketInputStream&);

            const socket::Socket::SharedPointer m_socket;

        };
    };
};

#endif	/* _XOT4CPP_STREAM_SOCKETINPUTSTREAM_HPP_ */
