#ifndef _XOT4CPP_STREAM_SOCKETOUTPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_SOCKETOUTPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "OutputStream.hpp"
#include "../socket/Socket.hpp"

namespace xot4cpp {

    namespace stream {

        class SocketOutputStream : public OutputStream {
        public:

        private:
            SocketOutputStream(const socket::Socket::SharedPointer& p_socket);

        public:
            virtual ~SocketOutputStream(void);

            static OutputStream::SharedPointer create(const socket::Socket::SharedPointer& p_socket);

            virtual void write(const Octet& p_source);
            virtual void write(const Octets& p_source);

        private:
            SocketOutputStream(void);
            SocketOutputStream(const SocketOutputStream&);
            SocketOutputStream & operator =(const SocketOutputStream&);

            const socket::Socket::SharedPointer m_socket;

        };
    };
};

#endif	/* _XOT4CPP_STREAM_SOCKETOUTPUTSTREAM_HPP_ */
