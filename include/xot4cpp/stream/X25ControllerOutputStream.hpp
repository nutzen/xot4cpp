#ifndef _XOT4CPP_STREAM_X25CONTROLLEROUTPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_X25CONTROLLEROUTPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "OutputStream.hpp"
#include "../x25/X25Controller.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace stream {

        class X25ControllerOutputStream : public OutputStream {
        private:
            X25ControllerOutputStream(
                    const x25::X25Controller::SharedPointer& p_controller,
                    const OutputStream::SharedPointer& p_outputStream);

        public:
            virtual ~X25ControllerOutputStream(void);

            static OutputStream::SharedPointer create(
                    const x25::X25Controller::SharedPointer& p_controller);

            virtual void write(const Octet& p_source);
            virtual void write(const Octets& p_source);

        private:
            X25ControllerOutputStream(void);
            X25ControllerOutputStream(const X25ControllerOutputStream&);
            X25ControllerOutputStream & operator =(const X25ControllerOutputStream&);

            const x25::X25Controller::SharedPointer m_controller;
            const OutputStream::SharedPointer m_outputStream;

        };
    };
};

#endif	/* _XOT4CPP_STREAM_X25CONTROLLEROUTPUTSTREAM_HPP_ */
