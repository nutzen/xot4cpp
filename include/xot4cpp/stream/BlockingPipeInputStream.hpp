#ifndef _XOT4CPP_STREAM_BLOCKINGPIPEINPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_BLOCKINGPIPEINPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Pipe.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace stream {

        class BlockingPipeInputStream : public InputStream {
        private:
            BlockingPipeInputStream(const Pipe::SharedPointer& p_pipe);

        public:
            virtual ~BlockingPipeInputStream(void);

            static InputStream::SharedPointer create(const Pipe::SharedPointer& p_pipe);

            virtual unsigned int available(void) const;
            virtual int read(void);
            virtual Octets read(const unsigned int& p_size);

        private:
            void waitForData(void) const;

            BlockingPipeInputStream(void);
            BlockingPipeInputStream(const BlockingPipeInputStream&);
            BlockingPipeInputStream & operator =(const BlockingPipeInputStream&);

            const Pipe::SharedPointer m_pipe;

        };
    };
};

#endif	/* _XOT4CPP_STREAM_BLOCKINGPIPEINPUTSTREAM_HPP_ */
