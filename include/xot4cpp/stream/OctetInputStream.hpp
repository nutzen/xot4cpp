#ifndef _XOT4CPP_STREAM_OCTETINPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_OCTETINPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "InputStream.hpp"

namespace xot4cpp {

    namespace stream {

        class OctetInputStream : public InputStream {
        public:
            typedef boost::shared_ptr<OctetInputStream> SharedPointer;

            static SharedPointer downCast(const InputStream::SharedPointer& p_pointer);
            static InputStream::SharedPointer upCast(const SharedPointer& p_pointer);

        private:
            OctetInputStream(const Octets& p_octets);

        public:
            virtual ~OctetInputStream(void);

            static InputStream::SharedPointer create(const Octets& p_octets);

            virtual unsigned int available(void) const;

            virtual int read(void);
            virtual Octets read(const unsigned int& p_size);

        private:
            OctetInputStream(void);
            OctetInputStream(const OctetInputStream&);
            OctetInputStream & operator =(const OctetInputStream&);

            Octets m_octets;

        };

    };
};

#endif	/* _XOT4CPP_STREAM_OCTETINPUTSTREAM_HPP_ */
