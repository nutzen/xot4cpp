#ifndef _XOT4CPP_STREAM_PIPE_HPP_
#define	_XOT4CPP_STREAM_PIPE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "InputStream.hpp"
#include "OutputStream.hpp"

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include <deque>

namespace xot4cpp {

    namespace stream {

        class Pipe : public InputStream, public OutputStream {
        public:
            typedef boost::shared_ptr<Pipe> SharedPointer;
            typedef boost::recursive_mutex Mutex;
            typedef boost::lock_guard<Mutex> MutexGuard;
            typedef std::deque<Octet> Deque;

            static SharedPointer downCast(const InputStream::SharedPointer& p_pointer);
            static InputStream::SharedPointer upCastAsInputStream(const SharedPointer& p_pointer);

            static SharedPointer downCast(const OutputStream::SharedPointer& p_pointer);
            static OutputStream::SharedPointer upCastAsOutputStream(const SharedPointer& p_pointer);

        private:
            Pipe(const Deque& p_octets);

        public:
            virtual ~Pipe(void);

            static SharedPointer create(const Octets& p_octets = Octets());
            virtual SharedPointer copy(void);

            virtual unsigned int available(void) const;

            virtual int read(void);
            virtual Octets read(const unsigned int& p_size);

            virtual void write(const Octet& p_source);
            virtual void write(const Octets& p_source);

        private:
            Pipe(void);
            Pipe(const Pipe&);
            Pipe & operator =(const Pipe&);

            Mutex m_mutex;
            Deque m_octets;

        };
    };
};

#endif	/* _XOT4CPP_STREAM_PIPE_HPP_ */
