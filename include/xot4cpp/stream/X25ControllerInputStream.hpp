#ifndef _XOT4CPP_STREAM_X25CONTROLLERINPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_X25CONTROLLERINPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "InputStream.hpp"
#include "../x25/X25Controller.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace stream {

        class X25ControllerInputStream : public InputStream {
        private:
            X25ControllerInputStream(
                    const x25::X25Controller::SharedPointer& p_controller,
                    const InputStream::SharedPointer& p_inputStream);

        public:
            virtual ~X25ControllerInputStream(void);

            static InputStream::SharedPointer create(
                    const x25::X25Controller::SharedPointer& p_controller);

            virtual unsigned int available(void) const;
            virtual int read(void);
            virtual Octets read(const unsigned int& p_size);

        private:
            X25ControllerInputStream(void);
            X25ControllerInputStream(const X25ControllerInputStream&);
            X25ControllerInputStream & operator =(const X25ControllerInputStream&);

            const x25::X25Controller::SharedPointer m_controller;
            const InputStream::SharedPointer m_inputStream;

        };
    };
};

#endif	/* _XOT4CPP_STREAM_X25CONTROLLERINPUTSTREAM_HPP_ */
