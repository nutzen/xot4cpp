#ifndef _XOT4CPP_STREAM_INPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_INPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../TypeDefinitions.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace stream {

        /**
         * Java style input stream
         *
         * @todo find something better
         */
        class InputStream {
        public:
            typedef boost::shared_ptr<InputStream> SharedPointer;

        protected:
            InputStream(void);

        public:
            virtual ~InputStream(void);

            /**
             * End of stream
             * @return -1
             */
            static int eos(void);

            virtual unsigned int available(void) const;

            /**
             * @return -1 if end of stream; otherwise the next Octet
             */
            virtual int read(void) = 0;
            virtual Octets read(const unsigned int& p_size) = 0;

        private:
            InputStream(const InputStream&);
            InputStream & operator =(const InputStream&);
        };

    };
};

#endif	/* _XOT4CPP_STREAM_INPUTSTREAM_HPP_ */
