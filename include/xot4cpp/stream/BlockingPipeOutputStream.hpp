#ifndef _XOT4CPP_STREAM_BLOCKINGPIPEOUTPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_BLOCKINGPIPEOUTPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Pipe.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace stream {

        class BlockingPipeOutputStream : public OutputStream {
        private:
            BlockingPipeOutputStream(const Pipe::SharedPointer& p_pipe);

        public:
            virtual ~BlockingPipeOutputStream(void);

            static OutputStream::SharedPointer create(const Pipe::SharedPointer& p_pipe);

            virtual void write(const Octet& p_source);
            virtual void write(const Octets& p_source);

        private:
            unsigned int available(void) const;
            void waitForData(void) const;

            BlockingPipeOutputStream(void);
            BlockingPipeOutputStream(const BlockingPipeOutputStream&);
            BlockingPipeOutputStream & operator =(const BlockingPipeOutputStream&);

            const Pipe::SharedPointer m_pipe;

        };
    };
};

#endif	/* _XOT4CPP_STREAM_BLOCKINGPIPEOUTPUTSTREAM_HPP_ */
