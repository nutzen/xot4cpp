#ifndef _XOT4CPP_STREAM_OCTETOUTPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_OCTETOUTPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "OutputStream.hpp"

namespace xot4cpp {

    namespace stream {

        class OctetOutputStream : public OutputStream {
        public:
            typedef boost::shared_ptr<OctetOutputStream> SharedPointer;

            static SharedPointer downCast(const OutputStream::SharedPointer& p_pointer);
            static OutputStream::SharedPointer upCast(const SharedPointer& p_pointer);

        private:
            OctetOutputStream(const Octets& p_octets);

        public:
            virtual ~OctetOutputStream(void);

            static OutputStream::SharedPointer create(const Octets& p_octets = Octets());

            virtual void write(const Octet& p_source);
            virtual void write(const Octets& p_source);

            const Octets& octets(void) const;

        private:
            OctetOutputStream(void);
            OctetOutputStream(const OctetOutputStream&);
            OctetOutputStream & operator =(const OctetOutputStream&);

            Octets m_octets;

        };

    };
};

#endif	/* _XOT4CPP_STREAM_OCTETOUTPUTSTREAM_HPP_ */
