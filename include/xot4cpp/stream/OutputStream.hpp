#ifndef _XOT4CPP_STREAM_OUTPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_OUTPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../TypeDefinitions.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace stream {

        /**
         * Java style output stream
         *
         * @todo find something better
         */
        class OutputStream {
        public:
            typedef boost::shared_ptr<OutputStream> SharedPointer;

        protected:
            OutputStream(void);

        public:
            virtual ~OutputStream(void);

            virtual void write(const Octet& p_source) = 0;
            virtual void write(const Octets& p_source) = 0;

        private:
            OutputStream(const OutputStream&);
            OutputStream & operator =(const OutputStream&);
        };

    };
};

#endif	/* _XOT4CPP_STREAM_OUTPUTSTREAM_HPP_ */
