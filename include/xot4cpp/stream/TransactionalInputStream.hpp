#ifndef _XOT4CPP_STREAM_TRANSACTIONALINPUTSTREAM_HPP_
#define	_XOT4CPP_STREAM_TRANSACTIONALINPUTSTREAM_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Pipe.hpp"

#include <boost/shared_ptr.hpp>

namespace xot4cpp {

    namespace stream {

        class TransactionalInputStream : public InputStream {
        public:
            typedef boost::shared_ptr<TransactionalInputStream> SharedPointer;
            typedef boost::recursive_mutex Mutex;
            typedef boost::lock_guard<Mutex> MutexGuard;

            static SharedPointer downCast(const InputStream::SharedPointer& p_pointer);
            static InputStream::SharedPointer upCast(const SharedPointer& p_pointer);

        private:
            TransactionalInputStream(const InputStream::SharedPointer& p_source,
                    const Pipe::SharedPointer& p_reference,
                    const Pipe::SharedPointer& p_sandbox);

        public:
            virtual ~TransactionalInputStream(void);

            static SharedPointer create(const InputStream::SharedPointer& p_source);

            void commit(void);
            void rollback(void);

            virtual unsigned int available(void) const;
            virtual int read(void);
            virtual Octets read(const unsigned int& p_size);

        private:
            int readAheadIfNecessary(const unsigned int& p_required);
            int readAhead(const unsigned int& p_size);

            TransactionalInputStream(void);
            TransactionalInputStream(const TransactionalInputStream&);
            TransactionalInputStream & operator =(const TransactionalInputStream&);

            Mutex m_mutex;
            const InputStream::SharedPointer m_source;
            Pipe::SharedPointer m_reference;
            Pipe::SharedPointer m_sandbox;

        };
    };
};

#endif	/* _XOT4CPP_STREAM_TRANSACTIONALINPUTSTREAM_HPP_ */
