#ifndef _XOT4CPP_STREAM_STREAMUTILITY_HPP_
#define	_XOT4CPP_STREAM_STREAMUTILITY_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "InputStream.hpp"
#include "OutputStream.hpp"

namespace xot4cpp {

    namespace stream {

        namespace StreamUtility {

            extern Octets readBuffer(const InputStream::SharedPointer& p_inputStream, const unsigned int& p_length);
            extern unsigned short readUnsignedShort(const InputStream::SharedPointer& p_inputStream);

            extern void write(const OutputStream::SharedPointer& p_outputStream, const unsigned short& p_value);
        };
    };
};

#endif	/* _XOT4CPP_STREAM_STREAMUTILITY_HPP_ */
