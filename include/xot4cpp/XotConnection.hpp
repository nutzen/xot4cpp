#ifndef _XOT4CPP_XOTCONNECTION_HPP_
#define	_XOT4CPP_XOTCONNECTION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "x25/context/SystemContext.hpp"
#include "socket/Socket.hpp"
#include "stream/InputStream.hpp"
#include "stream/OutputStream.hpp"
#include "x121/X121Address.hpp"
#include "TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    class XotConnection {
    public:
        typedef boost::shared_ptr<XotConnection> SharedPointer;

    protected:
        XotConnection(void);

    public:
        virtual ~XotConnection(void);

        static XotConnection::SharedPointer accept(
                const socket::Socket::SharedPointer& p_socket,
                const x25::context::SystemContext::SharedPointer& p_systemContext);

        static XotConnection::SharedPointer call(
                const socket::Socket::SharedPointer& p_socket,
                const x25::context::SystemContext::SharedPointer& p_systemContext,
                const x121::X121Address::SharedPointer& p_remoteAddress,
                const Octets& p_userData = Octets(1, 0xc4));

        virtual const x121::X121Address::SharedPointer& remoteAddress(void) const = 0;
        virtual void disconnect(void) = 0;
        virtual stream::InputStream::SharedPointer inputStream(void) const = 0;
        virtual stream::OutputStream::SharedPointer outputStream(void) const = 0;

    private:
        XotConnection(const XotConnection&);
        XotConnection & operator =(const XotConnection&);

    };
};

#endif	/* _XOT4CPP_XOTCONNECTION_HPP_ */
