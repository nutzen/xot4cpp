#ifndef _XOT4CPP_LOGGER_LOGGER_HPP_
#define	_XOT4CPP_LOGGER_LOGGER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <sstream>
#include <string>

namespace xot4cpp {

    namespace logger {

        /**
         * @todo find something better
         */
        namespace Logger {

            extern void debug(
                    const char* p_file,
                    const int& p_line,
                    const std::string& p_message);

            extern void trace(
                    const char* p_file,
                    const int& p_line,
                    const std::string& p_message);
        };
    };
};

#define XOT4CPP_DEBUG( p_message ) \
{ \
    std::stringstream _xot4cpp_logger_Logger_stringstream_; \
    _xot4cpp_logger_Logger_stringstream_ << p_message; \
    xot4cpp::logger::Logger::debug(__FILE__, __LINE__, _xot4cpp_logger_Logger_stringstream_.str()); \
}

#define XOT4CPP_TRACE( p_message ) \
{ \
    std::stringstream _xot4cpp_logger_Logger_stringstream_; \
    _xot4cpp_logger_Logger_stringstream_ << p_message; \
    xot4cpp::logger::Logger::trace(__FILE__, __LINE__, _xot4cpp_logger_Logger_stringstream_.str()); \
}

#endif	/* _XOT4CPP_LOGGER_LOGGER_HPP_ */
