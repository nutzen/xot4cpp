#ifndef _XOT4CPP_EXCEPTIONS_SOCKETTIMEOUTEXCEPTION_HPP_
#define	_XOT4CPP_EXCEPTIONS_SOCKETTIMEOUTEXCEPTION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "IOException.hpp"

namespace xot4cpp {

    namespace exceptions {

        class SocketTimeoutException : public IOException {
        protected:
            SocketTimeoutException(const std::string& p_message);

        public:
            SocketTimeoutException(const SocketTimeoutException& p_original);
            SocketTimeoutException & operator =(const SocketTimeoutException& p_original);
            virtual ~SocketTimeoutException(void) throw ();

            static SocketTimeoutException create(const char* p_file, const int& p_line, const std::string& p_message);

        private:
            SocketTimeoutException(void);

        };
    };
};

#define THROW_XOT4CPP_SOCKETTIMEOUTEXCEPTION( p_message ) \
{ \
    std::stringstream _xot4cpp_exceptions_SocketTimeoutException_stringstream_; \
    _xot4cpp_exceptions_SocketTimeoutException_stringstream_ << p_message; \
    throw xot4cpp::exceptions::SocketTimeoutException::create(__FILE__, __LINE__, _xot4cpp_exceptions_SocketTimeoutException_stringstream_.str()); \
}

#endif	/* _XOT4CPP_EXCEPTIONS_SOCKETTIMEOUTEXCEPTION_HPP_ */
