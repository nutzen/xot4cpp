#ifndef _XOT4CPP_EXCEPTIONS_XOT4CPPEXCEPTION_HPP_
#define	_XOT4CPP_EXCEPTIONS_XOT4CPPEXCEPTION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <exception>
#include <sstream>
#include <string>

namespace xot4cpp {

    namespace exceptions {

        class Xot4cppException : public std::exception {
        protected:
            Xot4cppException(const std::string& p_message);

        public:
            Xot4cppException(const Xot4cppException& p_original);
            Xot4cppException & operator =(const Xot4cppException& p_original);
            virtual ~Xot4cppException(void) throw ();

            virtual const char* what() const throw ();

            static Xot4cppException create(const char* p_file, const int& p_line, const std::string& p_message);

        protected:
            std::string m_message;

        private:
            Xot4cppException(void);

        };
    };
};

#define THROW_XOT4CPP_EXCEPTION( p_message ) \
{ \
    std::stringstream _xot4cpp_exceptions_Xot4cppException_stringstream_; \
    _xot4cpp_exceptions_Xot4cppException_stringstream_ << p_message; \
    throw xot4cpp::exceptions::Xot4cppException::create(__FILE__, __LINE__, _xot4cpp_exceptions_Xot4cppException_stringstream_.str()); \
}

#endif	/* _XOT4CPP_EXCEPTIONS_XOT4CPPEXCEPTION_HPP_ */
