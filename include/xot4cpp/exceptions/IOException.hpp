#ifndef _XOT4CPP_EXCEPTIONS_IOEXCEPTION_HPP_
#define	_XOT4CPP_EXCEPTIONS_IOEXCEPTION_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Xot4cppException.hpp"

namespace xot4cpp {

    namespace exceptions {

        class IOException : public Xot4cppException {
        protected:
            IOException(const std::string& p_message);

        public:
            IOException(const IOException& p_original);
            IOException & operator =(const IOException& p_original);
            virtual ~IOException(void) throw ();

            static IOException create(const char* p_file, const int& p_line, const std::string& p_message);

        private:
            IOException(void);

        };
    };
};

#define THROW_XOT4CPP_IOEXCEPTION( p_message ) \
{ \
    std::stringstream _xot4cpp_exceptions_IOException_stringstream_; \
    _xot4cpp_exceptions_IOException_stringstream_ << p_message; \
    throw xot4cpp::exceptions::IOException::create(__FILE__, __LINE__, _xot4cpp_exceptions_IOException_stringstream_.str()); \
}

#endif	/* _XOT4CPP_EXCEPTIONS_IOEXCEPTION_HPP_ */
