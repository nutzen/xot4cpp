#ifndef _XOT4CPP_EXCEPTIONS_X25STATETRANSITIONFAILURE_HPP_
#define	_XOT4CPP_EXCEPTIONS_X25STATETRANSITIONFAILURE_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "Xot4cppException.hpp"

namespace xot4cpp {

    namespace exceptions {

        class X25StateTransitionFailure : public Xot4cppException {
        protected:
            X25StateTransitionFailure(const std::string& p_message);

        public:
            X25StateTransitionFailure(const X25StateTransitionFailure& p_original);
            X25StateTransitionFailure & operator =(const X25StateTransitionFailure& p_original);
            virtual ~X25StateTransitionFailure(void) throw ();

            static X25StateTransitionFailure create(const char* p_file, const int& p_line, const std::string& p_message);

        private:
            X25StateTransitionFailure(void);

        };
    };
};

#define THROW_XOT4CPP_X25STATETRANSITIONFAILURE( p_message ) \
{ \
    std::stringstream _xot4cpp_exceptions_X25StateTransitionFailure_stringstream_; \
    _xot4cpp_exceptions_X25StateTransitionFailure_stringstream_ << p_message; \
    throw xot4cpp::exceptions::X25StateTransitionFailure::create(__FILE__, __LINE__, _xot4cpp_exceptions_X25StateTransitionFailure_stringstream_.str()); \
}

#endif	/* _XOT4CPP_EXCEPTIONS_X25STATETRANSITIONFAILURE_HPP_ */
