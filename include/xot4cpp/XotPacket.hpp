#ifndef _XOT4CPP_XOTPACKET_HPP_
#define	_XOT4CPP_XOTPACKET_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define XOT_VERSION 0

#include "x25/X25Packet.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    class XotPacket {
    public:
        typedef boost::shared_ptr<XotPacket> SharedPointer;

    private:
        XotPacket(const x25::X25Packet::SharedPointer& p_packet,
                const unsigned int& p_version);

    public:
        virtual ~XotPacket(void);

        static SharedPointer create(const x25::X25Packet::SharedPointer& p_packet,
                const unsigned int& p_version = XOT_VERSION);

        const x25::X25Packet::SharedPointer& packet(void) const;
        const unsigned int& version(void) const;

    private:
        XotPacket(void);
        XotPacket(const XotPacket&);
        XotPacket & operator =(const XotPacket&);

        const x25::X25Packet::SharedPointer m_packet;
        const unsigned int m_version;

    };
};

#endif	/* _XOT4CPP_XOTPACKET_HPP_ */
