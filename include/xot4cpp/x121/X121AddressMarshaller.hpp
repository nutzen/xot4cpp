#ifndef _XOT4CPP_X121_X121ADDRESSMARSHALLER_HPP_
#define	_XOT4CPP_X121_X121ADDRESSMARSHALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X121Address.hpp"
#include "X121AddressPair.hpp"
#include "../stream/InputStream.hpp"
#include "../stream/OutputStream.hpp"

namespace xot4cpp {

    namespace x121 {

        namespace X121AddressMarshaller {

            extern void marshal(
                    const stream::OutputStream::SharedPointer& p_outputStream,
                    const X121Address::SharedPointer& p_calledAddress,
                    const X121Address::SharedPointer& p_callingAddress,
                    const bool p_aBit);

            extern X121AddressPair::SharedPointer unmarshal(
                    const stream::InputStream::SharedPointer& p_inputStream,
                    const bool p_aBit);
        };
    };
};

#endif	/* _XOT4CPP_X121_X121ADDRESSMARSHALLER_HPP_ */

