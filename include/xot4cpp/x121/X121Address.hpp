#ifndef _XOT4CPP_X121_X121ADDRESS_HPP_
#define	_XOT4CPP_X121_X121ADDRESS_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "../TypeDefinitions.hpp"

#include <boost/smart_ptr.hpp>
#include <string>

namespace xot4cpp {

    namespace x121 {

        class X121Address {
        public:
            typedef boost::shared_ptr<X121Address> SharedPointer;

        private:
            X121Address(const std::string& p_address, const Octets& p_addressAsOctets);

        public:
            static SharedPointer create(const std::string& p_address);

            virtual ~X121Address(void);

            const std::string& address(void) const;
            const Octets& addressAsOctets(void) const;

        protected:
            static Octets extractAddress(const std::string& p_address);

        private:
            X121Address(void);
            X121Address(const X121Address&);
            X121Address & operator =(const X121Address&);

            const std::string m_address;
            const Octets m_addressAsOctets;
        };
    };
};

#endif	/* _XOT4CPP_X121_X121ADDRESS_HPP_ */
