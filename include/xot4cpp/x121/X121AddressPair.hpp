#ifndef _XOT4CPP_X121_X121ADDRESSPAIR_HPP_
#define	_XOT4CPP_X121_X121ADDRESSPAIR_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "X121Address.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace x121 {

        class X121AddressPair {
        public:
            typedef boost::shared_ptr<X121AddressPair> SharedPointer;

        protected:
            X121AddressPair(
                    const x121::X121Address::SharedPointer& p_calledAddress,
                    const x121::X121Address::SharedPointer& p_callingAddress);

        public:
            virtual ~X121AddressPair(void);

            static SharedPointer create(
                    const x121::X121Address::SharedPointer& p_calledAddress,
                    const x121::X121Address::SharedPointer& p_callingAddress);

            const x121::X121Address::SharedPointer& calledAddress(void) const;
            const x121::X121Address::SharedPointer& callingAddress(void) const;

        private:
            X121AddressPair(void);
            X121AddressPair(const X121AddressPair&);
            X121AddressPair & operator =(const X121AddressPair&);

            const x121::X121Address::SharedPointer m_calledAddress;
            const x121::X121Address::SharedPointer m_callingAddress;
        };
    };
};


#endif	/* _XOT4CPP_X121_X121ADDRESSPAIR_HPP_ */
