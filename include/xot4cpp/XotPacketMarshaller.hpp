#ifndef _XOT4CPP_XOTPACKETMARSHALLER_HPP_
#define	_XOT4CPP_XOTPACKETMARSHALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "XotPacket.hpp"

#include "stream/InputStream.hpp"
#include "stream/OutputStream.hpp"

#include <boost/smart_ptr.hpp>

namespace xot4cpp {

    namespace XotPacketMarshaller {

        extern void marshal(const stream::OutputStream::SharedPointer& p_outputStream, const XotPacket::SharedPointer& p_source);

        extern XotPacket::SharedPointer unmarshal(const stream::InputStream::SharedPointer& p_inputStream);
    };
};

#endif	/* _XOT4CPP_XOTPACKETMARSHALLER_HPP_ */
