#ifndef _XOT4CPP_EXAMPLE_SIMPLECALLER_HPP_
#define	_XOT4CPP_EXAMPLE_SIMPLECALLER_HPP_

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

namespace xot4cpp {

    namespace example {

        namespace SimpleCaller {

            extern int main(int p_argc, char** p_argv);
        };
    };
};

#endif	/* _XOT4CPP_EXAMPLE_SIMPLECALLER_HPP_ */
