#include "xot4cpp/x121/X121Address.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <cmath>

xot4cpp::x121::X121Address::X121Address(const std::string& p_address, const Octets& p_addressAsOctets)
: m_address(p_address),
m_addressAsOctets(p_addressAsOctets) {
}

xot4cpp::x121::X121Address::SharedPointer xot4cpp::x121::X121Address::create(const std::string& p_address) {

    const Octets addressAsOctets(X121Address::extractAddress(p_address));

    return SharedPointer(new X121Address(p_address, addressAsOctets));
}

xot4cpp::x121::X121Address::~X121Address(void) {
}

const std::string& xot4cpp::x121::X121Address::address(void) const {

    return m_address;
}

const xot4cpp::Octets& xot4cpp::x121::X121Address::addressAsOctets(void) const {

    return m_addressAsOctets;
}

xot4cpp::Octets xot4cpp::x121::X121Address::extractAddress(const std::string& p_address) {

    Octets destination;
    const unsigned int addressSize = p_address.size();
    const int length = static_cast<int> (std::ceil(static_cast<double> (addressSize) / 2));
    destination.resize(length, 0);

    for (unsigned int index = 0; addressSize > index; index += 2) {

        const unsigned int v0 = p_address[index];
        unsigned int v1;

        if ((addressSize - 1) > index) {
            v1 = p_address[index + 1];
        } else {
            v1 = 0;
        }

        destination[index / 2] = static_cast<Octet> (((v0 << 4) & 0x00f0) | (v1 & 0x000f));
    }

    return destination;
}
