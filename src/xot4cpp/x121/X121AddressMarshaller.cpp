#include "xot4cpp/x121/X121AddressMarshaller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <sstream>

void xot4cpp::x121::X121AddressMarshaller::marshal(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const X121Address::SharedPointer& p_calledAddress,
        const X121Address::SharedPointer& p_callingAddress,
        const bool p_aBit) {

    if (p_aBit) {
        p_outputStream->write(((p_calledAddress->address().size() << 4) & 0x00f0) | (p_callingAddress->address().size() & 0x000f));
    } else {
        p_outputStream->write(((p_callingAddress->address().size() << 4) & 0x00f0) | (p_calledAddress->address().size() & 0x000f));
    }

    const Octets& called = p_calledAddress->addressAsOctets();
    const Octets& calling = p_callingAddress->addressAsOctets();

    if (0 == (p_calledAddress->address().size() % 2)) {
        p_outputStream->write(called);
        p_outputStream->write(calling);

    } else {
        p_outputStream->write(Octets(called.begin(), called.end() - 1));
        Octet temporary = static_cast<Octet> (0x00f0 & called[called.size() - 1]);

        for (int counter = 0; p_callingAddress->address().size() > counter; counter += 2) {

            const int index = counter / 2;

            temporary = static_cast<Octet> ((0x00f0 & temporary) | (0x000f & (calling[index] >> 4)));
            p_outputStream->write(temporary);

            temporary = static_cast<Octet> (0x00f0 & (calling[index] << 4));
            if (p_callingAddress->address().size() == (2 + counter)) {
                p_outputStream->write(temporary);
            }
        }
    }
}

xot4cpp::x121::X121AddressPair::SharedPointer xot4cpp::x121::X121AddressMarshaller::unmarshal(
        const stream::InputStream::SharedPointer& p_inputStream,
        const bool p_aBit) {

    const unsigned int lengths = static_cast<unsigned int> (0x00ff & p_inputStream->read());
    unsigned int calledLength;
    unsigned int callingLength;

    if (p_aBit) {
        calledLength = static_cast<unsigned int> (0x00f0 & lengths) >> 4;
        callingLength = (0x000f & lengths);
    } else {
        callingLength = (0x00f0 & lengths) >> 4;
        calledLength = static_cast<unsigned int> (0x000f & lengths);
    }

    const unsigned int totalLength = calledLength + callingLength;
    unsigned int streamLength = 0;
    std::stringstream stringstream;
    stringstream << std::hex;

    while (totalLength > streamLength) {
        const unsigned int value = static_cast<unsigned int> (0x00ff & p_inputStream->read());

        const unsigned int value0 = static_cast<unsigned int> (0x00f0 & value) >> 4;
        stringstream << value0;
        ++streamLength;

        const unsigned int value1 = static_cast<unsigned int> (0x000f & value);
        stringstream << value1;
        ++streamLength;
    }

    const std::string addressBlock = stringstream.str();
    const std::string calledAddressText = addressBlock.substr(0, calledLength);
    const std::string callingAddressText = addressBlock.substr(calledLength, callingLength);

    const X121Address::SharedPointer calledAddress = X121Address::create(calledAddressText);
    const X121Address::SharedPointer callingAddress = X121Address::create(callingAddressText);

    return X121AddressPair::create(calledAddress, callingAddress);
}
