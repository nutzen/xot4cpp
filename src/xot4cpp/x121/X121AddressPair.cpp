#include "xot4cpp/x121/X121AddressPair.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x121::X121AddressPair::X121AddressPair(
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress)
: m_calledAddress(p_calledAddress),
m_callingAddress(p_callingAddress) {
}

xot4cpp::x121::X121AddressPair::~X121AddressPair(void) {
}

xot4cpp::x121::X121AddressPair::SharedPointer xot4cpp::x121::X121AddressPair::create(
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress) {

    const SharedPointer pointer(new X121AddressPair(p_calledAddress, p_callingAddress));
    return pointer;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x121::X121AddressPair::calledAddress(void) const {

    return m_calledAddress;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x121::X121AddressPair::callingAddress(void) const {

    return m_callingAddress;
}
