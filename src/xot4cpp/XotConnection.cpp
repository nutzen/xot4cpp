#include "xot4cpp/XotConnection.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/stream/X25ControllerInputStream.hpp"
#include "xot4cpp/stream/X25ControllerOutputStream.hpp"
#include "xot4cpp/transport/XotSocketTransportWrapper.hpp"
#include "xot4cpp/x25/context/SessionContext.hpp"
#include "xot4cpp/x25/X25Controller.hpp"

namespace xot4cpp {

    extern xot4cpp::x25::context::SessionContext::SharedPointer createSessionContext(
            const xot4cpp::x25::context::SystemContext::SharedPointer& p_systemContext,
            const xot4cpp::x121::X121Address::SharedPointer& p_remoteAddress = xot4cpp::x121::X121Address::SharedPointer());

    class BasicXotConnection : public XotConnection {
    private:
        BasicXotConnection(
                const x25::context::SessionContext::SharedPointer& p_sessionContext,
                const x25::X25Controller::SharedPointer& p_controller,
                const stream::InputStream::SharedPointer& p_inputStream,
                const stream::OutputStream::SharedPointer& p_outputStream);

    public:
        virtual ~BasicXotConnection(void);

        static XotConnection::SharedPointer create(
                const x25::context::SessionContext::SharedPointer& p_sessionContext,
                const x25::X25Controller::SharedPointer& p_controller);

        virtual const x121::X121Address::SharedPointer& remoteAddress(void) const;
        virtual void disconnect(void);
        virtual stream::InputStream::SharedPointer inputStream(void) const;
        virtual stream::OutputStream::SharedPointer outputStream(void) const;

    private:
        BasicXotConnection(void);
        BasicXotConnection(const BasicXotConnection&);
        BasicXotConnection & operator =(const BasicXotConnection&);

        const x25::context::SessionContext::SharedPointer m_sessionContext;
        const x25::X25Controller::SharedPointer m_controller;
        const stream::InputStream::SharedPointer m_inputStream;
        const stream::OutputStream::SharedPointer m_outputStream;

    };
};

xot4cpp::XotConnection::XotConnection(void) {
}

xot4cpp::XotConnection::~XotConnection(void) {
}

xot4cpp::XotConnection::SharedPointer xot4cpp::XotConnection::accept(
        const socket::Socket::SharedPointer& p_socket,
        const x25::context::SystemContext::SharedPointer& p_systemContext) {

    const transport::TransportWrapper::SharedPointer wrapper = transport::XotSocketTransportWrapper::create(p_socket);
    const x25::context::SessionContext::SharedPointer sessionContext = createSessionContext(p_systemContext);

    const x25::X25Controller::SharedPointer controller = x25::X25Controller::create(sessionContext, wrapper);
    controller->accept();

    return BasicXotConnection::create(sessionContext, controller);
}

xot4cpp::XotConnection::SharedPointer xot4cpp::XotConnection::call(
        const socket::Socket::SharedPointer& p_socket,
        const x25::context::SystemContext::SharedPointer& p_systemContext,
        const x121::X121Address::SharedPointer& p_remoteAddress,
        const Octets& p_userData) {

    const transport::TransportWrapper::SharedPointer wrapper = transport::XotSocketTransportWrapper::create(p_socket);
    const x25::context::SessionContext::SharedPointer sessionContext = createSessionContext(p_systemContext, p_remoteAddress);

    const x25::X25Controller::SharedPointer controller = x25::X25Controller::create(sessionContext, wrapper);
    controller->connect(p_userData);

    return BasicXotConnection::create(sessionContext, controller);
}

xot4cpp::x25::context::SessionContext::SharedPointer xot4cpp::createSessionContext(
        const xot4cpp::x25::context::SystemContext::SharedPointer& p_systemContext,
        const xot4cpp::x121::X121Address::SharedPointer& p_remoteAddress) {

    const x25::lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier = x25::lci::LogicalChannelIdentifier::create(p_systemContext->nextLci());
    const x121::X121Address::SharedPointer calledAddress = p_remoteAddress;
    const x121::X121Address::SharedPointer callingAddress = p_systemContext->localAddress();
    const x25::facilities::Facility::List facilities = p_systemContext->facilities();

    return x25::context::SessionContext::create(callingAddress, facilities, calledAddress, logicalChannelIdentifier);
}

xot4cpp::BasicXotConnection::BasicXotConnection(
        const x25::context::SessionContext::SharedPointer& p_sessionContext,
        const x25::X25Controller::SharedPointer& p_controller,
        const stream::InputStream::SharedPointer& p_inputStream,
        const stream::OutputStream::SharedPointer& p_outputStream)
: m_sessionContext(p_sessionContext),
m_controller(p_controller),
m_inputStream(p_inputStream),
m_outputStream(p_outputStream) {
}

xot4cpp::BasicXotConnection::~BasicXotConnection(void) {
}

xot4cpp::XotConnection::SharedPointer xot4cpp::BasicXotConnection::create(
        const x25::context::SessionContext::SharedPointer& p_sessionContext,
        const x25::X25Controller::SharedPointer& p_controller) {

    const stream::InputStream::SharedPointer inputStream =
            stream::X25ControllerInputStream::create(p_controller);
    const stream::OutputStream::SharedPointer outputStream =
            stream::X25ControllerOutputStream::create(p_controller);

    const XotConnection::SharedPointer pointer(new BasicXotConnection(
            p_sessionContext,
            p_controller,
            inputStream,
            outputStream));

    return pointer;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::BasicXotConnection::remoteAddress(void) const {

    return m_sessionContext->remoteAddress();
}

void xot4cpp::BasicXotConnection::disconnect(void) {

    m_controller->disconnect();
}

xot4cpp::stream::InputStream::SharedPointer xot4cpp::BasicXotConnection::inputStream(void) const {

    return m_inputStream;
}

xot4cpp::stream::OutputStream::SharedPointer xot4cpp::BasicXotConnection::outputStream(void) const {

    return m_outputStream;
}
