#include "xot4cpp/transport/XotSocketTransportWrapper.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/SocketTimeoutException.hpp"
#include "xot4cpp/stream/SocketInputStream.hpp"
#include "xot4cpp/stream/SocketOutputStream.hpp"
#include "xot4cpp/transport/XotStreamTransportWrapper.hpp"

xot4cpp::transport::XotSocketTransportWrapper::XotSocketTransportWrapper(
        const TransportWrapper::SharedPointer& p_wrapper,
        const stream::TransactionalInputStream::SharedPointer& p_inputStream)
: m_wrapper(p_wrapper),
m_inputStream(p_inputStream) {
}

xot4cpp::transport::XotSocketTransportWrapper::~XotSocketTransportWrapper(void) {
}

xot4cpp::transport::TransportWrapper::SharedPointer xot4cpp::transport::XotSocketTransportWrapper::create(const socket::Socket::SharedPointer& p_socket) {

    const stream::TransactionalInputStream::SharedPointer inputStream =
            stream::TransactionalInputStream::create(stream::SocketInputStream::create(p_socket));
    const stream::OutputStream::SharedPointer outputStream = stream::SocketOutputStream::create(p_socket);
    const TransportWrapper::SharedPointer wrapper = XotStreamTransportWrapper::create(
            stream::TransactionalInputStream::upCast(inputStream), outputStream);

    return TransportWrapper::SharedPointer(new XotSocketTransportWrapper(wrapper, inputStream));
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::transport::XotSocketTransportWrapper::poll(void) {

    x25::X25Packet::SharedPointer packet;

    try {
        packet = m_wrapper->poll();
        m_inputStream->commit();

    } catch (exceptions::SocketTimeoutException& e) {
        // ignore
        m_inputStream->rollback();
    }

    return packet;
}

void xot4cpp::transport::XotSocketTransportWrapper::submit(const x25::X25Packet::SharedPointer& p_packet) {

    m_wrapper->submit(p_packet);
}
