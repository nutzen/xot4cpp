#include "xot4cpp/transport/XotStreamTransportWrapper.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/XotPacketMarshaller.hpp"

xot4cpp::transport::XotStreamTransportWrapper::XotStreamTransportWrapper(
        const stream::InputStream::SharedPointer& p_inputStream,
        const stream::OutputStream::SharedPointer& p_outputStream)
: m_inputStream(p_inputStream),
m_outputStream(p_outputStream) {
}

xot4cpp::transport::XotStreamTransportWrapper::~XotStreamTransportWrapper(void) {
}

xot4cpp::transport::TransportWrapper::SharedPointer xot4cpp::transport::XotStreamTransportWrapper::create(
        const stream::InputStream::SharedPointer& p_inputStream,
        const stream::OutputStream::SharedPointer& p_outputStream) {

    const TransportWrapper::SharedPointer pointer(new XotStreamTransportWrapper(p_inputStream, p_outputStream));
    return pointer;
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::transport::XotStreamTransportWrapper::poll(void) {

    const XotPacket::SharedPointer packet = XotPacketMarshaller::unmarshal(m_inputStream);
    return packet->packet();
}

void xot4cpp::transport::XotStreamTransportWrapper::submit(const x25::X25Packet::SharedPointer& p_packet) {

    const XotPacket::SharedPointer packet = XotPacket::create(p_packet);
    XotPacketMarshaller::marshal(m_outputStream, packet);
}
