#include "xot4cpp/logger/Logger.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>

namespace xot4cpp {

    namespace logger {

        namespace Logger {

            extern void log(
                    std::ostream& p_ostream,
                    const char* p_level,
                    const char* p_file,
                    const int& p_line,
                    const std::string& p_message);
        };
    };
};

void xot4cpp::logger::Logger::debug(
        const char* p_file,
        const int& p_line,
        const std::string& p_message) {

    log(std::cerr, "DEBUG", p_file, p_line, p_message);
}

void xot4cpp::logger::Logger::trace(
        const char* p_file,
        const int& p_line,
        const std::string& p_message) {

    log(std::cout, "TRACE", p_file, p_line, p_message);
}

void xot4cpp::logger::Logger::log(
        std::ostream& p_ostream,
        const char* p_level,
        const char* p_file,
        const int& p_line,
        const std::string& p_message) {

    std::stringstream stringstream;
    stringstream << p_level;
    stringstream << " [" << p_file << ":" << p_line << "] ";
    stringstream << p_message;

    p_ostream << stringstream.str() << std::endl;
}