#include "xot4cpp/example/SimpleCaller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/XotConnection.hpp>
#include <xot4cpp/logger/Logger.hpp>

#include <boost/thread.hpp>
#include <string>

namespace xot4cpp {

    namespace example {

        namespace SimpleCaller {

            extern xot4cpp::Octets octets(const std::string& p_string);
        };
    };
};

int xot4cpp::example::SimpleCaller::main(int /*p_argc*/, char** /*p_argv*/) {

    int rv = 0;
    try {
        XOT4CPP_TRACE("connect");
        const xot4cpp::socket::Socket::SharedPointer socket =
                xot4cpp::socket::Socket::connect("10.10.10.2", "1998");
        const xot4cpp::x25::context::SystemContext::SharedPointer systemContext =
                xot4cpp::x25::context::SystemContext::create(
                xot4cpp::x121::X121Address::create("11009000"));
        XOT4CPP_TRACE("call");
        const xot4cpp::XotConnection::SharedPointer connection = xot4cpp::XotConnection::call(
                socket,
                systemContext,
                xot4cpp::x121::X121Address::create("1634567"),
                xot4cpp::Octets(1, 0xc4));
        boost::this_thread::sleep(boost::posix_time::seconds(10));

        XOT4CPP_TRACE("data");
        connection->outputStream()->write(octets(
                "hello world!                                                                                                                            "));
        boost::this_thread::sleep(boost::posix_time::seconds(30));

        XOT4CPP_TRACE("clear");
        connection->disconnect();

    } catch (std::exception& e) {
        XOT4CPP_DEBUG("Exception: " << e.what());
        rv = -1;
    }

    return rv;
}

xot4cpp::Octets xot4cpp::example::SimpleCaller::octets(const std::string& p_string) {

    xot4cpp::Octets octets;

    for (unsigned int index = 0; p_string.size() > index; ++index) {

        xot4cpp::Octet octet = static_cast<xot4cpp::Octet> (p_string[index]);
        octets.push_back(octet);
    }

    return octets;
}
