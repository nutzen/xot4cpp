#include "xot4cpp/example/SimpleReceiver.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/XotConnection.hpp>
#include <xot4cpp/logger/Logger.hpp>
#include <xot4cpp/socket/ServerSocket.hpp>

#include <string>

namespace xot4cpp {

    namespace example {

        namespace SimpleReceiver {

            extern xot4cpp::Octets octets(const std::string& p_string);
        };
    };
};

int xot4cpp::example::SimpleReceiver::main(int /*p_argc*/, char** /*p_argv*/) {

    int rv = 0;
    try {
        const xot4cpp::x25::context::SystemContext::SharedPointer systemContext =
                xot4cpp::x25::context::SystemContext::create(
                xot4cpp::x121::X121Address::create("1234567"));
        XOT4CPP_TRACE("bind");
        const xot4cpp::socket::ServerSocket::SharedPointer serverSocket = xot4cpp::socket::ServerSocket::bind("1998");

        while (true) {
            const xot4cpp::socket::Socket::SharedPointer socket =
                    serverSocket->accept();
            XOT4CPP_TRACE("accept");
            const xot4cpp::XotConnection::SharedPointer connection = xot4cpp::XotConnection::accept(
                    socket,
                    systemContext);

            XOT4CPP_TRACE("I received a call from " << connection->remoteAddress()->address());

            connection->outputStream()->write(octets("sorry mate the lights are on, but nobody is home."));

            connection->disconnect();
        }

    } catch (std::exception& e) {
        XOT4CPP_DEBUG("Exception: " << e.what());
        rv = -1;
    }

    return rv;
}

xot4cpp::Octets xot4cpp::example::SimpleReceiver::octets(const std::string& p_string) {

    xot4cpp::Octets octets;

    for (unsigned int index = 0; p_string.size() > index; ++index) {

        xot4cpp::Octet octet = static_cast<xot4cpp::Octet> (p_string[index]);
        octets.push_back(octet);
    }

    return octets;
}
