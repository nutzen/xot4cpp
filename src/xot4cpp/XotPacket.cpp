#include "xot4cpp/XotPacket.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::XotPacket::XotPacket(const x25::X25Packet::SharedPointer& p_packet,
        const unsigned int& p_version)
: m_packet(p_packet),
m_version(p_version) {
}

xot4cpp::XotPacket::~XotPacket(void) {
}

xot4cpp::XotPacket::SharedPointer xot4cpp::XotPacket::create(const x25::X25Packet::SharedPointer& p_packet,
        const unsigned int& p_version) {

    const SharedPointer pointer(new XotPacket(p_packet, p_version));
    return pointer;
}

const xot4cpp::x25::X25Packet::SharedPointer& xot4cpp::XotPacket::packet(void) const {

    return m_packet;
}

const unsigned int& xot4cpp::XotPacket::version(void) const {

    return m_version;
}
