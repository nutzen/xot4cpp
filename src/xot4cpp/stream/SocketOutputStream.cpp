#include "xot4cpp/stream/SocketOutputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::stream::SocketOutputStream::SocketOutputStream(const socket::Socket::SharedPointer& p_socket)
: m_socket(p_socket) {
}

xot4cpp::stream::SocketOutputStream::~SocketOutputStream(void) {
}

xot4cpp::stream::OutputStream::SharedPointer xot4cpp::stream::SocketOutputStream::create(const socket::Socket::SharedPointer& p_socket) {

    const OutputStream::SharedPointer pointer(new SocketOutputStream(p_socket));
    return pointer;
}

void xot4cpp::stream::SocketOutputStream::write(const Octet& p_source) {

    const Octet buffer [] = {p_source};
    write(Octets(buffer, buffer + 1));
}

void xot4cpp::stream::SocketOutputStream::write(const xot4cpp::Octets& p_source) {

    m_socket->write(p_source.size(), &p_source[0]);
}
