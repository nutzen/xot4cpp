#include "xot4cpp/stream/BlockingPipeOutputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::stream::BlockingPipeOutputStream::BlockingPipeOutputStream(const Pipe::SharedPointer& p_pipe)
: OutputStream(),
m_pipe(p_pipe) {
}

xot4cpp::stream::BlockingPipeOutputStream::~BlockingPipeOutputStream(void) {
}

xot4cpp::stream::OutputStream::SharedPointer xot4cpp::stream::BlockingPipeOutputStream::create(const Pipe::SharedPointer& p_pipe) {

    const OutputStream::SharedPointer pointer(new BlockingPipeOutputStream(p_pipe));
    return pointer;
}

void xot4cpp::stream::BlockingPipeOutputStream::write(const Octet& p_source) {

    m_pipe->write(p_source);
    waitForData();
}

void xot4cpp::stream::BlockingPipeOutputStream::write(const Octets& p_source) {

    return m_pipe->write(p_source);
    waitForData();
}

unsigned int xot4cpp::stream::BlockingPipeOutputStream::available(void) const {

    return m_pipe->available();
}

void xot4cpp::stream::BlockingPipeOutputStream::waitForData(void) const {

    const int timeout = 1000 * 30;
    const int sleepInterval = 100;
    const int maxCounter = timeout / sleepInterval;

    for (int counter = 0; (maxCounter > counter) && (0 < available()); ++counter) {

        boost::this_thread::sleep(boost::posix_time::milliseconds(sleepInterval));
    }
}

