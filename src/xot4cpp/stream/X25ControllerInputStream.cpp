#include "xot4cpp/stream/X25ControllerInputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/IOException.hpp"
#include "xot4cpp/stream/BlockingPipeInputStream.hpp"

xot4cpp::stream::X25ControllerInputStream::X25ControllerInputStream(
        const x25::X25Controller::SharedPointer& p_controller,
        const InputStream::SharedPointer& p_InputStream)
: InputStream(),
m_controller(p_controller),
m_inputStream(p_InputStream) {
}

xot4cpp::stream::X25ControllerInputStream::~X25ControllerInputStream(void) {
}

xot4cpp::stream::InputStream::SharedPointer xot4cpp::stream::X25ControllerInputStream::create(
        const x25::X25Controller::SharedPointer& p_controller) {

    const InputStream::SharedPointer inputStream =
            BlockingPipeInputStream::create(p_controller->localToRemote());
    const InputStream::SharedPointer pointer(new X25ControllerInputStream(p_controller, inputStream));

    return pointer;
}

unsigned int xot4cpp::stream::X25ControllerInputStream::available(void) const {

    return m_inputStream->available();
}

int xot4cpp::stream::X25ControllerInputStream::read(void) {

    if (0 >= available()) {
        if (!m_controller->isConnected()) {
            THROW_XOT4CPP_IOEXCEPTION("Session closed");
        }
    }
    return m_inputStream->read();
}

xot4cpp::Octets xot4cpp::stream::X25ControllerInputStream::read(const unsigned int& p_size) {

    if (0 >= available()) {
        if (!m_controller->isConnected()) {
            THROW_XOT4CPP_IOEXCEPTION("Session closed");
        }
    }
    return m_inputStream->read(p_size);
}
