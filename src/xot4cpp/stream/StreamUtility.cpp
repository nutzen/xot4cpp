#include "xot4cpp/stream/StreamUtility.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::Octets xot4cpp::stream::StreamUtility::readBuffer(const InputStream::SharedPointer& p_inputStream, const unsigned int& p_length) {

    Octets target;

    while (target.size() < p_length) {
        Octets octets = p_inputStream->read(p_length - target.size());
        if (octets.empty()) {
            break;
        }
        target.insert(target.end(), octets.begin(), octets.end());
    }

    return target;
}

unsigned short xot4cpp::stream::StreamUtility::readUnsignedShort(const InputStream::SharedPointer& p_inputStream) {

    unsigned int value = 0x00ff & p_inputStream->read();
    value = (value << 8);
    value = value | (0x00ff & p_inputStream->read());

    return static_cast<unsigned short> (value);
}

void xot4cpp::stream::StreamUtility::write(const OutputStream::SharedPointer& p_outputStream, const unsigned short& p_value) {

    p_outputStream->write(0x00ff & (p_value >> 8));
    p_outputStream->write(0x00ff & p_value);
}
