#include "xot4cpp/stream/OctetInputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <algorithm>

xot4cpp::stream::OctetInputStream::SharedPointer xot4cpp::stream::OctetInputStream::downCast(const InputStream::SharedPointer& p_pointer) {

    typedef OctetInputStream Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::InputStream::SharedPointer xot4cpp::stream::OctetInputStream::upCast(const SharedPointer& p_pointer) {

    typedef InputStream Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::OctetInputStream::OctetInputStream(const Octets& p_octets)
: m_octets(p_octets) {
}

xot4cpp::stream::OctetInputStream::~OctetInputStream(void) {
}

xot4cpp::stream::InputStream::SharedPointer xot4cpp::stream::OctetInputStream::create(const Octets& p_octets) {

    const Octets octets(p_octets.rbegin(), p_octets.rend());
    const InputStream::SharedPointer pointer(new OctetInputStream(octets));
    return pointer;
}

unsigned int xot4cpp::stream::OctetInputStream::available(void) const {

    return m_octets.size();
}

int xot4cpp::stream::OctetInputStream::read(void) {

    int destination;

    if (m_octets.empty()) {
        destination = eos();

    } else {
        destination = m_octets.back();
        m_octets.pop_back();
    }

    return destination;
}

xot4cpp::Octets xot4cpp::stream::OctetInputStream::read(const unsigned int& p_size) {

    typedef Octets::reverse_iterator Iterator;

    const unsigned int size = std::min((unsigned int) m_octets.size(), p_size);
    Iterator begin = m_octets.rbegin();
    Iterator end = begin + size;

    const Octets octets(begin, end);
    m_octets.erase(m_octets.end() - size, m_octets.end());

    return octets;
}
