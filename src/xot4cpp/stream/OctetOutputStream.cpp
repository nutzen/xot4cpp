#include "xot4cpp/stream/OctetOutputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::stream::OctetOutputStream::SharedPointer xot4cpp::stream::OctetOutputStream::downCast(const OutputStream::SharedPointer& p_pointer) {

    typedef OctetOutputStream Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::OutputStream::SharedPointer xot4cpp::stream::OctetOutputStream::upCast(const SharedPointer& p_pointer) {

    typedef OutputStream Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::OctetOutputStream::OctetOutputStream(const Octets& p_octets)
: m_octets(p_octets) {
}

xot4cpp::stream::OctetOutputStream::~OctetOutputStream(void) {
}

xot4cpp::stream::OutputStream::SharedPointer xot4cpp::stream::OctetOutputStream::create(const Octets& p_octets) {

    const OutputStream::SharedPointer pointer(new OctetOutputStream(p_octets));
    return pointer;
}

void xot4cpp::stream::OctetOutputStream::write(const Octet& p_source) {

    m_octets.push_back(p_source);
}

void xot4cpp::stream::OctetOutputStream::write(const Octets& p_source) {

    m_octets.insert(m_octets.end(), p_source.begin(), p_source.end());
}

const xot4cpp::Octets& xot4cpp::stream::OctetOutputStream::octets(void) const {

    return m_octets;
}
