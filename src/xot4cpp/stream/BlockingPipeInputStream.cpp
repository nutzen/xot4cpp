#include "xot4cpp/stream/BlockingPipeInputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/SocketTimeoutException.hpp"

xot4cpp::stream::BlockingPipeInputStream::BlockingPipeInputStream(const Pipe::SharedPointer& p_pipe)
: InputStream(),
m_pipe(p_pipe) {
}

xot4cpp::stream::BlockingPipeInputStream::~BlockingPipeInputStream(void) {
}

xot4cpp::stream::InputStream::SharedPointer xot4cpp::stream::BlockingPipeInputStream::create(const Pipe::SharedPointer& p_pipe) {

    const InputStream::SharedPointer pointer(new BlockingPipeInputStream(p_pipe));
    return pointer;
}

unsigned int xot4cpp::stream::BlockingPipeInputStream::available(void) const {

    return m_pipe->available();
}

int xot4cpp::stream::BlockingPipeInputStream::read(void) {

    waitForData();
    return m_pipe->read();
}

xot4cpp::Octets xot4cpp::stream::BlockingPipeInputStream::read(const unsigned int& p_size) {

    waitForData();
    return m_pipe->read(p_size);
}

void xot4cpp::stream::BlockingPipeInputStream::waitForData(void) const {

    const int timeout = 1000 * 30;
    const int sleepInterval = 100;
    const int maxCounter = timeout / sleepInterval;

    for (int counter = 0; (maxCounter > counter) && (0 >= available()); ++counter) {

        boost::this_thread::sleep(boost::posix_time::milliseconds(sleepInterval));
    }

    if (0 >= available()) {
        THROW_XOT4CPP_SOCKETTIMEOUTEXCEPTION("");
    }
}

