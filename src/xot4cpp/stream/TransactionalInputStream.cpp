#include "xot4cpp/stream/TransactionalInputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::stream::TransactionalInputStream::SharedPointer xot4cpp::stream::TransactionalInputStream::downCast(
        const InputStream::SharedPointer& p_pointer) {

    typedef TransactionalInputStream Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::InputStream::SharedPointer xot4cpp::stream::TransactionalInputStream::upCast(
        const SharedPointer& p_pointer) {

    typedef InputStream Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::TransactionalInputStream::TransactionalInputStream(
        const InputStream::SharedPointer& p_source,
        const Pipe::SharedPointer& p_reference,
        const Pipe::SharedPointer& p_sandbox)
: InputStream(),
m_source(p_source),
m_reference(p_reference),
m_sandbox(p_sandbox) {
}

xot4cpp::stream::TransactionalInputStream::~TransactionalInputStream(void) {
}

xot4cpp::stream::TransactionalInputStream::SharedPointer xot4cpp::stream::TransactionalInputStream::create(
        const InputStream::SharedPointer& p_source) {

    const Pipe::SharedPointer reference = Pipe::create();
    const Pipe::SharedPointer sandbox = reference->copy();
    const SharedPointer pointer(new TransactionalInputStream(p_source, reference, sandbox));

    return pointer;
}

void xot4cpp::stream::TransactionalInputStream::commit(void) {

    const MutexGuard mutexGuard(m_mutex);
    m_reference = m_sandbox->copy();
}

void xot4cpp::stream::TransactionalInputStream::rollback(void) {

    const MutexGuard mutexGuard(m_mutex);
    m_sandbox = m_reference->copy();
}

unsigned int xot4cpp::stream::TransactionalInputStream::available(void) const {

    return m_sandbox->available() + m_source->available();
}

int xot4cpp::stream::TransactionalInputStream::read(void) {

    const MutexGuard mutexGuard(m_mutex);
    readAheadIfNecessary(1);
    return m_sandbox->read();
}

xot4cpp::Octets xot4cpp::stream::TransactionalInputStream::read(
        const unsigned int& p_size) {

    const MutexGuard mutexGuard(m_mutex);
    readAheadIfNecessary(p_size);
    return m_sandbox->read(p_size);
}

int xot4cpp::stream::TransactionalInputStream::readAheadIfNecessary(
        const unsigned int& p_required) {

    const int size = p_required - m_sandbox->available();
    int read = 0;
    if (0 < size) {
        read = readAhead(static_cast<unsigned int> (size));
    }
    return read;
}

int xot4cpp::stream::TransactionalInputStream::readAhead(const unsigned int& p_size) {

    const Octets octets = m_source->read(p_size);
    if (0 < octets.size()) {
        m_sandbox->write(octets);
        m_reference->write(octets);
    }

    return octets.size();
}
