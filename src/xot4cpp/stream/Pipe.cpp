#include "xot4cpp/stream/Pipe.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <boost/thread.hpp>
#include <fcntl.h>

xot4cpp::stream::Pipe::SharedPointer xot4cpp::stream::Pipe::downCast(const InputStream::SharedPointer& p_pointer) {

    typedef Pipe Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::InputStream::SharedPointer xot4cpp::stream::Pipe::upCastAsInputStream(const SharedPointer& p_pointer) {

    typedef InputStream Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::Pipe::SharedPointer xot4cpp::stream::Pipe::downCast(const OutputStream::SharedPointer& p_pointer) {

    typedef Pipe Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::OutputStream::SharedPointer xot4cpp::stream::Pipe::upCastAsOutputStream(const SharedPointer& p_pointer) {

    typedef OutputStream Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::stream::Pipe::Pipe(const Deque& p_octets)
: m_octets(p_octets) {
}

xot4cpp::stream::Pipe::~Pipe(void) {
}

xot4cpp::stream::Pipe::SharedPointer xot4cpp::stream::Pipe::create(const Octets& p_octets) {

    const Deque octets(p_octets.begin(), p_octets.end());
    const SharedPointer pointer(new Pipe(octets));

    return pointer;
}

xot4cpp::stream::Pipe::SharedPointer xot4cpp::stream::Pipe::copy(void) {

    const MutexGuard mutexGuard(m_mutex);
    const Octets octets(m_octets.begin(), m_octets.end());
    return Pipe::create(octets);
}

unsigned int xot4cpp::stream::Pipe::available(void) const {

    return m_octets.size();
}

int xot4cpp::stream::Pipe::read(void) {

    const MutexGuard mutexGuard(m_mutex);
    int destination = -1;

    if (!m_octets.empty()) {
        destination = *(m_octets.begin());
        m_octets.erase(m_octets.begin());
    }

    return destination;
}

xot4cpp::Octets xot4cpp::stream::Pipe::read(const unsigned int& p_size) {

    typedef Deque::iterator Iterator;
    const MutexGuard mutexGuard(m_mutex);

    const unsigned int size = std::min((unsigned int) m_octets.size(), p_size);
    Iterator begin = m_octets.begin();
    Iterator end = begin + size;

    const Octets octets(begin, end);
    m_octets.erase(begin, end);

    return octets;
}

void xot4cpp::stream::Pipe::write(const Octet& p_source) {

    const MutexGuard mutexGuard(m_mutex);
    m_octets.insert(m_octets.end(), p_source);
}

void xot4cpp::stream::Pipe::write(const Octets& p_source) {

    const MutexGuard mutexGuard(m_mutex);
    m_octets.insert(m_octets.end(), p_source.begin(), p_source.end());
}
