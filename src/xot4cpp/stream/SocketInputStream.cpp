#include "xot4cpp/stream/SocketInputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <cstring>

xot4cpp::stream::SocketInputStream::SocketInputStream(const socket::Socket::SharedPointer& p_socket)
: m_socket(p_socket) {
}

xot4cpp::stream::SocketInputStream::~SocketInputStream(void) {
}

xot4cpp::stream::InputStream::SharedPointer xot4cpp::stream::SocketInputStream::create(const socket::Socket::SharedPointer& p_socket) {

    const InputStream::SharedPointer pointer(new SocketInputStream(p_socket));
    return pointer;
}

int xot4cpp::stream::SocketInputStream::read(void) {

    int value = eos();
    const Octets octets = read(1);
    if (!octets.empty()) {
        value = octets[0];
    }

    return value;
}

xot4cpp::Octets xot4cpp::stream::SocketInputStream::read(const unsigned int& p_size) {

    Octet buffer[p_size];
    ::memset(buffer, 0, p_size);
    const size_t length = m_socket->read(p_size, buffer);

    return Octets(buffer, buffer + length);
}
