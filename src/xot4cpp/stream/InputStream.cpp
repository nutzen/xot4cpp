#include "xot4cpp/stream/InputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define END_OF_STREAM -1

xot4cpp::stream::InputStream::InputStream(void) {
}

xot4cpp::stream::InputStream::~InputStream(void) {
}

int xot4cpp::stream::InputStream::eos(void) {

    return END_OF_STREAM;
}

unsigned int xot4cpp::stream::InputStream::available(void) const {

    return 0;
}
