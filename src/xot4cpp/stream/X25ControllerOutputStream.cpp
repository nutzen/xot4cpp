#include "xot4cpp/stream/X25ControllerOutputStream.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/IOException.hpp"
#include "xot4cpp/stream/BlockingPipeOutputStream.hpp"

xot4cpp::stream::X25ControllerOutputStream::X25ControllerOutputStream(
        const x25::X25Controller::SharedPointer& p_controller,
        const OutputStream::SharedPointer& p_outputStream)
: OutputStream(),
m_controller(p_controller),
m_outputStream(p_outputStream) {
}

xot4cpp::stream::X25ControllerOutputStream::~X25ControllerOutputStream(void) {
}

xot4cpp::stream::OutputStream::SharedPointer xot4cpp::stream::X25ControllerOutputStream::create(
        const x25::X25Controller::SharedPointer& p_controller) {

    const OutputStream::SharedPointer outputStream =
            BlockingPipeOutputStream::create(p_controller->localToRemote());
    const OutputStream::SharedPointer pointer(new X25ControllerOutputStream(p_controller, outputStream));

    return pointer;
}

void xot4cpp::stream::X25ControllerOutputStream::write(const Octet& p_source) {

    if (!m_controller->isConnected()) {
        THROW_XOT4CPP_IOEXCEPTION("Session closed");
    }
    m_outputStream->write(p_source);
}

void xot4cpp::stream::X25ControllerOutputStream::write(const Octets& p_source) {

    if (!m_controller->isConnected()) {
        THROW_XOT4CPP_IOEXCEPTION("Session closed");
    }
    m_outputStream->write(p_source);
}
