#include "xot4cpp/exceptions/Xot4cppException.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::exceptions::Xot4cppException::Xot4cppException(const std::string& p_message)
: m_message(p_message) {
}

xot4cpp::exceptions::Xot4cppException::Xot4cppException(const Xot4cppException& p_original)
: m_message(p_original.m_message) {
}

xot4cpp::exceptions::Xot4cppException& xot4cpp::exceptions::Xot4cppException::operator =(const Xot4cppException& p_original) {

    if (this != &p_original) {
        m_message = p_original.m_message;
    }

    return *this;
}

xot4cpp::exceptions::Xot4cppException::~Xot4cppException(void) throw() {
}

const char* xot4cpp::exceptions::Xot4cppException::what() const throw () {

    return m_message.c_str();
}

xot4cpp::exceptions::Xot4cppException xot4cpp::exceptions::Xot4cppException::create(const char* p_file, const int& p_line, const std::string& p_message) {

    std::stringstream stringstream;
    stringstream << "[" << p_file << ":" << p_line << "] " << p_message;

    return Xot4cppException(stringstream.str());
}
