#include "xot4cpp/exceptions/SocketTimeoutException.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::exceptions::SocketTimeoutException::SocketTimeoutException(const std::string& p_message)
: IOException(p_message) {
}

xot4cpp::exceptions::SocketTimeoutException::SocketTimeoutException(const SocketTimeoutException& p_original)
: IOException(p_original.m_message) {
}

xot4cpp::exceptions::SocketTimeoutException& xot4cpp::exceptions::SocketTimeoutException::operator =(const SocketTimeoutException& p_original) {

    if (this != &p_original) {
        m_message = p_original.m_message;
    }

    return *this;
}

xot4cpp::exceptions::SocketTimeoutException::~SocketTimeoutException(void) throw () {
}

xot4cpp::exceptions::SocketTimeoutException xot4cpp::exceptions::SocketTimeoutException::create(const char* p_file, const int& p_line, const std::string& p_message) {

    std::stringstream stringstream;
    stringstream << "[" << p_file << ":" << p_line << "] " << p_message;

    return SocketTimeoutException(stringstream.str());
}
