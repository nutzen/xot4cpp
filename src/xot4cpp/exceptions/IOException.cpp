#include "xot4cpp/exceptions/IOException.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::exceptions::IOException::IOException(const std::string& p_message)
: Xot4cppException(p_message) {
}

xot4cpp::exceptions::IOException::IOException(const IOException& p_original)
: Xot4cppException(p_original.m_message) {
}

xot4cpp::exceptions::IOException& xot4cpp::exceptions::IOException::operator =(const IOException& p_original) {

    if (this != &p_original) {
        m_message = p_original.m_message;
    }

    return *this;
}

xot4cpp::exceptions::IOException::~IOException(void) throw () {
}

xot4cpp::exceptions::IOException xot4cpp::exceptions::IOException::create(const char* p_file, const int& p_line, const std::string& p_message) {

    std::stringstream stringstream;
    stringstream << "[" << p_file << ":" << p_line << "] " << p_message;

    return IOException(stringstream.str());
}

