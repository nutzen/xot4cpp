#include "xot4cpp/XotPacketMarshaller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/logger/Logger.hpp"
#include "xot4cpp/stream/OctetInputStream.hpp"
#include "xot4cpp/stream/OctetOutputStream.hpp"
#include "xot4cpp/stream/StreamUtility.hpp"
#include "xot4cpp/x25/X25PacketMarshaller.hpp"
#include "xot4cpp/x25/utility/HexUtility.hpp"

#include <exception>

void xot4cpp::XotPacketMarshaller::marshal(const stream::OutputStream::SharedPointer& p_outputStream, const XotPacket::SharedPointer& p_source) {

    const unsigned int version = p_source->version();
    const x25::X25Packet::SharedPointer packet = p_source->packet();

    Octets buffer;
    {
        const stream::OutputStream::SharedPointer target = stream::OctetOutputStream::create();
        x25::X25PacketMarshaller::marshal(target, packet);
        buffer = stream::OctetOutputStream::downCast(target)->octets();
    }

    stream::StreamUtility::write(p_outputStream, static_cast<unsigned short> (version));
    stream::StreamUtility::write(p_outputStream, static_cast<unsigned short> (buffer.size()));
    p_outputStream->write(buffer);
}

xot4cpp::XotPacket::SharedPointer xot4cpp::XotPacketMarshaller::unmarshal(const stream::InputStream::SharedPointer& p_inputStream) {

    const unsigned int version = stream::StreamUtility::readUnsignedShort(p_inputStream);
    const unsigned int length = stream::StreamUtility::readUnsignedShort(p_inputStream);
    const Octets buffer = stream::StreamUtility::readBuffer(p_inputStream, length);

    try {
        const x25::X25Packet::SharedPointer packet = x25::X25PacketMarshaller::unmarshal(stream::OctetInputStream::create(buffer));

        return XotPacket::create(packet, version);

    } catch (std::exception& e) {
        XOT4CPP_DEBUG("Unable to unmarshal: " << x25::utility::HexUtility::toHex(buffer));
        throw e;
    }
}
