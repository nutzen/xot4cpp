#include "xot4cpp/x25/X25Packet.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::X25Packet::X25Packet(const X25Header::SharedPointer& p_header,
        const payload::X25Payload::SharedPointer& p_payload)
: m_header(p_header),
m_payload(p_payload) {
}

xot4cpp::x25::X25Packet::~X25Packet(void) {
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::X25Packet::create(const X25Header::SharedPointer& p_header,
        const payload::X25Payload::SharedPointer& p_payload) {

    const SharedPointer pointer(new X25Packet(p_header, p_payload));
    return pointer;
}

const xot4cpp::x25::X25Header::SharedPointer& xot4cpp::x25::X25Packet::header(void) const {

    return m_header;
}

const xot4cpp::x25::payload::X25Payload::SharedPointer& xot4cpp::x25::X25Packet::payload(void) const {

    return m_payload;
}
