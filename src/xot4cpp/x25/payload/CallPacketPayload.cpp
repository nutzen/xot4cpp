#include "xot4cpp/x25/payload/CallPacketPayload.hpp"

//          Copyright Digitata Limited 2008 - 2011.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::payload::CallPacketPayload::SharedPointer xot4cpp::x25::payload::CallPacketPayload::downCast(
        const X25Payload::SharedPointer& p_pointer) {

    typedef CallPacketPayload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::CallPacketPayload::upCast(
        const SharedPointer& p_pointer) {

    typedef X25Payload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::CallPacketPayload::CallPacketPayload(
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress,
        const facilities::Facility::List& p_facilities,
        const Octets& p_userData)
: m_calledAddress(p_calledAddress),
m_callingAddress(p_callingAddress),
m_facilities(p_facilities),
m_userData(p_userData) {
}

xot4cpp::x25::payload::CallPacketPayload::~CallPacketPayload(void) {
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::CallPacketPayload::create(
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress,
        const facilities::Facility::List& p_facilities,
        const Octets& p_userData) {

    const X25Payload::SharedPointer pointer(new CallPacketPayload(
            p_calledAddress,
            p_callingAddress,
            p_facilities,
            p_userData));

    return pointer;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::payload::CallPacketPayload::calledAddress(void) const {

    return m_calledAddress;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::payload::CallPacketPayload::callingAddress(void) const {

    return m_callingAddress;
}

const xot4cpp::x25::facilities::Facility::List& xot4cpp::x25::payload::CallPacketPayload::facilities(void) const {

    return m_facilities;
}

const xot4cpp::Octets& xot4cpp::x25::payload::CallPacketPayload::userData(void) const {

    return m_userData;
}
