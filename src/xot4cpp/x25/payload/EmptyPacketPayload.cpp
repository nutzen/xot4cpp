#include "xot4cpp/x25/payload/EmptyPacketPayload.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::payload::EmptyPacketPayload::SharedPointer xot4cpp::x25::payload::EmptyPacketPayload::downCast(
        const X25Payload::SharedPointer& p_pointer) {

    typedef EmptyPacketPayload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::EmptyPacketPayload::upCast(
        const SharedPointer& p_pointer) {

    typedef X25Payload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::EmptyPacketPayload::EmptyPacketPayload(void) {
}

xot4cpp::x25::payload::EmptyPacketPayload::~EmptyPacketPayload(void) {
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::EmptyPacketPayload::create(void) {

    const X25Payload::SharedPointer pointer(new EmptyPacketPayload);

    return pointer;
}
