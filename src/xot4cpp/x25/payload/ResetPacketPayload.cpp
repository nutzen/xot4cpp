#include "xot4cpp/x25/payload/ResetPacketPayload.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::payload::ResetPacketPayload::SharedPointer xot4cpp::x25::payload::ResetPacketPayload::downCast(
        const X25Payload::SharedPointer& p_pointer) {

    typedef ResetPacketPayload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::ResetPacketPayload::upCast(
        const SharedPointer& p_pointer) {

    typedef X25Payload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::ResetPacketPayload::ResetPacketPayload(
        const codes::CauseCodeEnumerations::ResetRequestEnumeration& p_cause,
        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic)
: m_cause(p_cause),
m_diagnostic(p_diagnostic) {
}

xot4cpp::x25::payload::ResetPacketPayload::~ResetPacketPayload(void) {
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::ResetPacketPayload::create(
        const codes::CauseCodeEnumerations::ResetRequestEnumeration& p_cause,
        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic) {

    const X25Payload::SharedPointer pointer(new ResetPacketPayload(
            p_cause,
            p_diagnostic));

    return pointer;
}

const xot4cpp::x25::codes::CauseCodeEnumerations::ResetRequestEnumeration& xot4cpp::x25::payload::ResetPacketPayload::cause(void) const {

    return m_cause;
}

const xot4cpp::x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& xot4cpp::x25::payload::ResetPacketPayload::diagnostic(void) const {

    return m_diagnostic;
}
