#include "xot4cpp/x25/payload/DataPacketPayload.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::payload::DataPacketPayload::SharedPointer xot4cpp::x25::payload::DataPacketPayload::downCast(
        const X25Payload::SharedPointer& p_pointer) {

    typedef DataPacketPayload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::DataPacketPayload::upCast(
        const SharedPointer& p_pointer) {

    typedef X25Payload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::DataPacketPayload::DataPacketPayload(const Octets& p_userData)
: m_userData(p_userData) {
}

xot4cpp::x25::payload::DataPacketPayload::~DataPacketPayload(void) {
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::DataPacketPayload::create(
        const Octets& p_userData) {

    const X25Payload::SharedPointer pointer(new DataPacketPayload(p_userData));

    return pointer;
}

const xot4cpp::Octets& xot4cpp::x25::payload::DataPacketPayload::userData(void) const {

    return m_userData;
}
