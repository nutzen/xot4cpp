#include "xot4cpp/x25/payload/ClearConfirmationPayload.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::payload::ClearConfirmationPayload::SharedPointer xot4cpp::x25::payload::ClearConfirmationPayload::downCast(
        const X25Payload::SharedPointer& p_pointer) {

    typedef ClearConfirmationPayload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::ClearConfirmationPayload::upCast(
        const SharedPointer& p_pointer) {

    typedef X25Payload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::ClearConfirmationPayload::ClearConfirmationPayload(
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress,
        const facilities::Facility::List& p_facilities)
: m_calledAddress(p_calledAddress),
m_callingAddress(p_callingAddress),
m_facilities(p_facilities) {
}

xot4cpp::x25::payload::ClearConfirmationPayload::~ClearConfirmationPayload(void) {
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::ClearConfirmationPayload::create(
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress,
        const facilities::Facility::List& p_facilities) {

    const X25Payload::SharedPointer pointer(new ClearConfirmationPayload(
            p_calledAddress,
            p_callingAddress,
            p_facilities));

    return pointer;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::payload::ClearConfirmationPayload::calledAddress(void) const {

    return m_calledAddress;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::payload::ClearConfirmationPayload::callingAddress(void) const {

    return m_callingAddress;
}

const xot4cpp::x25::facilities::Facility::List& xot4cpp::x25::payload::ClearConfirmationPayload::facilities(void) const {

    return m_facilities;
}
