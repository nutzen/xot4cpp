#include "xot4cpp/x25/payload/X25PayloadMarshaller.hpp"

//          Copyright Digitata Limited 2008 - 2011.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x121/X121AddressMarshaller.hpp"
#include "xot4cpp/x25/facilities/FacilityMarshaller.hpp"

#include "xot4cpp/x25/payload/CallPacketPayload.hpp"
#include "xot4cpp/x25/payload/ClearConfirmationPayload.hpp"
#include "xot4cpp/x25/payload/ClearPacketPayload.hpp"
#include "xot4cpp/x25/payload/DataPacketPayload.hpp"
#include "xot4cpp/x25/payload/EmptyPacketPayload.hpp"
#include "xot4cpp/x25/payload/ResetPacketPayload.hpp"
#include "xot4cpp/x25/payload/RestartPacketPayload.hpp"

void xot4cpp::x25::payload::X25PayloadMarshaller::marshalCallPacketPayload(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const X25Payload::SharedPointer& p_source,
        const bool& p_aBit) {

    const CallPacketPayload::SharedPointer source = CallPacketPayload::downCast(p_source);

    const x121::X121Address::SharedPointer calledAddress = source->calledAddress();
    const x121::X121Address::SharedPointer callingAddress = source->callingAddress();
    const facilities::Facility::List facilities = source->facilities();
    const Octets userData = source->userData();

    if (calledAddress.get() && callingAddress.get()) {

        x121::X121AddressMarshaller::marshal(p_outputStream, calledAddress, callingAddress, p_aBit);

        const Octets facilitiesBlock = facilities::FacilityMarshaller::marshal(facilities);
        p_outputStream->write(static_cast<Octet> (0x00ff & facilitiesBlock.size()));
        p_outputStream->write(facilitiesBlock);

        if (!userData.empty()) {
            p_outputStream->write(userData);
        }
    }
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::X25PayloadMarshaller::unmarshalCallPacketPayload(
        const stream::InputStream::SharedPointer& p_inputStream,
        const bool& p_aBit) {

    X25Payload::SharedPointer payload;

    if (p_inputStream->available()) {

        const x121::X121AddressPair::SharedPointer addressPair = x121::X121AddressMarshaller::unmarshal(p_inputStream, p_aBit);
        const x121::X121Address::SharedPointer calledAddress = addressPair->calledAddress();
        const x121::X121Address::SharedPointer callingAddress = addressPair->callingAddress();

        if (p_inputStream->available()) {

            const unsigned int octetLengthOfFacilities = static_cast<unsigned int> (0x00ff & p_inputStream->read());
            const Octets facilitiesBlock = p_inputStream->read(octetLengthOfFacilities);
            const facilities::Facility::List facilities = facilities::FacilityMarshaller::unmarshal(facilitiesBlock);

            if (p_inputStream->available()) {

                const Octets userData = p_inputStream->read(p_inputStream->available());
                payload = CallPacketPayload::create(calledAddress, callingAddress, facilities, userData);

            } else {
                payload = CallPacketPayload::create(calledAddress, callingAddress, facilities);
            }

        } else {
            payload = CallPacketPayload::create(calledAddress, callingAddress);
        }

    } else {
        payload = CallPacketPayload::create();
    }

    return payload;
}

void xot4cpp::x25::payload::X25PayloadMarshaller::marshalClearPacketPayload(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const X25Payload::SharedPointer& p_source,
        const bool& p_aBit) {

    const ClearPacketPayload::SharedPointer source = ClearPacketPayload::downCast(p_source);
    p_outputStream->write(0x00ff & codes::CauseCodeEnumerations::toInt(source->cause()));
    p_outputStream->write(0x0ff & codes::DiagnosticCodeEnumerations::toInt(source->diagnostic()));

    const x121::X121Address::SharedPointer calledAddress = source->calledAddress();
    const x121::X121Address::SharedPointer callingAddress = source->callingAddress();
    const facilities::Facility::List facilities = source->facilities();
    const Octets userData = source->userData();

    if (calledAddress.get() && callingAddress.get()) {

        x121::X121AddressMarshaller::marshal(p_outputStream, calledAddress, callingAddress, p_aBit);

        if (!facilities.empty()) {

            const Octets facilitiesBlock = facilities::FacilityMarshaller::marshal(facilities);
            p_outputStream->write(static_cast<Octet> (0x00ff & facilitiesBlock.size()));
            p_outputStream->write(facilitiesBlock);

            if (!userData.empty()) {

                p_outputStream->write(userData);
            }
        }
    }
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::X25PayloadMarshaller::unmarshalClearPacketPayload(
        const stream::InputStream::SharedPointer& p_inputStream,
        const bool& p_aBit) {

    X25Payload::SharedPointer payload;
    const codes::CauseCodeEnumerations::ClearRequestEnumeration cause = codes::CauseCodeEnumerations::clearRequestFromInt(0x00ff & p_inputStream->read());

    if (p_inputStream->available()) {
        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostics = codes::DiagnosticCodeEnumerations::fromInt(0x00ff & p_inputStream->read());

        if (p_inputStream->available()) {

            const x121::X121AddressPair::SharedPointer addressPair = x121::X121AddressMarshaller::unmarshal(p_inputStream, p_aBit);
            const x121::X121Address::SharedPointer calledAddress = addressPair->calledAddress();
            const x121::X121Address::SharedPointer callingAddress = addressPair->callingAddress();

            if (p_inputStream->available()) {

                const unsigned int octetLengthOfFacilities = static_cast<unsigned int> (0x00ff & p_inputStream->read());
                const Octets facilitiesBlock = p_inputStream->read(octetLengthOfFacilities);
                const facilities::Facility::List facilities = facilities::FacilityMarshaller::unmarshal(facilitiesBlock);

                if (p_inputStream->available()) {

                    const Octets userData = p_inputStream->read(p_inputStream->available());
                    payload = ClearPacketPayload::create(cause, diagnostics, calledAddress, callingAddress, facilities, userData);

                } else {
                    payload = ClearPacketPayload::create(cause, diagnostics, calledAddress, callingAddress, facilities);
                }

            } else {
                payload = ClearPacketPayload::create(cause, diagnostics, calledAddress, callingAddress);
            }

        } else {
            payload = ClearPacketPayload::create(cause, diagnostics);
        }

    } else {
        payload = ClearPacketPayload::create(cause, codes::DiagnosticCodeEnumerations::NoAdditionalInformation);
    }

    return payload;
}

void xot4cpp::x25::payload::X25PayloadMarshaller::marshalClearConfirmationPayload(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const X25Payload::SharedPointer& p_source,
        const bool& p_aBit) {

    const ClearConfirmationPayload::SharedPointer source = ClearConfirmationPayload::downCast(p_source);

    const x121::X121Address::SharedPointer calledAddress = source->calledAddress();
    const x121::X121Address::SharedPointer callingAddress = source->callingAddress();
    const facilities::Facility::List facilities = source->facilities();

    if (calledAddress.get() && callingAddress.get()) {

        x121::X121AddressMarshaller::marshal(p_outputStream, calledAddress, callingAddress, p_aBit);

        if (!facilities.empty()) {

            const Octets facilitiesBlock = facilities::FacilityMarshaller::marshal(facilities);
            p_outputStream->write(static_cast<Octet> (0x00ff & facilitiesBlock.size()));
            p_outputStream->write(facilitiesBlock);
        }
    }
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::X25PayloadMarshaller::unmarshalClearConfirmationPayload(
        const stream::InputStream::SharedPointer& p_inputStream,
        const bool& p_aBit) {

    X25Payload::SharedPointer payload;

    if (p_inputStream->available()) {

        const x121::X121AddressPair::SharedPointer addressPair = x121::X121AddressMarshaller::unmarshal(p_inputStream, p_aBit);
        const x121::X121Address::SharedPointer calledAddress = addressPair->calledAddress();
        const x121::X121Address::SharedPointer callingAddress = addressPair->callingAddress();

        if (p_inputStream->available()) {

            const unsigned int octetLengthOfFacilities = static_cast<unsigned int> (0x00ff & p_inputStream->read());
            const Octets facilitiesBlock = p_inputStream->read(octetLengthOfFacilities);
            const facilities::Facility::List facilities = facilities::FacilityMarshaller::unmarshal(facilitiesBlock);

            payload = ClearConfirmationPayload::create(calledAddress, callingAddress, facilities);

        } else {
            payload = ClearConfirmationPayload::create(calledAddress, callingAddress);
        }

    } else {
        payload = ClearConfirmationPayload::create();
    }

    return payload;
}

void xot4cpp::x25::payload::X25PayloadMarshaller::marshalResetPacketPayload(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const X25Payload::SharedPointer& p_source) {

    const ResetPacketPayload::SharedPointer source = ResetPacketPayload::downCast(p_source);
    p_outputStream->write(0x00ff & codes::CauseCodeEnumerations::toInt(source->cause()));
    p_outputStream->write(0x0ff & codes::DiagnosticCodeEnumerations::toInt(source->diagnostic()));
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::X25PayloadMarshaller::unmarshalResetPacketPayload(
        const stream::InputStream::SharedPointer& p_inputStream) {

    const codes::CauseCodeEnumerations::ResetRequestEnumeration cause = codes::CauseCodeEnumerations::resetRequestFromInt(0x00ff & p_inputStream->read());
    const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostics = codes::DiagnosticCodeEnumerations::fromInt(0x00ff & p_inputStream->read());

    return ResetPacketPayload::create(cause, diagnostics);
}

void xot4cpp::x25::payload::X25PayloadMarshaller::marshalRestartPacketPayload(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const X25Payload::SharedPointer& p_source) {

    const RestartPacketPayload::SharedPointer source = RestartPacketPayload::downCast(p_source);
    p_outputStream->write(0x00ff & codes::CauseCodeEnumerations::toInt(source->cause()));
    p_outputStream->write(0x0ff & codes::DiagnosticCodeEnumerations::toInt(source->diagnostic()));
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::X25PayloadMarshaller::unmarshalRestartPacketPayload(
        const stream::InputStream::SharedPointer& p_inputStream) {

    const codes::CauseCodeEnumerations::RestartRequestEnumeration cause = codes::CauseCodeEnumerations::restartRequestFromInt(0x00ff & p_inputStream->read());
    const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration diagnostics = codes::DiagnosticCodeEnumerations::fromInt(0x00ff & p_inputStream->read());

    return RestartPacketPayload::create(cause, diagnostics);
}

void xot4cpp::x25::payload::X25PayloadMarshaller::marshalDataPacketPayload(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const X25Payload::SharedPointer& p_source) {

    const DataPacketPayload::SharedPointer source = DataPacketPayload::downCast(p_source);
    p_outputStream->write(source->userData());
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::X25PayloadMarshaller::unmarshalDataPacketPayload(
        const stream::InputStream::SharedPointer& p_inputStream) {

    const Octets userData = p_inputStream->read(p_inputStream->available());
    return DataPacketPayload::create(userData);
}

void xot4cpp::x25::payload::X25PayloadMarshaller::marshalEmptyPacketPayload(
        const stream::OutputStream::SharedPointer& /* p_outputStream */,
        const X25Payload::SharedPointer& /* p_source */) {

    // do nothing
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::X25PayloadMarshaller::unmarshalEmptyPacketPayload(
        const stream::InputStream::SharedPointer& /* p_inputStream */) {

    return EmptyPacketPayload::create();
}
