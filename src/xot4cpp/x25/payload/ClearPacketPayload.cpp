#include "xot4cpp/x25/payload/ClearPacketPayload.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::payload::ClearPacketPayload::SharedPointer xot4cpp::x25::payload::ClearPacketPayload::downCast(
        const X25Payload::SharedPointer& p_pointer) {

    typedef ClearPacketPayload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::ClearPacketPayload::upCast(
        const SharedPointer& p_pointer) {

    typedef X25Payload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::ClearPacketPayload::ClearPacketPayload(
        const codes::CauseCodeEnumerations::ClearRequestEnumeration& p_cause,
        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic,
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress,
        const facilities::Facility::List& p_facilities,
        const Octets& p_userData)
: m_cause(p_cause),
m_diagnostic(p_diagnostic),
m_calledAddress(p_calledAddress),
m_callingAddress(p_callingAddress),
m_facilities(p_facilities),
m_userData(p_userData) {
}

xot4cpp::x25::payload::ClearPacketPayload::~ClearPacketPayload(void) {
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::ClearPacketPayload::create(
        const codes::CauseCodeEnumerations::ClearRequestEnumeration& p_cause,
        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic,
        const x121::X121Address::SharedPointer& p_calledAddress,
        const x121::X121Address::SharedPointer& p_callingAddress,
        const facilities::Facility::List& p_facilities,
        const Octets& p_userData) {

    const X25Payload::SharedPointer pointer(new ClearPacketPayload(
            p_cause,
            p_diagnostic,
            p_calledAddress,
            p_callingAddress,
            p_facilities,
            p_userData));

    return pointer;
}

const xot4cpp::x25::codes::CauseCodeEnumerations::ClearRequestEnumeration& xot4cpp::x25::payload::ClearPacketPayload::cause(void) const {

    return m_cause;
}

const xot4cpp::x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& xot4cpp::x25::payload::ClearPacketPayload::diagnostic(void) const {

    return m_diagnostic;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::payload::ClearPacketPayload::calledAddress(void) const {

    return m_calledAddress;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::payload::ClearPacketPayload::callingAddress(void) const {

    return m_callingAddress;
}

const xot4cpp::x25::facilities::Facility::List& xot4cpp::x25::payload::ClearPacketPayload::facilities(void) const {

    return m_facilities;
}

const xot4cpp::Octets& xot4cpp::x25::payload::ClearPacketPayload::userData(void) const {

    return m_userData;
}
