#include "xot4cpp/x25/payload/RestartPacketPayload.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::payload::RestartPacketPayload::SharedPointer xot4cpp::x25::payload::RestartPacketPayload::downCast(
        const X25Payload::SharedPointer& p_pointer) {

    typedef RestartPacketPayload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::RestartPacketPayload::upCast(
        const SharedPointer& p_pointer) {

    typedef X25Payload Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::payload::RestartPacketPayload::RestartPacketPayload(
        const codes::CauseCodeEnumerations::RestartRequestEnumeration& p_cause,
        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic)
: m_cause(p_cause),
m_diagnostic(p_diagnostic) {
}

xot4cpp::x25::payload::RestartPacketPayload::~RestartPacketPayload(void) {
}

xot4cpp::x25::payload::X25Payload::SharedPointer xot4cpp::x25::payload::RestartPacketPayload::create(
        const codes::CauseCodeEnumerations::RestartRequestEnumeration& p_cause,
        const codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& p_diagnostic) {

    const X25Payload::SharedPointer pointer(new RestartPacketPayload(
            p_cause,
            p_diagnostic));

    return pointer;
}

const xot4cpp::x25::codes::CauseCodeEnumerations::RestartRequestEnumeration& xot4cpp::x25::payload::RestartPacketPayload::cause(void) const {

    return m_cause;
}

const xot4cpp::x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& xot4cpp::x25::payload::RestartPacketPayload::diagnostic(void) const {

    return m_diagnostic;
}
