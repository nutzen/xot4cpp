#include "xot4cpp/x25/facilities/PacketSizeSelection.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::PacketSizeSelection

xot4cpp::x25::facilities::PacketSizeSelection::SharedPointer xot4cpp::x25::facilities::PacketSizeSelection::downCast(const Facility::SharedPointer& p_pointer) {

    typedef PacketSizeSelection Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::PacketSizeSelection::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::PacketSizeSelection::PacketSizeSelection(
        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_transmitPacketSize,
        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_receivePacketSize)
: Facility(ENUMERATION),
m_transmitPacketSize(p_transmitPacketSize),
m_receivePacketSize(p_receivePacketSize) {
}

xot4cpp::x25::facilities::PacketSizeSelection::~PacketSizeSelection(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::PacketSizeSelection::create(
        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_transmitPacketSize,
        const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_receivePacketSize) {

    const Facility::SharedPointer pointer(new PacketSizeSelection(p_transmitPacketSize, p_receivePacketSize));
    return pointer;
}

const xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& xot4cpp::x25::facilities::PacketSizeSelection::transmitPacketSize(void) const {

    return m_transmitPacketSize;
}

const xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& xot4cpp::x25::facilities::PacketSizeSelection::receivePacketSize(void) const {

    return m_receivePacketSize;
}
