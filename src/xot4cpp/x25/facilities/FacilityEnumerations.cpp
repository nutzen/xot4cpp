#include "xot4cpp/x25/facilities/FacilityEnumerations.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

// DESCRIPTION
#define REVERSE_CHARGING_AND_FAST_SELECT_DESCRIPTION "Reverse_charging_and_fast_select"
#define THROUGHPUT_CLASS_DESCRIPTION "Throughput_class"
#define CLOSED_USER_GROUP_SELECTION_DESCRIPTION "Closed_user_group_selection"
#define CHARGING_INFORMATION_REQUEST_DESCRIPTION "Charging_information_request"
#define CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_DESCRIPTION "Called_line_address_modified_notification"
#define CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_DESCRIPTION "Closed_User_Group_(CUG)_with_outgoing_access"
#define QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_DESCRIPTION "Quality_of_Service_Negotiation_-_minimum_throughput_class"
#define EXPEDITED_DATA_NEGOTIATION_DESCRIPTION "Expedited_Data_Negotiation"
#define BILATERAL_CLOSED_USER_GROUP_SELECTION_DESCRIPTION "Bilateral_closed_user_group_selection"
#define PACKET_SIZE_SELECTION_DESCRIPTION "Packet_size_selection"
#define WINDOW_SIZE_SELECTION_DESCRIPTION "Window_size_selection"
#define RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_DESCRIPTION "Recognized_Private_Operating_Agency_(RPOA)_selection_(basic_format)"
#define TRANSIT_DELAY_SELECTION_AND_INDICATION_DESCRIPTION "Transit_delay_selection_and_indication"
#define CHARGING_CALL_DURATION_DESCRIPTION "Charging_(call_duration)"
#define CHARGING_SEGMENT_COUNT_DESCRIPTION "Charging_(segment_count)"
#define CALL_REDIRECTION_NOTIFICATION_DESCRIPTION "Call_redirection_notification"
#define RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_DESCRIPTION "Recognized_Private_Operating_Agency_(RPOA)_selection_(extended_format)"
#define CHARGING_MONETARY_UNIT_DESCRIPTION "Charging_(monetary_unit)"
#define NETWORK_USER_IDENTIFICATION_DESCRIPTION "Network_User_identification_(NUI)"
#define CALLED_ADDRESS_EXTENSION_OSI_DESCRIPTION "Called_Address_Extension_(OSI)"
#define QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_DESCRIPTION "Quality_of_Service_Negotiation_-_End_to_end_transit_delay"
#define CALLING_ADDRESS_EXTENTION_OSI_DESCRIPTION "Calling_Address_Extension_(OSI)"
// MASK
#define REVERSE_CHARGING_AND_FAST_SELECT_MASK 0x01
#define THROUGHPUT_CLASS_MASK 0x02
#define CLOSED_USER_GROUP_SELECTION_MASK 0x03
#define CHARGING_INFORMATION_REQUEST_MASK 0x04
#define CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_MASK 0x08
#define CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_MASK 0x09
#define QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_MASK 0x0a
#define EXPEDITED_DATA_NEGOTIATION_MASK 0x0b
#define BILATERAL_CLOSED_USER_GROUP_SELECTION_MASK 0x41
#define PACKET_SIZE_SELECTION_MASK 0x42
#define WINDOW_SIZE_SELECTION_MASK 0x43
#define RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_MASK 0x44
#define TRANSIT_DELAY_SELECTION_AND_INDICATION_MASK 0x49
#define CHARGING_CALL_DURATION_MASK 0xc1
#define CHARGING_SEGMENT_COUNT_MASK 0xc2
#define CALL_REDIRECTION_NOTIFICATION_MASK 0xc4
#define RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_MASK 0xc5
#define CHARGING_MONETARY_UNIT_MASK 0xc3
#define NETWORK_USER_IDENTIFICATION_MASK 0xc6
#define CALLED_ADDRESS_EXTENSION_OSI_MASK 0xc9
#define QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_MASK 0xca
#define CALLING_ADDRESS_EXTENTION_OSI_MASK 0xcb
// Unhandled values
#define UNHANDLED_TEXT ""
#define UNHANDLED_VALUE -1

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            namespace FacilityEnumerations {

                extern bool compareWithMask(const int& p_mask, const int& p_source);
            };
        };
    };
};

std::string xot4cpp::x25::facilities::FacilityEnumerations::toDescription(
        const FacilityEnumeration& p_source) {

    std::string destination;

    switch (p_source) {
        case ReverseChargingAndFastSelect:
            destination = REVERSE_CHARGING_AND_FAST_SELECT_DESCRIPTION;
            break;

        case ThroughputClass:
            destination = THROUGHPUT_CLASS_DESCRIPTION;
            break;

        case ClosedUserGroupSelection:
            destination = CLOSED_USER_GROUP_SELECTION_DESCRIPTION;
            break;

        case ChargingInformationRequest:
            destination = CHARGING_INFORMATION_REQUEST_DESCRIPTION;
            break;

        case CalledLineAddressModifiedNotification:
            destination = CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_DESCRIPTION;
            break;

        case ClosedUserGroupWithOutgoingAccess:
            destination = CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_DESCRIPTION;
            break;

        case QualityOfServiceNegotiationMinimumThroughputClass:
            destination = QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_DESCRIPTION;
            break;

        case ExpeditedDataNegotiation:
            destination = EXPEDITED_DATA_NEGOTIATION_DESCRIPTION;
            break;

        case BilateralClosedUserGroupSelection:
            destination = BILATERAL_CLOSED_USER_GROUP_SELECTION_DESCRIPTION;
            break;

        case PacketSizeSelection:
            destination = PACKET_SIZE_SELECTION_DESCRIPTION;
            break;

        case WindowSizeSelection:
            destination = WINDOW_SIZE_SELECTION_DESCRIPTION;
            break;

        case RecognizedPrivateOperatingAgencySelectionBasicFormat:
            destination = RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_DESCRIPTION;
            break;

        case TransitDelaySelectionAndIndication:
            destination = TRANSIT_DELAY_SELECTION_AND_INDICATION_DESCRIPTION;
            break;

        case ChargingCallDuration:
            destination = CHARGING_CALL_DURATION_DESCRIPTION;
            break;

        case ChargingSegmentCount:
            destination = CHARGING_SEGMENT_COUNT_DESCRIPTION;
            break;

        case CallRedirectionNotification:
            destination = CALL_REDIRECTION_NOTIFICATION_DESCRIPTION;
            break;

        case RecognizedPrivateOperatingAgencySelectionExtendedFormat:
            destination = RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_DESCRIPTION;
            break;

        case ChargingMonetaryUnit:
            destination = CHARGING_MONETARY_UNIT_DESCRIPTION;
            break;

        case NetworkUserIdentification:
            destination = NETWORK_USER_IDENTIFICATION_DESCRIPTION;
            break;

        case CalledAddressExtensionOsi:
            destination = CALLED_ADDRESS_EXTENSION_OSI_DESCRIPTION;
            break;

        case QualityOfServiceNegotiationEndToEndTransitDelay:
            destination = QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_DESCRIPTION;
            break;

        case CallingAddressExtensionOsi:
            destination = CALLING_ADDRESS_EXTENTION_OSI_DESCRIPTION;
            break;

        default:
            destination = UNHANDLED_TEXT;
            break;
    }

    return destination;
}

xot4cpp::x25::facilities::FacilityEnumerations::FacilityEnumeration xot4cpp::x25::facilities::FacilityEnumerations::fromDescription(
        const std::string& p_source) {

    FacilityEnumeration destination;

    if (0 == p_source.compare(REVERSE_CHARGING_AND_FAST_SELECT_DESCRIPTION)) {
        destination = ReverseChargingAndFastSelect;

    } else if (0 == p_source.compare(THROUGHPUT_CLASS_DESCRIPTION)) {
        destination = ThroughputClass;

    } else if (0 == p_source.compare(CLOSED_USER_GROUP_SELECTION_DESCRIPTION)) {
        destination = ClosedUserGroupSelection;

    } else if (0 == p_source.compare(CHARGING_INFORMATION_REQUEST_DESCRIPTION)) {
        destination = ChargingInformationRequest;

    } else if (0 == p_source.compare(CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_DESCRIPTION)) {
        destination = CalledLineAddressModifiedNotification;

    } else if (0 == p_source.compare(CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_DESCRIPTION)) {
        destination = ClosedUserGroupWithOutgoingAccess;

    } else if (0 == p_source.compare(QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_DESCRIPTION)) {
        destination = QualityOfServiceNegotiationMinimumThroughputClass;

    } else if (0 == p_source.compare(EXPEDITED_DATA_NEGOTIATION_DESCRIPTION)) {
        destination = ExpeditedDataNegotiation;

    } else if (0 == p_source.compare(BILATERAL_CLOSED_USER_GROUP_SELECTION_DESCRIPTION)) {
        destination = BilateralClosedUserGroupSelection;

    } else if (0 == p_source.compare(PACKET_SIZE_SELECTION_DESCRIPTION)) {
        destination = PacketSizeSelection;

    } else if (0 == p_source.compare(WINDOW_SIZE_SELECTION_DESCRIPTION)) {
        destination = WindowSizeSelection;

    } else if (0 == p_source.compare(RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_DESCRIPTION)) {
        destination = RecognizedPrivateOperatingAgencySelectionBasicFormat;

    } else if (0 == p_source.compare(TRANSIT_DELAY_SELECTION_AND_INDICATION_DESCRIPTION)) {
        destination = TransitDelaySelectionAndIndication;

    } else if (0 == p_source.compare(CHARGING_CALL_DURATION_DESCRIPTION)) {
        destination = ChargingCallDuration;

    } else if (0 == p_source.compare(CHARGING_SEGMENT_COUNT_DESCRIPTION)) {
        destination = ChargingSegmentCount;

    } else if (0 == p_source.compare(CALL_REDIRECTION_NOTIFICATION_DESCRIPTION)) {
        destination = CallRedirectionNotification;

    } else if (0 == p_source.compare(RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_DESCRIPTION)) {
        destination = RecognizedPrivateOperatingAgencySelectionExtendedFormat;

    } else if (0 == p_source.compare(CHARGING_MONETARY_UNIT_DESCRIPTION)) {
        destination = ChargingMonetaryUnit;

    } else if (0 == p_source.compare(NETWORK_USER_IDENTIFICATION_DESCRIPTION)) {
        destination = NetworkUserIdentification;

    } else if (0 == p_source.compare(CALLED_ADDRESS_EXTENSION_OSI_DESCRIPTION)) {
        destination = CalledAddressExtensionOsi;

    } else if (0 == p_source.compare(QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_DESCRIPTION)) {
        destination = QualityOfServiceNegotiationEndToEndTransitDelay;

    } else if (0 == p_source.compare(CALLING_ADDRESS_EXTENTION_OSI_DESCRIPTION)) {
        return CallingAddressExtensionOsi;

    } else {
        destination = static_cast<FacilityEnumeration> (UNHANDLED_VALUE);
    }

    return destination;
}

int xot4cpp::x25::facilities::FacilityEnumerations::toInt(
        const FacilityEnumeration& p_source) {

    int destination;

    switch (p_source) {
        case ReverseChargingAndFastSelect:
            destination = REVERSE_CHARGING_AND_FAST_SELECT_MASK;
            break;

        case ThroughputClass:
            destination = THROUGHPUT_CLASS_MASK;
            break;

        case ClosedUserGroupSelection:
            destination = CLOSED_USER_GROUP_SELECTION_MASK;
            break;

        case ChargingInformationRequest:
            destination = CHARGING_INFORMATION_REQUEST_MASK;
            break;

        case CalledLineAddressModifiedNotification:
            destination = CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_MASK;
            break;

        case ClosedUserGroupWithOutgoingAccess:
            destination = CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_MASK;
            break;

        case QualityOfServiceNegotiationMinimumThroughputClass:
            destination = QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_MASK;
            break;

        case ExpeditedDataNegotiation:
            destination = EXPEDITED_DATA_NEGOTIATION_MASK;
            break;

        case BilateralClosedUserGroupSelection:
            destination = BILATERAL_CLOSED_USER_GROUP_SELECTION_MASK;
            break;

        case PacketSizeSelection:
            destination = PACKET_SIZE_SELECTION_MASK;
            break;

        case WindowSizeSelection:
            destination = WINDOW_SIZE_SELECTION_MASK;
            break;

        case RecognizedPrivateOperatingAgencySelectionBasicFormat:
            destination = RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_MASK;
            break;

        case TransitDelaySelectionAndIndication:
            destination = TRANSIT_DELAY_SELECTION_AND_INDICATION_MASK;
            break;

        case ChargingCallDuration:
            destination = CHARGING_CALL_DURATION_MASK;
            break;

        case ChargingSegmentCount:
            destination = CHARGING_SEGMENT_COUNT_MASK;
            break;

        case CallRedirectionNotification:
            destination = CALL_REDIRECTION_NOTIFICATION_MASK;
            break;

        case RecognizedPrivateOperatingAgencySelectionExtendedFormat:
            destination = RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_MASK;
            break;

        case ChargingMonetaryUnit:
            destination = CHARGING_MONETARY_UNIT_MASK;
            break;

        case NetworkUserIdentification:
            destination = NETWORK_USER_IDENTIFICATION_MASK;
            break;

        case CalledAddressExtensionOsi:
            destination = CALLED_ADDRESS_EXTENSION_OSI_MASK;
            break;

        case QualityOfServiceNegotiationEndToEndTransitDelay:
            destination = QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_MASK;
            break;

        case CallingAddressExtensionOsi:
            destination = CALLING_ADDRESS_EXTENTION_OSI_MASK;
            break;

        default:
            destination = UNHANDLED_VALUE;
            break;
    }

    return destination;
}

xot4cpp::x25::facilities::FacilityEnumerations::FacilityEnumeration xot4cpp::x25::facilities::FacilityEnumerations::fromInt(
        const int& p_source) {

    FacilityEnumeration destination;

    if (compareWithMask(REVERSE_CHARGING_AND_FAST_SELECT_MASK, p_source)) {
        destination = ReverseChargingAndFastSelect;

    } else if (compareWithMask(THROUGHPUT_CLASS_MASK, p_source)) {
        destination = ThroughputClass;

    } else if (compareWithMask(CLOSED_USER_GROUP_SELECTION_MASK, p_source)) {
        destination = ClosedUserGroupSelection;

    } else if (compareWithMask(CHARGING_INFORMATION_REQUEST_MASK, p_source)) {
        destination = ChargingInformationRequest;

    } else if (compareWithMask(CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_MASK, p_source)) {
        destination = CalledLineAddressModifiedNotification;

    } else if (compareWithMask(CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_MASK, p_source)) {
        destination = ClosedUserGroupWithOutgoingAccess;

    } else if (compareWithMask(QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_MASK, p_source)) {
        destination = QualityOfServiceNegotiationMinimumThroughputClass;

    } else if (compareWithMask(EXPEDITED_DATA_NEGOTIATION_MASK, p_source)) {
        destination = ExpeditedDataNegotiation;

    } else if (compareWithMask(BILATERAL_CLOSED_USER_GROUP_SELECTION_MASK, p_source)) {
        destination = BilateralClosedUserGroupSelection;

    } else if (compareWithMask(PACKET_SIZE_SELECTION_MASK, p_source)) {
        destination = PacketSizeSelection;

    } else if (compareWithMask(WINDOW_SIZE_SELECTION_MASK, p_source)) {
        destination = WindowSizeSelection;

    } else if (compareWithMask(RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_MASK, p_source)) {
        destination = RecognizedPrivateOperatingAgencySelectionBasicFormat;

    } else if (compareWithMask(TRANSIT_DELAY_SELECTION_AND_INDICATION_MASK, p_source)) {
        destination = TransitDelaySelectionAndIndication;

    } else if (compareWithMask(CHARGING_CALL_DURATION_MASK, p_source)) {
        destination = ChargingCallDuration;

    } else if (compareWithMask(CHARGING_SEGMENT_COUNT_MASK, p_source)) {
        destination = ChargingSegmentCount;

    } else if (compareWithMask(CALL_REDIRECTION_NOTIFICATION_MASK, p_source)) {
        destination = CallRedirectionNotification;

    } else if (compareWithMask(RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_MASK, p_source)) {
        destination = RecognizedPrivateOperatingAgencySelectionExtendedFormat;

    } else if (compareWithMask(CHARGING_MONETARY_UNIT_MASK, p_source)) {
        destination = ChargingMonetaryUnit;

    } else if (compareWithMask(NETWORK_USER_IDENTIFICATION_MASK, p_source)) {
        destination = NetworkUserIdentification;

    } else if (compareWithMask(CALLED_ADDRESS_EXTENSION_OSI_MASK, p_source)) {
        destination = CalledAddressExtensionOsi;

    } else if (compareWithMask(QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_MASK, p_source)) {
        destination = QualityOfServiceNegotiationEndToEndTransitDelay;

    } else if (compareWithMask(CALLING_ADDRESS_EXTENTION_OSI_MASK, p_source)) {
        destination = CallingAddressExtensionOsi;

    } else {
        destination = static_cast<FacilityEnumeration> (UNHANDLED_VALUE);
    }

    return destination;
}

bool xot4cpp::x25::facilities::FacilityEnumerations::compareWithMask(const int& p_mask, const int& p_source) {

    return (p_mask == p_source);
}
