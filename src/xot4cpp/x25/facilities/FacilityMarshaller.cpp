#include "xot4cpp/x25/facilities/FacilityMarshaller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

// one parameter
#include "xot4cpp/x25/facilities/ReverseChargingAndFastSelect.hpp"
#include "xot4cpp/x25/facilities/ThroughputClass.hpp"
#include "xot4cpp/x25/facilities/ClosedUserGroupSelection.hpp"
#include "xot4cpp/x25/facilities/ChargingInformationRequest.hpp"
#include "xot4cpp/x25/facilities/CalledLineAddressModifiedNotification.hpp"
#include "xot4cpp/x25/facilities/ClosedUserGroupWithOutgoingAccess.hpp"
#include "xot4cpp/x25/facilities/QualityOfServiceNegotiationMinimumThroughputClass.hpp"
#include "xot4cpp/x25/facilities/ExpeditedDataNegotiation.hpp"
// two parameters
#include "xot4cpp/x25/facilities/BilateralClosedUserGroupSelection.hpp"
#include "xot4cpp/x25/facilities/PacketSizeSelection.hpp"
#include "xot4cpp/x25/facilities/WindowSizeSelection.hpp"
#include "xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionBasicFormat.hpp"
#include "xot4cpp/x25/facilities/TransitDelaySelectionAndIndication.hpp"
// variable parameters
#include "xot4cpp/x25/facilities/ChargingCallDuration.hpp"
#include "xot4cpp/x25/facilities/ChargingSegmentCount.hpp"
#include "xot4cpp/x25/facilities/CallRedirectionNotification.hpp"
#include "xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionExtendedFormat.hpp"
#include "xot4cpp/x25/facilities/ChargingMonetaryUnit.hpp"
#include "xot4cpp/x25/facilities/NetworkUserIdentification.hpp"
#include "xot4cpp/x25/facilities/CalledAddressExtensionOsi.hpp"
#include "xot4cpp/x25/facilities/QualityOfServiceNegotiationEndToEndTransitDelay.hpp"
#include "xot4cpp/x25/facilities/CallingAddressExtensionOsi.hpp"

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            namespace FacilityMarshaller {
                extern Octets marshal(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshal(const FacilityEnumerations::FacilityEnumeration& p_enumeration, OctetStack& p_source);

                // one parameter
                extern Octets marshalReverseChargingAndFastSelect(const Facility::SharedPointer& p_source);

                extern Facility::SharedPointer unmarshalReverseChargingAndFastSelect(OctetStack& p_source);

                extern Octets marshalThroughputClass(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalThroughputClass(OctetStack& p_source);

                extern Octets marshalClosedUserGroupSelection(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalClosedUserGroupSelection(OctetStack& p_source);

                extern Octets marshalChargingInformationRequest(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalChargingInformationRequest(OctetStack& p_source);

                extern Octets marshalCalledLineAddressModifiedNotification(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalCalledLineAddressModifiedNotification(OctetStack& p_source);

                extern Octets marshalClosedUserGroupWithOutgoingAccess(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalClosedUserGroupWithOutgoingAccess(OctetStack& p_source);

                extern Octets marshalQualityOfServiceNegotiationMinimumThroughputClass(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalQualityOfServiceNegotiationMinimumThroughputClass(OctetStack& p_source);

                extern Octets marshalExpeditedDataNegotiation(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalExpeditedDataNegotiation(OctetStack& p_source);

                // two parameters
                extern Octets marshalBilateralClosedUserGroupSelection(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalBilateralClosedUserGroupSelection(OctetStack& p_source);

                extern Octets marshalPacketSizeSelection(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalPacketSizeSelection(OctetStack& p_source);

                extern Octets marshalWindowSizeSelection(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalWindowSizeSelection(OctetStack& p_source);

                extern Octets marshalRecognizedPrivateOperatingAgencySelectionBasicFormat(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalRecognizedPrivateOperatingAgencySelectionBasicFormat(OctetStack& p_source);

                extern Octets marshalTransitDelaySelectionAndIndication(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalTransitDelaySelectionAndIndication(OctetStack& p_source);

                // variable parameters
                extern Octets marshalChargingCallDuration(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalChargingCallDuration(OctetStack& p_source);

                extern Octets marshalChargingSegmentCount(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalChargingSegmentCount(OctetStack& p_source);

                extern Octets marshalCallRedirectionNotification(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalCallRedirectionNotification(OctetStack& p_source);

                extern Octets marshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(OctetStack& p_source);

                extern Octets marshalChargingMonetaryUnit(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalChargingMonetaryUnit(OctetStack& p_source);

                extern Octets marshalNetworkUserIdentification(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalNetworkUserIdentification(OctetStack& p_source);

                extern Octets marshalCalledAddressExtensionOsi(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalCalledAddressExtensionOsi(OctetStack& p_source);

                extern Octets marshalQualityOfServiceNegotiationEndToEndTransitDelay(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalQualityOfServiceNegotiationEndToEndTransitDelay(OctetStack& p_source);

                extern Octets marshalCallingAddressExtensionOsi(const Facility::SharedPointer& p_source);
                extern Facility::SharedPointer unmarshalCallingAddressExtensionOsi(OctetStack& p_source);
            };
        };
    };
};

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshal(const Facility::List& p_source) {

    Octets target;

    typedef Facility::List::const_iterator Iterator;
    for (Iterator iterator = p_source.begin(); p_source.end() != iterator; ++iterator) {
        const Facility::SharedPointer facility = *iterator;
        const Octets buffer = marshal(facility);

        target.insert(target.end(), buffer.begin(), buffer.end());
    }

    return target;
}

xot4cpp::x25::facilities::Facility::List xot4cpp::x25::facilities::FacilityMarshaller::unmarshal(const Octets& p_source) {

    Facility::List target;
    OctetStack source(OctetStackSequence(p_source.rbegin(), p_source.rend()));
    // Octets source(p_source);

    while (0 != source.size()) {
        const Octet octet = source.top();
        source.pop();
        const FacilityEnumerations::FacilityEnumeration enumeration = FacilityEnumerations::fromInt((int) (octet & 0x00ff));

        const Facility::SharedPointer facility = unmarshal(enumeration, source);
        if (facility.get()) {
            target.push_back(facility);
        }
    }

    return target;
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshal(const Facility::SharedPointer& p_source) {

    Octets target;
    if (p_source.get()) {
        const FacilityEnumerations::FacilityEnumeration enumeration = p_source->enumeration();

        switch (enumeration) {

                // one parameter
            case FacilityEnumerations::ReverseChargingAndFastSelect:
                target = marshalReverseChargingAndFastSelect(p_source);
                break;

            case FacilityEnumerations::ThroughputClass:
                target = marshalThroughputClass(p_source);
                break;

            case FacilityEnumerations::ClosedUserGroupSelection:
                target = marshalClosedUserGroupSelection(p_source);
                break;

            case FacilityEnumerations::ChargingInformationRequest:
                target = marshalChargingInformationRequest(p_source);
                break;

            case FacilityEnumerations::CalledLineAddressModifiedNotification:
                target = marshalCalledLineAddressModifiedNotification(p_source);
                break;

            case FacilityEnumerations::ClosedUserGroupWithOutgoingAccess:
                target = marshalClosedUserGroupWithOutgoingAccess(p_source);
                break;

            case FacilityEnumerations::QualityOfServiceNegotiationMinimumThroughputClass:
                target = marshalQualityOfServiceNegotiationMinimumThroughputClass(p_source);
                break;

            case FacilityEnumerations::ExpeditedDataNegotiation:
                target = marshalExpeditedDataNegotiation(p_source);
                break;

                // two parameters
            case FacilityEnumerations::BilateralClosedUserGroupSelection:
                target = marshalBilateralClosedUserGroupSelection(p_source);
                break;

            case FacilityEnumerations::PacketSizeSelection:
                target = marshalPacketSizeSelection(p_source);
                break;

            case FacilityEnumerations::WindowSizeSelection:
                target = marshalWindowSizeSelection(p_source);
                break;

            case FacilityEnumerations::RecognizedPrivateOperatingAgencySelectionBasicFormat:
                target = marshalRecognizedPrivateOperatingAgencySelectionBasicFormat(p_source);
                break;

            case FacilityEnumerations::TransitDelaySelectionAndIndication:
                target = marshalTransitDelaySelectionAndIndication(p_source);
                break;

                // variable parameters
            case FacilityEnumerations::ChargingCallDuration:
                target = marshalChargingCallDuration(p_source);
                break;

            case FacilityEnumerations::ChargingSegmentCount:
                target = marshalChargingSegmentCount(p_source);
                break;

            case FacilityEnumerations::CallRedirectionNotification:
                target = marshalCallRedirectionNotification(p_source);
                break;

            case FacilityEnumerations::RecognizedPrivateOperatingAgencySelectionExtendedFormat:
                target = marshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(p_source);
                break;

            case FacilityEnumerations::ChargingMonetaryUnit:
                target = marshalChargingMonetaryUnit(p_source);
                break;

            case FacilityEnumerations::NetworkUserIdentification:
                target = marshalNetworkUserIdentification(p_source);
                break;

            case FacilityEnumerations::CalledAddressExtensionOsi:
                target = marshalCalledAddressExtensionOsi(p_source);
                break;

            case FacilityEnumerations::QualityOfServiceNegotiationEndToEndTransitDelay:
                target = marshalQualityOfServiceNegotiationEndToEndTransitDelay(p_source);
                break;

            case FacilityEnumerations::CallingAddressExtensionOsi:
                target = marshalCallingAddressExtensionOsi(p_source);
                break;

            default:
                THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
                break;
        }
    }

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshal(const FacilityEnumerations::FacilityEnumeration& p_enumeration, OctetStack& p_source) {

    Facility::SharedPointer target;

    switch (p_enumeration) {

            // one parameter
        case FacilityEnumerations::ReverseChargingAndFastSelect:
            target = unmarshalReverseChargingAndFastSelect(p_source);
            break;

        case FacilityEnumerations::ThroughputClass:
            target = unmarshalThroughputClass(p_source);
            break;

        case FacilityEnumerations::ClosedUserGroupSelection:
            target = unmarshalClosedUserGroupSelection(p_source);
            break;

        case FacilityEnumerations::ChargingInformationRequest:
            target = unmarshalChargingInformationRequest(p_source);
            break;

        case FacilityEnumerations::CalledLineAddressModifiedNotification:
            target = unmarshalCalledLineAddressModifiedNotification(p_source);
            break;

        case FacilityEnumerations::ClosedUserGroupWithOutgoingAccess:
            target = unmarshalClosedUserGroupWithOutgoingAccess(p_source);
            break;

        case FacilityEnumerations::QualityOfServiceNegotiationMinimumThroughputClass:
            target = unmarshalQualityOfServiceNegotiationMinimumThroughputClass(p_source);
            break;

        case FacilityEnumerations::ExpeditedDataNegotiation:
            target = unmarshalExpeditedDataNegotiation(p_source);
            break;

            // two parameters
        case FacilityEnumerations::BilateralClosedUserGroupSelection:
            target = unmarshalBilateralClosedUserGroupSelection(p_source);
            break;

        case FacilityEnumerations::PacketSizeSelection:
            target = unmarshalPacketSizeSelection(p_source);
            break;

        case FacilityEnumerations::WindowSizeSelection:
            target = unmarshalWindowSizeSelection(p_source);
            break;

        case FacilityEnumerations::RecognizedPrivateOperatingAgencySelectionBasicFormat:
            target = unmarshalRecognizedPrivateOperatingAgencySelectionBasicFormat(p_source);
            break;

        case FacilityEnumerations::TransitDelaySelectionAndIndication:
            target = unmarshalTransitDelaySelectionAndIndication(p_source);
            break;

            // variable parameters
        case FacilityEnumerations::ChargingCallDuration:
            target = unmarshalChargingCallDuration(p_source);
            break;

        case FacilityEnumerations::ChargingSegmentCount:
            target = unmarshalChargingSegmentCount(p_source);
            break;

        case FacilityEnumerations::CallRedirectionNotification:
            target = unmarshalCallRedirectionNotification(p_source);
            break;

        case FacilityEnumerations::RecognizedPrivateOperatingAgencySelectionExtendedFormat:
            target = unmarshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(p_source);
            break;

        case FacilityEnumerations::ChargingMonetaryUnit:
            target = unmarshalChargingMonetaryUnit(p_source);
            break;

        case FacilityEnumerations::NetworkUserIdentification:
            target = unmarshalNetworkUserIdentification(p_source);
            break;

        case FacilityEnumerations::CalledAddressExtensionOsi:
            target = unmarshalCalledAddressExtensionOsi(p_source);
            break;

        case FacilityEnumerations::QualityOfServiceNegotiationEndToEndTransitDelay:
            target = unmarshalQualityOfServiceNegotiationEndToEndTransitDelay(p_source);
            break;

        case FacilityEnumerations::CallingAddressExtensionOsi:
            target = unmarshalCallingAddressExtensionOsi(p_source);
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_enumeration);
            break;
    }

    return target;
}

// one parameter

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalReverseChargingAndFastSelect(const Facility::SharedPointer& p_source) {

    typedef ReverseChargingAndFastSelect MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalReverseChargingAndFastSelect(OctetStack& p_source) {

    typedef ReverseChargingAndFastSelect MyFacility;

    const Octet parameter = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalThroughputClass(const Facility::SharedPointer& p_source) {

    typedef ThroughputClass MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalThroughputClass(OctetStack& p_source) {

    typedef ThroughputClass MyFacility;

    const Octet parameter = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalClosedUserGroupSelection(const Facility::SharedPointer& p_source) {

    typedef ClosedUserGroupSelection MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalClosedUserGroupSelection(OctetStack& p_source) {

    typedef ClosedUserGroupSelection MyFacility;

    const Octet parameter = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalChargingInformationRequest(const Facility::SharedPointer& p_source) {

    typedef ChargingInformationRequest MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalChargingInformationRequest(OctetStack& p_source) {

    typedef ChargingInformationRequest MyFacility;

    const Octet parameter = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalCalledLineAddressModifiedNotification(const Facility::SharedPointer& p_source) {

    typedef CalledLineAddressModifiedNotification MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalCalledLineAddressModifiedNotification(OctetStack& p_source) {

    typedef CalledLineAddressModifiedNotification MyFacility;

    const Octet parameter = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalClosedUserGroupWithOutgoingAccess(const Facility::SharedPointer& p_source) {

    typedef ClosedUserGroupWithOutgoingAccess MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalClosedUserGroupWithOutgoingAccess(OctetStack& p_source) {

    typedef ClosedUserGroupWithOutgoingAccess MyFacility;

    const Octet parameter = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalQualityOfServiceNegotiationMinimumThroughputClass(const Facility::SharedPointer& p_source) {

    typedef QualityOfServiceNegotiationMinimumThroughputClass MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalQualityOfServiceNegotiationMinimumThroughputClass(OctetStack& p_source) {

    typedef QualityOfServiceNegotiationMinimumThroughputClass MyFacility;

    const Octet parameter = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalExpeditedDataNegotiation(const Facility::SharedPointer& p_source) {

    typedef ExpeditedDataNegotiation MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalExpeditedDataNegotiation(OctetStack& p_source) {

    typedef ExpeditedDataNegotiation MyFacility;

    const Octet parameter = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter);
}
// two parameters

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalBilateralClosedUserGroupSelection(const Facility::SharedPointer& p_source) {

    typedef BilateralClosedUserGroupSelection MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter0 = source->parameter0();
    const Octet parameter1 = source->parameter1();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter0);
    target.push_back(parameter1);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalBilateralClosedUserGroupSelection(OctetStack& p_source) {

    typedef BilateralClosedUserGroupSelection MyFacility;

    const Octet parameter0 = p_source.top();
    p_source.pop();
    const Octet parameter1 = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter0, parameter1);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalPacketSizeSelection(const Facility::SharedPointer& p_source) {

    typedef PacketSizeSelection MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration transmitPacketSize = source->transmitPacketSize();
    const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration receivePacketSize = source->receivePacketSize();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(PacketSizeSelectionEnumerations::toInt(transmitPacketSize) & 0x000f);
    target.push_back(PacketSizeSelectionEnumerations::toInt(receivePacketSize) & 0x000f);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalPacketSizeSelection(OctetStack& p_source) {

    typedef PacketSizeSelection MyFacility;

    const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration transmitPacketSize = PacketSizeSelectionEnumerations::fromInt(p_source.top() & 0x000f);
    p_source.pop();
    const PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration receivePacketSize = PacketSizeSelectionEnumerations::fromInt(p_source.top() & 0x000f);
    p_source.pop();

    return MyFacility::create(transmitPacketSize, receivePacketSize);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalWindowSizeSelection(const Facility::SharedPointer& p_source) {

    typedef WindowSizeSelection MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const unsigned int transmissionWindowSize = source->transmissionWindowSize();
    const unsigned int receiveWindowSize = source->receiveWindowSize();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) (transmissionWindowSize & 0x000f));
    target.push_back((Octet) (receiveWindowSize & 0x000f));

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalWindowSizeSelection(OctetStack& p_source) {

    typedef WindowSizeSelection MyFacility;

    const unsigned int transmissionWindowSize = (p_source.top() & 0x000f);
    p_source.pop();
    const unsigned int receiveWindowSize = (p_source.top() & 0x000f);
    p_source.pop();

    return MyFacility::create(transmissionWindowSize, receiveWindowSize);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalRecognizedPrivateOperatingAgencySelectionBasicFormat(const Facility::SharedPointer& p_source) {

    typedef RecognizedPrivateOperatingAgencySelectionBasicFormat MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter0 = source->parameter0();
    const Octet parameter1 = source->parameter1();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter0);
    target.push_back(parameter1);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalRecognizedPrivateOperatingAgencySelectionBasicFormat(OctetStack& p_source) {

    typedef RecognizedPrivateOperatingAgencySelectionBasicFormat MyFacility;

    const Octet parameter0 = p_source.top();
    p_source.pop();
    const Octet parameter1 = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter0, parameter1);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalTransitDelaySelectionAndIndication(const Facility::SharedPointer& p_source) {

    typedef TransitDelaySelectionAndIndication MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octet parameter0 = source->parameter0();
    const Octet parameter1 = source->parameter1();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back(parameter0);
    target.push_back(parameter1);

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalTransitDelaySelectionAndIndication(OctetStack& p_source) {

    typedef TransitDelaySelectionAndIndication MyFacility;

    const Octet parameter0 = p_source.top();
    p_source.pop();
    const Octet parameter1 = p_source.top();
    p_source.pop();

    return MyFacility::create(parameter0, parameter1);
}

// variable parameters

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalChargingCallDuration(const Facility::SharedPointer& p_source) {

    typedef ChargingCallDuration MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalChargingCallDuration(OctetStack& p_source) {

    typedef ChargingCallDuration MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalChargingSegmentCount(const Facility::SharedPointer& p_source) {

    typedef ChargingSegmentCount MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalChargingSegmentCount(OctetStack& p_source) {

    typedef ChargingSegmentCount MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalCallRedirectionNotification(const Facility::SharedPointer& p_source) {

    typedef CallRedirectionNotification MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalCallRedirectionNotification(OctetStack& p_source) {

    typedef CallRedirectionNotification MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(const Facility::SharedPointer& p_source) {

    typedef RecognizedPrivateOperatingAgencySelectionExtendedFormat MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(OctetStack& p_source) {

    typedef RecognizedPrivateOperatingAgencySelectionExtendedFormat MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalChargingMonetaryUnit(const Facility::SharedPointer& p_source) {

    typedef ChargingMonetaryUnit MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalChargingMonetaryUnit(OctetStack& p_source) {

    typedef ChargingMonetaryUnit MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalNetworkUserIdentification(const Facility::SharedPointer& p_source) {

    typedef NetworkUserIdentification MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalNetworkUserIdentification(OctetStack& p_source) {

    typedef NetworkUserIdentification MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalCalledAddressExtensionOsi(const Facility::SharedPointer& p_source) {

    typedef CalledAddressExtensionOsi MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalCalledAddressExtensionOsi(OctetStack& p_source) {

    typedef CalledAddressExtensionOsi MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalQualityOfServiceNegotiationEndToEndTransitDelay(const Facility::SharedPointer& p_source) {

    typedef QualityOfServiceNegotiationEndToEndTransitDelay MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalQualityOfServiceNegotiationEndToEndTransitDelay(OctetStack& p_source) {

    typedef QualityOfServiceNegotiationEndToEndTransitDelay MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}

xot4cpp::Octets xot4cpp::x25::facilities::FacilityMarshaller::marshalCallingAddressExtensionOsi(const Facility::SharedPointer& p_source) {

    typedef CallingAddressExtensionOsi MyFacility;

    Octets target;
    const MyFacility::SharedPointer source = MyFacility::downCast(p_source);
    const FacilityEnumerations::FacilityEnumeration enumeration = source->enumeration();
    const Octets parameter = source->parameter();

    target.push_back((Octet) (FacilityEnumerations::toInt(enumeration) & 0x00ff));
    target.push_back((Octet) parameter.size());
    target.insert(target.end(), parameter.begin(), parameter.end());

    return target;
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::FacilityMarshaller::unmarshalCallingAddressExtensionOsi(OctetStack& p_source) {

    typedef CallingAddressExtensionOsi MyFacility;

    Octets parameter;
    const unsigned int length = p_source.top();
    p_source.pop();

    while (parameter.size() < length) {
        const Octet value = p_source.top();
        p_source.pop();
        parameter.push_back(value);
    }

    return MyFacility::create(parameter);
}
