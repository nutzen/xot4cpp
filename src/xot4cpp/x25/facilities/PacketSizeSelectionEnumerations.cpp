#include "xot4cpp/x25/facilities/PacketSizeSelectionEnumerations.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/facilities/PacketSizeSelection.hpp"
#include "xot4cpp/exceptions/Xot4cppException.hpp"

#define SIZE_16_OCTETS_DESCRIPTION "16_octets"
#define SIZE_32_OCTETS_DESCRIPTION "32_octets"
#define SIZE_64_OCTETS_DESCRIPTION "64_octets"
#define SIZE_128_OCTETS_DESCRIPTION "128_octets"
#define SIZE_256_OCTETS_DESCRIPTION "256_octets"
#define SIZE_512_OCTETS_DESCRIPTION "512_octets"
#define SIZE_1024_OCTETS_DESCRIPTION "1024_octets"
#define SIZE_2048_OCTETS_DESCRIPTION "2048_octets"
#define SIZE_4096_OCTETS_DESCRIPTION "4096_octets"
// MASK
#define SIZE_16_OCTETS_MASK 0x04
#define SIZE_32_OCTETS_MASK 0x05
#define SIZE_64_OCTETS_MASK 0x06
#define SIZE_128_OCTETS_MASK 0x07
#define SIZE_256_OCTETS_MASK 0x08
#define SIZE_512_OCTETS_MASK 0x09
#define SIZE_1024_OCTETS_MASK 0x0a
#define SIZE_2048_OCTETS_MASK 0x0b
#define SIZE_4096_OCTETS_MASK 0x0c

namespace xot4cpp {

    namespace x25 {

        namespace facilities {

            namespace PacketSizeSelectionEnumerations {

                extern bool compareWithMask(const int& p_mask, const int& p_source);
            };
        };
    };
};

std::string xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::toDescription(
        const PacketSizeSelectionEnumeration& p_source) {

    std::string destination;

    switch (p_source) {
        case Size16octets:
            destination = SIZE_16_OCTETS_DESCRIPTION;
            break;

        case Size32octets:
            destination = SIZE_32_OCTETS_DESCRIPTION;
            break;

        case Size64octets:
            destination = SIZE_64_OCTETS_DESCRIPTION;
            break;

        case Size128octets:
            destination = SIZE_128_OCTETS_DESCRIPTION;
            break;

        case Size256octets:
            destination = SIZE_256_OCTETS_DESCRIPTION;
            break;

        case Size512octets:
            destination = SIZE_512_OCTETS_DESCRIPTION;
            break;

        case Size1024octets:
            destination = SIZE_1024_OCTETS_DESCRIPTION;
            break;

        case Size2048octets:
            destination = SIZE_2048_OCTETS_DESCRIPTION;
            break;

        case Size4096octets:
            destination = SIZE_4096_OCTETS_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::fromDescription(
        const std::string& p_source) {

    PacketSizeSelectionEnumeration destination;

    if (0 == p_source.compare(SIZE_16_OCTETS_DESCRIPTION)) {
        destination = Size16octets;

    } else if (0 == p_source.compare(SIZE_32_OCTETS_DESCRIPTION)) {
        destination = Size32octets;

    } else if (0 == p_source.compare(SIZE_64_OCTETS_DESCRIPTION)) {
        destination = Size64octets;

    } else if (0 == p_source.compare(SIZE_128_OCTETS_DESCRIPTION)) {
        destination = Size128octets;

    } else if (0 == p_source.compare(SIZE_256_OCTETS_DESCRIPTION)) {
        destination = Size256octets;

    } else if (0 == p_source.compare(SIZE_512_OCTETS_DESCRIPTION)) {
        destination = Size512octets;

    } else if (0 == p_source.compare(SIZE_1024_OCTETS_DESCRIPTION)) {
        destination = Size1024octets;

    } else if (0 == p_source.compare(SIZE_2048_OCTETS_DESCRIPTION)) {
        destination = Size2048octets;

    } else if (0 == p_source.compare(SIZE_4096_OCTETS_DESCRIPTION)) {
        destination = Size4096octets;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

int xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::toInt(
        const xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration& p_source) {

    int destination;

    switch (p_source) {
        case Size16octets:
            destination = SIZE_16_OCTETS_MASK;
            break;

        case Size32octets:
            destination = SIZE_32_OCTETS_MASK;
            break;

        case Size64octets:
            destination = SIZE_64_OCTETS_MASK;
            break;

        case Size128octets:
            destination = SIZE_128_OCTETS_MASK;
            break;

        case Size256octets:
            destination = SIZE_256_OCTETS_MASK;
            break;

        case Size512octets:
            destination = SIZE_512_OCTETS_MASK;
            break;

        case Size1024octets:
            destination = SIZE_1024_OCTETS_MASK;
            break;

        case Size2048octets:
            destination = SIZE_2048_OCTETS_MASK;
            break;

        case Size4096octets:
            destination = SIZE_4096_OCTETS_MASK;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::fromInt(
        const int& p_source) {

    PacketSizeSelectionEnumeration destination;

    if (compareWithMask(SIZE_16_OCTETS_MASK, p_source)) {
        destination = Size16octets;

    } else if (compareWithMask(SIZE_32_OCTETS_MASK, p_source)) {
        destination = Size32octets;

    } else if (compareWithMask(SIZE_64_OCTETS_MASK, p_source)) {
        destination = Size64octets;

    } else if (compareWithMask(SIZE_128_OCTETS_MASK, p_source)) {
        destination = Size128octets;

    } else if (compareWithMask(SIZE_256_OCTETS_MASK, p_source)) {
        destination = Size256octets;

    } else if (compareWithMask(SIZE_512_OCTETS_MASK, p_source)) {
        destination = Size512octets;

    } else if (compareWithMask(SIZE_1024_OCTETS_MASK, p_source)) {
        destination = Size1024octets;

    } else if (compareWithMask(SIZE_2048_OCTETS_MASK, p_source)) {
        destination = Size2048octets;

    } else if (compareWithMask(SIZE_4096_OCTETS_MASK, p_source)) {
        destination = Size4096octets;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

unsigned int xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::toPacketSize(const PacketSizeSelectionEnumeration& p_source) {

    unsigned int destination;

    switch (p_source) {
        case Size16octets:
            destination = 16;
            break;

        case Size32octets:
            destination = 32;
            break;

        case Size64octets:
            destination = 64;
            break;

        case Size128octets:
            destination = 128;
            break;

        case Size256octets:
            destination = 256;
            break;

        case Size512octets:
            destination = 512;
            break;

        case Size1024octets:
            destination = 1024;
            break;

        case Size2048octets:
            destination = 2048;
            break;

        case Size4096octets:
            destination = 4096;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << toDescription(p_source));
    }

    return destination;
}

xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::fromPacketSize(const unsigned int& p_source) {

    PacketSizeSelectionEnumeration destination;

    switch (p_source) {
        case 16:
            destination = Size16octets;
            break;

        case 32:
            destination = Size32octets;
            break;

        case 64:
            destination = Size64octets;
            break;

        case 128:
            destination = Size128octets;
            break;

        case 256:
            destination = Size256octets;
            break;

        case 512:
            destination = Size512octets;
            break;

        case 1024:
            destination = Size1024octets;
            break;

        case 2048:
            destination = Size2048octets;
            break;

        case 4096:
            destination = Size4096octets;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

bool xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::compareWithMask(const int& p_mask, const int& p_source) {

    return (p_mask == p_source);
}
