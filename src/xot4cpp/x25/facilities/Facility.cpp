#include "xot4cpp/x25/facilities/Facility.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::facilities::Facility::Facility(const FacilityEnumerations::FacilityEnumeration& p_enumeration)
: m_enumeration(p_enumeration) {
}

xot4cpp::x25::facilities::Facility::~Facility(void) {
}

const xot4cpp::x25::facilities::FacilityEnumerations::FacilityEnumeration& xot4cpp::x25::facilities::Facility::enumeration(void) const {

    return m_enumeration;
}
