#include "xot4cpp/x25/facilities/ThroughputClass.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::ThroughputClass

xot4cpp::x25::facilities::ThroughputClass::SharedPointer xot4cpp::x25::facilities::ThroughputClass::downCast(const Facility::SharedPointer& p_pointer) {

    typedef ThroughputClass Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ThroughputClass::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::ThroughputClass::ThroughputClass(const Octet& p_parameter)
: BasicOneByteParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::ThroughputClass::~ThroughputClass(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ThroughputClass::create(const Octet& p_parameter) {

    const Facility::SharedPointer pointer(new ThroughputClass(p_parameter));
    return pointer;
}
