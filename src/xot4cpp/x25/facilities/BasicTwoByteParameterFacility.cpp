#include "xot4cpp/x25/facilities/BasicTwoByteParameterFacility.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::facilities::BasicTwoByteParameterFacility::BasicTwoByteParameterFacility(
        const FacilityEnumerations::FacilityEnumeration& p_enumeration,
        const Octet& p_parameter0,
        const Octet& p_parameter1)
: Facility(p_enumeration),
m_parameter0(p_parameter0),
m_parameter1(p_parameter1) {
}

xot4cpp::x25::facilities::BasicTwoByteParameterFacility::~BasicTwoByteParameterFacility(void) {
}

const xot4cpp::Octet& xot4cpp::x25::facilities::BasicTwoByteParameterFacility::parameter0(void) const {

    return m_parameter0;
}

const xot4cpp::Octet& xot4cpp::x25::facilities::BasicTwoByteParameterFacility::parameter1(void) const {

    return m_parameter1;
}
