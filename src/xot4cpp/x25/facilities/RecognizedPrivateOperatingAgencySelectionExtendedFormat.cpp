#include "xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionExtendedFormat.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::RecognizedPrivateOperatingAgencySelectionExtendedFormat

xot4cpp::x25::facilities::RecognizedPrivateOperatingAgencySelectionExtendedFormat::SharedPointer xot4cpp::x25::facilities::RecognizedPrivateOperatingAgencySelectionExtendedFormat::downCast(const Facility::SharedPointer& p_pointer) {

    typedef RecognizedPrivateOperatingAgencySelectionExtendedFormat Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::RecognizedPrivateOperatingAgencySelectionExtendedFormat::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::RecognizedPrivateOperatingAgencySelectionExtendedFormat::RecognizedPrivateOperatingAgencySelectionExtendedFormat(const Octets& p_parameter)
: BasicVariableLengthParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::RecognizedPrivateOperatingAgencySelectionExtendedFormat::~RecognizedPrivateOperatingAgencySelectionExtendedFormat(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::RecognizedPrivateOperatingAgencySelectionExtendedFormat::create(const Octets& p_parameter) {

    const Facility::SharedPointer pointer(new RecognizedPrivateOperatingAgencySelectionExtendedFormat(p_parameter));
    return pointer;
}
