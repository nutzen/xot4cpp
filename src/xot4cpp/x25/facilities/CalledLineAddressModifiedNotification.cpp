#include "xot4cpp/x25/facilities/CalledLineAddressModifiedNotification.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::CalledLineAddressModifiedNotification

xot4cpp::x25::facilities::CalledLineAddressModifiedNotification::SharedPointer xot4cpp::x25::facilities::CalledLineAddressModifiedNotification::downCast(const Facility::SharedPointer& p_pointer) {

    typedef CalledLineAddressModifiedNotification Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::CalledLineAddressModifiedNotification::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::CalledLineAddressModifiedNotification::CalledLineAddressModifiedNotification(const Octet& p_parameter)
: BasicOneByteParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::CalledLineAddressModifiedNotification::~CalledLineAddressModifiedNotification(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::CalledLineAddressModifiedNotification::create(const Octet& p_parameter) {

    const Facility::SharedPointer pointer(new CalledLineAddressModifiedNotification(p_parameter));
    return pointer;
}
