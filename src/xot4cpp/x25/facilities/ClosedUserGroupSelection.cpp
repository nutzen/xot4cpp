#include "xot4cpp/x25/facilities/ClosedUserGroupSelection.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::ClosedUserGroupSelection

xot4cpp::x25::facilities::ClosedUserGroupSelection::SharedPointer xot4cpp::x25::facilities::ClosedUserGroupSelection::downCast(const Facility::SharedPointer& p_pointer) {

    typedef ClosedUserGroupSelection Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ClosedUserGroupSelection::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::ClosedUserGroupSelection::ClosedUserGroupSelection(const Octet& p_parameter)
: BasicOneByteParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::ClosedUserGroupSelection::~ClosedUserGroupSelection(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ClosedUserGroupSelection::create(const Octet& p_parameter) {

    const Facility::SharedPointer pointer(new ClosedUserGroupSelection(p_parameter));
    return pointer;
}
