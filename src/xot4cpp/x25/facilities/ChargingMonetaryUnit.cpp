#include "xot4cpp/x25/facilities/ChargingMonetaryUnit.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::ChargingMonetaryUnit

xot4cpp::x25::facilities::ChargingMonetaryUnit::SharedPointer xot4cpp::x25::facilities::ChargingMonetaryUnit::downCast(const Facility::SharedPointer& p_pointer) {

    typedef ChargingMonetaryUnit Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ChargingMonetaryUnit::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::ChargingMonetaryUnit::ChargingMonetaryUnit(const Octets& p_parameter)
: BasicVariableLengthParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::ChargingMonetaryUnit::~ChargingMonetaryUnit(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ChargingMonetaryUnit::create(const Octets& p_parameter) {

    const Facility::SharedPointer pointer(new ChargingMonetaryUnit(p_parameter));
    return pointer;
}
