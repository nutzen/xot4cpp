#include "xot4cpp/x25/facilities/WindowSizeSelection.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <algorithm>

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::WindowSizeSelection

xot4cpp::x25::facilities::WindowSizeSelection::SharedPointer xot4cpp::x25::facilities::WindowSizeSelection::downCast(const Facility::SharedPointer& p_pointer) {

    typedef WindowSizeSelection Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::WindowSizeSelection::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::WindowSizeSelection::WindowSizeSelection(
        const unsigned int& p_transmissionWindowSize,
        const unsigned int& p_receiveWindowSize)
: Facility(ENUMERATION),
m_transmissionWindowSize(p_transmissionWindowSize),
m_receiveWindowSize(p_receiveWindowSize) {
}

xot4cpp::x25::facilities::WindowSizeSelection::~WindowSizeSelection(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::WindowSizeSelection::create(
        const unsigned int& p_transmissionWindowSize,
        const unsigned int& p_receiveWindowSize) {

    const unsigned int max = 0x07;
    const unsigned int min = 0x01;

    const unsigned int transmissionWindowSize = std::max(min, std::min(max, p_transmissionWindowSize));
    const unsigned int receiveWindowSize = std::max(min, std::min(max, p_receiveWindowSize));

    const Facility::SharedPointer pointer(new WindowSizeSelection(transmissionWindowSize, receiveWindowSize));
    return pointer;
}

const unsigned int& xot4cpp::x25::facilities::WindowSizeSelection::transmissionWindowSize(void) const {

    return m_transmissionWindowSize;
}

const unsigned int& xot4cpp::x25::facilities::WindowSizeSelection::receiveWindowSize(void) const {

    return m_receiveWindowSize;
}
