#include "xot4cpp/x25/facilities/ReverseChargingAndFastSelect.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::ReverseChargingAndFastSelect

xot4cpp::x25::facilities::ReverseChargingAndFastSelect::SharedPointer xot4cpp::x25::facilities::ReverseChargingAndFastSelect::downCast(const Facility::SharedPointer& p_pointer) {

    typedef ReverseChargingAndFastSelect Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ReverseChargingAndFastSelect::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::ReverseChargingAndFastSelect::ReverseChargingAndFastSelect(const Octet& p_parameter)
: BasicOneByteParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::ReverseChargingAndFastSelect::~ReverseChargingAndFastSelect(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ReverseChargingAndFastSelect::create(const Octet& p_parameter) {

    const Facility::SharedPointer pointer(new ReverseChargingAndFastSelect(p_parameter));
    return pointer;
}
