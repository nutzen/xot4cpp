#include "xot4cpp/x25/facilities/BilateralClosedUserGroupSelection.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::BilateralClosedUserGroupSelection

xot4cpp::x25::facilities::BilateralClosedUserGroupSelection::SharedPointer xot4cpp::x25::facilities::BilateralClosedUserGroupSelection::downCast(const Facility::SharedPointer& p_pointer) {

    typedef BilateralClosedUserGroupSelection Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::BilateralClosedUserGroupSelection::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::BilateralClosedUserGroupSelection::BilateralClosedUserGroupSelection(const Octet& p_parameter0, const Octet& p_parameter1)
: BasicTwoByteParameterFacility(ENUMERATION, p_parameter0, p_parameter1) {
}

xot4cpp::x25::facilities::BilateralClosedUserGroupSelection::~BilateralClosedUserGroupSelection(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::BilateralClosedUserGroupSelection::create(const Octet& p_parameter0, const Octet& p_parameter1) {

    const Facility::SharedPointer pointer(new BilateralClosedUserGroupSelection(p_parameter0, p_parameter1));
    return pointer;
}
