#include "xot4cpp/x25/facilities/ChargingInformationRequest.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::ChargingInformationRequest

xot4cpp::x25::facilities::ChargingInformationRequest::SharedPointer xot4cpp::x25::facilities::ChargingInformationRequest::downCast(const Facility::SharedPointer& p_pointer) {

    typedef ChargingInformationRequest Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ChargingInformationRequest::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::ChargingInformationRequest::ChargingInformationRequest(const Octet& p_parameter)
: BasicOneByteParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::ChargingInformationRequest::~ChargingInformationRequest(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::ChargingInformationRequest::create(const Octet& p_parameter) {

    const Facility::SharedPointer pointer(new ChargingInformationRequest(p_parameter));
    return pointer;
}
