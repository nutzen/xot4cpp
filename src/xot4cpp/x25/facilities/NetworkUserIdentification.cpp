#include "xot4cpp/x25/facilities/NetworkUserIdentification.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::NetworkUserIdentification

xot4cpp::x25::facilities::NetworkUserIdentification::SharedPointer xot4cpp::x25::facilities::NetworkUserIdentification::downCast(const Facility::SharedPointer& p_pointer) {

    typedef NetworkUserIdentification Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::NetworkUserIdentification::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::NetworkUserIdentification::NetworkUserIdentification(const Octets& p_parameter)
: BasicVariableLengthParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::NetworkUserIdentification::~NetworkUserIdentification(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::NetworkUserIdentification::create(const Octets& p_parameter) {

    const Facility::SharedPointer pointer(new NetworkUserIdentification(p_parameter));
    return pointer;
}
