#include "xot4cpp/x25/facilities/CallRedirectionNotification.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define ENUMERATION xot4cpp::x25::facilities::FacilityEnumerations::CallRedirectionNotification

xot4cpp::x25::facilities::CallRedirectionNotification::SharedPointer xot4cpp::x25::facilities::CallRedirectionNotification::downCast(const Facility::SharedPointer& p_pointer) {

    typedef CallRedirectionNotification Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::CallRedirectionNotification::upCast(const SharedPointer& p_pointer) {

    typedef Facility Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::facilities::CallRedirectionNotification::CallRedirectionNotification(const Octets& p_parameter)
: BasicVariableLengthParameterFacility(ENUMERATION, p_parameter) {
}

xot4cpp::x25::facilities::CallRedirectionNotification::~CallRedirectionNotification(void) {
}

xot4cpp::x25::facilities::Facility::SharedPointer xot4cpp::x25::facilities::CallRedirectionNotification::create(const Octets& p_parameter) {

    const Facility::SharedPointer pointer(new CallRedirectionNotification(p_parameter));
    return pointer;
}
