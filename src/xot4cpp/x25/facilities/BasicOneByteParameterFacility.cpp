#include "xot4cpp/x25/facilities/BasicOneByteParameterFacility.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::facilities::BasicOneByteParameterFacility::BasicOneByteParameterFacility(
        const FacilityEnumerations::FacilityEnumeration& p_enumeration,
        const Octet& p_parameter)
: Facility(p_enumeration),
m_parameter(p_parameter) {
}

xot4cpp::x25::facilities::BasicOneByteParameterFacility::~BasicOneByteParameterFacility(void) {
}

const xot4cpp::Octet& xot4cpp::x25::facilities::BasicOneByteParameterFacility::parameter(void) const {

    return m_parameter;
}
