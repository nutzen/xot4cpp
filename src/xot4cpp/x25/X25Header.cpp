#include "xot4cpp/x25/X25Header.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::X25Header::X25Header(
        const gfi::GeneralFormatIdentifier::SharedPointer& p_generalFormatIdentifier,
        const lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
        const pti::PacketTypeIdentifier::SharedPointer& p_packetTypeIdentifier)
: m_generalFormatIdentifier(p_generalFormatIdentifier),
m_logicalChannelIdentifier(p_logicalChannelIdentifier),
m_packetTypeIdentifier(p_packetTypeIdentifier) {
}

xot4cpp::x25::X25Header::SharedPointer xot4cpp::x25::X25Header::create(
        const gfi::GeneralFormatIdentifier::SharedPointer& p_generalFormatIdentifier,
        const lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
        const pti::PacketTypeIdentifier::SharedPointer& p_packetTypeIdentifier) {

    return SharedPointer(new X25Header(p_generalFormatIdentifier, p_logicalChannelIdentifier, p_packetTypeIdentifier));
}

xot4cpp::x25::X25Header::~X25Header(void) {
}

const xot4cpp::x25::gfi::GeneralFormatIdentifier::SharedPointer& xot4cpp::x25::X25Header::generalFormatIdentifier(void) const {

    return m_generalFormatIdentifier;
}

const xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer& xot4cpp::x25::X25Header::logicalChannelIdentifier(void) const {

    return m_logicalChannelIdentifier;
}

const xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer& xot4cpp::x25::X25Header::packetTypeIdentifier(void) const {

    return m_packetTypeIdentifier;
}
