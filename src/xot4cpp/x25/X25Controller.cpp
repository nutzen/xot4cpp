#include "xot4cpp/x25/X25Controller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/IOException.hpp"
#include "xot4cpp/exceptions/Xot4cppException.hpp"
#include "xot4cpp/logger/Logger.hpp"
#include "xot4cpp/stream/OctetInputStream.hpp"
#include "xot4cpp/x25/transitions/Transition.hpp"
#include "xot4cpp/x25/utility/SessionContextUtility.hpp"
#include "xot4cpp/x25/utility/X25PacketUtility.hpp"

#include <algorithm>

xot4cpp::x25::X25Controller::X25Controller(
        const Thread_SharedPointer& p_thread,
        const stream::Pipe::SharedPointer& p_localToRemote,
        const stream::Pipe::SharedPointer& p_remoteToLocal,
        const states::StateMachine::SharedPointer& p_stateMachine,
        const context::SessionContext::SharedPointer& p_sessionContext,
        const transport::TransportWrapper::SharedPointer& p_wrapper,
        const bool& p_running)
: m_thread(p_thread),
m_localToRemote(p_localToRemote),
m_remoteToLocal(p_remoteToLocal),
m_stateMachine(p_stateMachine),
m_sessionContext(p_sessionContext),
m_wrapper(p_wrapper),
m_running(p_running) {
}

xot4cpp::x25::X25Controller::~X25Controller(void) {

    if (m_thread.get()) {
        disconnect();
    }
}

xot4cpp::x25::X25Controller::SharedPointer xot4cpp::x25::X25Controller::create(
        const context::SessionContext::SharedPointer& p_sessionContext,
        const transport::TransportWrapper::SharedPointer& p_wrapper) {

    const Thread_SharedPointer thread = Thread_SharedPointer();
    const stream::Pipe::SharedPointer localToRemote = stream::Pipe::create();
    const stream::Pipe::SharedPointer remoteToLocal = stream::Pipe::create();
    const states::StateMachine::SharedPointer stateMachine = states::StateMachine::create();
    const context::SessionContext::SharedPointer sessionContext = p_sessionContext;
    const transport::TransportWrapper::SharedPointer wrapper = p_wrapper;
    const bool running = false;

    const SharedPointer pointer(new X25Controller(
            thread,
            localToRemote,
            remoteToLocal,
            stateMachine,
            sessionContext,
            wrapper,
            running));

    return pointer;
}

const xot4cpp::stream::Pipe::SharedPointer& xot4cpp::x25::X25Controller::localToRemote(void) const {

    return m_localToRemote;
}

const xot4cpp::stream::Pipe::SharedPointer& xot4cpp::x25::X25Controller::remoteToLocal(void) const {

    return m_remoteToLocal;
}

const bool xot4cpp::x25::X25Controller::isConnected(void) const {

    return m_running && isInFlowControlState();
}

void xot4cpp::x25::X25Controller::operator()(void) {

    while (m_running) {
        try {
            boost::this_thread::sleep(boost::posix_time::milliseconds(100));

            poll();

            if (isInFlowControlReadyState()) {
                const stream::InputStream::SharedPointer inputStream = stream::Pipe::upCastAsInputStream(m_localToRemote);
                const unsigned int available = inputStream->available();
                if (0 < available) {
                    const Octets userData = inputStream->read(available);

                    const std::vector<Octets> dataPackets = splitData(m_sessionContext, userData);
                    for (unsigned int index = 0; dataPackets.size() > index; ++index) {
                        const Octets& dataPacket = dataPackets[index];
                        const bool more = (dataPackets.size() > (1 + index));
                        const X25Packet::SharedPointer packet = utility::X25PacketUtility::data(m_sessionContext, dataPacket, more);
                        submit(packet);
                    }
                }
            }

        } catch (exceptions::IOException& e) {
            m_running = false;
            XOT4CPP_DEBUG("IOException: " << e.what());

        } catch (std::exception& e) {
            m_running = false;
            XOT4CPP_DEBUG("Exception: " << e.what());
            disconnect();
        }
    }
}

void xot4cpp::x25::X25Controller::accept(void) {

    poll();
    callSetupCheck();

    m_running = true;
    m_thread.reset(new Thread(boost::ref(*this)));
}

void xot4cpp::x25::X25Controller::connect(const Octets & p_userData) {

    const xot4cpp::x25::X25Packet::SharedPointer packet =
            xot4cpp::x25::utility::X25PacketUtility::callRequest(m_sessionContext, p_userData);
    submit(packet);
    callSetupCheck();

    m_running = true;
    m_thread.reset(new Thread(boost::ref(*this)));
}

void xot4cpp::x25::X25Controller::disconnect(void) {

    try {
        if (m_running) {

            m_running = false;
            m_thread->join();
        }

        const X25Packet::SharedPointer packet = utility::X25PacketUtility::clearRequest(m_sessionContext);
        submit(packet);

    } catch (std::exception& e) {
        XOT4CPP_DEBUG("Exception: " << e.what());
    }
}

void xot4cpp::x25::X25Controller::poll(void) {

    const X25Packet::SharedPointer packet = m_wrapper->poll();
    if (packet.get()) {

        const stream::OutputStream::SharedPointer outputStream = stream::Pipe::upCastAsOutputStream(m_remoteToLocal);
        const transitions::TransitionParameters::SharedPointer parameters =
                transitions::TransitionParameters::create(outputStream, m_stateMachine, m_sessionContext, context::RoleEnumerations::DCE, packet);
        const transitions::Transition::SharedPointer transition = transitions::Transition::create(parameters);
        const transitions::TransitionResult::SharedPointer result = (*transition)();

        handle(result);
    }
}

void xot4cpp::x25::X25Controller::submit(const X25Packet::SharedPointer & p_packet) {

    const stream::OutputStream::SharedPointer outputStream =
            stream::Pipe::upCastAsOutputStream(m_remoteToLocal);
    const transitions::TransitionParameters::SharedPointer parameters =
            transitions::TransitionParameters::create(outputStream, m_stateMachine, m_sessionContext, context::RoleEnumerations::DTE, p_packet);
    m_wrapper->submit(p_packet);

    const transitions::Transition::SharedPointer transition = transitions::Transition::create(parameters);
    const transitions::TransitionResult::SharedPointer result = (*transition)();

    handle(result);
}

void xot4cpp::x25::X25Controller::handle(const transitions::TransitionResult::SharedPointer & p_result) {

    switch (p_result->action()) {
        case transitions::Poll:
            poll();
            break;
        case transitions::Submit:
            submit(p_result->packet());

            break;
        case transitions::None:
            // ignore
            break;
    }
}

std::vector<xot4cpp::Octets> xot4cpp::x25::X25Controller::splitData(const context::SessionContext::SharedPointer& p_sessionContext, const Octets & p_userData) {

    std::vector<xot4cpp::Octets> dataPackets;
    const unsigned int packetSize = utility::SessionContextUtility::transmitPacketSize(p_sessionContext);

    const stream::InputStream::SharedPointer inputStream = stream::OctetInputStream::create(p_userData);
    while (0 < inputStream->available()) {
        const unsigned int size = std::min(packetSize, inputStream->available());
        const Octets buffer = inputStream->read(size);
        dataPackets.push_back(buffer);
    }

    return dataPackets;
}

void xot4cpp::x25::X25Controller::callSetupCheck(void) const {

    if (isInFlowControlState()) {
        // happy days
    } else {
        // logHistory(m_sessionContext, m_stateMachine);
        THROW_XOT4CPP_EXCEPTION("Call setup failure: "
                << "state=" << states::StateEnumerations::toDescription(m_stateMachine->stateEnumeration())
                << ", cause=" << codes::CauseCodeEnumerations::toDescription(m_sessionContext->cause())
                << ", diagnostic=" << codes::DiagnosticCodeEnumerations::toDescription(m_sessionContext->diagnostic())
                << "");
    }
}

bool xot4cpp::x25::X25Controller::isInFlowControlReadyState(void) const {

    return (states::StateEnumerations::D1_FlowControlReady == m_stateMachine->stateEnumeration());
}

bool xot4cpp::x25::X25Controller::isInFlowControlState(void) const {

    bool destination;

    switch (m_stateMachine->stateEnumeration()) {
        case states::StateEnumerations::D1_FlowControlReady:
        case states::StateEnumerations::D2_DTEReseting:
        case states::StateEnumerations::D3_DCEReseting:
            destination = true;
            break;

        default:
            destination = false;
            break;
    }

    return destination;
}
