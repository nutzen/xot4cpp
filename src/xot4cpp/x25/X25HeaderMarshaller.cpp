#include "xot4cpp/x25/X25HeaderMarshaller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.hpp"
#include "xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.hpp"

void xot4cpp::x25::X25HeaderMarshaller::marshal(const stream::OutputStream::SharedPointer& p_outputStream, const xot4cpp::x25::X25Header::SharedPointer& p_header) {

    Octets target(3, 0);
    // [0] = GFI | LCGN
    // [1] = LCN
    // [2] = PTI
    const Octet gfi = static_cast<Octet> ((gfi::GeneralFormatIdentifierMarshaller::marshal(p_header->generalFormatIdentifier()) << 4) & 0x00f0);
    const Octet lcgn = static_cast<Octet> (p_header->logicalChannelIdentifier()->logicalChannelGroupNumber() & 0x000f);
    const Octet pti = pti::PacketTypeIdentifierMarshaller::marshalModule8(p_header->packetTypeIdentifier());

    target[0] = static_cast<Octet> ((gfi | lcgn) & 0x00ff);
    target[1] = p_header->logicalChannelIdentifier()->logicalChannelNumber();
    target[2] = pti;

    p_outputStream->write(target);
}

xot4cpp::x25::X25Header::SharedPointer xot4cpp::x25::X25HeaderMarshaller::unmarshal(const stream::InputStream::SharedPointer& p_inputStream) {

    // [0] = GFI | LCGN
    // [1] = LCN
    // [2] = PTI
    const int value0 = p_inputStream->read();
    const Octet gfi = static_cast<Octet> (0x000f & (value0 >> 4));
    const Octet lcgn = static_cast<Octet> (0x000f & value0);
    const Octet lcn = static_cast<Octet> (0x00ff & p_inputStream->read());
    const Octet pti = static_cast<Octet> (0x00ff & p_inputStream->read());

    const gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = gfi::GeneralFormatIdentifierMarshaller::unmarshal(gfi);
    const lci::LogicalChannelIdentifier::SharedPointer logicalChannelIdentifier = lci::LogicalChannelIdentifier::create(lcgn, lcn);
    const pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier = pti::PacketTypeIdentifierMarshaller::unmarshalModule8(pti);

    return X25Header::create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
}
