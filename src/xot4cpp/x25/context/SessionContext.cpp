#include "xot4cpp/x25/context/SessionContext.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::context::SessionContext::SessionContext(
        const x121::X121Address::SharedPointer& p_localAddress,
        const facilities::Facility::List& p_facilities,
        const x121::X121Address::SharedPointer& p_remoteAddress,
        const lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier)
: m_localAddress(p_localAddress),
m_facilities(p_facilities),
m_remoteAddress(p_remoteAddress),
m_logicalChannelIdentifier(p_logicalChannelIdentifier),
m_packetReceiveSequenceNumber(0),
m_packetSendSequenceNumber(0),
m_cause(codes::CauseCodeEnumerations::ClearRequestDteOriginated),
m_diagnostic(codes::DiagnosticCodeEnumerations::NoAdditionalInformation) {
}

xot4cpp::x25::context::SessionContext::~SessionContext(void) {
}

xot4cpp::x25::context::SessionContext::SharedPointer xot4cpp::x25::context::SessionContext::create(
        const x121::X121Address::SharedPointer& p_localAddress,
        const facilities::Facility::List& p_facilities,
        const x121::X121Address::SharedPointer& p_remoteAddress,
        const lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier) {

    const SharedPointer sessionContext(new SessionContext(
            p_localAddress,
            p_facilities,
            p_remoteAddress,
            p_logicalChannelIdentifier));

    return sessionContext;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::context::SessionContext::localAddress(void) const {

    return m_localAddress;
}

const xot4cpp::x25::facilities::Facility::List& xot4cpp::x25::context::SessionContext::facilities(void) const {

    return m_facilities;
}

xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::context::SessionContext::remoteAddress(void) {

    return m_remoteAddress;
}

xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer& xot4cpp::x25::context::SessionContext::logicalChannelIdentifier(void) {

    return m_logicalChannelIdentifier;
}

unsigned int& xot4cpp::x25::context::SessionContext::packetReceiveSequenceNumber(void) {

    return m_packetReceiveSequenceNumber;
}

unsigned int& xot4cpp::x25::context::SessionContext::packetSendSequenceNumber(void) {

    return m_packetSendSequenceNumber;
}

xot4cpp::x25::codes::CauseCodeEnumerations::ClearRequestEnumeration& xot4cpp::x25::context::SessionContext::cause(void) {

    return m_cause;
}

xot4cpp::x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration& xot4cpp::x25::context::SessionContext::diagnostic(void) {

    return m_diagnostic;
}
