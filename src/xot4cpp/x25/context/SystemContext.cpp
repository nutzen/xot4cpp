#include "xot4cpp/x25/context/SystemContext.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/facilities/PacketSizeSelection.hpp>
#include <xot4cpp/x25/facilities/WindowSizeSelection.hpp>

xot4cpp::x25::context::SystemContext::SystemContext(
        const x121::X121Address::SharedPointer& p_localAddress,
        const facilities::Facility::List& p_facilities,
        const unsigned short& p_lciSeed)
: m_localAddress(p_localAddress),
m_facilities(p_facilities),
m_lciSeed(p_lciSeed) {
}

xot4cpp::x25::context::SystemContext::~SystemContext(void) {
}

xot4cpp::x25::context::SystemContext::SharedPointer xot4cpp::x25::context::SystemContext::create(
        const x121::X121Address::SharedPointer& p_localAddress,
        const facilities::Facility::List& p_facilities) {

    const SharedPointer systemContext(new SystemContext(
            p_localAddress,
            p_facilities,
            static_cast<unsigned short> (0x0fff)));

    return systemContext;
}

const xot4cpp::x121::X121Address::SharedPointer& xot4cpp::x25::context::SystemContext::localAddress(void) const {

    return m_localAddress;
}

const xot4cpp::x25::facilities::Facility::List& xot4cpp::x25::context::SystemContext::facilities(void) const {

    return m_facilities;
}

const unsigned short xot4cpp::x25::context::SystemContext::nextLci(void) {

    const unsigned short lci = m_lciSeed;

    m_lciSeed = static_cast<unsigned short> (0x0fff & (m_lciSeed - 1));

    return lci;
}

xot4cpp::x25::facilities::Facility::List xot4cpp::x25::context::SystemContext::defaultFacilities(void) {

    facilities::Facility::List facilities;

    facilities.push_back(xot4cpp::x25::facilities::WindowSizeSelection::create(1, 1));
    facilities.push_back(xot4cpp::x25::facilities::PacketSizeSelection::create(
            xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::Size128octets,
            xot4cpp::x25::facilities::PacketSizeSelectionEnumerations::Size128octets));

    return facilities;
}
