#include "xot4cpp/x25/context/RoleEnumerations.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

// DESCRIPTION
#define DTE_DESCRIPTION "DTE"
#define DCE_DESCRIPTION "DCE"

std::string xot4cpp::x25::context::RoleEnumerations::toDescription(const RoleEnumeration& p_source) {
    std::string destination;

    switch (p_source) {

        case DTE:
            destination = DTE_DESCRIPTION;
            break;

        case DCE:
            destination = DCE_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::context::RoleEnumerations::RoleEnumeration xot4cpp::x25::context::RoleEnumerations::fromDescription(const std::string& p_source) {

    RoleEnumeration destination;

    if (0 == p_source.compare(DTE_DESCRIPTION)) {
        destination = DTE;

    } else if (0 == p_source.compare(DCE_DESCRIPTION)) {
        destination = DCE;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}
