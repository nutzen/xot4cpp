#include "xot4cpp/x25/pti/ReceiverPacketTypeIdentifier.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::downCast(const PacketTypeIdentifier::SharedPointer& p_pointer) {

    typedef ReceiverPacketTypeIdentifier Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::upCast(const SharedPointer& p_pointer) {

    typedef PacketTypeIdentifier Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::ReceiverPacketTypeIdentifier(
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
        const unsigned int& p_packetReceiveSequenceNumber)
: PacketTypeIdentifier(p_enumeration),
m_packetReceiveSequenceNumber(p_packetReceiveSequenceNumber) {
}

xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::~ReceiverPacketTypeIdentifier(void) {
}

xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::create(
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
        const unsigned int& p_packetReceiveSequenceNumber) {

    const SharedPointer pointer(new ReceiverPacketTypeIdentifier(p_enumeration, p_packetReceiveSequenceNumber));
    return pointer;
}

bool xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::isReceiverPacketTypeIdentifier(void) const {

    return true;
}

const unsigned int& xot4cpp::x25::pti::ReceiverPacketTypeIdentifier::packetReceiveSequenceNumber(void) const {

    return m_packetReceiveSequenceNumber;
}
