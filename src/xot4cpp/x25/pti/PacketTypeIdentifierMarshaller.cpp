#include "xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::Octet xot4cpp::x25::pti::PacketTypeIdentifierMarshaller::marshalModule8(const PacketTypeIdentifier::SharedPointer& p_source) {

    Octet target;

    if (p_source->isDataPacketTypeIdentifier()) {
        const DataPacketTypeIdentifier::SharedPointer identifier = DataPacketTypeIdentifier::downCast(p_source);
        const bool moreData = identifier->moreData();
        const unsigned int packetReceiveSequenceNumber = identifier->packetReceiveSequenceNumber();
        const unsigned int packetSendSequenceNumber = identifier->packetSendSequenceNumber();

        int value = (packetReceiveSequenceNumber & 0x0007);
        value = ((value << 1) & 0x00fe) | (moreData ? 0x01 : 0x00);
        value = ((value << 3) & 0x00f8) | (packetSendSequenceNumber & 0x0007);
        value = ((value << 1) & 0x00fe);
        target = (Octet) (value);

    } else if (p_source->isReceiverPacketTypeIdentifier()) {
        const ReceiverPacketTypeIdentifier::SharedPointer identifier = ReceiverPacketTypeIdentifier::downCast(p_source);
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration enumeration = identifier->enumeration();
        const int packetReceiveSequenceNumber = identifier->packetReceiveSequenceNumber();
        int value = (packetReceiveSequenceNumber & 0x0007);
        value = (0x00e0 & (value << 5));
        value = (value | (0x0f & PacketTypeIdentifierEnumerations::toInt(enumeration)));
        target = (Octet) (value & 0x00ff);

    } else /* if (source.isDteToDcePacketTypeIdentifier()) */ {
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration enumeration = p_source->enumeration();
        const int value = PacketTypeIdentifierEnumerations::toInt(enumeration);
        target = (Octet) (value & 0x00ff);
    }

    return target;
}

xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::PacketTypeIdentifierMarshaller::unmarshalModule8(const Octet& p_source) {

    PacketTypeIdentifier::SharedPointer target;

    const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumerations::fromInt(p_source & 0x00ff);
    switch (enumeration) {
        case PacketTypeIdentifierEnumerations::Data:
        {
            int value = (p_source & 0x00fe);

            const bool moreData = ((value & 0x0010) == 0x0010);
            const unsigned int packetReceiveSequenceNumber = ((value >> 5) & 0x0007);
            const unsigned int packetSendSequenceNumber = ((value >> 1) & 0x0007);

            target = PacketTypeIdentifier::create(enumeration, moreData, packetReceiveSequenceNumber, packetSendSequenceNumber);
            break;
        }

        case PacketTypeIdentifierEnumerations::ReceiverReady:
        case PacketTypeIdentifierEnumerations::ReceiverNotReady:
        case PacketTypeIdentifierEnumerations::Reject:
        {
            int value = 0x00e0 & p_source;
            const int packetReceiveSequenceNumber = ((value >> 5) & 0x0007);
            target = PacketTypeIdentifier::create(enumeration, packetReceiveSequenceNumber);
            break;
        }

        default:
            target = PacketTypeIdentifier::create(enumeration);
            break;
    }

    return target;
}
