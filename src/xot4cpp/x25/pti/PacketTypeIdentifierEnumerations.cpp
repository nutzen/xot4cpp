#include "xot4cpp/x25/pti/PacketTypeIdentifierEnumerations.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

// DESCRIPTION
#define CALL_REQUEST_INCOMING_CALL_DESCRIPTION "Call_Request/Incoming_Call"
#define CALL_ACCEPTED_CALL_CONNECTED_DESCRIPTION "Call_Accepted/Call_Connected"
#define CLEAR_REQUEST_CLEAR_INDICATION_DESCRIPTION "Clear_Request/Clear_Indication"
#define CLEAR_CONFIRMATION_DESCRIPTION "Clear_Confirmation"
#define DATA_DESCRIPTION "Data"
#define INTERRUPT_DESCRIPTION "Interrupt"
#define INTERRUPT_CONFIRMATION_DESCRIPTION "Interrupt_Confirmation"
#define RECEIVER_READY_DESCRIPTION "Receiver_Ready"
#define RECEIVER_NOT_READY_DESCRIPTION "Receiver_Not_Ready"
#define REJECT_DESCRIPTION "Reject"
#define RESET_REQUEST_RESET_INDICATION_DESCRIPTION "Reset_Request/Reset_Indication"
#define RESET_CONFIRMATION_DESCRIPTION "Reset_Confirmation"
#define RESTART_REQUEST_RESTART_INDICATION_DESCRIPTION "Restart_Request/Restart_Indication"
#define RESTART_CONFIRMATION_DESCRIPTION "Restart_Confirmation"
#define DIAGNOSTIC_DESCRIPTION "Diagnostic"
// MASK
#define CALL_REQUEST_INCOMING_CALL_MASK 0x0b
#define CALL_ACCEPTED_CALL_CONNECTED_MASK 0x0f
#define CLEAR_REQUEST_CLEAR_INDICATION_MASK 0x13
#define CLEAR_CONFIRMATION_MASK 0x17
#define DATA_MASK 0x00
#define INTERRUPT_MASK 0x23
#define INTERRUPT_CONFIRMATION_MASK 0x27
#define RECEIVER_READY_MASK 0x01
#define RECEIVER_NOT_READY_MASK 0x05
#define REJECT_MASK 0x09
#define RESET_REQUEST_RESET_INDICATION_MASK 0x1b
#define RESET_CONFIRMATION_MASK 0x1f
#define RESTART_REQUEST_RESTART_INDICATION_MASK 0xfb
#define RESTART_CONFIRMATION_MASK 0xff
#define DIAGNOSTIC_MASK 0xf1

namespace xot4cpp {

    namespace x25 {

        namespace pti {

            namespace PacketTypeIdentifierEnumerations {

                extern bool compareWithMask(const int& p_mask, const int& p_source);
            };
        };
    };
};

std::string xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::toDescription(const PacketTypeIdentifierEnumeration& p_source) {

    std::string destination;

    switch (p_source) {
        case CallRequest_IncomingCall:
            destination = CALL_REQUEST_INCOMING_CALL_DESCRIPTION;
            break;

        case CallAccepted_CallConnected:
            destination = CALL_ACCEPTED_CALL_CONNECTED_DESCRIPTION;
            break;

        case ClearRequest_ClearIndication:
            destination = CLEAR_REQUEST_CLEAR_INDICATION_DESCRIPTION;
            break;

        case ClearConfirmation:
            destination = CLEAR_CONFIRMATION_DESCRIPTION;
            break;

        case Data:
            destination = DATA_DESCRIPTION;
            break;

        case Interrupt:
            destination = INTERRUPT_DESCRIPTION;
            break;

        case InterruptConfirmation:
            destination = INTERRUPT_CONFIRMATION_DESCRIPTION;
            break;

        case ReceiverReady:
            destination = RECEIVER_READY_DESCRIPTION;
            break;

        case ReceiverNotReady:
            destination = RECEIVER_NOT_READY_DESCRIPTION;
            break;

        case Reject:
            destination = REJECT_DESCRIPTION;
            break;

        case ResetRequest_ResetIndication:
            destination = RESET_REQUEST_RESET_INDICATION_DESCRIPTION;
            break;

        case ResetConfirmation:
            destination = RESET_CONFIRMATION_DESCRIPTION;
            break;

        case RestartRequest_RestartIndication:
            destination = RESTART_REQUEST_RESTART_INDICATION_DESCRIPTION;
            break;

        case RestartConfirmation:
            destination = RESTART_CONFIRMATION_DESCRIPTION;
            break;

        case Diagnostic:
            destination = DIAGNOSTIC_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::fromDescription(const std::string& p_source) {

    PacketTypeIdentifierEnumeration destination;

    if (0 == p_source.compare(CALL_REQUEST_INCOMING_CALL_DESCRIPTION)) {
        destination = CallRequest_IncomingCall;

    } else if (0 == p_source.compare(CALL_ACCEPTED_CALL_CONNECTED_DESCRIPTION)) {
        destination = CallAccepted_CallConnected;

    } else if (0 == p_source.compare(CLEAR_REQUEST_CLEAR_INDICATION_DESCRIPTION)) {
        destination = ClearRequest_ClearIndication;

    } else if (0 == p_source.compare(CLEAR_CONFIRMATION_DESCRIPTION)) {
        destination = ClearConfirmation;

    } else if (0 == p_source.compare(DATA_DESCRIPTION)) {
        destination = Data;

    } else if (0 == p_source.compare(INTERRUPT_DESCRIPTION)) {
        destination = Interrupt;

    } else if (0 == p_source.compare(INTERRUPT_CONFIRMATION_DESCRIPTION)) {
        destination = InterruptConfirmation;

    } else if (0 == p_source.compare(RECEIVER_READY_DESCRIPTION)) {
        destination = ReceiverReady;

    } else if (0 == p_source.compare(RECEIVER_NOT_READY_DESCRIPTION)) {
        destination = ReceiverNotReady;

    } else if (0 == p_source.compare(REJECT_DESCRIPTION)) {
        destination = Reject;

    } else if (0 == p_source.compare(RESET_REQUEST_RESET_INDICATION_DESCRIPTION)) {
        destination = ResetRequest_ResetIndication;

    } else if (0 == p_source.compare(RESET_CONFIRMATION_DESCRIPTION)) {
        destination = ResetConfirmation;

    } else if (0 == p_source.compare(RESTART_REQUEST_RESTART_INDICATION_DESCRIPTION)) {
        destination = RestartRequest_RestartIndication;

    } else if (0 == p_source.compare(RESTART_CONFIRMATION_DESCRIPTION)) {
        destination = RestartConfirmation;

    } else if (0 == p_source.compare(DIAGNOSTIC_DESCRIPTION)) {
        destination = Diagnostic;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

int xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::toInt(const PacketTypeIdentifierEnumeration& p_source) {

    int destination;

    switch (p_source) {
        case CallRequest_IncomingCall:
            destination = CALL_REQUEST_INCOMING_CALL_MASK;
            break;

        case CallAccepted_CallConnected:
            destination = CALL_ACCEPTED_CALL_CONNECTED_MASK;
            break;

        case ClearRequest_ClearIndication:
            destination = CLEAR_REQUEST_CLEAR_INDICATION_MASK;
            break;

        case ClearConfirmation:
            destination = CLEAR_CONFIRMATION_MASK;
            break;

        case Data:
            destination = DATA_MASK;
            break;

        case Interrupt:
            destination = INTERRUPT_MASK;
            break;

        case InterruptConfirmation:
            destination = INTERRUPT_CONFIRMATION_MASK;
            break;

        case ReceiverReady:
            destination = RECEIVER_READY_MASK;
            break;

        case ReceiverNotReady:
            destination = RECEIVER_NOT_READY_MASK;
            break;

        case Reject:
            destination = REJECT_MASK;
            break;

        case ResetRequest_ResetIndication:
            destination = RESET_REQUEST_RESET_INDICATION_MASK;
            break;

        case ResetConfirmation:
            destination = RESET_CONFIRMATION_MASK;
            break;

        case RestartRequest_RestartIndication:
            destination = RESTART_REQUEST_RESTART_INDICATION_MASK;
            break;

        case RestartConfirmation:
            destination = RESTART_CONFIRMATION_MASK;
            break;

        case Diagnostic:
            destination = DIAGNOSTIC_MASK;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::fromInt(const int& p_source) {

    PacketTypeIdentifierEnumeration destination;

    if (compareWithMask(DIAGNOSTIC_MASK, p_source)) {
        destination = Diagnostic;

    } else if (compareWithMask(CALL_REQUEST_INCOMING_CALL_MASK, p_source)) {
        destination = CallRequest_IncomingCall;

    } else if (compareWithMask(CALL_ACCEPTED_CALL_CONNECTED_MASK, p_source)) {
        destination = CallAccepted_CallConnected;

    } else if (compareWithMask(CLEAR_REQUEST_CLEAR_INDICATION_MASK, p_source)) {
        destination = ClearRequest_ClearIndication;

    } else if (compareWithMask(CLEAR_CONFIRMATION_MASK, p_source)) {
        destination = ClearConfirmation;

    } else if (compareWithMask(DATA_MASK, p_source)) {
        destination = Data;

    } else if (compareWithMask(INTERRUPT_MASK, p_source)) {
        destination = Interrupt;

    } else if (compareWithMask(INTERRUPT_CONFIRMATION_MASK, p_source)) {
        destination = InterruptConfirmation;

    } else if (compareWithMask(RECEIVER_READY_MASK, p_source)) {
        destination = ReceiverReady;

    } else if (compareWithMask(RECEIVER_NOT_READY_MASK, p_source)) {
        destination = ReceiverNotReady;

    } else if (compareWithMask(REJECT_MASK, p_source)) {
        destination = Reject;

    } else if (compareWithMask(RESET_REQUEST_RESET_INDICATION_MASK, p_source)) {
        destination = ResetRequest_ResetIndication;

    } else if (compareWithMask(RESET_CONFIRMATION_MASK, p_source)) {
        destination = ResetConfirmation;

    } else if (compareWithMask(RESTART_REQUEST_RESTART_INDICATION_MASK, p_source)) {
        destination = RestartRequest_RestartIndication;

    } else if (compareWithMask(RESTART_CONFIRMATION_MASK, p_source)) {
        destination = RestartConfirmation;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

bool xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::compareWithMask(const int& p_mask, const int& p_source) {

    bool destination;

    switch (p_mask) {
        case RECEIVER_READY_MASK:
        case RECEIVER_NOT_READY_MASK:
        case REJECT_MASK:
            destination = (0x0f & p_source) == p_mask;
            break;

        case DATA_MASK:
            destination = (0x01 & p_source) == p_mask;
            break;

        default:
            destination = (p_mask == p_source);
            break;
    };

    return destination;
}
