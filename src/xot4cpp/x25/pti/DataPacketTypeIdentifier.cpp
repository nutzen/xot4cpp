#include "xot4cpp/x25/pti/DataPacketTypeIdentifier.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::pti::DataPacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::DataPacketTypeIdentifier::downCast(const PacketTypeIdentifier::SharedPointer& p_pointer) {

    typedef DataPacketTypeIdentifier Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::DataPacketTypeIdentifier::upCast(const SharedPointer& p_pointer) {

    typedef PacketTypeIdentifier Target;

    return boost::dynamic_pointer_cast<Target > (p_pointer);
}

xot4cpp::x25::pti::DataPacketTypeIdentifier::DataPacketTypeIdentifier(
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
        const bool& p_moreData,
        const unsigned int& p_packetReceiveSequenceNumber,
        const unsigned int& p_packetSendSequenceNumber)
: PacketTypeIdentifier(p_enumeration),
m_moreData(p_moreData),
m_packetReceiveSequenceNumber(p_packetReceiveSequenceNumber),
m_packetSendSequenceNumber(p_packetSendSequenceNumber) {
}

xot4cpp::x25::pti::DataPacketTypeIdentifier::~DataPacketTypeIdentifier(void) {
}

xot4cpp::x25::pti::DataPacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::DataPacketTypeIdentifier::create(const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
        const bool& p_moreData,
        const unsigned int& p_packetReceiveSequenceNumber,
        const unsigned int& p_packetSendSequenceNumber) {

    const SharedPointer pointer(new DataPacketTypeIdentifier(p_enumeration, p_moreData, p_packetReceiveSequenceNumber, p_packetSendSequenceNumber));
    return pointer;
}

bool xot4cpp::x25::pti::DataPacketTypeIdentifier::isDataPacketTypeIdentifier(void) const {

    return true;
}

const bool& xot4cpp::x25::pti::DataPacketTypeIdentifier::moreData(void) const {

    return m_moreData;
}

const unsigned int& xot4cpp::x25::pti::DataPacketTypeIdentifier::packetReceiveSequenceNumber(void) const {

    return m_packetReceiveSequenceNumber;
}

const unsigned int& xot4cpp::x25::pti::DataPacketTypeIdentifier::packetSendSequenceNumber(void) const {

    return m_packetSendSequenceNumber;
}
