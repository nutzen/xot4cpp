#include "xot4cpp/x25/pti/PacketTypeIdentifier.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::pti::PacketTypeIdentifier::PacketTypeIdentifier(const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration)
: m_enumeration(p_enumeration) {
}

xot4cpp::x25::pti::PacketTypeIdentifier::~PacketTypeIdentifier(void) {
}

xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::PacketTypeIdentifier::create(
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration) {

    return SharedPointer(new PacketTypeIdentifier(p_enumeration));
}

xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::PacketTypeIdentifier::create(
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
        const unsigned int& p_packetReceiveSequenceNumber) {

    const ReceiverPacketTypeIdentifier::SharedPointer pointer = ReceiverPacketTypeIdentifier::create(p_enumeration, p_packetReceiveSequenceNumber);
    return ReceiverPacketTypeIdentifier::upCast(pointer);
}

xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer xot4cpp::x25::pti::PacketTypeIdentifier::create(
        const PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_enumeration,
        const bool& p_moreData,
        const unsigned int& p_packetReceiveSequenceNumber,
        const unsigned int& p_packetSendSequenceNumber) {

    const DataPacketTypeIdentifier::SharedPointer pointer = DataPacketTypeIdentifier::create(p_enumeration, p_moreData, p_packetReceiveSequenceNumber, p_packetSendSequenceNumber);
    return DataPacketTypeIdentifier::upCast(pointer);
}

const xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& xot4cpp::x25::pti::PacketTypeIdentifier::enumeration(void) const {

    return m_enumeration;
}

bool xot4cpp::x25::pti::PacketTypeIdentifier::isDataPacketTypeIdentifier(void) const {

    return false;
}

bool xot4cpp::x25::pti::PacketTypeIdentifier::isReceiverPacketTypeIdentifier(void) const {

    return false;
}
