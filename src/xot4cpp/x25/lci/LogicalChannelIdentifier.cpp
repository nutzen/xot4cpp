#include "xot4cpp/x25/lci/LogicalChannelIdentifier.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::lci::LogicalChannelIdentifier::LogicalChannelIdentifier(const Octet& p_logicalChannelGroupNumber,
        const Octet& p_logicalChannelNumber)
: m_logicalChannelGroupNumber(p_logicalChannelGroupNumber),
m_logicalChannelNumber(p_logicalChannelNumber) {
}

xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer xot4cpp::x25::lci::LogicalChannelIdentifier::create(
        const unsigned int& p_logicalChannelIdentifier) {

    const Octet logicalChannelGroupNumber = static_cast<Octet> ((0x0f00 & p_logicalChannelIdentifier) >> 8);
    const Octet logicalChannelNumber = static_cast<Octet> (0x00ff & p_logicalChannelIdentifier);

    return create(logicalChannelGroupNumber, logicalChannelNumber);
}

xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer xot4cpp::x25::lci::LogicalChannelIdentifier::create(
        const Octet& p_logicalChannelGroupNumber,
        const Octet& p_logicalChannelNumber) {

    return SharedPointer(new LogicalChannelIdentifier(p_logicalChannelGroupNumber, p_logicalChannelNumber));
}

xot4cpp::x25::lci::LogicalChannelIdentifier::~LogicalChannelIdentifier(void) {
}

const xot4cpp::Octet& xot4cpp::x25::lci::LogicalChannelIdentifier::logicalChannelGroupNumber(void) const {

    return m_logicalChannelGroupNumber;
}

const xot4cpp::Octet& xot4cpp::x25::lci::LogicalChannelIdentifier::logicalChannelNumber(void) const {

    return m_logicalChannelNumber;
}
