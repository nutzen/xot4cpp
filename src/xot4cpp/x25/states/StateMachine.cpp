#include "xot4cpp/x25/states/StateMachine.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/R1_PacketLayerReadyState.hpp"

xot4cpp::x25::states::StateMachine::StateMachine(const StateContext::SharedPointer& p_stateContext)
: m_stateContext(p_stateContext) {
}

xot4cpp::x25::states::StateMachine::~StateMachine(void) {
}

xot4cpp::x25::states::StateMachine::SharedPointer xot4cpp::x25::states::StateMachine::create(void) {

    const State::SharedPointer state = R1_PacketLayerReadyState::create();
    const StateContext::SharedPointer stateContext = StateContext::create(state);
    const SharedPointer stateMachine(new StateMachine(stateContext));

    return stateMachine;
}

const xot4cpp::x25::states::StateEnumerations::StateEnumeration& xot4cpp::x25::states::StateMachine::stateEnumeration(void) const {

    const State::SharedPointer currentState = m_stateContext->state();
    return currentState->enumeration();
}

void xot4cpp::x25::states::StateMachine::handle(const transitions::TransitionEnumerations::TransitionEnumeration& p_transitionEnumeration) {

    const MutexGuard mutexGuard(m_mutex);
    const State::SharedPointer currentState = m_stateContext->state();

    currentState->handle(*m_stateContext.get(), p_transitionEnumeration);
}
