#include "xot4cpp/x25/states/R2_DTERestartingState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/R1_PacketLayerReadyState.hpp"

xot4cpp::x25::states::R2_DTERestartingState::R2_DTERestartingState(const StateEnumerations::StateEnumeration& p_enumeration)
: State(p_enumeration) {
}

xot4cpp::x25::states::R2_DTERestartingState::~R2_DTERestartingState(void) {
}

void xot4cpp::x25::states::R2_DTERestartingState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {
        case transitions::TransitionEnumerations::RestartRequest:
            break;

        case transitions::TransitionEnumerations::RestartIndication:
            p_stateContext.setState(R1_PacketLayerReadyState::create());
            break;

        case transitions::TransitionEnumerations::RestartConfirmation:
            p_stateContext.setState(R1_PacketLayerReadyState::create());
            break;

        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::R2_DTERestartingState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::R2_DTERestarting;
    const State::SharedPointer state(new R2_DTERestartingState(enumeration));

    return state;
}
