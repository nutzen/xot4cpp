#include "xot4cpp/x25/states/R1_PacketLayerReadyState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/P1_ReadyState.hpp"
#include "xot4cpp/x25/states/R2_DTERestartingState.hpp"
#include "xot4cpp/x25/states/R3_DCERestartingState.hpp"

xot4cpp::x25::states::R1_PacketLayerReadyState::R1_PacketLayerReadyState(const StateEnumerations::StateEnumeration& p_enumeration,
        const State::SharedPointer& p_state)
: State(p_enumeration),
StateContext(p_state) {
}

xot4cpp::x25::states::R1_PacketLayerReadyState::~R1_PacketLayerReadyState(void) {
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::R1_PacketLayerReadyState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::R1_PacketLayerReady;
    const State::SharedPointer subState(P1_ReadyState::create());
    const State::SharedPointer state(new R1_PacketLayerReadyState(enumeration, subState));

    return state;
}

const xot4cpp::x25::states::StateEnumerations::StateEnumeration& xot4cpp::x25::states::R1_PacketLayerReadyState::enumeration(void) const {

    return state()->enumeration();
}

void xot4cpp::x25::states::R1_PacketLayerReadyState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {
        case transitions::TransitionEnumerations::Data:
        case transitions::TransitionEnumerations::Interrupt:
        case transitions::TransitionEnumerations::InterruptConfirmation:
        case transitions::TransitionEnumerations::ReceiverReady:
        case transitions::TransitionEnumerations::ReceiverNotReady:
        case transitions::TransitionEnumerations::Reject:
            // P-State transitions
        case transitions::TransitionEnumerations::CallRequest:
        case transitions::TransitionEnumerations::CallIncoming:
        case transitions::TransitionEnumerations::CallAccepted:
        case transitions::TransitionEnumerations::CallConnected:
        case transitions::TransitionEnumerations::ClearRequest:
        case transitions::TransitionEnumerations::ClearIndication:
        case transitions::TransitionEnumerations::ClearConfirmation:
            // D-State transitions
        case transitions::TransitionEnumerations::ResetRequest:
        case transitions::TransitionEnumerations::ResetIndication:
        case transitions::TransitionEnumerations::ResetConfirmation:
            state()->handle(*this, p_transition);
            break;

            // R-State transitions
        case transitions::TransitionEnumerations::RestartRequest:
            p_stateContext.setState(R2_DTERestartingState::create());
            break;

        case transitions::TransitionEnumerations::RestartIndication:
            p_stateContext.setState(R3_DCERestartingState::create());
            break;

        case transitions::TransitionEnumerations::RestartConfirmation:
        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}
