#include "xot4cpp/x25/states/State.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/StateContext.hpp"
#include "xot4cpp/exceptions/X25StateTransitionFailure.hpp"

xot4cpp::x25::states::State::State(const StateEnumerations::StateEnumeration& p_enumeration)
: m_enumeration(p_enumeration) {
}

xot4cpp::x25::states::State::~State(void) {
}

const xot4cpp::x25::states::StateEnumerations::StateEnumeration& xot4cpp::x25::states::State::enumeration(void) const {

    return m_enumeration;
}

void xot4cpp::x25::states::State::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {
        case transitions::TransitionEnumerations::Data:
        case transitions::TransitionEnumerations::Interrupt:
        case transitions::TransitionEnumerations::InterruptConfirmation:
        case transitions::TransitionEnumerations::ReceiverReady:
        case transitions::TransitionEnumerations::ReceiverNotReady:
        case transitions::TransitionEnumerations::Reject:
        case transitions::TransitionEnumerations::Diagnostic:
            // P-State transitions
        case transitions::TransitionEnumerations::CallRequest:
        case transitions::TransitionEnumerations::CallIncoming:
        case transitions::TransitionEnumerations::CallAccepted:
        case transitions::TransitionEnumerations::CallConnected:
        case transitions::TransitionEnumerations::ClearRequest:
        case transitions::TransitionEnumerations::ClearIndication:
        case transitions::TransitionEnumerations::ClearConfirmation:
            // D-State transitions
        case transitions::TransitionEnumerations::ResetRequest:
        case transitions::TransitionEnumerations::ResetIndication:
        case transitions::TransitionEnumerations::ResetConfirmation:
            // R-State transitions
        case transitions::TransitionEnumerations::RestartRequest:
        case transitions::TransitionEnumerations::RestartIndication:
        case transitions::TransitionEnumerations::RestartConfirmation:
        default:
            THROW_XOT4CPP_X25STATETRANSITIONFAILURE(
                    "Unhandled transition "
                    << transitions::TransitionEnumerations::toDescription(p_transition)
                    << " in state "
                    << StateEnumerations::toDescription(this->enumeration()));
            break;
    }
}
