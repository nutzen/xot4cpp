#include "xot4cpp/x25/states/P2_DTEWaitingState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/P4_DataTransferState.hpp"
#include "xot4cpp/x25/states/P5_CallCollisionState.hpp"
#include "xot4cpp/x25/states/P6_DTEClearingState.hpp"
#include "xot4cpp/x25/states/P7_DCEClearingState.hpp"

xot4cpp::x25::states::P2_DTEWaitingState::P2_DTEWaitingState(const StateEnumerations::StateEnumeration& p_enumeration)
: State(p_enumeration) {
}

xot4cpp::x25::states::P2_DTEWaitingState::~P2_DTEWaitingState(void) {
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::P2_DTEWaitingState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::P2_DTEWaiting;
    const State::SharedPointer state(new P2_DTEWaitingState(enumeration));

    return state;
}

void xot4cpp::x25::states::P2_DTEWaitingState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {

        case transitions::TransitionEnumerations::CallIncoming:
            p_stateContext.setState(P5_CallCollisionState::create());
            break;

        case transitions::TransitionEnumerations::CallConnected:
            p_stateContext.setState(P4_DataTransferState::create());
            break;

        case transitions::TransitionEnumerations::ClearRequest:
            p_stateContext.setState(P6_DTEClearingState::create());
            break;

        case transitions::TransitionEnumerations::ClearIndication:
            p_stateContext.setState(P7_DCEClearingState::create());
            break;

        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}
