#include "xot4cpp/x25/states/D1_FlowControlReadyState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/D2_DTEResetingState.hpp"
#include "xot4cpp/x25/states/D3_DCEResetingState.hpp"

xot4cpp::x25::states::D1_FlowControlReadyState::D1_FlowControlReadyState(const StateEnumerations::StateEnumeration& p_enumeration)
: State(p_enumeration) {
}

xot4cpp::x25::states::D1_FlowControlReadyState::~D1_FlowControlReadyState(void) {
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::D1_FlowControlReadyState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::D1_FlowControlReady;
    const State::SharedPointer state(new D1_FlowControlReadyState(enumeration));

    return state;
}

void xot4cpp::x25::states::D1_FlowControlReadyState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {
        case transitions::TransitionEnumerations::Data:
        case transitions::TransitionEnumerations::Interrupt:
        case transitions::TransitionEnumerations::InterruptConfirmation:
        case transitions::TransitionEnumerations::ReceiverReady:
        case transitions::TransitionEnumerations::ReceiverNotReady:
        case transitions::TransitionEnumerations::Reject:
            break;

        case transitions::TransitionEnumerations::ResetRequest:
            p_stateContext.setState(D2_DTEResetingState::create());
            break;

        case transitions::TransitionEnumerations::ResetIndication:
            p_stateContext.setState(D3_DCEResetingState::create());
            break;

        case transitions::TransitionEnumerations::ResetConfirmation:
        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}
