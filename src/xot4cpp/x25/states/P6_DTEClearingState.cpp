#include "xot4cpp/x25/states/P6_DTEClearingState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/P1_ReadyState.hpp"

xot4cpp::x25::states::P6_DTEClearingState::P6_DTEClearingState(const StateEnumerations::StateEnumeration& p_enumeration)
: State(p_enumeration) {
}

xot4cpp::x25::states::P6_DTEClearingState::~P6_DTEClearingState(void) {
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::P6_DTEClearingState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::P6_DTEClearing;
    const State::SharedPointer state(new P6_DTEClearingState(enumeration));

    return state;
}

void xot4cpp::x25::states::P6_DTEClearingState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {

        case transitions::TransitionEnumerations::CallIncoming:
            break;

        case transitions::TransitionEnumerations::CallConnected:
            break;

        case transitions::TransitionEnumerations::ClearRequest:
            break;

        case transitions::TransitionEnumerations::ClearIndication:
            p_stateContext.setState(P1_ReadyState::create());
            break;

        case transitions::TransitionEnumerations::ClearConfirmation:
            p_stateContext.setState(P1_ReadyState::create());
            break;

        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}
