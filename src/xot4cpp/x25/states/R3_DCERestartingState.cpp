#include "xot4cpp/x25/states/R3_DCERestartingState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/R1_PacketLayerReadyState.hpp"

xot4cpp::x25::states::R3_DCERestartingState::R3_DCERestartingState(const StateEnumerations::StateEnumeration& p_enumeration)
: State(p_enumeration) {
}

xot4cpp::x25::states::R3_DCERestartingState::~R3_DCERestartingState(void) {
}

void xot4cpp::x25::states::R3_DCERestartingState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {
        case transitions::TransitionEnumerations::RestartRequest:
            p_stateContext.setState(R1_PacketLayerReadyState::create());
            break;

        case transitions::TransitionEnumerations::RestartIndication:
            break;

        case transitions::TransitionEnumerations::RestartConfirmation:
            p_stateContext.setState(R1_PacketLayerReadyState::create());
            break;

        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::R3_DCERestartingState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::R3_DCERestarting;
    const State::SharedPointer state(new R3_DCERestartingState(enumeration));

    return state;
}
