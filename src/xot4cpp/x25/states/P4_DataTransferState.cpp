#include "xot4cpp/x25/states/P4_DataTransferState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/D1_FlowControlReadyState.hpp"
#include "xot4cpp/x25/states/P6_DTEClearingState.hpp"
#include "xot4cpp/x25/states/P7_DCEClearingState.hpp"

xot4cpp::x25::states::P4_DataTransferState::P4_DataTransferState(
        const StateEnumerations::StateEnumeration& p_enumeration,
        const State::SharedPointer& p_state)
: State(p_enumeration),
StateContext(p_state) {
}

xot4cpp::x25::states::P4_DataTransferState::~P4_DataTransferState(void) {
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::P4_DataTransferState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::P4_DataTransfer;
    const State::SharedPointer subState(D1_FlowControlReadyState::create());
    const State::SharedPointer state(new P4_DataTransferState(enumeration, subState));

    return state;
}

const xot4cpp::x25::states::StateEnumerations::StateEnumeration& xot4cpp::x25::states::P4_DataTransferState::enumeration(void) const {

    return state()->enumeration();
}

void xot4cpp::x25::states::P4_DataTransferState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {

        case transitions::TransitionEnumerations::ClearRequest:
            p_stateContext.setState(P6_DTEClearingState::create());
            break;

        case transitions::TransitionEnumerations::ClearIndication:
            p_stateContext.setState(P7_DCEClearingState::create());
            break;

        case transitions::TransitionEnumerations::Data:
        case transitions::TransitionEnumerations::Interrupt:
        case transitions::TransitionEnumerations::InterruptConfirmation:
        case transitions::TransitionEnumerations::ReceiverReady:
        case transitions::TransitionEnumerations::ReceiverNotReady:
        case transitions::TransitionEnumerations::Reject:
            // D-State transitions
        case transitions::TransitionEnumerations::ResetRequest:
        case transitions::TransitionEnumerations::ResetIndication:
        case transitions::TransitionEnumerations::ResetConfirmation:
            state()->handle(*this, p_transition);
            break;

        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}
