#include "xot4cpp/x25/states/P1_ReadyState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/P2_DTEWaitingState.hpp"
#include "xot4cpp/x25/states/P3_DCEWaitingState.hpp"
#include "xot4cpp/x25/states/P6_DTEClearingState.hpp"
#include "xot4cpp/x25/states/P7_DCEClearingState.hpp"

xot4cpp::x25::states::P1_ReadyState::P1_ReadyState(const StateEnumerations::StateEnumeration& p_enumeration)
: State(p_enumeration) {
}

xot4cpp::x25::states::P1_ReadyState::~P1_ReadyState(void) {
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::P1_ReadyState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::P1_Ready;
    const State::SharedPointer state(new P1_ReadyState(enumeration));

    return state;
}

void xot4cpp::x25::states::P1_ReadyState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {

            // P-State transitions
        case transitions::TransitionEnumerations::CallRequest:
            p_stateContext.setState(P2_DTEWaitingState::create());
            break;

        case transitions::TransitionEnumerations::CallIncoming:
            p_stateContext.setState(P3_DCEWaitingState::create());
            break;

        case transitions::TransitionEnumerations::ClearRequest:
            p_stateContext.setState(P6_DTEClearingState::create());
            break;

        case transitions::TransitionEnumerations::ClearIndication:
            p_stateContext.setState(P7_DCEClearingState::create());
            break;

        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}
