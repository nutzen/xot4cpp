#include "xot4cpp/x25/states/StateEnumerations.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

// P-states DESCRIPTION
#define P1_READY_DESCRIPTION "P1_Ready"
#define P2_DTE_WAITING_DESCRIPTION "P2_DTE_Waiting"
#define P3_DCE_WAITING_DESCRIPTION "P3_DCE_Waiting"
#define P4_DATA_TRANSFER_DESCRIPTION "P4_Data_Transfer"
#define P5_CALL_COLLISION_DESCRIPTION "P5_Call_Collision"
#define P6_DTE_CLEARING_DESCRIPTION "P6_DTE_Clearing"
#define P7_DCE_CLEARING_DESCRIPTION "P7_DCE_Clearing"
// D-states DESCRIPTION
#define D1_FLOW_CONTROL_READY_DESCRIPTION "D1_Flow_Control_Ready"
#define D2_DTE_RESETING_DESCRIPTION "D2_DTE_Reseting"
#define D3_DCE_RESETING_DESCRIPTION "D3_DCE_Reseting"
// R-states DESCRIPTION
#define R1_PACKET_LAYER_READY_DESCRIPTION "R1_Packet_Layer_Ready"
#define R2_DTE_RESTARTING_DESCRIPTION "R2_DTE_Restarting"
#define R3_DCE_RESTARTING_DESCRIPTION "R3_DCE_Restarting"

std::string xot4cpp::x25::states::StateEnumerations::toDescription(const StateEnumeration& p_source) {

    std::string destination;

    switch (p_source) {
        case P1_Ready:
            destination = P1_READY_DESCRIPTION;
            break;

        case P2_DTEWaiting:
            destination = P2_DTE_WAITING_DESCRIPTION;
            break;

        case P3_DCEWaiting:
            destination = P3_DCE_WAITING_DESCRIPTION;
            break;

        case P4_DataTransfer:
            destination = P4_DATA_TRANSFER_DESCRIPTION;
            break;

        case P5_CallCollision:
            destination = P5_CALL_COLLISION_DESCRIPTION;
            break;

        case P6_DTEClearing:
            destination = P6_DTE_CLEARING_DESCRIPTION;
            break;

        case P7_DCEClearing:
            destination = P7_DCE_CLEARING_DESCRIPTION;
            break;

        case D1_FlowControlReady:
            destination = D1_FLOW_CONTROL_READY_DESCRIPTION;
            break;

        case D2_DTEReseting:
            destination = D2_DTE_RESETING_DESCRIPTION;
            break;

        case D3_DCEReseting:
            destination = D3_DCE_RESETING_DESCRIPTION;
            break;

        case R1_PacketLayerReady:
            destination = R1_PACKET_LAYER_READY_DESCRIPTION;
            break;

        case R2_DTERestarting:
            destination = R2_DTE_RESTARTING_DESCRIPTION;
            break;

        case R3_DCERestarting:
            destination = R3_DCE_RESTARTING_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::states::StateEnumerations::StateEnumeration xot4cpp::x25::states::StateEnumerations::fromDescription(const std::string& p_source) {

    StateEnumeration destination;

    if (0 == p_source.compare(P1_READY_DESCRIPTION)) {
        destination = P1_Ready;

    } else if (0 == p_source.compare(P2_DTE_WAITING_DESCRIPTION)) {
        destination = P2_DTEWaiting;

    } else if (0 == p_source.compare(P3_DCE_WAITING_DESCRIPTION)) {
        destination = P3_DCEWaiting;

    } else if (0 == p_source.compare(P4_DATA_TRANSFER_DESCRIPTION)) {
        destination = P4_DataTransfer;

    } else if (0 == p_source.compare(P5_CALL_COLLISION_DESCRIPTION)) {
        destination = P5_CallCollision;

    } else if (0 == p_source.compare(P6_DTE_CLEARING_DESCRIPTION)) {
        destination = P6_DTEClearing;

    } else if (0 == p_source.compare(P7_DCE_CLEARING_DESCRIPTION)) {
        destination = P7_DCEClearing;

    } else if (0 == p_source.compare(D1_FLOW_CONTROL_READY_DESCRIPTION)) {
        destination = D1_FlowControlReady;

    } else if (0 == p_source.compare(D2_DTE_RESETING_DESCRIPTION)) {
        destination = D2_DTEReseting;

    } else if (0 == p_source.compare(D3_DCE_RESETING_DESCRIPTION)) {
        destination = D3_DCEReseting;

    } else if (0 == p_source.compare(R1_PACKET_LAYER_READY_DESCRIPTION)) {
        destination = R1_PacketLayerReady;

    } else if (0 == p_source.compare(R2_DTE_RESTARTING_DESCRIPTION)) {
        destination = R2_DTERestarting;

    } else if (0 == p_source.compare(R3_DCE_RESTARTING_DESCRIPTION)) {
        destination = R3_DCERestarting;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}
