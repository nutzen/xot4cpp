#include "xot4cpp/x25/states/D3_DCEResetingState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/D1_FlowControlReadyState.hpp"

xot4cpp::x25::states::D3_DCEResetingState::D3_DCEResetingState(const StateEnumerations::StateEnumeration& p_enumeration)
: State(p_enumeration) {
}

xot4cpp::x25::states::D3_DCEResetingState::~D3_DCEResetingState(void) {
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::D3_DCEResetingState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::D3_DCEReseting;
    const State::SharedPointer state(new D3_DCEResetingState(enumeration));

    return state;
}

void xot4cpp::x25::states::D3_DCEResetingState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {
        case transitions::TransitionEnumerations::ResetRequest:
            p_stateContext.setState(D1_FlowControlReadyState::create());
            break;

        case transitions::TransitionEnumerations::ResetIndication:
            break;

        case transitions::TransitionEnumerations::ResetConfirmation:
            p_stateContext.setState(D1_FlowControlReadyState::create());
            break;

        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}
