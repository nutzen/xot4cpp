#include "xot4cpp/x25/states/D2_DTEResetingState.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/states/D1_FlowControlReadyState.hpp"

xot4cpp::x25::states::D2_DTEResetingState::D2_DTEResetingState(const StateEnumerations::StateEnumeration& p_enumeration)
: State(p_enumeration) {
}

xot4cpp::x25::states::D2_DTEResetingState::~D2_DTEResetingState(void) {
}

xot4cpp::x25::states::State::SharedPointer xot4cpp::x25::states::D2_DTEResetingState::create(void) {

    const StateEnumerations::StateEnumeration enumeration = StateEnumerations::D2_DTEReseting;
    const State::SharedPointer state(new D2_DTEResetingState(enumeration));

    return state;
}

void xot4cpp::x25::states::D2_DTEResetingState::handle(StateContext& p_stateContext, const transitions::TransitionEnumerations::TransitionEnumeration& p_transition) {

    switch (p_transition) {
        case transitions::TransitionEnumerations::ResetRequest:
            break;

        case transitions::TransitionEnumerations::ResetIndication:
            p_stateContext.setState(D1_FlowControlReadyState::create());
            break;

        case transitions::TransitionEnumerations::ResetConfirmation:
            p_stateContext.setState(D1_FlowControlReadyState::create());
            break;

        default:
            State::handle(p_stateContext, p_transition);
            break;
    }
}
