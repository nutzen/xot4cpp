#include "xot4cpp/x25/states/StateContext.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::states::StateContext::StateContext(const State::SharedPointer& p_state)
: m_state(p_state) {
}

xot4cpp::x25::states::StateContext::~StateContext(void) {
}

xot4cpp::x25::states::StateContext::SharedPointer xot4cpp::x25::states::StateContext::create(const State::SharedPointer& p_state) {

    const SharedPointer stateContext(new StateContext(p_state));
    return stateContext;
}

const xot4cpp::x25::states::State::SharedPointer& xot4cpp::x25::states::StateContext::state(void) const {

    return m_state;
}

void xot4cpp::x25::states::StateContext::setState(const State::SharedPointer& p_state) {

    m_state = p_state;
}
