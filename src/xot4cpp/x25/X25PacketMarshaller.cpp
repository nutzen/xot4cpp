#include "xot4cpp/x25/X25PacketMarshaller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/X25HeaderMarshaller.hpp"
#include "xot4cpp/x25/payload/X25PayloadMarshaller.hpp"

#include "xot4cpp/exceptions/Xot4cppException.hpp"

/**
 *
 * @todo extract a-bit from x.25 header
 */
#define A_BIT false

void xot4cpp::x25::X25PacketMarshaller::marshal(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const X25Packet::SharedPointer& p_source) {

    const X25Header::SharedPointer header = p_source->header();
    const payload::X25Payload::SharedPointer payload = p_source->payload();

    X25HeaderMarshaller::marshal(p_outputStream, header);

    const pti::PacketTypeIdentifier::SharedPointer pti = header->packetTypeIdentifier();
    const pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration enumeration = pti->enumeration();

    switch (enumeration) {
        case pti::PacketTypeIdentifierEnumerations::CallRequest_IncomingCall:
        case pti::PacketTypeIdentifierEnumerations::CallAccepted_CallConnected:
            payload::X25PayloadMarshaller::marshalCallPacketPayload(p_outputStream, payload, A_BIT);
            break;

        case pti::PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication:
            payload::X25PayloadMarshaller::marshalClearPacketPayload(p_outputStream, payload, A_BIT);
            break;

        case pti::PacketTypeIdentifierEnumerations::ClearConfirmation:
            payload::X25PayloadMarshaller::marshalClearConfirmationPayload(p_outputStream, payload, A_BIT);
            break;

        case pti::PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication:
            payload::X25PayloadMarshaller::marshalResetPacketPayload(p_outputStream, payload);
            break;

        case pti::PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication:
            payload::X25PayloadMarshaller::marshalRestartPacketPayload(p_outputStream, payload);
            break;

        case pti::PacketTypeIdentifierEnumerations::Data:
            payload::X25PayloadMarshaller::marshalDataPacketPayload(p_outputStream, payload);
            break;

        case pti::PacketTypeIdentifierEnumerations::ReceiverReady:
        case pti::PacketTypeIdentifierEnumerations::ReceiverNotReady:
        case pti::PacketTypeIdentifierEnumerations::Reject:
        case pti::PacketTypeIdentifierEnumerations::ResetConfirmation:
        case pti::PacketTypeIdentifierEnumerations::RestartConfirmation:
            payload::X25PayloadMarshaller::marshalEmptyPacketPayload(p_outputStream, payload);
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << enumeration);
            break;
    };
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::X25PacketMarshaller::unmarshal(
        const stream::InputStream::SharedPointer& p_inputStream) {

    const X25Header::SharedPointer header = X25HeaderMarshaller::unmarshal(p_inputStream);

    payload::X25Payload::SharedPointer payload;

    const pti::PacketTypeIdentifier::SharedPointer pti = header->packetTypeIdentifier();
    const pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration enumeration = pti->enumeration();

    switch (enumeration) {
        case pti::PacketTypeIdentifierEnumerations::CallRequest_IncomingCall:
        case pti::PacketTypeIdentifierEnumerations::CallAccepted_CallConnected:
            payload = payload::X25PayloadMarshaller::unmarshalCallPacketPayload(p_inputStream, A_BIT);
            break;

        case pti::PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication:
            payload = payload::X25PayloadMarshaller::unmarshalClearPacketPayload(p_inputStream, A_BIT);
            break;

        case pti::PacketTypeIdentifierEnumerations::ClearConfirmation:
            payload = payload::X25PayloadMarshaller::unmarshalClearConfirmationPayload(p_inputStream, A_BIT);
            break;

        case pti::PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication:
            payload = payload::X25PayloadMarshaller::unmarshalResetPacketPayload(p_inputStream);
            break;

        case pti::PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication:
            payload = payload::X25PayloadMarshaller::unmarshalRestartPacketPayload(p_inputStream);
            break;

        case pti::PacketTypeIdentifierEnumerations::Data:
            payload = payload::X25PayloadMarshaller::unmarshalDataPacketPayload(p_inputStream);
            break;

        case pti::PacketTypeIdentifierEnumerations::ReceiverReady:
        case pti::PacketTypeIdentifierEnumerations::ReceiverNotReady:
        case pti::PacketTypeIdentifierEnumerations::Reject:
        case pti::PacketTypeIdentifierEnumerations::ResetConfirmation:
        case pti::PacketTypeIdentifierEnumerations::RestartConfirmation:
            payload = payload::X25PayloadMarshaller::unmarshalEmptyPacketPayload(p_inputStream);
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << enumeration);
            break;
    }

    return X25Packet::create(header, payload);
}
