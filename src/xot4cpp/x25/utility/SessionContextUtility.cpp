#include "xot4cpp/x25/utility/SessionContextUtility.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/facilities/Facility.hpp"
#include "xot4cpp/x25/facilities/PacketSizeSelection.hpp"
#include "xot4cpp/x25/facilities/WindowSizeSelection.hpp"
#include "xot4cpp/x25/pti/PacketTypeIdentifierEnumerations.hpp"

void xot4cpp::x25::utility::SessionContextUtility::updatePacketReceiveSequenceNumber(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    unsigned int sequenceNumber = p_sessionContext->packetReceiveSequenceNumber();
    sequenceNumber = (1 + sequenceNumber) % 8;
    p_sessionContext->packetReceiveSequenceNumber() = sequenceNumber;
}

void xot4cpp::x25::utility::SessionContextUtility::updatePacketSendSequenceNumber(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    unsigned int sequenceNumber = p_sessionContext->packetSendSequenceNumber();
    sequenceNumber = (1 + sequenceNumber) % 8;
    p_sessionContext->packetSendSequenceNumber() = sequenceNumber;
}

unsigned int xot4cpp::x25::utility::SessionContextUtility::receivePacketSize(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    unsigned int size = facilities::PacketSizeSelectionEnumerations::toPacketSize(facilities::PacketSizeSelectionEnumerations::Size128octets);

    for (unsigned int index = 0; p_sessionContext->facilities().size() > index; ++index) {
        const facilities::Facility::SharedPointer facility = p_sessionContext->facilities()[index];
        if (facilities::FacilityEnumerations::PacketSizeSelection == facility->enumeration()) {

            const facilities::PacketSizeSelection::SharedPointer sizeSelection =
                    facilities::PacketSizeSelection::downCast(facility);

            const facilities::PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration enumeration =
                    sizeSelection->receivePacketSize();
            size = facilities::PacketSizeSelectionEnumerations::toPacketSize(enumeration);

            break;
        }
    }

    return size;
}

unsigned int xot4cpp::x25::utility::SessionContextUtility::transmitPacketSize(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    unsigned int size = facilities::PacketSizeSelectionEnumerations::toPacketSize(facilities::PacketSizeSelectionEnumerations::Size128octets);

    for (unsigned int index = 0; p_sessionContext->facilities().size() > index; ++index) {
        const facilities::Facility::SharedPointer facility = p_sessionContext->facilities()[index];
        if (facilities::FacilityEnumerations::PacketSizeSelection == facility->enumeration()) {

            const facilities::PacketSizeSelection::SharedPointer sizeSelection =
                    facilities::PacketSizeSelection::downCast(facility);

            const facilities::PacketSizeSelectionEnumerations::PacketSizeSelectionEnumeration enumeration =
                    sizeSelection->transmitPacketSize();
            size = facilities::PacketSizeSelectionEnumerations::toPacketSize(enumeration);

            break;
        }
    }

    return size;
}

unsigned int xot4cpp::x25::utility::SessionContextUtility::receiveWindowSize(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    unsigned int size = 2;

    for (unsigned int index = 0; p_sessionContext->facilities().size() > index; ++index) {
        const facilities::Facility::SharedPointer facility = p_sessionContext->facilities()[index];
        if (facilities::FacilityEnumerations::WindowSizeSelection == facility->enumeration()) {

            const facilities::WindowSizeSelection::SharedPointer sizeSelection =
                    facilities::WindowSizeSelection::downCast(facility);

            size = sizeSelection->receiveWindowSize();

            break;
        }
    }

    return size;
}

unsigned int xot4cpp::x25::utility::SessionContextUtility::transmissionWindowSize(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    unsigned int size = 2;

    for (unsigned int index = 0; p_sessionContext->facilities().size() > index; ++index) {
        const facilities::Facility::SharedPointer facility = p_sessionContext->facilities()[index];
        if (facilities::FacilityEnumerations::WindowSizeSelection == facility->enumeration()) {

            const facilities::WindowSizeSelection::SharedPointer sizeSelection =
                    facilities::WindowSizeSelection::downCast(facility);

            size = sizeSelection->transmissionWindowSize();

            break;
        }
    }

    return size;
}
