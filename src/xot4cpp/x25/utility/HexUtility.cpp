#include "xot4cpp/x25/utility/HexUtility.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <sstream>

std::string xot4cpp::x25::utility::HexUtility::toHex(const Octet& p_octet) {

    std::stringstream stringstream;
    stringstream << "00";
    stringstream << std::hex << static_cast<unsigned int> (0x00ff & p_octet);

    const std::string text = stringstream.str();
    return text.substr(text.size() - 2);
}

std::string xot4cpp::x25::utility::HexUtility::toHex(const Octets& p_octets) {

    std::stringstream stringstream;

    for (unsigned int index = 0; p_octets.size() > index; ++index) {
        if (index) {
            stringstream << " ";
        }
        stringstream << toHex(p_octets[index]);
    }

    return stringstream.str();
}
