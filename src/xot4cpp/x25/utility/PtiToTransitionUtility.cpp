#include "xot4cpp/x25/utility/PtiToTransitionUtility.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

xot4cpp::x25::transitions::TransitionEnumerations::TransitionEnumeration xot4cpp::x25::utility::PtiToTransitionUtility::transition(
        const transitions::TransitionParameters::SharedPointer& p_parameters) {

    const context::RoleEnumerations::RoleEnumeration origin =
            p_parameters->origin();
    const pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration pti =
            p_parameters->packet()->header()->packetTypeIdentifier()->enumeration();

    return transition(origin, pti);
}

xot4cpp::x25::transitions::TransitionEnumerations::TransitionEnumeration xot4cpp::x25::utility::PtiToTransitionUtility::transition(
        const context::RoleEnumerations::RoleEnumeration& p_role,
        const pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_packetTypeIdentifier) {


    transitions::TransitionEnumerations::TransitionEnumeration destination;

    switch (p_packetTypeIdentifier) {
        case pti::PacketTypeIdentifierEnumerations::CallRequest_IncomingCall:
            if (context::RoleEnumerations::DTE == p_role) {
                destination = transitions::TransitionEnumerations::CallRequest;
            } else /* if (SessionRole.DCE == p_role) */ {
                destination = transitions::TransitionEnumerations::CallIncoming;
            }
            break;

        case pti::PacketTypeIdentifierEnumerations::CallAccepted_CallConnected:
            if (context::RoleEnumerations::DTE == p_role) {
                destination = transitions::TransitionEnumerations::CallAccepted;
            } else /* if (SessionRole.DCE == p_role) */ {
                destination = transitions::TransitionEnumerations::CallConnected;
            }
            break;

        case pti::PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication:
            if (context::RoleEnumerations::DTE == p_role) {
                destination = transitions::TransitionEnumerations::ClearRequest;
            } else /* if (SessionRole.DCE == p_role) */ {
                destination = transitions::TransitionEnumerations::ClearIndication;
            }
            break;

        case pti::PacketTypeIdentifierEnumerations::ClearConfirmation:
            destination = transitions::TransitionEnumerations::ClearConfirmation;
            break;

        case pti::PacketTypeIdentifierEnumerations::Data:
            destination = transitions::TransitionEnumerations::Data;
            break;

        case pti::PacketTypeIdentifierEnumerations::Interrupt:
            destination = transitions::TransitionEnumerations::Interrupt;
            break;

        case pti::PacketTypeIdentifierEnumerations::InterruptConfirmation:
            destination = transitions::TransitionEnumerations::InterruptConfirmation;
            break;

        case pti::PacketTypeIdentifierEnumerations::ReceiverReady:
            destination = transitions::TransitionEnumerations::ReceiverReady;
            break;

        case pti::PacketTypeIdentifierEnumerations::ReceiverNotReady:
            destination = transitions::TransitionEnumerations::ReceiverNotReady;
            break;

        case pti::PacketTypeIdentifierEnumerations::Reject:
            destination = transitions::TransitionEnumerations::Reject;
            break;

        case pti::PacketTypeIdentifierEnumerations::ResetRequest_ResetIndication:
            if (context::RoleEnumerations::DTE == p_role) {
                destination = transitions::TransitionEnumerations::ResetRequest;
            } else /* if (SessionRole.DCE == p_role) */ {
                destination = transitions::TransitionEnumerations::ResetIndication;
            }
            break;

        case pti::PacketTypeIdentifierEnumerations::ResetConfirmation:
            destination = transitions::TransitionEnumerations::ResetConfirmation;
            break;

        case pti::PacketTypeIdentifierEnumerations::RestartRequest_RestartIndication:
            if (context::RoleEnumerations::DTE == p_role) {
                destination = transitions::TransitionEnumerations::RestartRequest;
            } else /* if (SessionRole.DCE == p_role) */ {
                destination = transitions::TransitionEnumerations::RestartIndication;
            }
            break;

        case pti::PacketTypeIdentifierEnumerations::RestartConfirmation:
            destination = transitions::TransitionEnumerations::RestartConfirmation;
            break;

        case pti::PacketTypeIdentifierEnumerations::Diagnostic:
            destination = transitions::TransitionEnumerations::Diagnostic;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION(
                    "Unhandled value: " << p_packetTypeIdentifier);
            break;
    }

    return destination;
}
