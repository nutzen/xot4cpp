#include "xot4cpp/x25/utility/X25PacketUtility.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <xot4cpp/x25/payload/CallPacketPayload.hpp>
#include <xot4cpp/x25/payload/ClearConfirmationPayload.hpp>
#include <xot4cpp/x25/payload/ClearPacketPayload.hpp>
#include <xot4cpp/x25/payload/DataPacketPayload.hpp>
#include <xot4cpp/x25/payload/EmptyPacketPayload.hpp>

namespace xot4cpp {

    namespace x25 {

        namespace utility {

            namespace X25PacketUtility {

                extern xot4cpp::x25::X25Header::SharedPointer header(
                        const xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
                        const xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_packetTypeIdentifierEnumeration);

                extern xot4cpp::x25::X25Header::SharedPointer header(
                        const xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
                        const xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_packetTypeIdentifierEnumeration,
                        const bool& p_moreData,
                        const unsigned int& p_packetReceiveSequenceNumber,
                        const unsigned int& p_packetSendSequenceNumber);

                extern xot4cpp::x25::X25Header::SharedPointer header(
                        const xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
                        const xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_packetTypeIdentifierEnumeration,
                        const unsigned int& p_packetReceiveSequenceNumber);
            };
        };
    };
};

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::utility::X25PacketUtility::callRequest(
        const context::SessionContext::SharedPointer& p_sessionContext,
        const Octets& p_userData) {

    const xot4cpp::x25::X25Header::SharedPointer header = X25PacketUtility::header(
            p_sessionContext->logicalChannelIdentifier(),
            xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::CallRequest_IncomingCall);

    xot4cpp::x25::payload::X25Payload::SharedPointer payload;
    {
        const xot4cpp::x121::X121Address::SharedPointer calledAddress =
                p_sessionContext->remoteAddress();

        const xot4cpp::x121::X121Address::SharedPointer callingAddress =
                p_sessionContext->localAddress();

        const xot4cpp::x25::facilities::Facility::List facilities =
                p_sessionContext->facilities();

        payload = xot4cpp::x25::payload::CallPacketPayload::create(calledAddress, callingAddress, facilities, p_userData);
    }

    return xot4cpp::x25::X25Packet::create(header, payload);
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::utility::X25PacketUtility::callAccept(
        const context::SessionContext::SharedPointer& p_sessionContext,
        const Octets& p_userData) {

    const xot4cpp::x25::X25Header::SharedPointer header = X25PacketUtility::header(
            p_sessionContext->logicalChannelIdentifier(),
            xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::CallAccepted_CallConnected);

    xot4cpp::x25::payload::X25Payload::SharedPointer payload;
    {
        const xot4cpp::x121::X121Address::SharedPointer calledAddress;

        const xot4cpp::x121::X121Address::SharedPointer callingAddress;

        const xot4cpp::x25::facilities::Facility::List facilities;

        payload = xot4cpp::x25::payload::CallPacketPayload::create(calledAddress, callingAddress, facilities, p_userData);
    }

    return xot4cpp::x25::X25Packet::create(header, payload);
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::utility::X25PacketUtility::clearConfirm(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    const xot4cpp::x25::X25Header::SharedPointer header = X25PacketUtility::header(
            p_sessionContext->logicalChannelIdentifier(),
            xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::ClearConfirmation);

    const xot4cpp::x25::payload::X25Payload::SharedPointer payload =
            xot4cpp::x25::payload::ClearConfirmationPayload::create();

    return xot4cpp::x25::X25Packet::create(header, payload);
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::utility::X25PacketUtility::clearRequest(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    const xot4cpp::x25::X25Header::SharedPointer header = X25PacketUtility::header(
            p_sessionContext->logicalChannelIdentifier(),
            xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::ClearRequest_ClearIndication);

    const xot4cpp::x25::payload::X25Payload::SharedPointer payload =
            xot4cpp::x25::payload::ClearPacketPayload::create(
            xot4cpp::x25::codes::CauseCodeEnumerations::ClearRequestDteOriginated,
            xot4cpp::x25::codes::DiagnosticCodeEnumerations::NoAdditionalInformation);

    return xot4cpp::x25::X25Packet::create(header, payload);
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::utility::X25PacketUtility::data(
        const context::SessionContext::SharedPointer& p_sessionContext,
        const Octets& p_userData,
        const bool& p_moreData) {

    const xot4cpp::x25::X25Header::SharedPointer header = X25PacketUtility::header(
            p_sessionContext->logicalChannelIdentifier(),
            xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::Data,
            p_moreData,
            p_sessionContext->packetReceiveSequenceNumber(),
            p_sessionContext->packetSendSequenceNumber());

    const xot4cpp::x25::payload::X25Payload::SharedPointer payload =
            xot4cpp::x25::payload::DataPacketPayload::create(p_userData);

    return xot4cpp::x25::X25Packet::create(header, payload);
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::utility::X25PacketUtility::receiverReady(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    const xot4cpp::x25::X25Header::SharedPointer header = X25PacketUtility::header(
            p_sessionContext->logicalChannelIdentifier(),
            xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::ReceiverReady,
            p_sessionContext->packetReceiveSequenceNumber());

    const xot4cpp::x25::payload::X25Payload::SharedPointer payload =
            xot4cpp::x25::payload::EmptyPacketPayload::create();

    return xot4cpp::x25::X25Packet::create(header, payload);
}

xot4cpp::x25::X25Header::SharedPointer xot4cpp::x25::utility::X25PacketUtility::header(
        const xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
        const xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_packetTypeIdentifierEnumeration) {

    const xot4cpp::x25::gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = xot4cpp::x25::gfi::GeneralFormatIdentifier::create(
            xot4cpp::x25::gfi::GeneralFormatIdentifier::DataForUser,
            xot4cpp::x25::gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
            xot4cpp::x25::gfi::GeneralFormatIdentifier::Modulo8Sequencing);

    const xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
            xot4cpp::x25::pti::PacketTypeIdentifier::create(p_packetTypeIdentifierEnumeration);

    return xot4cpp::x25::X25Header::create(generalFormatIdentifier, p_logicalChannelIdentifier, packetTypeIdentifier);
}

xot4cpp::x25::X25Header::SharedPointer xot4cpp::x25::utility::X25PacketUtility::header(
        const xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
        const xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_packetTypeIdentifierEnumeration,
        const bool& p_moreData,
        const unsigned int& p_packetReceiveSequenceNumber,
        const unsigned int& p_packetSendSequenceNumber) {

    const xot4cpp::x25::gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = xot4cpp::x25::gfi::GeneralFormatIdentifier::create(
            xot4cpp::x25::gfi::GeneralFormatIdentifier::DataForUser,
            xot4cpp::x25::gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
            xot4cpp::x25::gfi::GeneralFormatIdentifier::Modulo8Sequencing);

    const xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
            xot4cpp::x25::pti::PacketTypeIdentifier::create(
            p_packetTypeIdentifierEnumeration,
            p_moreData,
            p_packetReceiveSequenceNumber,
            p_packetSendSequenceNumber);

    return xot4cpp::x25::X25Header::create(generalFormatIdentifier, p_logicalChannelIdentifier, packetTypeIdentifier);
}

xot4cpp::x25::X25Header::SharedPointer xot4cpp::x25::utility::X25PacketUtility::header(
        const xot4cpp::x25::lci::LogicalChannelIdentifier::SharedPointer& p_logicalChannelIdentifier,
        const xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::PacketTypeIdentifierEnumeration& p_packetTypeIdentifierEnumeration,
        const unsigned int& p_packetReceiveSequenceNumber) {

    const xot4cpp::x25::gfi::GeneralFormatIdentifier::SharedPointer generalFormatIdentifier = xot4cpp::x25::gfi::GeneralFormatIdentifier::create(
            xot4cpp::x25::gfi::GeneralFormatIdentifier::DataForUser,
            xot4cpp::x25::gfi::GeneralFormatIdentifier::ForLocalAcknowledgment,
            xot4cpp::x25::gfi::GeneralFormatIdentifier::Modulo8Sequencing);

    const xot4cpp::x25::pti::PacketTypeIdentifier::SharedPointer packetTypeIdentifier =
            xot4cpp::x25::pti::PacketTypeIdentifier::create(
            p_packetTypeIdentifierEnumeration,
            p_packetReceiveSequenceNumber);

    return xot4cpp::x25::X25Header::create(generalFormatIdentifier, p_logicalChannelIdentifier, packetTypeIdentifier);
}

xot4cpp::x25::X25Packet::SharedPointer xot4cpp::x25::utility::X25PacketUtility::resetConfirm(
        const context::SessionContext::SharedPointer& p_sessionContext) {

    const xot4cpp::x25::X25Header::SharedPointer header = X25PacketUtility::header(
            p_sessionContext->logicalChannelIdentifier(),
            xot4cpp::x25::pti::PacketTypeIdentifierEnumerations::ResetConfirmation);

    const xot4cpp::x25::payload::X25Payload::SharedPointer payload =
            xot4cpp::x25::payload::EmptyPacketPayload::create();

    return xot4cpp::x25::X25Packet::create(header, payload);
}
