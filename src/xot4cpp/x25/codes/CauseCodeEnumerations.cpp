#include "xot4cpp/x25/codes/CauseCodeEnumerations.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

// ClearRequest Enumeration DESCRIPTION
#define CLEAR_REQUEST_DTE_ORIGINATED_DESCRIPTION "DTE_originated"
#define CLEAR_REQUEST_NUMBER_BUSY_DESCRIPTION "Number_busy"
#define CLEAR_REQUEST_INVALID_FACILITY_REQUEST_DESCRIPTION "Invalid_facility_request"
#define CLEAR_REQUEST_NETWORK_CONGESTION_DESCRIPTION "Network_congestion"
#define CLEAR_REQUEST_OUT_OF_ORDER_DESCRIPTION "Out_of_order"
#define CLEAR_REQUEST_ACCESS_BARRED_DESCRIPTION "Access_barred"
#define CLEAR_REQUEST_NOT_OBTAINABLE_DESCRIPTION "Not_obtainable"
#define CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION "Remote_procedure_error"
#define CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION "Local_procedure_error"
#define CLEAR_REQUEST_RPOA_OUT_OF_ORDER_DESCRIPTION "RPOA_out_of_order"
#define CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_DESCRIPTION "Reverse_charging_not_accepted"
#define CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION "Incompatible_destination"
#define CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_DESCRIPTION "Fast_select_not_accepted"
#define CLEAR_REQUEST_SHIP_ABSENT_DESCRIPTION "Ship_absent"
// ClearRequest Enumeration MASK
#define CLEAR_REQUEST_DTE_ORIGINATED_MASK 0x00
#define CLEAR_REQUEST_NUMBER_BUSY_MASK 0x01
#define CLEAR_REQUEST_INVALID_FACILITY_REQUEST_MASK 0x03
#define CLEAR_REQUEST_NETWORK_CONGESTION_MASK 0x05
#define CLEAR_REQUEST_OUT_OF_ORDER_MASK 0x09
#define CLEAR_REQUEST_ACCESS_BARRED_MASK 0x0b
#define CLEAR_REQUEST_NOT_OBTAINABLE_MASK 0x0d
#define CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_MASK 0x11
#define CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_MASK 0x13
#define CLEAR_REQUEST_RPOA_OUT_OF_ORDER_MASK 0x15
#define CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_MASK 0x19
#define CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_MASK 0x21
#define CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_MASK 0x29
#define CLEAR_REQUEST_SHIP_ABSENT_MASK 0x39
// ResetRequest Enumeration DESCRIPTION
#define RESET_REQUEST_DTE_ORIGINATED_DESCRIPTION "DTE_originated"
#define RESET_REQUEST_OUT_OF_ORDER_DESCRIPTION "Out_of_order"
#define RESET_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION "Remote_procedure_error"
#define RESET_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION "Local_procedure_error"
#define RESET_REQUEST_NETWORK_CONGESTION_DESCRIPTION "Network_congestion"
#define RESET_REQUEST_REMOTE_DTE_OPERATIONAL_DESCRIPTION "Remote_DTE_operational"
#define RESET_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION "Network_operational"
#define RESET_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION "Incompatible_destination"
#define RESET_REQUEST_NETWORK_OUT_OF_ORDER_DESCRIPTION "Network_out_of_order"
// ResetRequest Enumeration MASK
#define RESET_REQUEST_DTE_ORIGINATED_MASK 0x00
#define RESET_REQUEST_OUT_OF_ORDER_MASK 0x01
#define RESET_REQUEST_REMOTE_PROCEDURE_ERROR_MASK 0x03
#define RESET_REQUEST_LOCAL_PROCEDURE_ERROR_MASK 0x05
#define RESET_REQUEST_NETWORK_CONGESTION_MASK 0x07
#define RESET_REQUEST_REMOTE_DTE_OPERATIONAL_MASK 0x09
#define RESET_REQUEST_NETWORK_OPERATIONAL_MASK 0x0f
#define RESET_REQUEST_INCOMPATIBLE_DESTINATION_MASK 0x11
#define RESET_REQUEST_NETWORK_OUT_OF_ORDER_MASK 0x1d
// RestartRequest Enumeration DESCRIPTION
#define RESTART_REQUEST_DTE_RESTARTING_DESCRIPTION "DTE_restarting"
#define RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION "Local_procedure_error"
#define RESTART_REQUEST_NETWORK_CONGESTION_DESCRIPTION "Network_congestion"
#define RESTART_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION "Network_operational"
#define RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_DESCRIPTION "Registration/cancellation_confirmed"
// RestartRequest Enumeration MASK
#define RESTART_REQUEST_DTE_RESTARTING_MASK 0x00
#define RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_MASK 0x01
#define RESTART_REQUEST_NETWORK_CONGESTION_MASK 0x03
#define RESTART_REQUEST_NETWORK_OPERATIONAL_MASK 0x07
#define RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_MASK 0x7f

namespace xot4cpp {

    namespace x25 {

        namespace codes {

            namespace CauseCodeEnumerations {

                extern bool compareWithMask(const int& p_mask, const int& p_value);
            };
        };
    };
};

// ClearRequestEnumeration

std::string xot4cpp::x25::codes::CauseCodeEnumerations::toDescription(const xot4cpp::x25::codes::CauseCodeEnumerations::ClearRequestEnumeration& p_source) {

    std::string destination;

    switch (p_source) {
        case ClearRequestDteOriginated:
            destination = CLEAR_REQUEST_DTE_ORIGINATED_DESCRIPTION;
            break;

        case ClearRequestNumberBusy:
            destination = CLEAR_REQUEST_NUMBER_BUSY_DESCRIPTION;
            break;

        case ClearRequestInvalidFacilityRequest:
            destination = CLEAR_REQUEST_INVALID_FACILITY_REQUEST_DESCRIPTION;
            break;

        case ClearRequestNetworkCongestion:
            destination = CLEAR_REQUEST_NETWORK_CONGESTION_DESCRIPTION;
            break;

        case ClearRequestOutOfOrder:
            destination = CLEAR_REQUEST_OUT_OF_ORDER_DESCRIPTION;
            break;

        case ClearRequestAccessBarred:
            destination = CLEAR_REQUEST_ACCESS_BARRED_DESCRIPTION;
            break;

        case ClearRequestNotObtainable:
            destination = CLEAR_REQUEST_NOT_OBTAINABLE_DESCRIPTION;
            break;

        case ClearRequestRemoteProcedureError:
            destination = CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION;
            break;

        case ClearRequestLocalProcedureError:
            destination = CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION;
            break;

        case ClearRequestRpoaOutOfOrder:
            destination = CLEAR_REQUEST_RPOA_OUT_OF_ORDER_DESCRIPTION;
            break;

        case ClearRequestReverseChargingNotAccepted:
            destination = CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_DESCRIPTION;
            break;

        case ClearRequestIncompatibleDestination:
            destination = CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION;
            break;

        case ClearRequestFastSelectNotAccepted:
            destination = CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_DESCRIPTION;
            break;

        case ClearRequestShipAbsent:
            destination = CLEAR_REQUEST_SHIP_ABSENT_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::codes::CauseCodeEnumerations::ClearRequestEnumeration xot4cpp::x25::codes::CauseCodeEnumerations::clearRequestFromDescription(const std::string& p_source) {

    ClearRequestEnumeration destination;

    if (0 == p_source.compare(CLEAR_REQUEST_DTE_ORIGINATED_DESCRIPTION)) {
        destination = ClearRequestDteOriginated;

    } else if (0 == p_source.compare(CLEAR_REQUEST_NUMBER_BUSY_DESCRIPTION)) {
        destination = ClearRequestNumberBusy;

    } else if (0 == p_source.compare(CLEAR_REQUEST_INVALID_FACILITY_REQUEST_DESCRIPTION)) {
        destination = ClearRequestInvalidFacilityRequest;

    } else if (0 == p_source.compare(CLEAR_REQUEST_NETWORK_CONGESTION_DESCRIPTION)) {
        destination = ClearRequestNetworkCongestion;

    } else if (0 == p_source.compare(CLEAR_REQUEST_OUT_OF_ORDER_DESCRIPTION)) {
        destination = ClearRequestOutOfOrder;

    } else if (0 == p_source.compare(CLEAR_REQUEST_ACCESS_BARRED_DESCRIPTION)) {
        destination = ClearRequestAccessBarred;

    } else if (0 == p_source.compare(CLEAR_REQUEST_NOT_OBTAINABLE_DESCRIPTION)) {
        destination = ClearRequestNotObtainable;

    } else if (0 == p_source.compare(CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION)) {
        destination = ClearRequestRemoteProcedureError;

    } else if (0 == p_source.compare(CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION)) {
        destination = ClearRequestLocalProcedureError;

    } else if (0 == p_source.compare(CLEAR_REQUEST_RPOA_OUT_OF_ORDER_DESCRIPTION)) {
        destination = ClearRequestRpoaOutOfOrder;

    } else if (0 == p_source.compare(CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_DESCRIPTION)) {
        destination = ClearRequestReverseChargingNotAccepted;

    } else if (0 == p_source.compare(CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION)) {
        destination = ClearRequestIncompatibleDestination;

    } else if (0 == p_source.compare(CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_DESCRIPTION)) {
        destination = ClearRequestFastSelectNotAccepted;

    } else if (0 == p_source.compare(CLEAR_REQUEST_SHIP_ABSENT_DESCRIPTION)) {
        destination = ClearRequestShipAbsent;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

int xot4cpp::x25::codes::CauseCodeEnumerations::toInt(const ClearRequestEnumeration& p_source) {

    int destination;

    switch (p_source) {
        case ClearRequestDteOriginated:
            destination = CLEAR_REQUEST_DTE_ORIGINATED_MASK;
            break;

        case ClearRequestNumberBusy:
            destination = CLEAR_REQUEST_NUMBER_BUSY_MASK;
            break;

        case ClearRequestInvalidFacilityRequest:
            destination = CLEAR_REQUEST_INVALID_FACILITY_REQUEST_MASK;
            break;

        case ClearRequestNetworkCongestion:
            destination = CLEAR_REQUEST_NETWORK_CONGESTION_MASK;
            break;

        case ClearRequestOutOfOrder:
            destination = CLEAR_REQUEST_OUT_OF_ORDER_MASK;
            break;

        case ClearRequestAccessBarred:
            destination = CLEAR_REQUEST_ACCESS_BARRED_MASK;
            break;

        case ClearRequestNotObtainable:
            destination = CLEAR_REQUEST_NOT_OBTAINABLE_MASK;
            break;

        case ClearRequestRemoteProcedureError:
            destination = CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_MASK;
            break;

        case ClearRequestLocalProcedureError:
            destination = CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_MASK;
            break;

        case ClearRequestRpoaOutOfOrder:
            destination = CLEAR_REQUEST_RPOA_OUT_OF_ORDER_MASK;
            break;

        case ClearRequestReverseChargingNotAccepted:
            destination = CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_MASK;
            break;

        case ClearRequestIncompatibleDestination:
            destination = CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_MASK;
            break;

        case ClearRequestFastSelectNotAccepted:
            destination = CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_MASK;
            break;

        case ClearRequestShipAbsent:
            destination = CLEAR_REQUEST_SHIP_ABSENT_MASK;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::codes::CauseCodeEnumerations::ClearRequestEnumeration xot4cpp::x25::codes::CauseCodeEnumerations::clearRequestFromInt(const int& p_source) {

    ClearRequestEnumeration destination;

    if (compareWithMask(CLEAR_REQUEST_DTE_ORIGINATED_MASK, p_source)) {
        destination = ClearRequestDteOriginated;

    } else if (compareWithMask(CLEAR_REQUEST_NUMBER_BUSY_MASK, p_source)) {
        destination = ClearRequestNumberBusy;

    } else if (compareWithMask(CLEAR_REQUEST_INVALID_FACILITY_REQUEST_MASK, p_source)) {
        destination = ClearRequestInvalidFacilityRequest;

    } else if (compareWithMask(CLEAR_REQUEST_NETWORK_CONGESTION_MASK, p_source)) {
        destination = ClearRequestNetworkCongestion;

    } else if (compareWithMask(CLEAR_REQUEST_OUT_OF_ORDER_MASK, p_source)) {
        destination = ClearRequestOutOfOrder;

    } else if (compareWithMask(CLEAR_REQUEST_ACCESS_BARRED_MASK, p_source)) {
        destination = ClearRequestAccessBarred;

    } else if (compareWithMask(CLEAR_REQUEST_NOT_OBTAINABLE_MASK, p_source)) {
        destination = ClearRequestNotObtainable;

    } else if (compareWithMask(CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_MASK, p_source)) {
        destination = ClearRequestRemoteProcedureError;

    } else if (compareWithMask(CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_MASK, p_source)) {
        destination = ClearRequestLocalProcedureError;

    } else if (compareWithMask(CLEAR_REQUEST_RPOA_OUT_OF_ORDER_MASK, p_source)) {
        destination = ClearRequestRpoaOutOfOrder;

    } else if (compareWithMask(CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_MASK, p_source)) {
        destination = ClearRequestReverseChargingNotAccepted;

    } else if (compareWithMask(CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_MASK, p_source)) {
        destination = ClearRequestIncompatibleDestination;

    } else if (compareWithMask(CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_MASK, p_source)) {
        destination = ClearRequestFastSelectNotAccepted;

    } else if (compareWithMask(CLEAR_REQUEST_SHIP_ABSENT_MASK, p_source)) {
        destination = ClearRequestShipAbsent;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

// ResetRequestEnumeration

std::string xot4cpp::x25::codes::CauseCodeEnumerations::toDescription(const ResetRequestEnumeration& p_source) {

    std::string destination;

    switch (p_source) {
        case ResetRequestDteOriginated:
            destination = RESET_REQUEST_DTE_ORIGINATED_DESCRIPTION;
            break;

        case ResetRequestOutOfOrder:
            destination = RESET_REQUEST_OUT_OF_ORDER_DESCRIPTION;
            break;

        case ResetRequestRemoteProcedureError:
            destination = RESET_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION;
            break;

        case ResetRequestLocalProcedureError:
            destination = RESET_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION;
            break;

        case ResetRequestNetworkCongestion:
            destination = RESET_REQUEST_NETWORK_CONGESTION_DESCRIPTION;
            break;

        case ResetRequestRemoteDteOperational:
            destination = RESET_REQUEST_REMOTE_DTE_OPERATIONAL_DESCRIPTION;
            break;

        case ResetRequestNetworkOperational:
            destination = RESET_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION;
            break;

        case ResetRequestIncompatibleDestination:
            destination = RESET_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION;
            break;

        case ResetRequestNetworkOutOfOrder:
            destination = RESET_REQUEST_NETWORK_OUT_OF_ORDER_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::codes::CauseCodeEnumerations::ResetRequestEnumeration xot4cpp::x25::codes::CauseCodeEnumerations::resetRequestFromDescription(const std::string& p_source) {

    ResetRequestEnumeration destination;

    if (0 == p_source.compare(RESET_REQUEST_DTE_ORIGINATED_DESCRIPTION)) {
        destination = ResetRequestDteOriginated;

    } else if (0 == p_source.compare(RESET_REQUEST_OUT_OF_ORDER_DESCRIPTION)) {
        destination = ResetRequestOutOfOrder;

    } else if (0 == p_source.compare(RESET_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION)) {
        destination = ResetRequestRemoteProcedureError;

    } else if (0 == p_source.compare(RESET_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION)) {
        destination = ResetRequestLocalProcedureError;

    } else if (0 == p_source.compare(RESET_REQUEST_NETWORK_CONGESTION_DESCRIPTION)) {
        destination = ResetRequestNetworkCongestion;

    } else if (0 == p_source.compare(RESET_REQUEST_REMOTE_DTE_OPERATIONAL_DESCRIPTION)) {
        destination = ResetRequestRemoteDteOperational;

    } else if (0 == p_source.compare(RESET_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION)) {
        destination = ResetRequestNetworkOperational;

    } else if (0 == p_source.compare(RESET_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION)) {
        destination = ResetRequestIncompatibleDestination;

    } else if (0 == p_source.compare(RESET_REQUEST_NETWORK_OUT_OF_ORDER_DESCRIPTION)) {
        destination = ResetRequestNetworkOutOfOrder;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

int xot4cpp::x25::codes::CauseCodeEnumerations::toInt(const ResetRequestEnumeration& p_source) {

    int destination;

    switch (p_source) {
        case ResetRequestDteOriginated:
            destination = RESET_REQUEST_DTE_ORIGINATED_MASK;
            break;

        case ResetRequestOutOfOrder:
            destination = RESET_REQUEST_OUT_OF_ORDER_MASK;
            break;

        case ResetRequestRemoteProcedureError:
            destination = RESET_REQUEST_REMOTE_PROCEDURE_ERROR_MASK;
            break;

        case ResetRequestLocalProcedureError:
            destination = RESET_REQUEST_LOCAL_PROCEDURE_ERROR_MASK;
            break;

        case ResetRequestNetworkCongestion:
            destination = RESET_REQUEST_NETWORK_CONGESTION_MASK;
            break;

        case ResetRequestRemoteDteOperational:
            destination = RESET_REQUEST_REMOTE_DTE_OPERATIONAL_MASK;
            break;

        case ResetRequestNetworkOperational:
            destination = RESET_REQUEST_NETWORK_OPERATIONAL_MASK;
            break;

        case ResetRequestIncompatibleDestination:
            destination = RESET_REQUEST_INCOMPATIBLE_DESTINATION_MASK;
            break;

        case ResetRequestNetworkOutOfOrder:
            destination = RESET_REQUEST_NETWORK_OUT_OF_ORDER_MASK;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;

    }

    return destination;
}

xot4cpp::x25::codes::CauseCodeEnumerations::ResetRequestEnumeration xot4cpp::x25::codes::CauseCodeEnumerations::resetRequestFromInt(const int& p_source) {

    ResetRequestEnumeration destination;

    if (compareWithMask(RESET_REQUEST_DTE_ORIGINATED_MASK, p_source)) {
        destination = ResetRequestDteOriginated;

    } else if (compareWithMask(RESET_REQUEST_OUT_OF_ORDER_MASK, p_source)) {
        destination = ResetRequestOutOfOrder;

    } else if (compareWithMask(RESET_REQUEST_REMOTE_PROCEDURE_ERROR_MASK, p_source)) {
        destination = ResetRequestRemoteProcedureError;

    } else if (compareWithMask(RESET_REQUEST_LOCAL_PROCEDURE_ERROR_MASK, p_source)) {
        destination = ResetRequestLocalProcedureError;

    } else if (compareWithMask(RESET_REQUEST_NETWORK_CONGESTION_MASK, p_source)) {
        destination = ResetRequestNetworkCongestion;

    } else if (compareWithMask(RESET_REQUEST_REMOTE_DTE_OPERATIONAL_MASK, p_source)) {
        destination = ResetRequestRemoteDteOperational;

    } else if (compareWithMask(RESET_REQUEST_NETWORK_OPERATIONAL_MASK, p_source)) {
        destination = ResetRequestNetworkOperational;

    } else if (compareWithMask(RESET_REQUEST_INCOMPATIBLE_DESTINATION_MASK, p_source)) {
        destination = ResetRequestIncompatibleDestination;

    } else if (compareWithMask(RESET_REQUEST_NETWORK_OUT_OF_ORDER_MASK, p_source)) {
        destination = ResetRequestNetworkOutOfOrder;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

// RestartRequestEnumeration

std::string xot4cpp::x25::codes::CauseCodeEnumerations::toDescription(const RestartRequestEnumeration& p_source) {

    std::string destination;

    switch (p_source) {
        case RestartRequestDteRestarting:
            destination = RESTART_REQUEST_DTE_RESTARTING_DESCRIPTION;
            break;

        case RestartRequestLocalProcedureError:
            destination = RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION;
            break;

        case RestartRequestNetworkCongestion:
            destination = RESTART_REQUEST_NETWORK_CONGESTION_DESCRIPTION;
            break;

        case RestartRequestNetworkOperational:
            destination = RESTART_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION;
            break;

        case RestartRequestRegistrationCancellationConfirmed:
            destination = RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::codes::CauseCodeEnumerations::RestartRequestEnumeration xot4cpp::x25::codes::CauseCodeEnumerations::restartRequestFromDescription(const std::string& p_source) {

    RestartRequestEnumeration destination;

    if (0 == p_source.compare(RESTART_REQUEST_DTE_RESTARTING_DESCRIPTION)) {
        destination = RestartRequestDteRestarting;

    } else if (0 == p_source.compare(RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION)) {
        destination = RestartRequestLocalProcedureError;

    } else if (0 == p_source.compare(RESTART_REQUEST_NETWORK_CONGESTION_DESCRIPTION)) {
        destination = RestartRequestNetworkCongestion;

    } else if (0 == p_source.compare(RESTART_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION)) {
        destination = RestartRequestNetworkOperational;

    } else if (0 == p_source.compare(RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_DESCRIPTION)) {
        destination = RestartRequestRegistrationCancellationConfirmed;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

int xot4cpp::x25::codes::CauseCodeEnumerations::toInt(const RestartRequestEnumeration& p_source) {

    int destination;

    switch (p_source) {
        case RestartRequestDteRestarting:
            destination = RESTART_REQUEST_DTE_RESTARTING_MASK;
            break;

        case RestartRequestLocalProcedureError:
            destination = RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_MASK;
            break;

        case RestartRequestNetworkCongestion:
            destination = RESTART_REQUEST_NETWORK_CONGESTION_MASK;
            break;

        case RestartRequestNetworkOperational:
            destination = RESTART_REQUEST_NETWORK_OPERATIONAL_MASK;
            break;

        case RestartRequestRegistrationCancellationConfirmed:
            destination = RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_MASK;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;

    }

    return destination;
}

xot4cpp::x25::codes::CauseCodeEnumerations::RestartRequestEnumeration xot4cpp::x25::codes::CauseCodeEnumerations::restartRequestFromInt(const int& p_source) {

    RestartRequestEnumeration destination;

    if (compareWithMask(RESTART_REQUEST_DTE_RESTARTING_MASK, p_source)) {
        destination = RestartRequestDteRestarting;

    } else if (compareWithMask(RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_MASK, p_source)) {
        destination = RestartRequestLocalProcedureError;

    } else if (compareWithMask(RESTART_REQUEST_NETWORK_CONGESTION_MASK, p_source)) {
        destination = RestartRequestNetworkCongestion;

    } else if (compareWithMask(RESTART_REQUEST_NETWORK_OPERATIONAL_MASK, p_source)) {
        destination = RestartRequestNetworkOperational;

    } else if (compareWithMask(RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_MASK, p_source)) {
        destination = RestartRequestRegistrationCancellationConfirmed;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

bool xot4cpp::x25::codes::CauseCodeEnumerations::compareWithMask(const int& p_mask, const int& p_value) {

    return (p_mask == p_value);
}
