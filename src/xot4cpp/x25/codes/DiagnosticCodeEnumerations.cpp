#include "xot4cpp/x25/codes/DiagnosticCodeEnumerations.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

// DESCRIPTION
#define NO_ADDITIONAL_INFORMATION_DESCRIPTION "No_additional_information"
#define INVALID_PACKET_SEND_SEQUENCE_NUMBER_DESCRIPTION "Invalid_P(S)_(Packet_Send_sequence_number)"
#define INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_DESCRIPTION "Invalid_P(R)_(Packet_Receive_sequence_number)"
#define PACKET_TYPE_INVALID_DESCRIPTION "Packet_type_invalid"
#define PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_DESCRIPTION "Packet_type_invalid_for_state_R1_(Packet_level_ready)"
#define PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_DESCRIPTION "Packet_type_invalid_for_state_R2_(DTE_restart_request)"
#define PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_DESCRIPTION "Packet_type_invalid_for_state_R3_(DCE_restart_indication)"
#define PACKET_TYPE_INVALID_FOR_STATE_P1_READY_DESCRIPTION "Packet_type_invalid_for_state_P1_(Ready)"
#define PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_DESCRIPTION "Packet_type_invalid_for_state_P2_(DTE_Waiting)"
#define PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_DESCRIPTION "Packet_type_invalid_for_state_P3_(DCE_Waiting)"
#define PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_DESCRIPTION "Packet_type_invalid_for_state_P4_(Data_Transfer)"
#define PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_DESCRIPTION "Packet_type_invalid_for_state_P5_(Call_Collision)"
#define PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_DESCRIPTION "Packet_type_invalid_for_state_P6_(DTE_clear_request)"
#define PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_DESCRIPTION "Packet_type_invalid_for_state_P7_(DCE_clear_indication)"
#define PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_DESCRIPTION "Packet_type_invalid_for_state_D1_(Flow_control_ready)"
#define PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_DESCRIPTION "Packet_type_invalid_for_state_D2_(DTE_reset_ready)"
#define PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_DESCRIPTION "Packet_type_invalid_for_state_D3_(DCE_reset_indication)"
#define PACKET_NOT_ALLOWED_DESCRIPTION "Packet_not_allowed"
#define UNIDENTIFIABLE_PACKET_DESCRIPTION "Unidentifiable_packet"
#define CALL_ON_ONE_WAY_LOGICAL_CHANNEL_DESCRIPTION "Call_on_one-way_logical_channel"
#define INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_DESCRIPTION "Invalid_packet_type_on_a_permanent_virtual_circuit"
#define PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_DESCRIPTION "Packet_on_unassigned_LCN_(Logical_Channel_Number)"
#define REJECT_NOT_SUBSCRIBED_TO_DESCRIPTION "Reject_not_subscribed_to"
#define PACKET_TOO_SHORT_DESCRIPTION "Packet_too_short"
#define PACKET_TOO_LONG_DESCRIPTION "Packet_too_long"
#define INVALID_GENERAL_FORMAT_IDENTIFIER_DESCRIPTION "Invalid_GFI_(General_Format_Identifier)"
#define RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_DESCRIPTION "Restart_or_registration_packet_with_nonzero_LCI_(bits_1-4_of_octet_1,_or_bits_1-8_of_octet_2)"
#define PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_DESCRIPTION "Packet_type_not_compatible_with_facility"
#define UNAUTHORIZED_INTERRUPT_CONFIRMATION_DESCRIPTION "Unauthorized_interrupt_confirmation"
#define UNAUTHORIZED_INTERRUPT_DESCRIPTION "Unauthorized_interrupt"
#define UNAUTHORIZED_REJECT_DESCRIPTION "Unauthorized_reject"
#define TIMER_EXPIRED_DESCRIPTION "Timer_expired"
#define TIMER_EXPIRED_FOR_INCOMING_CALL_DESCRIPTION "Timer_expired_for_incoming_call"
#define TIMER_EXPIRED_FOR_CLEAR_INDICATION_DESCRIPTION "Timer_expired_for_clear_indication"
#define TIMER_EXPIRED_FOR_RESET_INDICATION_DESCRIPTION "Timer_expired_for_reset_indication"
#define TIMER_EXPIRED_FOR_RESTART_INDICATION_DESCRIPTION "Timer_expired_for_restart_indication"
#define TIMER_EXPIRED_FOR_CALL_DEFLECTION_DESCRIPTION "Timer_expired_for_call_deflection"
#define CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_DESCRIPTION "Call_setup,_clearing,_or_registration_problem"
#define FACILITY_CODE_NOT_ALLOWED_DESCRIPTION "Facility_code_not_allowed"
#define FACILITY_PARAMETER_NOT_ALLOWED_DESCRIPTION "Facility_parameter_not_allowed"
#define INVALID_CALLED_ADDRESS_DESCRIPTION "Invalid_called_address"
#define INVALID_CALLING_ADDRESS_DESCRIPTION "Invalid_calling_address"
#define INVALID_FACILITY_LENGTH_DESCRIPTION "Invalid_facility_length"
#define INCOMING_CALL_BARRED_DESCRIPTION "Incoming_call_barred"
#define NO_LOGICAL_CHANNEL_AVAILABLE_DESCRIPTION "No_logical_channel_available"
#define CALL_COLLISION_DESCRIPTION "Call_collision"
#define DUPLICATE_FACILITY_REQUESTED_DESCRIPTION "Duplicate_facility_requested"
#define NONZERO_ADDRESS_LENGTH_DESCRIPTION "Nonzero_address_length"
#define NONZERO_FACILITY_LENGTH_DESCRIPTION "Nonzero_facility_length"
#define FACILITY_NOT_PROVIDED_WHEN_EXPECTED_DESCRIPTION "Facility_not_provided_when_expected"
#define INVALID_ITU_T_SPECIFIED_DTE_FACILITY_DESCRIPTION "Invalid_ITU-T-specified_DTE_facility"
#define MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_DESCRIPTION "Maximum_number_of_call_redirections_or_deflections_exceeded"
#define MISCELLANEOUS_DESCRIPTION "Miscellaneous"
#define IMPROPER_CAUSE_CODE_FOR_DTE_DESCRIPTION "Improper_cause_code_for_DTE"
#define OCTET_NOT_ALIGNED_DESCRIPTION "Octet_not_aligned"
#define INCONSISTENT_Q_BIT_SETTING_DESCRIPTION "Inconsistent_Q_bit_setting"
#define NETWORK_USER_IDENTIFICATION_PROBLEM_DESCRIPTION "NUI_(Network_User_Identification)_problem"
#define INTERNATIONAL_PROBLEM_DESCRIPTION "International_problem"
#define REMOTE_NETWORK_PROBLEM_DESCRIPTION "Remote_network_problem"
#define INTERNATIONAL_PROTOCOL_PROBLEM_DESCRIPTION "International_protocol_problem"
#define INTERNATIONAL_LINK_OUT_OF_ORDER_DESCRIPTION "International_link_out_of_order"
#define INTERNATIONAL_LINK_BUSY_DESCRIPTION "International_link_busy"
#define TRANSIT_NETWORK_FACILITY_PROBLEM_DESCRIPTION "Transit_network_facility_problem"
#define REMOTE_NETWORK_FACILITY_PROBLEM_DESCRIPTION "Remote_network_facility_problem"
#define INTERNATIONAL_ROUTING_PROBLEM_DESCRIPTION "International_routing_problem"
#define TEMPORARY_ROUTING_PROBLEM_DESCRIPTION "Temporary_routing_problem"
#define UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_DESCRIPTION "Unknown_called_DNIC_(Data_Network_Identification_Code)"
#define MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_DESCRIPTION "Maintenance_action_(clear_x25_vc_command_issued)"

#define NORMAL_TERMINATION_DESCRIPTION "Normal_termination"
#define OUT_OF_RESOURCES_DESCRIPTION "Out_of_resources"
#define AUTHENTICATION_FAILURE_DESCRIPTION "Authentication_failure"
#define INBOUND_USER_DATA_TOO_LARGE_DESCRIPTION "Inbound_user_data_too_large"
#define IDLE_TIMER_EXPIRED_DESCRIPTION "Idle_timer_expired"

// MASK
#define NO_ADDITIONAL_INFORMATION_MASK 0x00
#define INVALID_PACKET_SEND_SEQUENCE_NUMBER_MASK 0x01
#define INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_MASK 0x02
#define PACKET_TYPE_INVALID_MASK 0x10
#define PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_MASK 0x11
#define PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_MASK 0x12
#define PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_MASK 0x13
#define PACKET_TYPE_INVALID_FOR_STATE_P1_READY_MASK 0x14
#define PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_MASK 0x15
#define PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_MASK 0x16
#define PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_MASK 0x17
#define PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_MASK 0x18
#define PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_MASK 0x19
#define PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_MASK 0x1a
#define PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_MASK 0x1b
#define PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_MASK 0x1c
#define PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_MASK 0x1d
#define PACKET_NOT_ALLOWED_MASK 0x20
#define UNIDENTIFIABLE_PACKET_MASK 0x21
#define CALL_ON_ONE_WAY_LOGICAL_CHANNEL_MASK 0x22
#define INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_MASK 0x23
#define PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_MASK 0x24
#define REJECT_NOT_SUBSCRIBED_TO_MASK 0x25
#define PACKET_TOO_SHORT_MASK 0x26
#define PACKET_TOO_LONG_MASK 0x27
#define INVALID_GENERAL_FORMAT_IDENTIFIER_MASK 0x28
#define RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_MASK 0x29
#define PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_MASK 0x2a
#define UNAUTHORIZED_INTERRUPT_CONFIRMATION_MASK 0x2b
#define UNAUTHORIZED_INTERRUPT_MASK 0x2c
#define UNAUTHORIZED_REJECT_MASK 0x2d
#define TIMER_EXPIRED_MASK 0x30
#define TIMER_EXPIRED_FOR_INCOMING_CALL_MASK 0x31
#define TIMER_EXPIRED_FOR_CLEAR_INDICATION_MASK 0x32
#define TIMER_EXPIRED_FOR_RESET_INDICATION_MASK 0x33
#define TIMER_EXPIRED_FOR_RESTART_INDICATION_MASK 0x34
#define TIMER_EXPIRED_FOR_CALL_DEFLECTION_MASK 0x35
#define CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_MASK 0x40
#define FACILITY_CODE_NOT_ALLOWED_MASK 0x41
#define FACILITY_PARAMETER_NOT_ALLOWED_MASK 0x42
#define INVALID_CALLED_ADDRESS_MASK 0x43
#define INVALID_CALLING_ADDRESS_MASK 0x44
#define INVALID_FACILITY_LENGTH_MASK 0x45
#define INCOMING_CALL_BARRED_MASK 0x46
#define NO_LOGICAL_CHANNEL_AVAILABLE_MASK 0x47
#define CALL_COLLISION_MASK 0x48
#define DUPLICATE_FACILITY_REQUESTED_MASK 0x49
#define NONZERO_ADDRESS_LENGTH_MASK 0x4a
#define NONZERO_FACILITY_LENGTH_MASK 0x4b
#define FACILITY_NOT_PROVIDED_WHEN_EXPECTED_MASK 0x4c
#define INVALID_ITU_T_SPECIFIED_DTE_FACILITY_MASK 0x4d
#define MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_MASK 0x4e
#define MISCELLANEOUS_MASK 0x50
#define IMPROPER_CAUSE_CODE_FOR_DTE_MASK 0x51
#define OCTET_NOT_ALIGNED_MASK 0x52
#define INCONSISTENT_Q_BIT_SETTING_MASK 0x53
#define NETWORK_USER_IDENTIFICATION_PROBLEM_MASK 0x54
#define INTERNATIONAL_PROBLEM_MASK 0x70
#define REMOTE_NETWORK_PROBLEM_MASK 0x71
#define INTERNATIONAL_PROTOCOL_PROBLEM_MASK 0x72
#define INTERNATIONAL_LINK_OUT_OF_ORDER_MASK 0x73
#define INTERNATIONAL_LINK_BUSY_MASK 0x74
#define TRANSIT_NETWORK_FACILITY_PROBLEM_MASK 0x75
#define REMOTE_NETWORK_FACILITY_PROBLEM_MASK 0x76
#define INTERNATIONAL_ROUTING_PROBLEM_MASK 0x77
#define TEMPORARY_ROUTING_PROBLEM_MASK 0x78
#define UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_MASK 0x79
#define MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_MASK 0x7a

#define NORMAL_TERMINATION_MASK 0xf0
#define OUT_OF_RESOURCES_MASK 0xf1
#define AUTHENTICATION_FAILURE_MASK 0xf2
#define INBOUND_USER_DATA_TOO_LARGE_MASK 0xf3
#define IDLE_TIMER_EXPIRED_MASK 0xf4

namespace xot4cpp {

    namespace x25 {

        namespace codes {

            namespace DiagnosticCodeEnumerations {

                extern bool compareWithMask(const int& p_mask, const int& p_value);
            };
        };
    };
};

std::string xot4cpp::x25::codes::DiagnosticCodeEnumerations::toDescription(const DiagnosticCodeEnumeration& p_source) {

    std::string destination;

    switch (p_source) {
        case NoAdditionalInformation:
            destination = NO_ADDITIONAL_INFORMATION_DESCRIPTION;
            break;

        case InvalidPacketSendSequenceNumber:
            destination = INVALID_PACKET_SEND_SEQUENCE_NUMBER_DESCRIPTION;
            break;

        case InvalidPacketReceiveSequenceNumber:
            destination = INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_DESCRIPTION;
            break;

        case PacketTypeInvalid:
            destination = PACKET_TYPE_INVALID_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateR1PacketLevelReady:
            destination = PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateR2DteRestartRequest:
            destination = PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateR3DceRestartIndication:
            destination = PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateP1Ready:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P1_READY_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateP2DteWaiting:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateP3DceWaiting:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateP4DataTransfer:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateP5CallCollision:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateP6DteClearRequest:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateP7DceClearIndication:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateD1FlowControlReady:
            destination = PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateD2DteResetReady:
            destination = PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_DESCRIPTION;
            break;

        case PacketTypeInvalidForStateD3DceResetIndication:
            destination = PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_DESCRIPTION;
            break;

        case PacketNotAllowed:
            destination = PACKET_NOT_ALLOWED_DESCRIPTION;
            break;

        case UnidentifiablePacket:
            destination = UNIDENTIFIABLE_PACKET_DESCRIPTION;
            break;

        case CallOnOneWayLogicalChannel:
            destination = CALL_ON_ONE_WAY_LOGICAL_CHANNEL_DESCRIPTION;
            break;

        case InvalidPacketTypeOnAPermanentVirtualCircuit:
            destination = INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_DESCRIPTION;
            break;

        case PacketOnUnassignedLogicalChannelNumber:
            destination = PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_DESCRIPTION;
            break;

        case RejectNotSubscribedTo:
            destination = REJECT_NOT_SUBSCRIBED_TO_DESCRIPTION;
            break;

        case PacketTooShort:
            destination = PACKET_TOO_SHORT_DESCRIPTION;
            break;

        case PacketTooLong:
            destination = PACKET_TOO_LONG_DESCRIPTION;
            break;

        case InvalidGeneralFormatIdentifier:
            destination = INVALID_GENERAL_FORMAT_IDENTIFIER_DESCRIPTION;
            break;

        case RestartOrRegistrationPacketWithNonzeroLci:
            destination = RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_DESCRIPTION;
            break;

        case PacketTypeNotCompatibleWithFacility:
            destination = PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_DESCRIPTION;
            break;

        case UnauthorizedInterruptConfirmation:
            destination = UNAUTHORIZED_INTERRUPT_CONFIRMATION_DESCRIPTION;
            break;

        case UnauthorizedInterrupt:
            destination = UNAUTHORIZED_INTERRUPT_DESCRIPTION;
            break;

        case UnauthorizedReject:
            destination = UNAUTHORIZED_REJECT_DESCRIPTION;
            break;

        case TimerExpired:
            destination = TIMER_EXPIRED_DESCRIPTION;
            break;

        case TimerExpiredForIncomingCall:
            destination = TIMER_EXPIRED_FOR_INCOMING_CALL_DESCRIPTION;
            break;

        case TimerExpiredForClearIndication:
            destination = TIMER_EXPIRED_FOR_CLEAR_INDICATION_DESCRIPTION;
            break;

        case TimerExpiredForResetIndication:
            destination = TIMER_EXPIRED_FOR_RESET_INDICATION_DESCRIPTION;
            break;

        case TimerExpiredForRestartIndication:
            destination = TIMER_EXPIRED_FOR_RESTART_INDICATION_DESCRIPTION;
            break;

        case TimerExpiredForCallDeflection:
            destination = TIMER_EXPIRED_FOR_CALL_DEFLECTION_DESCRIPTION;
            break;

        case CallSetupClearingOrRegistrationProblem:
            destination = CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_DESCRIPTION;
            break;

        case FacilityCodeNotAllowed:
            destination = FACILITY_CODE_NOT_ALLOWED_DESCRIPTION;
            break;

        case FacilityParameterNotAllowed:
            destination = FACILITY_PARAMETER_NOT_ALLOWED_DESCRIPTION;
            break;

        case InvalidCalledAddress:
            destination = INVALID_CALLED_ADDRESS_DESCRIPTION;
            break;

        case InvalidCallingAddress:
            destination = INVALID_CALLING_ADDRESS_DESCRIPTION;
            break;

        case InvalidFacilityLength:
            destination = INVALID_FACILITY_LENGTH_DESCRIPTION;
            break;

        case IncomingCallBarred:
            destination = INCOMING_CALL_BARRED_DESCRIPTION;
            break;

        case NoLogicalChannelAvailable:
            destination = NO_LOGICAL_CHANNEL_AVAILABLE_DESCRIPTION;
            break;

        case CallCollision:
            destination = CALL_COLLISION_DESCRIPTION;
            break;

        case DuplicateFacilityRequested:
            destination = DUPLICATE_FACILITY_REQUESTED_DESCRIPTION;
            break;

        case NonzeroAddressLength:
            destination = NONZERO_ADDRESS_LENGTH_DESCRIPTION;
            break;

        case NonzeroFacilityLength:
            destination = NONZERO_FACILITY_LENGTH_DESCRIPTION;
            break;

        case FacilityNotProvidedWhenExpected:
            destination = FACILITY_NOT_PROVIDED_WHEN_EXPECTED_DESCRIPTION;
            break;

        case InvalidItuTSpecifiedDteFacility:
            destination = INVALID_ITU_T_SPECIFIED_DTE_FACILITY_DESCRIPTION;
            break;

        case MaximumNumberOfCallRedirectionsOrDeflectionsExceeded:
            destination = MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_DESCRIPTION;
            break;

        case Miscellaneous:
            destination = MISCELLANEOUS_DESCRIPTION;
            break;

        case ImproperCauseCodeForDte:
            destination = IMPROPER_CAUSE_CODE_FOR_DTE_DESCRIPTION;
            break;

        case OctetNotAaligned:
            destination = OCTET_NOT_ALIGNED_DESCRIPTION;
            break;

        case InconsistentQBitSetting:
            destination = INCONSISTENT_Q_BIT_SETTING_DESCRIPTION;
            break;

        case NetworkUserIdentificationProblem:
            destination = NETWORK_USER_IDENTIFICATION_PROBLEM_DESCRIPTION;
            break;

        case InternationalProblem:
            destination = INTERNATIONAL_PROBLEM_DESCRIPTION;
            break;

        case RemoteNetworkProblem:
            destination = REMOTE_NETWORK_PROBLEM_DESCRIPTION;
            break;

        case InternationalProtocolProblem:
            destination = INTERNATIONAL_PROTOCOL_PROBLEM_DESCRIPTION;
            break;

        case InternationalLinkOutOfOrder:
            destination = INTERNATIONAL_LINK_OUT_OF_ORDER_DESCRIPTION;
            break;

        case InternationalLinkBusy:
            destination = INTERNATIONAL_LINK_BUSY_DESCRIPTION;
            break;

        case TransitNetworkFacilityProblem:
            destination = TRANSIT_NETWORK_FACILITY_PROBLEM_DESCRIPTION;
            break;

        case RemoteNetworkFacilityProblem:
            destination = REMOTE_NETWORK_FACILITY_PROBLEM_DESCRIPTION;
            break;

        case InternationalRoutingProblem:
            destination = INTERNATIONAL_ROUTING_PROBLEM_DESCRIPTION;
            break;

        case TemporaryRoutingProblem:
            destination = TEMPORARY_ROUTING_PROBLEM_DESCRIPTION;
            break;

        case UnknownCalledDataNetworkIdentificationCode:
            destination = UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_DESCRIPTION;
            break;

        case MaintenanceActionClearX25VcCommandIssued:
            destination = MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_DESCRIPTION;
            break;

        case NormalTermination:
            destination = NORMAL_TERMINATION_DESCRIPTION;
            break;

        case OutOfResources:
            destination = OUT_OF_RESOURCES_DESCRIPTION;
            break;

        case AuthenticationFailure:
            destination = AUTHENTICATION_FAILURE_DESCRIPTION;
            break;

        case InboundUserDataTooLarge:
            destination = INBOUND_USER_DATA_TOO_LARGE_DESCRIPTION;
            break;

        case IdleTimerExpired:
            destination = IDLE_TIMER_EXPIRED_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration xot4cpp::x25::codes::DiagnosticCodeEnumerations::fromDescription(const std::string& p_source) {

    DiagnosticCodeEnumeration destination;

    if (0 == p_source.compare(NO_ADDITIONAL_INFORMATION_DESCRIPTION)) {
        destination = NoAdditionalInformation;

    } else if (0 == p_source.compare(INVALID_PACKET_SEND_SEQUENCE_NUMBER_DESCRIPTION)) {
        destination = InvalidPacketSendSequenceNumber;

    } else if (0 == p_source.compare(INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_DESCRIPTION)) {
        destination = InvalidPacketReceiveSequenceNumber;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_DESCRIPTION)) {
        destination = PacketTypeInvalid;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateR1PacketLevelReady;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateR2DteRestartRequest;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateR3DceRestartIndication;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_P1_READY_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateP1Ready;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateP2DteWaiting;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateP3DceWaiting;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateP4DataTransfer;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateP5CallCollision;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateP6DteClearRequest;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateP7DceClearIndication;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateD1FlowControlReady;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateD2DteResetReady;

    } else if (0 == p_source.compare(PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_DESCRIPTION)) {
        destination = PacketTypeInvalidForStateD3DceResetIndication;

    } else if (0 == p_source.compare(PACKET_NOT_ALLOWED_DESCRIPTION)) {
        destination = PacketNotAllowed;

    } else if (0 == p_source.compare(UNIDENTIFIABLE_PACKET_DESCRIPTION)) {
        destination = UnidentifiablePacket;

    } else if (0 == p_source.compare(CALL_ON_ONE_WAY_LOGICAL_CHANNEL_DESCRIPTION)) {
        destination = CallOnOneWayLogicalChannel;

    } else if (0 == p_source.compare(INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_DESCRIPTION)) {
        destination = InvalidPacketTypeOnAPermanentVirtualCircuit;

    } else if (0 == p_source.compare(PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_DESCRIPTION)) {
        destination = PacketOnUnassignedLogicalChannelNumber;

    } else if (0 == p_source.compare(REJECT_NOT_SUBSCRIBED_TO_DESCRIPTION)) {
        destination = RejectNotSubscribedTo;

    } else if (0 == p_source.compare(PACKET_TOO_SHORT_DESCRIPTION)) {
        destination = PacketTooShort;

    } else if (0 == p_source.compare(PACKET_TOO_LONG_DESCRIPTION)) {
        destination = PacketTooLong;

    } else if (0 == p_source.compare(INVALID_GENERAL_FORMAT_IDENTIFIER_DESCRIPTION)) {
        destination = InvalidGeneralFormatIdentifier;

    } else if (0 == p_source.compare(RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_DESCRIPTION)) {
        destination = RestartOrRegistrationPacketWithNonzeroLci;

    } else if (0 == p_source.compare(PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_DESCRIPTION)) {
        destination = PacketTypeNotCompatibleWithFacility;

    } else if (0 == p_source.compare(UNAUTHORIZED_INTERRUPT_CONFIRMATION_DESCRIPTION)) {
        destination = UnauthorizedInterruptConfirmation;

    } else if (0 == p_source.compare(UNAUTHORIZED_INTERRUPT_DESCRIPTION)) {
        destination = UnauthorizedInterrupt;

    } else if (0 == p_source.compare(UNAUTHORIZED_REJECT_DESCRIPTION)) {
        destination = UnauthorizedReject;

    } else if (0 == p_source.compare(TIMER_EXPIRED_DESCRIPTION)) {
        destination = TimerExpired;

    } else if (0 == p_source.compare(TIMER_EXPIRED_FOR_INCOMING_CALL_DESCRIPTION)) {
        destination = TimerExpiredForIncomingCall;

    } else if (0 == p_source.compare(TIMER_EXPIRED_FOR_CLEAR_INDICATION_DESCRIPTION)) {
        destination = TimerExpiredForClearIndication;

    } else if (0 == p_source.compare(TIMER_EXPIRED_FOR_RESET_INDICATION_DESCRIPTION)) {
        destination = TimerExpiredForResetIndication;

    } else if (0 == p_source.compare(TIMER_EXPIRED_FOR_RESTART_INDICATION_DESCRIPTION)) {
        destination = TimerExpiredForRestartIndication;

    } else if (0 == p_source.compare(TIMER_EXPIRED_FOR_CALL_DEFLECTION_DESCRIPTION)) {
        destination = TimerExpiredForCallDeflection;

    } else if (0 == p_source.compare(CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_DESCRIPTION)) {
        destination = CallSetupClearingOrRegistrationProblem;

    } else if (0 == p_source.compare(FACILITY_CODE_NOT_ALLOWED_DESCRIPTION)) {
        destination = FacilityCodeNotAllowed;

    } else if (0 == p_source.compare(FACILITY_PARAMETER_NOT_ALLOWED_DESCRIPTION)) {
        destination = FacilityParameterNotAllowed;

    } else if (0 == p_source.compare(INVALID_CALLED_ADDRESS_DESCRIPTION)) {
        destination = InvalidCalledAddress;

    } else if (0 == p_source.compare(INVALID_CALLING_ADDRESS_DESCRIPTION)) {
        destination = InvalidCallingAddress;

    } else if (0 == p_source.compare(INVALID_FACILITY_LENGTH_DESCRIPTION)) {
        destination = InvalidFacilityLength;

    } else if (0 == p_source.compare(INCOMING_CALL_BARRED_DESCRIPTION)) {
        destination = IncomingCallBarred;

    } else if (0 == p_source.compare(NO_LOGICAL_CHANNEL_AVAILABLE_DESCRIPTION)) {
        destination = NoLogicalChannelAvailable;

    } else if (0 == p_source.compare(CALL_COLLISION_DESCRIPTION)) {
        destination = CallCollision;

    } else if (0 == p_source.compare(DUPLICATE_FACILITY_REQUESTED_DESCRIPTION)) {
        destination = DuplicateFacilityRequested;

    } else if (0 == p_source.compare(NONZERO_ADDRESS_LENGTH_DESCRIPTION)) {
        destination = NonzeroAddressLength;

    } else if (0 == p_source.compare(NONZERO_FACILITY_LENGTH_DESCRIPTION)) {
        destination = NonzeroFacilityLength;

    } else if (0 == p_source.compare(FACILITY_NOT_PROVIDED_WHEN_EXPECTED_DESCRIPTION)) {
        destination = FacilityNotProvidedWhenExpected;

    } else if (0 == p_source.compare(INVALID_ITU_T_SPECIFIED_DTE_FACILITY_DESCRIPTION)) {
        destination = InvalidItuTSpecifiedDteFacility;

    } else if (0 == p_source.compare(MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_DESCRIPTION)) {
        destination = MaximumNumberOfCallRedirectionsOrDeflectionsExceeded;

    } else if (0 == p_source.compare(MISCELLANEOUS_DESCRIPTION)) {
        destination = Miscellaneous;

    } else if (0 == p_source.compare(IMPROPER_CAUSE_CODE_FOR_DTE_DESCRIPTION)) {
        destination = ImproperCauseCodeForDte;

    } else if (0 == p_source.compare(OCTET_NOT_ALIGNED_DESCRIPTION)) {
        destination = OctetNotAaligned;

    } else if (0 == p_source.compare(INCONSISTENT_Q_BIT_SETTING_DESCRIPTION)) {
        destination = InconsistentQBitSetting;

    } else if (0 == p_source.compare(NETWORK_USER_IDENTIFICATION_PROBLEM_DESCRIPTION)) {
        destination = NetworkUserIdentificationProblem;

    } else if (0 == p_source.compare(INTERNATIONAL_PROBLEM_DESCRIPTION)) {
        destination = InternationalProblem;

    } else if (0 == p_source.compare(REMOTE_NETWORK_PROBLEM_DESCRIPTION)) {
        destination = RemoteNetworkProblem;

    } else if (0 == p_source.compare(INTERNATIONAL_PROTOCOL_PROBLEM_DESCRIPTION)) {
        destination = InternationalProtocolProblem;

    } else if (0 == p_source.compare(INTERNATIONAL_LINK_OUT_OF_ORDER_DESCRIPTION)) {
        destination = InternationalLinkOutOfOrder;

    } else if (0 == p_source.compare(INTERNATIONAL_LINK_BUSY_DESCRIPTION)) {
        destination = InternationalLinkBusy;

    } else if (0 == p_source.compare(TRANSIT_NETWORK_FACILITY_PROBLEM_DESCRIPTION)) {
        destination = TransitNetworkFacilityProblem;

    } else if (0 == p_source.compare(REMOTE_NETWORK_FACILITY_PROBLEM_DESCRIPTION)) {
        destination = RemoteNetworkFacilityProblem;

    } else if (0 == p_source.compare(INTERNATIONAL_ROUTING_PROBLEM_DESCRIPTION)) {
        destination = InternationalRoutingProblem;

    } else if (0 == p_source.compare(TEMPORARY_ROUTING_PROBLEM_DESCRIPTION)) {
        destination = TemporaryRoutingProblem;

    } else if (0 == p_source.compare(UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_DESCRIPTION)) {
        destination = UnknownCalledDataNetworkIdentificationCode;

    } else if (0 == p_source.compare(MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_DESCRIPTION)) {
        destination = MaintenanceActionClearX25VcCommandIssued;

    } else if (0 == p_source.compare(NORMAL_TERMINATION_DESCRIPTION)) {
        destination = NormalTermination;

    } else if (0 == p_source.compare(OUT_OF_RESOURCES_DESCRIPTION)) {
        destination = OutOfResources;

    } else if (0 == p_source.compare(AUTHENTICATION_FAILURE_DESCRIPTION)) {
        destination = AuthenticationFailure;

    } else if (0 == p_source.compare(INBOUND_USER_DATA_TOO_LARGE_DESCRIPTION)) {
        destination = InboundUserDataTooLarge;

    } else if (0 == p_source.compare(IDLE_TIMER_EXPIRED_DESCRIPTION)) {
        destination = IdleTimerExpired;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

int xot4cpp::x25::codes::DiagnosticCodeEnumerations::toInt(const DiagnosticCodeEnumeration& p_source) {

    int destination;

    switch (p_source) {
        case NoAdditionalInformation:
            destination = NO_ADDITIONAL_INFORMATION_MASK;
            break;

        case InvalidPacketSendSequenceNumber:
            destination = INVALID_PACKET_SEND_SEQUENCE_NUMBER_MASK;
            break;

        case InvalidPacketReceiveSequenceNumber:
            destination = INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_MASK;
            break;

        case PacketTypeInvalid:
            destination = PACKET_TYPE_INVALID_MASK;
            break;

        case PacketTypeInvalidForStateR1PacketLevelReady:
            destination = PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_MASK;
            break;

        case PacketTypeInvalidForStateR2DteRestartRequest:
            destination = PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_MASK;
            break;

        case PacketTypeInvalidForStateR3DceRestartIndication:
            destination = PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_MASK;
            break;

        case PacketTypeInvalidForStateP1Ready:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P1_READY_MASK;
            break;

        case PacketTypeInvalidForStateP2DteWaiting:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_MASK;
            break;

        case PacketTypeInvalidForStateP3DceWaiting:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_MASK;
            break;

        case PacketTypeInvalidForStateP4DataTransfer:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_MASK;
            break;

        case PacketTypeInvalidForStateP5CallCollision:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_MASK;
            break;

        case PacketTypeInvalidForStateP6DteClearRequest:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_MASK;
            break;

        case PacketTypeInvalidForStateP7DceClearIndication:
            destination = PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_MASK;
            break;

        case PacketTypeInvalidForStateD1FlowControlReady:
            destination = PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_MASK;
            break;

        case PacketTypeInvalidForStateD2DteResetReady:
            destination = PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_MASK;
            break;

        case PacketTypeInvalidForStateD3DceResetIndication:
            destination = PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_MASK;
            break;

        case PacketNotAllowed:
            destination = PACKET_NOT_ALLOWED_MASK;
            break;

        case UnidentifiablePacket:
            destination = UNIDENTIFIABLE_PACKET_MASK;
            break;

        case CallOnOneWayLogicalChannel:
            destination = CALL_ON_ONE_WAY_LOGICAL_CHANNEL_MASK;
            break;

        case InvalidPacketTypeOnAPermanentVirtualCircuit:
            destination = INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_MASK;
            break;

        case PacketOnUnassignedLogicalChannelNumber:
            destination = PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_MASK;
            break;

        case RejectNotSubscribedTo:
            destination = REJECT_NOT_SUBSCRIBED_TO_MASK;
            break;

        case PacketTooShort:
            destination = PACKET_TOO_SHORT_MASK;
            break;

        case PacketTooLong:
            destination = PACKET_TOO_LONG_MASK;
            break;

        case InvalidGeneralFormatIdentifier:
            destination = INVALID_GENERAL_FORMAT_IDENTIFIER_MASK;
            break;

        case RestartOrRegistrationPacketWithNonzeroLci:
            destination = RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_MASK;
            break;

        case PacketTypeNotCompatibleWithFacility:
            destination = PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_MASK;
            break;

        case UnauthorizedInterruptConfirmation:
            destination = UNAUTHORIZED_INTERRUPT_CONFIRMATION_MASK;
            break;

        case UnauthorizedInterrupt:
            destination = UNAUTHORIZED_INTERRUPT_MASK;
            break;

        case UnauthorizedReject:
            destination = UNAUTHORIZED_REJECT_MASK;
            break;

        case TimerExpired:
            destination = TIMER_EXPIRED_MASK;
            break;

        case TimerExpiredForIncomingCall:
            destination = TIMER_EXPIRED_FOR_INCOMING_CALL_MASK;
            break;

        case TimerExpiredForClearIndication:
            destination = TIMER_EXPIRED_FOR_CLEAR_INDICATION_MASK;
            break;

        case TimerExpiredForResetIndication:
            destination = TIMER_EXPIRED_FOR_RESET_INDICATION_MASK;
            break;

        case TimerExpiredForRestartIndication:
            destination = TIMER_EXPIRED_FOR_RESTART_INDICATION_MASK;
            break;

        case TimerExpiredForCallDeflection:
            destination = TIMER_EXPIRED_FOR_CALL_DEFLECTION_MASK;
            break;

        case CallSetupClearingOrRegistrationProblem:
            destination = CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_MASK;
            break;

        case FacilityCodeNotAllowed:
            destination = FACILITY_CODE_NOT_ALLOWED_MASK;
            break;

        case FacilityParameterNotAllowed:
            destination = FACILITY_PARAMETER_NOT_ALLOWED_MASK;
            break;

        case InvalidCalledAddress:
            destination = INVALID_CALLED_ADDRESS_MASK;
            break;

        case InvalidCallingAddress:
            destination = INVALID_CALLING_ADDRESS_MASK;
            break;

        case InvalidFacilityLength:
            destination = INVALID_FACILITY_LENGTH_MASK;
            break;

        case IncomingCallBarred:
            destination = INCOMING_CALL_BARRED_MASK;
            break;

        case NoLogicalChannelAvailable:
            destination = NO_LOGICAL_CHANNEL_AVAILABLE_MASK;
            break;

        case CallCollision:
            destination = CALL_COLLISION_MASK;
            break;

        case DuplicateFacilityRequested:
            destination = DUPLICATE_FACILITY_REQUESTED_MASK;
            break;

        case NonzeroAddressLength:
            destination = NONZERO_ADDRESS_LENGTH_MASK;
            break;

        case NonzeroFacilityLength:
            destination = NONZERO_FACILITY_LENGTH_MASK;
            break;

        case FacilityNotProvidedWhenExpected:
            destination = FACILITY_NOT_PROVIDED_WHEN_EXPECTED_MASK;
            break;

        case InvalidItuTSpecifiedDteFacility:
            destination = INVALID_ITU_T_SPECIFIED_DTE_FACILITY_MASK;
            break;

        case MaximumNumberOfCallRedirectionsOrDeflectionsExceeded:
            destination = MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_MASK;
            break;

        case Miscellaneous:
            destination = MISCELLANEOUS_MASK;
            break;

        case ImproperCauseCodeForDte:
            destination = IMPROPER_CAUSE_CODE_FOR_DTE_MASK;
            break;

        case OctetNotAaligned:
            destination = OCTET_NOT_ALIGNED_MASK;
            break;

        case InconsistentQBitSetting:
            destination = INCONSISTENT_Q_BIT_SETTING_MASK;
            break;

        case NetworkUserIdentificationProblem:
            destination = NETWORK_USER_IDENTIFICATION_PROBLEM_MASK;
            break;

        case InternationalProblem:
            destination = INTERNATIONAL_PROBLEM_MASK;
            break;

        case RemoteNetworkProblem:
            destination = REMOTE_NETWORK_PROBLEM_MASK;
            break;

        case InternationalProtocolProblem:
            destination = INTERNATIONAL_PROTOCOL_PROBLEM_MASK;
            break;

        case InternationalLinkOutOfOrder:
            destination = INTERNATIONAL_LINK_OUT_OF_ORDER_MASK;
            break;

        case InternationalLinkBusy:
            destination = INTERNATIONAL_LINK_BUSY_MASK;
            break;

        case TransitNetworkFacilityProblem:
            destination = TRANSIT_NETWORK_FACILITY_PROBLEM_MASK;
            break;

        case RemoteNetworkFacilityProblem:
            destination = REMOTE_NETWORK_FACILITY_PROBLEM_MASK;
            break;

        case InternationalRoutingProblem:
            destination = INTERNATIONAL_ROUTING_PROBLEM_MASK;
            break;

        case TemporaryRoutingProblem:
            destination = TEMPORARY_ROUTING_PROBLEM_MASK;
            break;

        case UnknownCalledDataNetworkIdentificationCode:
            destination = UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_MASK;
            break;

        case MaintenanceActionClearX25VcCommandIssued:
            destination = MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_MASK;
            break;

        case NormalTermination:
            destination = NORMAL_TERMINATION_MASK;
            break;

        case OutOfResources:
            destination = OUT_OF_RESOURCES_MASK;
            break;

        case AuthenticationFailure:
            destination = AUTHENTICATION_FAILURE_MASK;
            break;

        case InboundUserDataTooLarge:
            destination = INBOUND_USER_DATA_TOO_LARGE_MASK;
            break;

        case IdleTimerExpired:
            destination = IDLE_TIMER_EXPIRED_MASK;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::codes::DiagnosticCodeEnumerations::DiagnosticCodeEnumeration xot4cpp::x25::codes::DiagnosticCodeEnumerations::fromInt(const int& p_source) {

    DiagnosticCodeEnumeration destination;

    if (compareWithMask(NO_ADDITIONAL_INFORMATION_MASK, p_source)) {
        destination = NoAdditionalInformation;

    } else if (compareWithMask(INVALID_PACKET_SEND_SEQUENCE_NUMBER_MASK, p_source)) {
        destination = InvalidPacketSendSequenceNumber;

    } else if (compareWithMask(INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_MASK, p_source)) {
        destination = InvalidPacketReceiveSequenceNumber;

    } else if (compareWithMask(PACKET_TYPE_INVALID_MASK, p_source)) {
        destination = PacketTypeInvalid;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_MASK, p_source)) {
        destination = PacketTypeInvalidForStateR1PacketLevelReady;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_MASK, p_source)) {
        destination = PacketTypeInvalidForStateR2DteRestartRequest;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_MASK, p_source)) {
        destination = PacketTypeInvalidForStateR3DceRestartIndication;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P1_READY_MASK, p_source)) {
        destination = PacketTypeInvalidForStateP1Ready;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_MASK, p_source)) {
        destination = PacketTypeInvalidForStateP2DteWaiting;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_MASK, p_source)) {
        destination = PacketTypeInvalidForStateP3DceWaiting;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_MASK, p_source)) {
        destination = PacketTypeInvalidForStateP4DataTransfer;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_MASK, p_source)) {
        destination = PacketTypeInvalidForStateP5CallCollision;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_MASK, p_source)) {
        destination = PacketTypeInvalidForStateP6DteClearRequest;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_MASK, p_source)) {
        destination = PacketTypeInvalidForStateP7DceClearIndication;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_MASK, p_source)) {
        destination = PacketTypeInvalidForStateD1FlowControlReady;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_MASK, p_source)) {
        destination = PacketTypeInvalidForStateD2DteResetReady;

    } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_MASK, p_source)) {
        destination = PacketTypeInvalidForStateD3DceResetIndication;

    } else if (compareWithMask(PACKET_NOT_ALLOWED_MASK, p_source)) {
        destination = PacketNotAllowed;

    } else if (compareWithMask(UNIDENTIFIABLE_PACKET_MASK, p_source)) {
        destination = UnidentifiablePacket;

    } else if (compareWithMask(CALL_ON_ONE_WAY_LOGICAL_CHANNEL_MASK, p_source)) {
        destination = CallOnOneWayLogicalChannel;

    } else if (compareWithMask(INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_MASK, p_source)) {
        destination = InvalidPacketTypeOnAPermanentVirtualCircuit;

    } else if (compareWithMask(PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_MASK, p_source)) {
        destination = PacketOnUnassignedLogicalChannelNumber;

    } else if (compareWithMask(REJECT_NOT_SUBSCRIBED_TO_MASK, p_source)) {
        destination = RejectNotSubscribedTo;

    } else if (compareWithMask(PACKET_TOO_SHORT_MASK, p_source)) {
        destination = PacketTooShort;

    } else if (compareWithMask(PACKET_TOO_LONG_MASK, p_source)) {
        destination = PacketTooLong;

    } else if (compareWithMask(INVALID_GENERAL_FORMAT_IDENTIFIER_MASK, p_source)) {
        destination = InvalidGeneralFormatIdentifier;

    } else if (compareWithMask(RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_MASK, p_source)) {
        destination = RestartOrRegistrationPacketWithNonzeroLci;

    } else if (compareWithMask(PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_MASK, p_source)) {
        destination = PacketTypeNotCompatibleWithFacility;

    } else if (compareWithMask(UNAUTHORIZED_INTERRUPT_CONFIRMATION_MASK, p_source)) {
        destination = UnauthorizedInterruptConfirmation;

    } else if (compareWithMask(UNAUTHORIZED_INTERRUPT_MASK, p_source)) {
        destination = UnauthorizedInterrupt;

    } else if (compareWithMask(UNAUTHORIZED_REJECT_MASK, p_source)) {
        destination = UnauthorizedReject;

    } else if (compareWithMask(TIMER_EXPIRED_MASK, p_source)) {
        destination = TimerExpired;

    } else if (compareWithMask(TIMER_EXPIRED_FOR_INCOMING_CALL_MASK, p_source)) {
        destination = TimerExpiredForIncomingCall;

    } else if (compareWithMask(TIMER_EXPIRED_FOR_CLEAR_INDICATION_MASK, p_source)) {
        destination = TimerExpiredForClearIndication;

    } else if (compareWithMask(TIMER_EXPIRED_FOR_RESET_INDICATION_MASK, p_source)) {
        destination = TimerExpiredForResetIndication;

    } else if (compareWithMask(TIMER_EXPIRED_FOR_RESTART_INDICATION_MASK, p_source)) {
        destination = TimerExpiredForRestartIndication;

    } else if (compareWithMask(TIMER_EXPIRED_FOR_CALL_DEFLECTION_MASK, p_source)) {
        destination = TimerExpiredForCallDeflection;

    } else if (compareWithMask(CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_MASK, p_source)) {
        destination = CallSetupClearingOrRegistrationProblem;

    } else if (compareWithMask(FACILITY_CODE_NOT_ALLOWED_MASK, p_source)) {
        destination = FacilityCodeNotAllowed;

    } else if (compareWithMask(FACILITY_PARAMETER_NOT_ALLOWED_MASK, p_source)) {
        destination = FacilityParameterNotAllowed;

    } else if (compareWithMask(INVALID_CALLED_ADDRESS_MASK, p_source)) {
        destination = InvalidCalledAddress;

    } else if (compareWithMask(INVALID_CALLING_ADDRESS_MASK, p_source)) {
        destination = InvalidCallingAddress;

    } else if (compareWithMask(INVALID_FACILITY_LENGTH_MASK, p_source)) {
        destination = InvalidFacilityLength;

    } else if (compareWithMask(INCOMING_CALL_BARRED_MASK, p_source)) {
        destination = IncomingCallBarred;

    } else if (compareWithMask(NO_LOGICAL_CHANNEL_AVAILABLE_MASK, p_source)) {
        destination = NoLogicalChannelAvailable;

    } else if (compareWithMask(CALL_COLLISION_MASK, p_source)) {
        destination = CallCollision;

    } else if (compareWithMask(DUPLICATE_FACILITY_REQUESTED_MASK, p_source)) {
        destination = DuplicateFacilityRequested;

    } else if (compareWithMask(NONZERO_ADDRESS_LENGTH_MASK, p_source)) {
        destination = NonzeroAddressLength;

    } else if (compareWithMask(NONZERO_FACILITY_LENGTH_MASK, p_source)) {
        destination = NonzeroFacilityLength;

    } else if (compareWithMask(FACILITY_NOT_PROVIDED_WHEN_EXPECTED_MASK, p_source)) {
        destination = FacilityNotProvidedWhenExpected;

    } else if (compareWithMask(INVALID_ITU_T_SPECIFIED_DTE_FACILITY_MASK, p_source)) {
        destination = InvalidItuTSpecifiedDteFacility;

    } else if (compareWithMask(MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_MASK, p_source)) {
        destination = MaximumNumberOfCallRedirectionsOrDeflectionsExceeded;

    } else if (compareWithMask(MISCELLANEOUS_MASK, p_source)) {
        destination = Miscellaneous;

    } else if (compareWithMask(IMPROPER_CAUSE_CODE_FOR_DTE_MASK, p_source)) {
        destination = ImproperCauseCodeForDte;

    } else if (compareWithMask(OCTET_NOT_ALIGNED_MASK, p_source)) {
        destination = OctetNotAaligned;

    } else if (compareWithMask(INCONSISTENT_Q_BIT_SETTING_MASK, p_source)) {
        destination = InconsistentQBitSetting;

    } else if (compareWithMask(NETWORK_USER_IDENTIFICATION_PROBLEM_MASK, p_source)) {
        destination = NetworkUserIdentificationProblem;

    } else if (compareWithMask(INTERNATIONAL_PROBLEM_MASK, p_source)) {
        destination = InternationalProblem;

    } else if (compareWithMask(REMOTE_NETWORK_PROBLEM_MASK, p_source)) {
        destination = RemoteNetworkProblem;

    } else if (compareWithMask(INTERNATIONAL_PROTOCOL_PROBLEM_MASK, p_source)) {
        destination = InternationalProtocolProblem;

    } else if (compareWithMask(INTERNATIONAL_LINK_OUT_OF_ORDER_MASK, p_source)) {
        destination = InternationalLinkOutOfOrder;

    } else if (compareWithMask(INTERNATIONAL_LINK_BUSY_MASK, p_source)) {
        destination = InternationalLinkBusy;

    } else if (compareWithMask(TRANSIT_NETWORK_FACILITY_PROBLEM_MASK, p_source)) {
        destination = TransitNetworkFacilityProblem;

    } else if (compareWithMask(REMOTE_NETWORK_FACILITY_PROBLEM_MASK, p_source)) {
        destination = RemoteNetworkFacilityProblem;

    } else if (compareWithMask(INTERNATIONAL_ROUTING_PROBLEM_MASK, p_source)) {
        destination = InternationalRoutingProblem;

    } else if (compareWithMask(TEMPORARY_ROUTING_PROBLEM_MASK, p_source)) {
        destination = TemporaryRoutingProblem;

    } else if (compareWithMask(UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_MASK, p_source)) {
        destination = UnknownCalledDataNetworkIdentificationCode;

    } else if (compareWithMask(MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_MASK, p_source)) {
        destination = MaintenanceActionClearX25VcCommandIssued;

    } else if (compareWithMask(NORMAL_TERMINATION_MASK, p_source)) {
        destination = NormalTermination;

    } else if (compareWithMask(OUT_OF_RESOURCES_MASK, p_source)) {
        destination = OutOfResources;

    } else if (compareWithMask(AUTHENTICATION_FAILURE_MASK, p_source)) {
        destination = AuthenticationFailure;

    } else if (compareWithMask(INBOUND_USER_DATA_TOO_LARGE_MASK, p_source)) {
        destination = InboundUserDataTooLarge;

    } else if (compareWithMask(IDLE_TIMER_EXPIRED_MASK, p_source)) {
        destination = IdleTimerExpired;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}

bool xot4cpp::x25::codes::DiagnosticCodeEnumerations::compareWithMask(const int& p_mask, const int& p_value) {

    return (p_mask == p_value);
}
