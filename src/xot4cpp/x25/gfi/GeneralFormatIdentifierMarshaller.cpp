#include "xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::Octet xot4cpp::x25::gfi::GeneralFormatIdentifierMarshaller::marshal(const GeneralFormatIdentifier::SharedPointer& p_source) {

    unsigned int target;

    target = (target & 0x0000) | p_source->qualifiedData();
    target = ((target << 1) & 0x00fe) | p_source->deliveryConfirmation();
    target = ((target << 2) & 0x00fc) | p_source->protocolIdentification();

    return static_cast<Octet> (target & 0x00ff);
}

xot4cpp::x25::gfi::GeneralFormatIdentifier::SharedPointer xot4cpp::x25::gfi::GeneralFormatIdentifierMarshaller::unmarshal(const Octet& p_source) {

    const GeneralFormatIdentifier::QualifiedData qualifiedData = static_cast<GeneralFormatIdentifier::QualifiedData> ((p_source >> 3) & 0x01);
    const GeneralFormatIdentifier::DeliveryConfirmation deliveryConfirmation = static_cast<GeneralFormatIdentifier::DeliveryConfirmation> ((p_source >> 2) & 0x01);
    const GeneralFormatIdentifier::ProtocolIdentification protocolIdentification = static_cast<GeneralFormatIdentifier::ProtocolIdentification> (p_source & 0x03);

    return GeneralFormatIdentifier::create(qualifiedData, deliveryConfirmation, protocolIdentification);
}
