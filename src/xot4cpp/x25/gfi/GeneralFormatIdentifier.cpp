#include "xot4cpp/x25/gfi/GeneralFormatIdentifier.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::gfi::GeneralFormatIdentifier::GeneralFormatIdentifier(
        const QualifiedData& p_qualifiedData,
        const DeliveryConfirmation& p_deliveryConfirmation,
        const ProtocolIdentification& p_protocolIdentification)
: m_qualifiedData(p_qualifiedData),
m_deliveryConfirmation(p_deliveryConfirmation),
m_protocolIdentification(p_protocolIdentification) {
}

xot4cpp::x25::gfi::GeneralFormatIdentifier::SharedPointer xot4cpp::x25::gfi::GeneralFormatIdentifier::create(
        const QualifiedData& p_qualifiedData,
        const DeliveryConfirmation& p_deliveryConfirmation,
        const ProtocolIdentification& p_protocolIdentification) {

    return SharedPointer(new GeneralFormatIdentifier(p_qualifiedData, p_deliveryConfirmation, p_protocolIdentification));
}

xot4cpp::x25::gfi::GeneralFormatIdentifier::~GeneralFormatIdentifier(void) {
}

const xot4cpp::x25::gfi::GeneralFormatIdentifier::QualifiedData& xot4cpp::x25::gfi::GeneralFormatIdentifier::qualifiedData(void) const {

    return m_qualifiedData;
}

const xot4cpp::x25::gfi::GeneralFormatIdentifier::DeliveryConfirmation& xot4cpp::x25::gfi::GeneralFormatIdentifier::deliveryConfirmation(void) const {

    return m_deliveryConfirmation;
}

const xot4cpp::x25::gfi::GeneralFormatIdentifier::ProtocolIdentification& xot4cpp::x25::gfi::GeneralFormatIdentifier::protocolIdentification(void) const {

    return m_protocolIdentification;
}
