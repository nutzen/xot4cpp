#include "xot4cpp/x25/transitions/CallAcceptedTransition.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/x25/utility/PtiToTransitionUtility.hpp"

xot4cpp::x25::transitions::CallAcceptedTransition::CallAcceptedTransition(
        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
        const TransitionParameters::SharedPointer& p_parameters)
: Transition(p_enumeration, p_parameters) {
}

xot4cpp::x25::transitions::CallAcceptedTransition::~CallAcceptedTransition(void) {
}

xot4cpp::x25::transitions::Transition::SharedPointer xot4cpp::x25::transitions::CallAcceptedTransition::create(
        const TransitionParameters::SharedPointer& p_parameters) {

    const TransitionEnumerations::TransitionEnumeration transition =
            utility::PtiToTransitionUtility::transition(p_parameters);

    const Transition::SharedPointer pointer(new CallAcceptedTransition(transition, p_parameters));
    return pointer;
}

xot4cpp::x25::transitions::TransitionResult::SharedPointer xot4cpp::x25::transitions::CallAcceptedTransition::operator ()(
        const TransitionParameters::SharedPointer& p_parameters) const {

    return TransitionResult::create(None);
}
