#include "xot4cpp/x25/transitions/TransitionParameters.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::transitions::TransitionParameters::TransitionParameters(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const states::StateMachine::SharedPointer& p_stateMachine,
        const context::SessionContext::SharedPointer& p_sessionContext,
        const context::RoleEnumerations::RoleEnumeration& p_origin,
        const X25Packet::SharedPointer& p_packet)
: m_outputStream(p_outputStream),
m_stateMachine(p_stateMachine),
m_sessionContext(p_sessionContext),
m_origin(p_origin),
m_packet(p_packet) {
}

xot4cpp::x25::transitions::TransitionParameters::~TransitionParameters(void) {
}

xot4cpp::x25::transitions::TransitionParameters::SharedPointer xot4cpp::x25::transitions::TransitionParameters::create(
        const stream::OutputStream::SharedPointer& p_outputStream,
        const states::StateMachine::SharedPointer& p_stateMachine,
        const context::SessionContext::SharedPointer& p_sessionContext,
        const context::RoleEnumerations::RoleEnumeration& p_origin,
        const X25Packet::SharedPointer& p_packet) {

    const SharedPointer pointer(new TransitionParameters(
            p_outputStream,
            p_stateMachine,
            p_sessionContext,
            p_origin,
            p_packet));

    return pointer;
}

const xot4cpp::stream::OutputStream::SharedPointer& xot4cpp::x25::transitions::TransitionParameters::outputStream(void) const {

    return m_outputStream;
}

const xot4cpp::x25::states::StateMachine::SharedPointer& xot4cpp::x25::transitions::TransitionParameters::stateMachine(void) const {

    return m_stateMachine;
}

const xot4cpp::x25::context::SessionContext::SharedPointer& xot4cpp::x25::transitions::TransitionParameters::sessionContext(void) const {

    return m_sessionContext;
}

const xot4cpp::x25::context::RoleEnumerations::RoleEnumeration& xot4cpp::x25::transitions::TransitionParameters::origin(void) const {

    return m_origin;
}

const xot4cpp::x25::X25Packet::SharedPointer& xot4cpp::x25::transitions::TransitionParameters::packet(void) const {

    return m_packet;
}
