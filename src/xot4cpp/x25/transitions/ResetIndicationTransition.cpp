#include "xot4cpp/x25/transitions/ResetIndicationTransition.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/logger/Logger.hpp"
#include "xot4cpp/x25/payload/ResetPacketPayload.hpp"
#include "xot4cpp/x25/utility/PtiToTransitionUtility.hpp"
#include "xot4cpp/x25/utility/X25PacketUtility.hpp"

xot4cpp::x25::transitions::ResetIndicationTransition::ResetIndicationTransition(
        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
        const TransitionParameters::SharedPointer& p_parameters)
: Transition(p_enumeration, p_parameters) {
}

xot4cpp::x25::transitions::ResetIndicationTransition::~ResetIndicationTransition(void) {
}

xot4cpp::x25::transitions::Transition::SharedPointer xot4cpp::x25::transitions::ResetIndicationTransition::create(
        const TransitionParameters::SharedPointer& p_parameters) {

    const TransitionEnumerations::TransitionEnumeration transition =
            utility::PtiToTransitionUtility::transition(p_parameters);

    const Transition::SharedPointer pointer(new ResetIndicationTransition(transition, p_parameters));
    return pointer;
}

xot4cpp::x25::transitions::TransitionResult::SharedPointer xot4cpp::x25::transitions::ResetIndicationTransition::operator ()(
        const TransitionParameters::SharedPointer& p_parameters) const {

    p_parameters->sessionContext()->packetReceiveSequenceNumber() = 0;
    p_parameters->sessionContext()->packetSendSequenceNumber() = 0;
    log(p_parameters);

    const X25Packet::SharedPointer packet = utility::X25PacketUtility::resetConfirm(p_parameters->sessionContext());

    return TransitionResult::create(Submit, packet);
}

void xot4cpp::x25::transitions::ResetIndicationTransition::log(
        const TransitionParameters::SharedPointer& p_parameters) {

    const payload::ResetPacketPayload::SharedPointer payload = payload::ResetPacketPayload::downCast(p_parameters->packet()->payload());

    XOT4CPP_TRACE("reset-indication: "
            << "local=" << p_parameters->sessionContext()->remoteAddress()->address()
            << ", remote=" << p_parameters->sessionContext()->remoteAddress()->address()
            << ", lcgn=" << static_cast<unsigned int> (p_parameters->sessionContext()->logicalChannelIdentifier()->logicalChannelGroupNumber())
            << ", lcn=" << static_cast<unsigned int> (p_parameters->sessionContext()->logicalChannelIdentifier()->logicalChannelNumber())
            << ", cause=" << codes::CauseCodeEnumerations::toDescription(payload->cause())
            << ", diagnostic=" << codes::DiagnosticCodeEnumerations::toDescription(payload->diagnostic())
            << "");
}
