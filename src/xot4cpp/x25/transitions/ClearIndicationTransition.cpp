#include "xot4cpp/x25/transitions/ClearIndicationTransition.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/logger/Logger.hpp"
#include "xot4cpp/x25/payload/ClearPacketPayload.hpp"
#include "xot4cpp/x25/utility/PtiToTransitionUtility.hpp"
#include "xot4cpp/x25/utility/X25PacketUtility.hpp"

xot4cpp::x25::transitions::ClearIndicationTransition::ClearIndicationTransition(
        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
        const TransitionParameters::SharedPointer& p_parameters)
: Transition(p_enumeration, p_parameters) {
}

xot4cpp::x25::transitions::ClearIndicationTransition::~ClearIndicationTransition(void) {
}

xot4cpp::x25::transitions::Transition::SharedPointer xot4cpp::x25::transitions::ClearIndicationTransition::create(
        const TransitionParameters::SharedPointer& p_parameters) {

    const TransitionEnumerations::TransitionEnumeration transition =
            utility::PtiToTransitionUtility::transition(p_parameters);

    const Transition::SharedPointer pointer(new ClearIndicationTransition(transition, p_parameters));
    return pointer;
}

xot4cpp::x25::transitions::TransitionResult::SharedPointer xot4cpp::x25::transitions::ClearIndicationTransition::operator ()(
        const TransitionParameters::SharedPointer& p_parameters) const {

    const payload::ClearPacketPayload::SharedPointer payload = payload::ClearPacketPayload::downCast(p_parameters->packet()->payload());
    p_parameters->sessionContext()->cause() = payload->cause();
    p_parameters->sessionContext()->diagnostic() = payload->diagnostic();
    log(p_parameters);

    const X25Packet::SharedPointer packet = utility::X25PacketUtility::clearConfirm(p_parameters->sessionContext());

    return TransitionResult::create(Submit, packet);
}

void xot4cpp::x25::transitions::ClearIndicationTransition::log(
        const TransitionParameters::SharedPointer& p_parameters) {

    XOT4CPP_TRACE("clear-indication: "
            << "local=" << p_parameters->sessionContext()->remoteAddress()->address()
            << ", remote=" << p_parameters->sessionContext()->remoteAddress()->address()
            << ", lcgn=" << static_cast<unsigned int> (p_parameters->sessionContext()->logicalChannelIdentifier()->logicalChannelGroupNumber())
            << ", lcn=" << static_cast<unsigned int> (p_parameters->sessionContext()->logicalChannelIdentifier()->logicalChannelNumber())
            << ", cause=" << codes::CauseCodeEnumerations::toDescription(p_parameters->sessionContext()->cause())
            << ", diagnostic=" << codes::DiagnosticCodeEnumerations::toDescription(p_parameters->sessionContext()->diagnostic())
            << "");
}
