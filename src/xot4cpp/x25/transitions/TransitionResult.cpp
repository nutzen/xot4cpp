#include "xot4cpp/x25/transitions/TransitionResult.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

xot4cpp::x25::transitions::TransitionResult::TransitionResult(
        const PostTransitionActionEnumeration& p_action,
        const X25Packet::SharedPointer& p_packet)
: m_action(p_action),
m_packet(p_packet) {
}

xot4cpp::x25::transitions::TransitionResult::~TransitionResult(void) {
}

xot4cpp::x25::transitions::TransitionResult::SharedPointer xot4cpp::x25::transitions::TransitionResult::create(
        const PostTransitionActionEnumeration& p_action,
        const X25Packet::SharedPointer& p_packet) {

    const TransitionResult::SharedPointer pointer(new TransitionResult(p_action, p_packet));
    return pointer;
}

const xot4cpp::x25::transitions::PostTransitionActionEnumeration& xot4cpp::x25::transitions::TransitionResult::action(void) const {

    return m_action;
}

const xot4cpp::x25::X25Packet::SharedPointer& xot4cpp::x25::transitions::TransitionResult::packet(void) const {

    return m_packet;
}
