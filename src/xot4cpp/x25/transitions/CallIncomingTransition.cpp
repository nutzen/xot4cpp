#include "xot4cpp/x25/transitions/CallIncomingTransition.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/logger/Logger.hpp"
#include "xot4cpp/x25/payload/CallPacketPayload.hpp"
#include "xot4cpp/x25/utility/PtiToTransitionUtility.hpp"
#include "xot4cpp/x25/utility/X25PacketUtility.hpp"

xot4cpp::x25::transitions::CallIncomingTransition::CallIncomingTransition(
        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
        const TransitionParameters::SharedPointer& p_parameters)
: Transition(p_enumeration, p_parameters) {
}

xot4cpp::x25::transitions::CallIncomingTransition::~CallIncomingTransition(void) {
}

xot4cpp::x25::transitions::Transition::SharedPointer xot4cpp::x25::transitions::CallIncomingTransition::create(
        const TransitionParameters::SharedPointer& p_parameters) {

    const TransitionEnumerations::TransitionEnumeration transition =
            utility::PtiToTransitionUtility::transition(p_parameters);

    const Transition::SharedPointer pointer(new CallIncomingTransition(transition, p_parameters));
    return pointer;
}

xot4cpp::x25::transitions::TransitionResult::SharedPointer xot4cpp::x25::transitions::CallIncomingTransition::operator ()(
        const TransitionParameters::SharedPointer& p_parameters) const {

    TransitionResult::SharedPointer destination;

    const context::SessionContext::SharedPointer sessionContext = p_parameters->sessionContext();
    X25Packet::SharedPointer packet = p_parameters->packet();

    const payload::CallPacketPayload::SharedPointer payload = payload::CallPacketPayload::downCast(packet->payload());
    sessionContext->remoteAddress() = payload->callingAddress();

    if (sessionContext->localAddress()->address().compare(payload->calledAddress()->address())) {

        packet = utility::X25PacketUtility::callAccept(sessionContext);
        destination = TransitionResult::create(Submit, packet);

    } else {
        XOT4CPP_DEBUG("Wrong address: I am on " <<
                sessionContext->localAddress()->address() << " and " <<
                sessionContext->remoteAddress()->address() << " is looking for " <<
                payload->calledAddress()->address() << ".");

        packet = utility::X25PacketUtility::clearRequest(sessionContext);
        destination = TransitionResult::create(Submit, packet);

    }

    return destination;
}
