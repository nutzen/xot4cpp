#include "xot4cpp/x25/transitions/TransitionEnumerations.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

// Call set-up and clearing DESCRIPTION
#define CALL_REQUEST_DESCRIPTION "Call_Request"
#define CALL_INCOMING_DESCRIPTION "Call_Incoming"
#define CALL_ACCEPTED_DESCRIPTION "Call_Accepted"
#define CALL_CONNECTED_DESCRIPTION "Call_Connected"
#define CLEAR_REQUEST_DESCRIPTION "Clear_Request"
#define CLEAR_INDICATION_DESCRIPTION "Clear_Indication"
#define CLEAR_CONFIRMATION_DESCRIPTION "Clear_Confirmation"
// Data and interrupt DESCRIPTION
#define DATA_DESCRIPTION "Data"
#define INTERRUPT_DESCRIPTION "Interrupt"
#define INTERRUPT_CONFIRMATION_DESCRIPTION "Interrupt_Confirmation"
// Flow control and reset DESCRIPTION
#define RECEIVER_READY_DESCRIPTION "Receiver_Ready"
#define RECEIVER_NOT_READY_DESCRIPTION "Receiver_Not_Ready"
#define REJECT_DESCRIPTION "Reject"
#define RESET_REQUEST_DESCRIPTION "Reset_Request"
#define RESET_INDICATION_DESCRIPTION "Reset_Indication"
#define RESET_CONFIRMATION_DESCRIPTION "Reset_Confirmation"
// Restart DESCRIPTION
#define RESTART_REQUEST_DESCRIPTION "Restart_Request"
#define RESTART_INDICATION_DESCRIPTION "Restart_Indication"
#define RESTART_CONFIRMATION_DESCRIPTION "Restart_Confirmation"
// Diagnostic DESCRIPTION
#define DIAGNOSTIC_DESCRIPTION "Diagnostic"

std::string xot4cpp::x25::transitions::TransitionEnumerations::toDescription(const TransitionEnumeration& p_source) {

    std::string destination;

    switch (p_source) {

        case CallRequest:
            destination = CALL_REQUEST_DESCRIPTION;
            break;

        case CallIncoming:
            destination = CALL_INCOMING_DESCRIPTION;
            break;

        case CallAccepted:
            destination = CALL_ACCEPTED_DESCRIPTION;
            break;

        case CallConnected:
            destination = CALL_CONNECTED_DESCRIPTION;
            break;

        case ClearRequest:
            destination = CLEAR_REQUEST_DESCRIPTION;
            break;

        case ClearIndication:
            destination = CLEAR_INDICATION_DESCRIPTION;
            break;

        case ClearConfirmation:
            destination = CLEAR_CONFIRMATION_DESCRIPTION;
            break;

        case Data:
            destination = DATA_DESCRIPTION;
            break;

        case Interrupt:
            destination = INTERRUPT_DESCRIPTION;
            break;

        case InterruptConfirmation:
            destination = INTERRUPT_CONFIRMATION_DESCRIPTION;
            break;

        case ReceiverReady:
            destination = RECEIVER_READY_DESCRIPTION;
            break;

        case ReceiverNotReady:
            destination = RECEIVER_NOT_READY_DESCRIPTION;
            break;

        case Reject:
            destination = REJECT_DESCRIPTION;
            break;

        case ResetRequest:
            destination = RESET_REQUEST_DESCRIPTION;
            break;

        case ResetIndication:
            destination = RESET_INDICATION_DESCRIPTION;
            break;

        case ResetConfirmation:
            destination = RESET_CONFIRMATION_DESCRIPTION;
            break;

        case RestartRequest:
            destination = RESTART_REQUEST_DESCRIPTION;
            break;

        case RestartIndication:
            destination = RESTART_INDICATION_DESCRIPTION;
            break;

        case RestartConfirmation:
            destination = RESTART_CONFIRMATION_DESCRIPTION;
            break;

        case Diagnostic:
            destination = DIAGNOSTIC_DESCRIPTION;
            break;

        default:
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
            break;
    }

    return destination;
}

xot4cpp::x25::transitions::TransitionEnumerations::TransitionEnumeration xot4cpp::x25::transitions::TransitionEnumerations::fromDescription(const std::string& p_source) {

    TransitionEnumeration destination;

    if (0 == p_source.compare(CALL_REQUEST_DESCRIPTION)) {
        destination = CallRequest;

    } else if (0 == p_source.compare(CALL_INCOMING_DESCRIPTION)) {
        destination = CallIncoming;

    } else if (0 == p_source.compare(CALL_ACCEPTED_DESCRIPTION)) {
        destination = CallAccepted;

    } else if (0 == p_source.compare(CALL_CONNECTED_DESCRIPTION)) {
        destination = CallConnected;

    } else if (0 == p_source.compare(CLEAR_REQUEST_DESCRIPTION)) {
        destination = ClearRequest;

    } else if (0 == p_source.compare(CLEAR_INDICATION_DESCRIPTION)) {
        destination = ClearIndication;

    } else if (0 == p_source.compare(CLEAR_CONFIRMATION_DESCRIPTION)) {
        destination = ClearConfirmation;

    } else if (0 == p_source.compare(DATA_DESCRIPTION)) {
        destination = Data;

    } else if (0 == p_source.compare(INTERRUPT_DESCRIPTION)) {
        destination = Interrupt;

    } else if (0 == p_source.compare(INTERRUPT_CONFIRMATION_DESCRIPTION)) {
        destination = InterruptConfirmation;

    } else if (0 == p_source.compare(RECEIVER_READY_DESCRIPTION)) {
        destination = ReceiverReady;

    } else if (0 == p_source.compare(RECEIVER_NOT_READY_DESCRIPTION)) {
        destination = ReceiverNotReady;

    } else if (0 == p_source.compare(REJECT_DESCRIPTION)) {
        destination = Reject;

    } else if (0 == p_source.compare(RESET_REQUEST_DESCRIPTION)) {
        destination = ResetRequest;

    } else if (0 == p_source.compare(RESET_INDICATION_DESCRIPTION)) {
        destination = ResetIndication;

    } else if (0 == p_source.compare(RESET_CONFIRMATION_DESCRIPTION)) {
        destination = ResetConfirmation;

    } else if (0 == p_source.compare(RESTART_REQUEST_DESCRIPTION)) {
        destination = RestartRequest;

    } else if (0 == p_source.compare(RESTART_INDICATION_DESCRIPTION)) {
        destination = RestartIndication;

    } else if (0 == p_source.compare(RESTART_CONFIRMATION_DESCRIPTION)) {
        destination = RestartConfirmation;

    } else if (0 == p_source.compare(DIAGNOSTIC_DESCRIPTION)) {
        destination = Diagnostic;

    } else {
        THROW_XOT4CPP_EXCEPTION("Unhandled value: " << p_source);
    }

    return destination;
}
