#include "xot4cpp/x25/transitions/Transition.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"

#include "xot4cpp/x25/transitions/CallRequestTransition.hpp"
#include "xot4cpp/x25/transitions/CallIncomingTransition.hpp"
#include "xot4cpp/x25/transitions/CallAcceptedTransition.hpp"
#include "xot4cpp/x25/transitions/CallConnectedTransition.hpp"
#include "xot4cpp/x25/transitions/ClearRequestTransition.hpp"
#include "xot4cpp/x25/transitions/ClearIndicationTransition.hpp"
#include "xot4cpp/x25/transitions/ClearConfirmationTransition.hpp"
#include "xot4cpp/x25/transitions/DataTransition.hpp"
#include "xot4cpp/x25/transitions/ReceiverReadyTransition.hpp"
#include "xot4cpp/x25/transitions/ResetIndicationTransition.hpp"
#include "xot4cpp/x25/transitions/ResetConfirmationTransition.hpp"

#include "xot4cpp/x25/utility/PtiToTransitionUtility.hpp"

xot4cpp::x25::transitions::Transition::Transition(
        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
        const TransitionParameters::SharedPointer& p_parameters)
: m_enumeration(p_enumeration),
m_parameters(p_parameters) {
}

xot4cpp::x25::transitions::Transition::~Transition(void) {
}

xot4cpp::x25::transitions::Transition::SharedPointer xot4cpp::x25::transitions::Transition::create(
        const TransitionParameters::SharedPointer& p_parameters) {

    Transition::SharedPointer destination;

    const TransitionEnumerations::TransitionEnumeration transition =
            utility::PtiToTransitionUtility::transition(p_parameters);

    switch (transition) {
            case TransitionEnumerations::CallRequest: { destination = CallRequestTransition::create(p_parameters); break; }
            case TransitionEnumerations::CallIncoming: { destination = CallIncomingTransition::create(p_parameters); break; }
            case TransitionEnumerations::CallAccepted: { destination = CallAcceptedTransition::create(p_parameters); break; }
            case TransitionEnumerations::CallConnected: { destination = CallConnectedTransition::create(p_parameters); break; }
            case TransitionEnumerations::ClearRequest: { destination = ClearRequestTransition::create(p_parameters); break; }
            case TransitionEnumerations::ClearIndication: { destination = ClearIndicationTransition::create(p_parameters); break; }
            case TransitionEnumerations::ClearConfirmation: { destination = ClearConfirmationTransition::create(p_parameters); break; }
            case TransitionEnumerations::Data: { destination = DataTransition::create(p_parameters); break; }
            // case TransitionEnumerations::Interrupt: { destination = InterruptTransition::create(p_parameters); break; }
            // case TransitionEnumerations::InterruptConfirmation: { destination = InterruptConfirmationTransition::create(p_parameters); break; }
            case TransitionEnumerations::ReceiverReady: { destination = ReceiverReadyTransition::create(p_parameters); break; }
            // case TransitionEnumerations::ReceiverNotReady: { destination = ReceiverNotReadyTransition::create(p_parameters); break; }
            // case TransitionEnumerations::Reject: { destination = RejectTransition::create(p_parameters); break; }
            // case TransitionEnumerations::ResetRequest: { destination = ResetRequestTransition::create(p_parameters); break; }
            case TransitionEnumerations::ResetIndication: { destination = ResetIndicationTransition::create(p_parameters); break; }
            case TransitionEnumerations::ResetConfirmation: { destination = ResetConfirmationTransition::create(p_parameters); break; }
            // case TransitionEnumerations::RestartRequest: { destination = RestartRequestTransition::create(p_parameters); break; }
            // case TransitionEnumerations::RestartIndication: { destination = RestartIndicationTransition::create(p_parameters); break; }
            // case TransitionEnumerations::RestartConfirmation: { destination = RestartConfirmationTransition::create(p_parameters); break; }
            // case TransitionEnumerations::Diagnostic: { destination = DiagnosticTransition::create(p_parameters); break; }
        default:
        {
            THROW_XOT4CPP_EXCEPTION("Unhandled value: " << TransitionEnumerations::toDescription(transition));
        }
    }

    return destination;
}

const xot4cpp::x25::transitions::TransitionEnumerations::TransitionEnumeration& xot4cpp::x25::transitions::Transition::enumeration(void) const {

    return m_enumeration;
}

xot4cpp::x25::transitions::TransitionResult::SharedPointer xot4cpp::x25::transitions::Transition::operator ()(void) const {

    m_parameters->stateMachine()->handle(enumeration());
    return (*this)(m_parameters);
}
