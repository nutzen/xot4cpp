#include "xot4cpp/x25/transitions/DataTransition.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/Xot4cppException.hpp"
#include "xot4cpp/x25/payload/DataPacketPayload.hpp"
#include "xot4cpp/x25/pti/DataPacketTypeIdentifier.hpp"
#include "xot4cpp/x25/utility/PtiToTransitionUtility.hpp"
#include "xot4cpp/x25/utility/SessionContextUtility.hpp"
#include "xot4cpp/x25/utility/X25PacketUtility.hpp"

xot4cpp::x25::transitions::DataTransition::DataTransition(
        const TransitionEnumerations::TransitionEnumeration& p_enumeration,
        const TransitionParameters::SharedPointer& p_parameters)
: Transition(p_enumeration, p_parameters) {
}

xot4cpp::x25::transitions::DataTransition::~DataTransition(void) {
}

xot4cpp::x25::transitions::Transition::SharedPointer xot4cpp::x25::transitions::DataTransition::create(
        const TransitionParameters::SharedPointer& p_parameters) {

    const TransitionEnumerations::TransitionEnumeration transition =
            utility::PtiToTransitionUtility::transition(p_parameters);

    const Transition::SharedPointer pointer(new DataTransition(transition, p_parameters));
    return pointer;
}

xot4cpp::x25::transitions::TransitionResult::SharedPointer xot4cpp::x25::transitions::DataTransition::operator ()(
        const TransitionParameters::SharedPointer& p_parameters) const {

    TransitionResult::SharedPointer destination;

    const context::RoleEnumerations::RoleEnumeration origin = p_parameters->origin();
    if (context::RoleEnumerations::DCE == origin) {
        X25Packet::SharedPointer packet = p_parameters->packet();
        const context::SessionContext::SharedPointer sessionContext = p_parameters->sessionContext();
        const pti::DataPacketTypeIdentifier::SharedPointer packetTypeIdentifier = pti::DataPacketTypeIdentifier::downCast(packet->header()->packetTypeIdentifier());
        sessionContext->packetReceiveSequenceNumber() = packetTypeIdentifier->packetSendSequenceNumber();
        utility::SessionContextUtility::updatePacketReceiveSequenceNumber(sessionContext);

        const payload::DataPacketPayload::SharedPointer payload = payload::DataPacketPayload::downCast(packet->payload());
        p_parameters->outputStream()->write(payload->userData());

        packet = utility::X25PacketUtility::receiverReady(sessionContext);
        destination = TransitionResult::create(Submit, packet);

    } else if (context::RoleEnumerations::DTE == origin) {
        utility::SessionContextUtility::updatePacketSendSequenceNumber(p_parameters->sessionContext());
        destination = TransitionResult::create(Poll);

    } else {
        THROW_XOT4CPP_EXCEPTION(
                "Unhandled origin: " << context::RoleEnumerations::toDescription(origin));
    }

    return destination;
}
