#include "xot4cpp/socket/ServerSocket.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/IOException.hpp"

#include <algorithm>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

xot4cpp::socket::ServerSocket::ServerSocket(const int& p_socketFileDescriptor)
: m_socketFileDescriptor(p_socketFileDescriptor) {
}

xot4cpp::socket::ServerSocket::~ServerSocket(void) {

    ::close(m_socketFileDescriptor);
}

xot4cpp::socket::ServerSocket::SharedPointer xot4cpp::socket::ServerSocket::bind(
        const char* p_port,
        const char* p_host) {

    const int port = ::atoi(p_port);
    return bind(port, p_host);
}

xot4cpp::socket::ServerSocket::SharedPointer xot4cpp::socket::ServerSocket::bind(
        const int& p_port,
        const char* p_host) {

    const SharedPointer pointer = create(::socket(AF_INET, SOCK_STREAM, 0));

    struct sockaddr_in addr;
    ::bzero(&addr, sizeof (addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(p_port);
    addr.sin_addr.s_addr = INADDR_ANY;

    if (p_host) {
        struct hostent* host = ::gethostbyname(p_host);
        if (0 == host) {
            THROW_XOT4CPP_IOEXCEPTION(
                    "::gethostbyname(" << p_host << ") failed: "
                    << errno << " " << ::strerror(errno));
        }
        ::bcopy((char *) host->h_addr,
                (char *) &addr.sin_addr.s_addr,
                host->h_length);
    }

    if (0 != ::bind(pointer->m_socketFileDescriptor, (struct sockaddr*) &addr, sizeof (addr))) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::bind(...) failed: "
                << errno << " " << ::strerror(errno));
    }

    if (0 != ::listen(pointer->m_socketFileDescriptor, 20)) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::listen(...) failed: "
                << errno << " " << ::strerror(errno));
    }

    return pointer;
}

xot4cpp::socket::ServerSocket::SharedPointer xot4cpp::socket::ServerSocket::create(
        const int& p_socketFileDescriptor) {

    if (0 > p_socketFileDescriptor) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::socket(...) failed: "
                << errno << " " << ::strerror(errno));
    }

    const SharedPointer pointer(new ServerSocket(p_socketFileDescriptor));
    return pointer;
}

xot4cpp::socket::Socket::SharedPointer xot4cpp::socket::ServerSocket::accept(void) {

    const int socketFileDescriptor = ::accept(m_socketFileDescriptor, 0, 0);

    if (0 > socketFileDescriptor) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::accept(...) failed: "
                << errno << " " << ::strerror(errno));
    }

    return Socket::create(socketFileDescriptor);
}
