#include "xot4cpp/socket/Socket.hpp"

//          Copyright Joe Els 2011 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "xot4cpp/exceptions/IOException.hpp"
#include "xot4cpp/exceptions/SocketTimeoutException.hpp"
#include "xot4cpp/logger/Logger.hpp"

#include <algorithm>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <signal.h>

namespace xot4cpp {

    namespace socket {

        extern void signalHandler(int p_signal);

        class Alarm {
        public:
            Alarm(unsigned int p_seconds);
            virtual ~Alarm(void);

            void cancel(void);

        private:
            Alarm(void);
            Alarm(const Alarm&);
            Alarm & operator=(const Alarm&);

            struct sigaction m_sigaction;
        };
    };
};

xot4cpp::socket::Socket::Socket(const int& p_socketFileDescriptor)
: m_socketFileDescriptor(p_socketFileDescriptor) {
}

xot4cpp::socket::Socket::~Socket(void) {

    ::close(m_socketFileDescriptor);
}

xot4cpp::socket::Socket::SharedPointer xot4cpp::socket::Socket::connect(
        const char* p_host,
        const char* p_port) {

    const int port = ::atoi(p_port);
    return connect(p_host, port);
}

xot4cpp::socket::Socket::SharedPointer xot4cpp::socket::Socket::connect(
        const char* p_host,
        const int& p_port) {

    const SharedPointer pointer = create(::socket(AF_INET, SOCK_STREAM, 0));

    struct hostent* host = ::gethostbyname(p_host);
    if (0 == host) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::gethostbyname(" << p_host << ") failed: "
                << errno << " " << ::strerror(errno));
    }

    struct sockaddr_in serv_addr;
    ::bzero((char *) &serv_addr, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    ::bcopy((char *) host->h_addr,
            (char *) &serv_addr.sin_addr.s_addr,
            host->h_length);
    serv_addr.sin_port = ::htons(p_port);


    Alarm alarm(3);
    if (0 > ::connect(pointer->m_socketFileDescriptor, (struct sockaddr *) &serv_addr, sizeof (serv_addr))) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::connect(...) failed: "
                << errno << " " << ::strerror(errno));
    }
    alarm.cancel();

    return pointer;
}

xot4cpp::socket::Socket::SharedPointer xot4cpp::socket::Socket::create(
        const int& p_socketFileDescriptor) {

    if (0 > p_socketFileDescriptor) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::socket(...) failed: "
                << errno << " " << ::strerror(errno));
    }

    const SharedPointer pointer(new Socket(p_socketFileDescriptor));

    ::signal(SIGPIPE, xot4cpp::socket::signalHandler);

    return pointer;
}

size_t xot4cpp::socket::Socket::read(const size_t& p_size, Octet* p_buffer) {

    struct timeval tv;
    ::memset(&tv, 0, sizeof (tv));
    tv.tv_sec = 1;
    ::setsockopt(m_socketFileDescriptor, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *) &tv, sizeof (struct timeval));

    const size_t size = ::read(m_socketFileDescriptor, p_buffer, p_size);

    if (-1 == size) {
        if ((EAGAIN == errno) || (EWOULDBLOCK == errno)) {
            THROW_XOT4CPP_SOCKETTIMEOUTEXCEPTION(
                    "::read(...) failed: "
                    << errno << " " << ::strerror(errno));

        } else {
            THROW_XOT4CPP_IOEXCEPTION(
                    "::read(...) failed: "
                    << errno << " " << ::strerror(errno));
        }
    }

    return size;
}

size_t xot4cpp::socket::Socket::write(const size_t& p_size, const Octet* p_buffer) {

    const size_t size = ::write(m_socketFileDescriptor, p_buffer, p_size);

    if (-1 == size) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::write(...) failed: "
                << errno << " " << ::strerror(errno));
    }

    return size;
}

void xot4cpp::socket::signalHandler(int p_signal) {

    switch (p_signal) {
        case SIGPIPE:
        {
            ::signal(SIGPIPE, xot4cpp::socket::signalHandler);
            break;
        }

        case SIGALRM:
        {
            break;
        }

        default:
        {
            XOT4CPP_DEBUG("Unhandled signal " << p_signal);
            break;
        }
    }
}

xot4cpp::socket::Alarm::Alarm(unsigned int p_seconds) {

    ::bzero(&m_sigaction, sizeof (m_sigaction));
    m_sigaction.sa_handler = xot4cpp::socket::signalHandler;
    m_sigaction.sa_flags = SA_ONESHOT;
    if (0 > ::sigaction(SIGALRM, &m_sigaction, 0)) {
        THROW_XOT4CPP_IOEXCEPTION(
                "::sigaction(...) failed: "
                << errno << " " << ::strerror(errno));
    }

    ::alarm(p_seconds);
}

xot4cpp::socket::Alarm::~Alarm(void) {

    cancel();
}

void xot4cpp::socket::Alarm::cancel(void) {

    ::alarm(0);
}
