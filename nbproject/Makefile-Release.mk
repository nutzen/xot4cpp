#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_CONF=Release
CND_DISTDIR=dist

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/xot4cpp/x25/payload/X25PayloadMarshaller.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ExpeditedDataNegotiation.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/NetworkUserIdentification.o \
	${OBJECTDIR}/src/xot4cpp/x121/X121AddressPair.o \
	${OBJECTDIR}/src/xot4cpp/stream/BlockingPipeOutputStream.o \
	${OBJECTDIR}/src/xot4cpp/logger/Logger.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearIndicationTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/StateEnumerations.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ThroughputClass.o \
	${OBJECTDIR}/src/xot4cpp/exceptions/IOException.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/RestartPacketPayload.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/R2_DTERestartingState.o \
	${OBJECTDIR}/src/xot4cpp/x25/codes/DiagnosticCodeEnumerations.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicTwoByteParameterFacility.o \
	${OBJECTDIR}/src/xot4cpp/stream/BlockingPipeInputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/context/SessionContext.o \
	${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifier.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityMarshaller.o \
	${OBJECTDIR}/src/xot4cpp/stream/StreamUtility.o \
	${OBJECTDIR}/src/xot4cpp/exceptions/SocketTimeoutException.o \
	${OBJECTDIR}/src/xot4cpp/transport/XotStreamTransportWrapper.o \
	${OBJECTDIR}/src/xot4cpp/x25/codes/CauseCodeEnumerations.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicOneByteParameterFacility.o \
	${OBJECTDIR}/src/xot4cpp/example/SimpleReceiver.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelection.o \
	${OBJECTDIR}/src/xot4cpp/x121/X121AddressMarshaller.o \
	${OBJECTDIR}/src/xot4cpp/socket/ServerSocket.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/ReceiverReadyTransition.o \
	${OBJECTDIR}/src/xot4cpp/transport/XotSocketTransportWrapper.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ClosedUserGroupWithOutgoingAccess.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/CallRedirectionNotification.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/CalledLineAddressModifiedNotification.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/P6_DTEClearingState.o \
	${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.o \
	${OBJECTDIR}/src/xot4cpp/x25/pti/DataPacketTypeIdentifier.o \
	${OBJECTDIR}/src/xot4cpp/XotPacketMarshaller.o \
	${OBJECTDIR}/src/xot4cpp/x25/X25Header.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/ResetIndicationTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingCallDuration.o \
	${OBJECTDIR}/src/xot4cpp/stream/OctetInputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/P1_ReadyState.o \
	${OBJECTDIR}/src/xot4cpp/stream/SocketInputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ReverseChargingAndFastSelect.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/ClearPacketPayload.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/P3_DCEWaitingState.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearRequestTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionExtendedFormat.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/P5_CallCollisionState.o \
	${OBJECTDIR}/src/xot4cpp/x25/lci/LogicalChannelIdentifier.o \
	${OBJECTDIR}/src/xot4cpp/exceptions/Xot4cppException.o \
	${OBJECTDIR}/src/xot4cpp/example/SimpleCaller.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/DataTransition.o \
	${OBJECTDIR}/src/xot4cpp/stream/TransactionalInputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionBasicFormat.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/CallPacketPayload.o \
	${OBJECTDIR}/src/xot4cpp/XotConnection.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/CallAcceptedTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicVariableLengthParameterFacility.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/D2_DTEResetingState.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/CallIncomingTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/utility/PtiToTransitionUtility.o \
	${OBJECTDIR}/src/xot4cpp/stream/Pipe.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/R1_PacketLayerReadyState.o \
	${OBJECTDIR}/src/xot4cpp/stream/OutputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/Facility.o \
	${OBJECTDIR}/src/xot4cpp/x25/context/RoleEnumerations.o \
	${OBJECTDIR}/src/xot4cpp/stream/X25ControllerOutputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearConfirmationTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/WindowSizeSelection.o \
	${OBJECTDIR}/src/xot4cpp/x25/utility/HexUtility.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/D3_DCEResetingState.o \
	${OBJECTDIR}/src/xot4cpp/XotPacket.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/DataPacketPayload.o \
	${OBJECTDIR}/src/xot4cpp/x25/X25Controller.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/P4_DataTransferState.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/R3_DCERestartingState.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/P2_DTEWaitingState.o \
	${OBJECTDIR}/src/xot4cpp/socket/Socket.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/StateContext.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/TransitDelaySelectionAndIndication.o \
	${OBJECTDIR}/src/xot4cpp/stream/OctetOutputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingSegmentCount.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionParameters.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/ResetConfirmationTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/X25HeaderMarshaller.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/D1_FlowControlReadyState.o \
	${OBJECTDIR}/src/xot4cpp/x121/X121Address.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/CallConnectedTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/X25PacketMarshaller.o \
	${OBJECTDIR}/src/xot4cpp/x25/context/SystemContext.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/CallRequestTransition.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/ResetPacketPayload.o \
	${OBJECTDIR}/src/xot4cpp/stream/SocketOutputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingInformationRequest.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/EmptyPacketPayload.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/CalledAddressExtensionOsi.o \
	${OBJECTDIR}/src/xot4cpp/stream/X25ControllerInputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/BilateralClosedUserGroupSelection.o \
	${OBJECTDIR}/src/xot4cpp/x25/X25Packet.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionResult.o \
	${OBJECTDIR}/src/xot4cpp/x25/utility/X25PacketUtility.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/P7_DCEClearingState.o \
	${OBJECTDIR}/src/xot4cpp/x25/pti/ReceiverPacketTypeIdentifier.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionEnumerations.o \
	${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerations.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/QualityOfServiceNegotiationEndToEndTransitDelay.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/X25Payload.o \
	${OBJECTDIR}/src/xot4cpp/exceptions/X25StateTransitionFailure.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/State.o \
	${OBJECTDIR}/src/xot4cpp/transport/TransportWrapper.o \
	${OBJECTDIR}/src/xot4cpp/stream/InputStream.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/CallingAddressExtensionOsi.o \
	${OBJECTDIR}/src/xot4cpp/x25/payload/ClearConfirmationPayload.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityEnumerations.o \
	${OBJECTDIR}/src/xot4cpp/x25/utility/SessionContextUtility.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingMonetaryUnit.o \
	${OBJECTDIR}/src/xot4cpp/x25/transitions/Transition.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerations.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/ClosedUserGroupSelection.o \
	${OBJECTDIR}/src/xot4cpp/x25/states/StateMachine.o \
	${OBJECTDIR}/src/xot4cpp/x25/facilities/QualityOfServiceNegotiationMinimumThroughputClass.o \
	${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.o \
	${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifier.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-Release.mk dist/Release/GNU-Linux-x86/libxot4cpp.so

dist/Release/GNU-Linux-x86/libxot4cpp.so: ${OBJECTFILES}
	${MKDIR} -p dist/Release/GNU-Linux-x86
	${LINK.cc} -shared -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libxot4cpp.so -fPIC ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/src/xot4cpp/x25/payload/X25PayloadMarshaller.o: src/xot4cpp/x25/payload/X25PayloadMarshaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/X25PayloadMarshaller.o src/xot4cpp/x25/payload/X25PayloadMarshaller.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ExpeditedDataNegotiation.o: src/xot4cpp/x25/facilities/ExpeditedDataNegotiation.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ExpeditedDataNegotiation.o src/xot4cpp/x25/facilities/ExpeditedDataNegotiation.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/NetworkUserIdentification.o: src/xot4cpp/x25/facilities/NetworkUserIdentification.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/NetworkUserIdentification.o src/xot4cpp/x25/facilities/NetworkUserIdentification.cpp

${OBJECTDIR}/src/xot4cpp/x121/X121AddressPair.o: src/xot4cpp/x121/X121AddressPair.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x121
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x121/X121AddressPair.o src/xot4cpp/x121/X121AddressPair.cpp

${OBJECTDIR}/src/xot4cpp/stream/BlockingPipeOutputStream.o: src/xot4cpp/stream/BlockingPipeOutputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/BlockingPipeOutputStream.o src/xot4cpp/stream/BlockingPipeOutputStream.cpp

${OBJECTDIR}/src/xot4cpp/logger/Logger.o: src/xot4cpp/logger/Logger.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/logger
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/logger/Logger.o src/xot4cpp/logger/Logger.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearIndicationTransition.o: src/xot4cpp/x25/transitions/ClearIndicationTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearIndicationTransition.o src/xot4cpp/x25/transitions/ClearIndicationTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/StateEnumerations.o: src/xot4cpp/x25/states/StateEnumerations.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/StateEnumerations.o src/xot4cpp/x25/states/StateEnumerations.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ThroughputClass.o: src/xot4cpp/x25/facilities/ThroughputClass.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ThroughputClass.o src/xot4cpp/x25/facilities/ThroughputClass.cpp

${OBJECTDIR}/src/xot4cpp/exceptions/IOException.o: src/xot4cpp/exceptions/IOException.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/exceptions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/exceptions/IOException.o src/xot4cpp/exceptions/IOException.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/RestartPacketPayload.o: src/xot4cpp/x25/payload/RestartPacketPayload.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/RestartPacketPayload.o src/xot4cpp/x25/payload/RestartPacketPayload.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/R2_DTERestartingState.o: src/xot4cpp/x25/states/R2_DTERestartingState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/R2_DTERestartingState.o src/xot4cpp/x25/states/R2_DTERestartingState.cpp

${OBJECTDIR}/src/xot4cpp/x25/codes/DiagnosticCodeEnumerations.o: src/xot4cpp/x25/codes/DiagnosticCodeEnumerations.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/codes
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/codes/DiagnosticCodeEnumerations.o src/xot4cpp/x25/codes/DiagnosticCodeEnumerations.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicTwoByteParameterFacility.o: src/xot4cpp/x25/facilities/BasicTwoByteParameterFacility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicTwoByteParameterFacility.o src/xot4cpp/x25/facilities/BasicTwoByteParameterFacility.cpp

${OBJECTDIR}/src/xot4cpp/stream/BlockingPipeInputStream.o: src/xot4cpp/stream/BlockingPipeInputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/BlockingPipeInputStream.o src/xot4cpp/stream/BlockingPipeInputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/context/SessionContext.o: src/xot4cpp/x25/context/SessionContext.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/context
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/context/SessionContext.o src/xot4cpp/x25/context/SessionContext.cpp

${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifier.o: src/xot4cpp/x25/pti/PacketTypeIdentifier.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/pti
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifier.o src/xot4cpp/x25/pti/PacketTypeIdentifier.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityMarshaller.o: src/xot4cpp/x25/facilities/FacilityMarshaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityMarshaller.o src/xot4cpp/x25/facilities/FacilityMarshaller.cpp

${OBJECTDIR}/src/xot4cpp/stream/StreamUtility.o: src/xot4cpp/stream/StreamUtility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/StreamUtility.o src/xot4cpp/stream/StreamUtility.cpp

${OBJECTDIR}/src/xot4cpp/exceptions/SocketTimeoutException.o: src/xot4cpp/exceptions/SocketTimeoutException.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/exceptions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/exceptions/SocketTimeoutException.o src/xot4cpp/exceptions/SocketTimeoutException.cpp

${OBJECTDIR}/src/xot4cpp/transport/XotStreamTransportWrapper.o: src/xot4cpp/transport/XotStreamTransportWrapper.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/transport
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/transport/XotStreamTransportWrapper.o src/xot4cpp/transport/XotStreamTransportWrapper.cpp

${OBJECTDIR}/src/xot4cpp/x25/codes/CauseCodeEnumerations.o: src/xot4cpp/x25/codes/CauseCodeEnumerations.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/codes
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/codes/CauseCodeEnumerations.o src/xot4cpp/x25/codes/CauseCodeEnumerations.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicOneByteParameterFacility.o: src/xot4cpp/x25/facilities/BasicOneByteParameterFacility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicOneByteParameterFacility.o src/xot4cpp/x25/facilities/BasicOneByteParameterFacility.cpp

${OBJECTDIR}/src/xot4cpp/example/SimpleReceiver.o: src/xot4cpp/example/SimpleReceiver.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/example
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/example/SimpleReceiver.o src/xot4cpp/example/SimpleReceiver.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelection.o: src/xot4cpp/x25/facilities/PacketSizeSelection.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelection.o src/xot4cpp/x25/facilities/PacketSizeSelection.cpp

${OBJECTDIR}/src/xot4cpp/x121/X121AddressMarshaller.o: src/xot4cpp/x121/X121AddressMarshaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x121
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x121/X121AddressMarshaller.o src/xot4cpp/x121/X121AddressMarshaller.cpp

${OBJECTDIR}/src/xot4cpp/socket/ServerSocket.o: src/xot4cpp/socket/ServerSocket.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/socket
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/socket/ServerSocket.o src/xot4cpp/socket/ServerSocket.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/ReceiverReadyTransition.o: src/xot4cpp/x25/transitions/ReceiverReadyTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/ReceiverReadyTransition.o src/xot4cpp/x25/transitions/ReceiverReadyTransition.cpp

${OBJECTDIR}/src/xot4cpp/transport/XotSocketTransportWrapper.o: src/xot4cpp/transport/XotSocketTransportWrapper.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/transport
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/transport/XotSocketTransportWrapper.o src/xot4cpp/transport/XotSocketTransportWrapper.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ClosedUserGroupWithOutgoingAccess.o: src/xot4cpp/x25/facilities/ClosedUserGroupWithOutgoingAccess.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ClosedUserGroupWithOutgoingAccess.o src/xot4cpp/x25/facilities/ClosedUserGroupWithOutgoingAccess.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/CallRedirectionNotification.o: src/xot4cpp/x25/facilities/CallRedirectionNotification.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/CallRedirectionNotification.o src/xot4cpp/x25/facilities/CallRedirectionNotification.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/CalledLineAddressModifiedNotification.o: src/xot4cpp/x25/facilities/CalledLineAddressModifiedNotification.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/CalledLineAddressModifiedNotification.o src/xot4cpp/x25/facilities/CalledLineAddressModifiedNotification.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/P6_DTEClearingState.o: src/xot4cpp/x25/states/P6_DTEClearingState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/P6_DTEClearingState.o src/xot4cpp/x25/states/P6_DTEClearingState.cpp

${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.o: src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/gfi
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.o src/xot4cpp/x25/gfi/GeneralFormatIdentifierMarshaller.cpp

${OBJECTDIR}/src/xot4cpp/x25/pti/DataPacketTypeIdentifier.o: src/xot4cpp/x25/pti/DataPacketTypeIdentifier.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/pti
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/pti/DataPacketTypeIdentifier.o src/xot4cpp/x25/pti/DataPacketTypeIdentifier.cpp

${OBJECTDIR}/src/xot4cpp/XotPacketMarshaller.o: src/xot4cpp/XotPacketMarshaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/XotPacketMarshaller.o src/xot4cpp/XotPacketMarshaller.cpp

${OBJECTDIR}/src/xot4cpp/x25/X25Header.o: src/xot4cpp/x25/X25Header.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/X25Header.o src/xot4cpp/x25/X25Header.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/ResetIndicationTransition.o: src/xot4cpp/x25/transitions/ResetIndicationTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/ResetIndicationTransition.o src/xot4cpp/x25/transitions/ResetIndicationTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingCallDuration.o: src/xot4cpp/x25/facilities/ChargingCallDuration.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingCallDuration.o src/xot4cpp/x25/facilities/ChargingCallDuration.cpp

${OBJECTDIR}/src/xot4cpp/stream/OctetInputStream.o: src/xot4cpp/stream/OctetInputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/OctetInputStream.o src/xot4cpp/stream/OctetInputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/P1_ReadyState.o: src/xot4cpp/x25/states/P1_ReadyState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/P1_ReadyState.o src/xot4cpp/x25/states/P1_ReadyState.cpp

${OBJECTDIR}/src/xot4cpp/stream/SocketInputStream.o: src/xot4cpp/stream/SocketInputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/SocketInputStream.o src/xot4cpp/stream/SocketInputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ReverseChargingAndFastSelect.o: src/xot4cpp/x25/facilities/ReverseChargingAndFastSelect.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ReverseChargingAndFastSelect.o src/xot4cpp/x25/facilities/ReverseChargingAndFastSelect.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/ClearPacketPayload.o: src/xot4cpp/x25/payload/ClearPacketPayload.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/ClearPacketPayload.o src/xot4cpp/x25/payload/ClearPacketPayload.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/P3_DCEWaitingState.o: src/xot4cpp/x25/states/P3_DCEWaitingState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/P3_DCEWaitingState.o src/xot4cpp/x25/states/P3_DCEWaitingState.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearRequestTransition.o: src/xot4cpp/x25/transitions/ClearRequestTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearRequestTransition.o src/xot4cpp/x25/transitions/ClearRequestTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionExtendedFormat.o: src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionExtendedFormat.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionExtendedFormat.o src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionExtendedFormat.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/P5_CallCollisionState.o: src/xot4cpp/x25/states/P5_CallCollisionState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/P5_CallCollisionState.o src/xot4cpp/x25/states/P5_CallCollisionState.cpp

${OBJECTDIR}/src/xot4cpp/x25/lci/LogicalChannelIdentifier.o: src/xot4cpp/x25/lci/LogicalChannelIdentifier.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/lci
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/lci/LogicalChannelIdentifier.o src/xot4cpp/x25/lci/LogicalChannelIdentifier.cpp

${OBJECTDIR}/src/xot4cpp/exceptions/Xot4cppException.o: src/xot4cpp/exceptions/Xot4cppException.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/exceptions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/exceptions/Xot4cppException.o src/xot4cpp/exceptions/Xot4cppException.cpp

${OBJECTDIR}/src/xot4cpp/example/SimpleCaller.o: src/xot4cpp/example/SimpleCaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/example
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/example/SimpleCaller.o src/xot4cpp/example/SimpleCaller.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/DataTransition.o: src/xot4cpp/x25/transitions/DataTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/DataTransition.o src/xot4cpp/x25/transitions/DataTransition.cpp

${OBJECTDIR}/src/xot4cpp/stream/TransactionalInputStream.o: src/xot4cpp/stream/TransactionalInputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/TransactionalInputStream.o src/xot4cpp/stream/TransactionalInputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionBasicFormat.o: src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionBasicFormat.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionBasicFormat.o src/xot4cpp/x25/facilities/RecognizedPrivateOperatingAgencySelectionBasicFormat.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/CallPacketPayload.o: src/xot4cpp/x25/payload/CallPacketPayload.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/CallPacketPayload.o src/xot4cpp/x25/payload/CallPacketPayload.cpp

${OBJECTDIR}/src/xot4cpp/XotConnection.o: src/xot4cpp/XotConnection.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/XotConnection.o src/xot4cpp/XotConnection.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/CallAcceptedTransition.o: src/xot4cpp/x25/transitions/CallAcceptedTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/CallAcceptedTransition.o src/xot4cpp/x25/transitions/CallAcceptedTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicVariableLengthParameterFacility.o: src/xot4cpp/x25/facilities/BasicVariableLengthParameterFacility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/BasicVariableLengthParameterFacility.o src/xot4cpp/x25/facilities/BasicVariableLengthParameterFacility.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/D2_DTEResetingState.o: src/xot4cpp/x25/states/D2_DTEResetingState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/D2_DTEResetingState.o src/xot4cpp/x25/states/D2_DTEResetingState.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/CallIncomingTransition.o: src/xot4cpp/x25/transitions/CallIncomingTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/CallIncomingTransition.o src/xot4cpp/x25/transitions/CallIncomingTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/utility/PtiToTransitionUtility.o: src/xot4cpp/x25/utility/PtiToTransitionUtility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/utility
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/utility/PtiToTransitionUtility.o src/xot4cpp/x25/utility/PtiToTransitionUtility.cpp

${OBJECTDIR}/src/xot4cpp/stream/Pipe.o: src/xot4cpp/stream/Pipe.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/Pipe.o src/xot4cpp/stream/Pipe.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/R1_PacketLayerReadyState.o: src/xot4cpp/x25/states/R1_PacketLayerReadyState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/R1_PacketLayerReadyState.o src/xot4cpp/x25/states/R1_PacketLayerReadyState.cpp

${OBJECTDIR}/src/xot4cpp/stream/OutputStream.o: src/xot4cpp/stream/OutputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/OutputStream.o src/xot4cpp/stream/OutputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/Facility.o: src/xot4cpp/x25/facilities/Facility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/Facility.o src/xot4cpp/x25/facilities/Facility.cpp

${OBJECTDIR}/src/xot4cpp/x25/context/RoleEnumerations.o: src/xot4cpp/x25/context/RoleEnumerations.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/context
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/context/RoleEnumerations.o src/xot4cpp/x25/context/RoleEnumerations.cpp

${OBJECTDIR}/src/xot4cpp/stream/X25ControllerOutputStream.o: src/xot4cpp/stream/X25ControllerOutputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/X25ControllerOutputStream.o src/xot4cpp/stream/X25ControllerOutputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearConfirmationTransition.o: src/xot4cpp/x25/transitions/ClearConfirmationTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/ClearConfirmationTransition.o src/xot4cpp/x25/transitions/ClearConfirmationTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/WindowSizeSelection.o: src/xot4cpp/x25/facilities/WindowSizeSelection.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/WindowSizeSelection.o src/xot4cpp/x25/facilities/WindowSizeSelection.cpp

${OBJECTDIR}/src/xot4cpp/x25/utility/HexUtility.o: src/xot4cpp/x25/utility/HexUtility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/utility
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/utility/HexUtility.o src/xot4cpp/x25/utility/HexUtility.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/D3_DCEResetingState.o: src/xot4cpp/x25/states/D3_DCEResetingState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/D3_DCEResetingState.o src/xot4cpp/x25/states/D3_DCEResetingState.cpp

${OBJECTDIR}/src/xot4cpp/XotPacket.o: src/xot4cpp/XotPacket.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/XotPacket.o src/xot4cpp/XotPacket.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/DataPacketPayload.o: src/xot4cpp/x25/payload/DataPacketPayload.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/DataPacketPayload.o src/xot4cpp/x25/payload/DataPacketPayload.cpp

${OBJECTDIR}/src/xot4cpp/x25/X25Controller.o: src/xot4cpp/x25/X25Controller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/X25Controller.o src/xot4cpp/x25/X25Controller.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/P4_DataTransferState.o: src/xot4cpp/x25/states/P4_DataTransferState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/P4_DataTransferState.o src/xot4cpp/x25/states/P4_DataTransferState.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/R3_DCERestartingState.o: src/xot4cpp/x25/states/R3_DCERestartingState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/R3_DCERestartingState.o src/xot4cpp/x25/states/R3_DCERestartingState.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/P2_DTEWaitingState.o: src/xot4cpp/x25/states/P2_DTEWaitingState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/P2_DTEWaitingState.o src/xot4cpp/x25/states/P2_DTEWaitingState.cpp

${OBJECTDIR}/src/xot4cpp/socket/Socket.o: src/xot4cpp/socket/Socket.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/socket
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/socket/Socket.o src/xot4cpp/socket/Socket.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/StateContext.o: src/xot4cpp/x25/states/StateContext.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/StateContext.o src/xot4cpp/x25/states/StateContext.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/TransitDelaySelectionAndIndication.o: src/xot4cpp/x25/facilities/TransitDelaySelectionAndIndication.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/TransitDelaySelectionAndIndication.o src/xot4cpp/x25/facilities/TransitDelaySelectionAndIndication.cpp

${OBJECTDIR}/src/xot4cpp/stream/OctetOutputStream.o: src/xot4cpp/stream/OctetOutputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/OctetOutputStream.o src/xot4cpp/stream/OctetOutputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingSegmentCount.o: src/xot4cpp/x25/facilities/ChargingSegmentCount.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingSegmentCount.o src/xot4cpp/x25/facilities/ChargingSegmentCount.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionParameters.o: src/xot4cpp/x25/transitions/TransitionParameters.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionParameters.o src/xot4cpp/x25/transitions/TransitionParameters.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/ResetConfirmationTransition.o: src/xot4cpp/x25/transitions/ResetConfirmationTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/ResetConfirmationTransition.o src/xot4cpp/x25/transitions/ResetConfirmationTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/X25HeaderMarshaller.o: src/xot4cpp/x25/X25HeaderMarshaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/X25HeaderMarshaller.o src/xot4cpp/x25/X25HeaderMarshaller.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/D1_FlowControlReadyState.o: src/xot4cpp/x25/states/D1_FlowControlReadyState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/D1_FlowControlReadyState.o src/xot4cpp/x25/states/D1_FlowControlReadyState.cpp

${OBJECTDIR}/src/xot4cpp/x121/X121Address.o: src/xot4cpp/x121/X121Address.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x121
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x121/X121Address.o src/xot4cpp/x121/X121Address.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/CallConnectedTransition.o: src/xot4cpp/x25/transitions/CallConnectedTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/CallConnectedTransition.o src/xot4cpp/x25/transitions/CallConnectedTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/X25PacketMarshaller.o: src/xot4cpp/x25/X25PacketMarshaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/X25PacketMarshaller.o src/xot4cpp/x25/X25PacketMarshaller.cpp

${OBJECTDIR}/src/xot4cpp/x25/context/SystemContext.o: src/xot4cpp/x25/context/SystemContext.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/context
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/context/SystemContext.o src/xot4cpp/x25/context/SystemContext.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/CallRequestTransition.o: src/xot4cpp/x25/transitions/CallRequestTransition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/CallRequestTransition.o src/xot4cpp/x25/transitions/CallRequestTransition.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/ResetPacketPayload.o: src/xot4cpp/x25/payload/ResetPacketPayload.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/ResetPacketPayload.o src/xot4cpp/x25/payload/ResetPacketPayload.cpp

${OBJECTDIR}/src/xot4cpp/stream/SocketOutputStream.o: src/xot4cpp/stream/SocketOutputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/SocketOutputStream.o src/xot4cpp/stream/SocketOutputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingInformationRequest.o: src/xot4cpp/x25/facilities/ChargingInformationRequest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingInformationRequest.o src/xot4cpp/x25/facilities/ChargingInformationRequest.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/EmptyPacketPayload.o: src/xot4cpp/x25/payload/EmptyPacketPayload.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/EmptyPacketPayload.o src/xot4cpp/x25/payload/EmptyPacketPayload.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/CalledAddressExtensionOsi.o: src/xot4cpp/x25/facilities/CalledAddressExtensionOsi.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/CalledAddressExtensionOsi.o src/xot4cpp/x25/facilities/CalledAddressExtensionOsi.cpp

${OBJECTDIR}/src/xot4cpp/stream/X25ControllerInputStream.o: src/xot4cpp/stream/X25ControllerInputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/X25ControllerInputStream.o src/xot4cpp/stream/X25ControllerInputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/BilateralClosedUserGroupSelection.o: src/xot4cpp/x25/facilities/BilateralClosedUserGroupSelection.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/BilateralClosedUserGroupSelection.o src/xot4cpp/x25/facilities/BilateralClosedUserGroupSelection.cpp

${OBJECTDIR}/src/xot4cpp/x25/X25Packet.o: src/xot4cpp/x25/X25Packet.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/X25Packet.o src/xot4cpp/x25/X25Packet.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionResult.o: src/xot4cpp/x25/transitions/TransitionResult.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionResult.o src/xot4cpp/x25/transitions/TransitionResult.cpp

${OBJECTDIR}/src/xot4cpp/x25/utility/X25PacketUtility.o: src/xot4cpp/x25/utility/X25PacketUtility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/utility
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/utility/X25PacketUtility.o src/xot4cpp/x25/utility/X25PacketUtility.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/P7_DCEClearingState.o: src/xot4cpp/x25/states/P7_DCEClearingState.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/P7_DCEClearingState.o src/xot4cpp/x25/states/P7_DCEClearingState.cpp

${OBJECTDIR}/src/xot4cpp/x25/pti/ReceiverPacketTypeIdentifier.o: src/xot4cpp/x25/pti/ReceiverPacketTypeIdentifier.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/pti
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/pti/ReceiverPacketTypeIdentifier.o src/xot4cpp/x25/pti/ReceiverPacketTypeIdentifier.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionEnumerations.o: src/xot4cpp/x25/transitions/TransitionEnumerations.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/TransitionEnumerations.o src/xot4cpp/x25/transitions/TransitionEnumerations.cpp

${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerations.o: src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerations.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/pti
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerations.o src/xot4cpp/x25/pti/PacketTypeIdentifierEnumerations.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/QualityOfServiceNegotiationEndToEndTransitDelay.o: src/xot4cpp/x25/facilities/QualityOfServiceNegotiationEndToEndTransitDelay.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/QualityOfServiceNegotiationEndToEndTransitDelay.o src/xot4cpp/x25/facilities/QualityOfServiceNegotiationEndToEndTransitDelay.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/X25Payload.o: src/xot4cpp/x25/payload/X25Payload.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/X25Payload.o src/xot4cpp/x25/payload/X25Payload.cpp

${OBJECTDIR}/src/xot4cpp/exceptions/X25StateTransitionFailure.o: src/xot4cpp/exceptions/X25StateTransitionFailure.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/exceptions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/exceptions/X25StateTransitionFailure.o src/xot4cpp/exceptions/X25StateTransitionFailure.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/State.o: src/xot4cpp/x25/states/State.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/State.o src/xot4cpp/x25/states/State.cpp

${OBJECTDIR}/src/xot4cpp/transport/TransportWrapper.o: src/xot4cpp/transport/TransportWrapper.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/transport
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/transport/TransportWrapper.o src/xot4cpp/transport/TransportWrapper.cpp

${OBJECTDIR}/src/xot4cpp/stream/InputStream.o: src/xot4cpp/stream/InputStream.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/stream
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/stream/InputStream.o src/xot4cpp/stream/InputStream.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/CallingAddressExtensionOsi.o: src/xot4cpp/x25/facilities/CallingAddressExtensionOsi.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/CallingAddressExtensionOsi.o src/xot4cpp/x25/facilities/CallingAddressExtensionOsi.cpp

${OBJECTDIR}/src/xot4cpp/x25/payload/ClearConfirmationPayload.o: src/xot4cpp/x25/payload/ClearConfirmationPayload.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/payload
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/payload/ClearConfirmationPayload.o src/xot4cpp/x25/payload/ClearConfirmationPayload.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityEnumerations.o: src/xot4cpp/x25/facilities/FacilityEnumerations.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/FacilityEnumerations.o src/xot4cpp/x25/facilities/FacilityEnumerations.cpp

${OBJECTDIR}/src/xot4cpp/x25/utility/SessionContextUtility.o: src/xot4cpp/x25/utility/SessionContextUtility.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/utility
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/utility/SessionContextUtility.o src/xot4cpp/x25/utility/SessionContextUtility.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingMonetaryUnit.o: src/xot4cpp/x25/facilities/ChargingMonetaryUnit.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ChargingMonetaryUnit.o src/xot4cpp/x25/facilities/ChargingMonetaryUnit.cpp

${OBJECTDIR}/src/xot4cpp/x25/transitions/Transition.o: src/xot4cpp/x25/transitions/Transition.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/transitions
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/transitions/Transition.o src/xot4cpp/x25/transitions/Transition.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerations.o: src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerations.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerations.o src/xot4cpp/x25/facilities/PacketSizeSelectionEnumerations.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/ClosedUserGroupSelection.o: src/xot4cpp/x25/facilities/ClosedUserGroupSelection.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/ClosedUserGroupSelection.o src/xot4cpp/x25/facilities/ClosedUserGroupSelection.cpp

${OBJECTDIR}/src/xot4cpp/x25/states/StateMachine.o: src/xot4cpp/x25/states/StateMachine.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/states
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/states/StateMachine.o src/xot4cpp/x25/states/StateMachine.cpp

${OBJECTDIR}/src/xot4cpp/x25/facilities/QualityOfServiceNegotiationMinimumThroughputClass.o: src/xot4cpp/x25/facilities/QualityOfServiceNegotiationMinimumThroughputClass.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/facilities
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/facilities/QualityOfServiceNegotiationMinimumThroughputClass.o src/xot4cpp/x25/facilities/QualityOfServiceNegotiationMinimumThroughputClass.cpp

${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.o: src/xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/pti
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.o src/xot4cpp/x25/pti/PacketTypeIdentifierMarshaller.cpp

${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifier.o: src/xot4cpp/x25/gfi/GeneralFormatIdentifier.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/xot4cpp/x25/gfi
	${RM} $@.d
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/src/xot4cpp/x25/gfi/GeneralFormatIdentifier.o src/xot4cpp/x25/gfi/GeneralFormatIdentifier.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Release
	${RM} dist/Release/GNU-Linux-x86/libxot4cpp.so

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
